<?php

use App\Entity\CarMark;
use App\Entity\CarModel;
use Illuminate\Database\Seeder;

class CarModelsSeeder extends Seeder
{
    private function createCategoryModels(CarMark $mark, string $models, int $category)
    {
        foreach (array_filter(array_map('trim', explode(',', $models))) as $modelName) {
            /** @var CarModel $model */
            $model = CarModel::make(['name' => $modelName, 'category' => $category]);
            $model->mark()->associate($mark);
            $model->save();
        }
    }

    public function run()
    {
        DB::table((new CarModel)->getTable())->delete();
        DB::table((new CarMark)->getTable())->delete();

        $handle = fopen(__DIR__ . '/csv/' . "mark-model.csv", "r");
        for (;($row = fgetcsv($handle, 0, ",")) !== false;) {
            $markName = trim($row[0]);
            if ($markName === '') continue;

            /** @var CarMark $mark */
            $mark = CarMark::create(['name' => $markName]);
            $this->createCategoryModels($mark, $row[1], CarModel::CATEGORY_1);
            $this->createCategoryModels($mark, $row[2], CarModel::CATEGORY_2);
            $this->createCategoryModels($mark, $row[3], CarModel::CATEGORY_3);
            $this->createCategoryModels($mark, $row[4], CarModel::CATEGORY_4);
        }

        CarModel::create(['name' => 'Другая модель, категория 1', 'category' => CarModel::CATEGORY_1]);
        CarModel::create(['name' => 'Другая модель, категория 2', 'category' => CarModel::CATEGORY_2]);
        CarModel::create(['name' => 'Другая модель, категория 3', 'category' => CarModel::CATEGORY_3]);
        CarModel::create(['name' => 'Другая модель, категория 4', 'category' => CarModel::CATEGORY_4]);
    }
}