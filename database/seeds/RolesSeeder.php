<?php

use App\Entity\User\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    public function run()
    {
        DB::table((new Role)->getTable())->delete();
        Role::create(['id' => Role::SUPER_ADMIN, 'name' => 'Супер администратор']);
        Role::create(['id' => Role::ADMIN, 'name' => 'Администратор', 'salary' => 5]);
        Role::create(['id' => Role::CASHIER, 'name' => 'Кассир', 'salary' => 1800]);
        Role::create(['id' => Role::WASHER, 'name' => 'Мойщик', 'salary' => 30]);
    }
}
