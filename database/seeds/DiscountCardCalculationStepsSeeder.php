<?php

use App\Entity\DiscountCard\CalculationStep;
use Illuminate\Database\Seeder;

class DiscountCardCalculationStepsSeeder extends Seeder
{
    public function run()
    {
        DB::table((new CalculationStep)->getTable())->delete();
        CalculationStep::create(['disc' => 20, 'sum' => '15000']);
    }
}
