<?php

use App\Entity\Car;
use App\Entity\Client;
use Illuminate\Database\Seeder;

class ClientsSeeder extends Seeder
{
    public function run()
    {
        factory(Client::class, 50)
            ->create()
            ->each(function(Client $client) {
                $cars = factory(Car::class, rand(1, 4))->create();
                $client->cars()->attach(array_pluck($cars, 'id'));
            });
    }
}
