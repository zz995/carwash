<?php

use App\Entity\ServiceCenter;
use App\Entity\User\Role;
use Illuminate\Database\Seeder;

class ServiceCentersSeeder extends Seeder
{
    public function run()
    {
        DB::table((new ServiceCenter)->getTable())->delete();
        ServiceCenter::create(['id' => 1, 'name' => 'Кунцево плаза', 'desc' => 'Ярцевская д.19, -2 этаж подземная парковка']);
        ServiceCenter::create(['id' => 2, 'name' => 'Каширская Плаза', 'desc' => 'Каширское шоссе 61к2, -1 этаж подземная парковка']);
        ServiceCenter::create(['id' => 3, 'name' => 'Западный', 'desc' => 'Рублевское шоссе 52А, -1 этаж подземная парковка']);
    }
}
