<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(SuperAdminSeeder::class);

        $this->call(CarModelsSeeder::class);

        $this->call(CarWashTypesSeeder::class);
        $this->call(CarWashWorksSeeder::class);

        $this->call(CafeSaleTypesSeeder::class);

        $this->call(DiscountCardCalculationStepsSeeder::class);

        $this->call(ServiceCentersSeeder::class);
    }
}
