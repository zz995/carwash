<?php

use App\Entity\CarWash\Type;
use Illuminate\Database\Seeder;

class CarWashTypesSeeder extends Seeder
{
    public function run()
    {
        DB::table((new Type)->getTable())->delete();
        Type::create(['id' => 1, 'name' => 'Мойка кузова']);
        Type::create(['id' => 2, 'name' => 'Уборка салона']);
        Type::create(['id' => 3, 'name' => 'Химчистка салона']);
        Type::create(['id' => 4, 'name' => 'Защита кузова']);
    }
}
