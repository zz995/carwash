<?php

use App\Entity\User\Role;
use App\Entity\User\User;
use Illuminate\Database\Seeder;

class SuperAdminSeeder extends Seeder
{

    public function run()
    {
        DB::table((new User())->getTable())->delete();
        factory(User::class)->create([
            'login' => 'root',
            'role_id' => Role::SUPER_ADMIN,
            'status' => User::STATUS_ACTIVE,
        ]);
    }
}
