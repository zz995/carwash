<?php

use App\Entity\User\User;
use Illuminate\Database\Seeder;

class EmployeesSeeder extends Seeder
{

    public function run()
    {
        factory(User::class, 25)->create();
    }
}
