<?php

use App\Entity\Cafe\SaleType;
use Illuminate\Database\Seeder;

class CafeSaleTypesSeeder extends Seeder
{
    public function run()
    {
        DB::table((new SaleType())->getTable())->delete();
        SaleType::create(['name' => 'Напиток']);
        SaleType::create(['name' => 'Десерт']);
        SaleType::create(['name' => 'Авто аксессуар']);
    }
}
