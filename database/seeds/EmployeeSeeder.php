<?php

use App\Entity\User\Role;
use App\Entity\User\User;
use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{

    public function run()
    {
        factory(User::class)->create([
            'login' => 'admin',
            'role_id' => Role::ADMIN,
            'status' => User::STATUS_ACTIVE,
        ]);
        factory(User::class)->create([
            'login' => 'cashier',
            'role_id' => Role::CASHIER,
            'status' => User::STATUS_ACTIVE,
        ]);
    }
}
