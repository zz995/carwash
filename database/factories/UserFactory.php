<?php

use App\Entity\User\Role;
use App\Entity\User\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {

    $phoneNumber = null;
    if ($phone = $faker->optional($weight = 0.9)->unique()) {
        $phoneNumber = $phone->e164PhoneNumber;
    }

    $safeEmail = null;
    if ($email = $faker->optional($weight = 0.8)->unique()) {
        $safeEmail = $email->safeEmail;
    }

    $role = $faker->randomElement([Role::CASHIER, Role::WASHER, Role::ADMIN]);

    return [
        'name' => $faker->firstName,
        'surname' => $faker->lastName,
        'patronymic' => null,
        'phone' => $phoneNumber,
        'email' => $safeEmail,
        'login' => $role === Role::WASHER ? null : $faker->unique()->userName,
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
        'role_id' => $role,
        'status' => $faker->randomElement([User::STATUS_ACTIVE, User::STATUS_DISABLED, User::STATUS_DELETE]),
    ];
});
