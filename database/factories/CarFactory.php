<?php

use App\Entity\Car;
use App\Entity\Client;
use App\Entity\CarModel;
use Faker\Generator as Faker;

$factory->define(Car::class, function (Faker $faker) {
    return [
        'number' => $faker->unique()->numberBetween($min = 1000, $max = 9000),
        'car_model_id' => rand(1, 4)
    ];
});