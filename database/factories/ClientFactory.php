<?php

use App\Entity\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {

    return [
        'name' => $faker->firstName,
        'phone' => $faker->e164PhoneNumber
    ];
});
