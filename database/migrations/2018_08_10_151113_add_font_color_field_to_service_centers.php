<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFontColorFieldToServiceCenters extends Migration
{
    public function up()
    {
        Schema::table('service_centers', function (Blueprint $table) {
            $table->renameColumn('color', 'background_color');
            $table->string('font_color', 10)->default("#ffffff");
        });
    }

    public function down()
    {
        Schema::table('service_centers', function (Blueprint $table) {
            $table->renameColumn('background_color', 'color');
            $table->dropColumn('font_color');
        });
    }
}
