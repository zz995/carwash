<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPersonalFineHistories extends Migration
{
    public function up()
    {
        Schema::table('personal_fines', function (Blueprint $table) {
            $table->dropForeign(['creator_id']);
            $table->dropColumn('creator_id');
            $table->dropColumn('create_reason');
            $table->dropColumn('update_reason');
        });

        Schema::create('personal_fine_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('timestamp');
            $table->text('comment');
            $table->decimal('value', 10, 2)->default(0);
            $table
                ->unsignedInteger('initiator_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
            $table
                ->unsignedInteger('personal_fine_id')
                ->references('id')
                ->on('personal_fines')
                ->onDelete('CASCADE');
        });

        Schema::table('team_fines', function (Blueprint $table) {
            $table->tinyInteger('type')->default(0);
            $table->dropForeign(['creator_id']);
            $table->dropColumn('creator_id');
            $table->dropColumn('create_reason');
            $table->dropColumn('update_reason');
        });

        Schema::create('team_fine_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('timestamp');
            $table->text('comment');
            $table->decimal('value', 10, 2)->default(0);
            $table
                ->unsignedInteger('initiator_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
            $table
                ->unsignedInteger('team_fine_id')
                ->references('id')
                ->on('team_fines')
                ->onDelete('CASCADE');
        });
    }

    public function down()
    {
        Schema::table('personal_fines', function (Blueprint $table) {
            $table->text('create_reason');
            $table->text('update_reason')->nullable();
            $table->unsignedInteger('creator_id')->nullable();
            $table
                ->foreign('creator_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
        });
        Schema::dropIfExists('personal_fine_histories');

        Schema::table('team_fines', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->text('create_reason');
            $table->text('update_reason')->nullable();
            $table->unsignedInteger('creator_id')->nullable();
            $table
                ->foreign('creator_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
        });
        Schema::dropIfExists('team_fine_histories');
    }
}
