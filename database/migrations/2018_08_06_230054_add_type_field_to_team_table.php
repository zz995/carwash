<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeFieldToTeamTable extends Migration
{

    public function up()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->tinyInteger('type')->default(0);
        });
    }

    public function down()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
