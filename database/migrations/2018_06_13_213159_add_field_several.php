<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldSeveral extends Migration
{
    public function up()
    {
        Schema::table('car_wash_works', function (Blueprint $table) {
            $table->tinyInteger('several')->default(0);
        });

        Schema::table('order-basket', function (Blueprint $table) {
            $table->tinyInteger('count')->default(1);
        });
    }

    public function down()
    {
        Schema::table('car_wash_works', function (Blueprint $table) {
            $table->dropColumn('several');
        });

        Schema::table('order-basket', function (Blueprint $table) {
            $table->dropColumn('count');
        });
    }
}
