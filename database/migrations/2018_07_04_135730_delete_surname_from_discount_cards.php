<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteSurnameFromDiscountCards extends Migration
{

    public function up()
    {
        Schema::table('discount_cards', function (Blueprint $table) {
            $table->dropColumn('surname');
            $table->dropColumn('patronymic');
            $table->dropColumn('fixed_disc');
            $table->dropColumn('cumulative_disc');
            $table->tinyInteger('disc')->default(0);
            $table->string('number')->nullable()->default(null)->change();
        });
    }

    public function down()
    {
        Schema::table('discount_cards', function (Blueprint $table) {
            $table->string('surname')->default(null);
            $table->string('patronymic')->nullable()->default(null);
            $table->tinyInteger('fixed_disc')->default(0);
            $table->tinyInteger('cumulative_disc')->default(0);
            $table->dropColumn('disc');
            $table->string('number')->nullable(false)->change();;
        });
    }
}
