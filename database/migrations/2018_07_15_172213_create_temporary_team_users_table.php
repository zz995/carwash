<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemporaryTeamUsersTable extends Migration
{
    public function up()
    {
        Schema::create('temporary_team_users', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedBigInteger('order_id')->nullable();
            $table->unsignedBigInteger('team_id')->nullable();
            $table->timestamps();

            $table->primary('user_id');
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');

            $table
                ->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('SET NULL');

            $table
                ->foreign('team_id')
                ->references('id')
                ->on('teams')
                ->onDelete('SET NULL');

        });
    }

    public function down()
    {
        Schema::dropIfExists('temporary_team_users');
    }
}
