<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeams extends Migration
{

    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->bigIncrements('id')->unsigned();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('team-user', function (Blueprint $table) {

            $table->bigInteger('team_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table
                ->foreign('team_id')
                ->references('id')
                ->on('teams')
                ->onDelete('CASCADE');
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
            $table->primary(['user_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('team-user');
        Schema::dropIfExists('teams');
    }
}
