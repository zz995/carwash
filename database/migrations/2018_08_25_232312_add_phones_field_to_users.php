<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhonesFieldToUsers extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->json('phones')->nullable()->default(null);
        });

        foreach (DB::table('users')->get() as $user) {
            if (!isset($user->phone)) continue;
            DB::table('users')->where('id', $user->id)->update(['phones' => json_encode([$user->phone])]);
        }

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phone');
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone', 40)->unique()->nullable()->default(null);
        });

        foreach (DB::table('users')->get() as $user) {
            if (!isset($user->phones) || empty($phones = json_decode($user->phones))) continue;
            $phone = $phones[0];
            DB::table('users')->where('id', $user->id)->update(['phone' => $phone]);
        }

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('phones');
        });
    }
}
