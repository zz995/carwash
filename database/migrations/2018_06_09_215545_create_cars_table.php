<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number', 20)->unique();
            $table->integer('client_id')->unsigned();
            $table->timestamps();

            $table
                ->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
