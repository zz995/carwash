<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypePaymentToOrders extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->tinyInteger('payment_type')->nullable();
            $table->unsignedInteger('car_wash_contract_id')->nullable();
            $table
                ->foreign('car_wash_contract_id')
                ->references('id')
                ->on('car_wash_contracts')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['car_wash_contract_id']);
            $table->dropColumn('car_wash_contract_id');
            $table->dropColumn('payment_type');
        });
    }
}
