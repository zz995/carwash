<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToDiscountCardTable extends Migration
{
    public function up()
    {
        Schema::table('discount_cards', function (Blueprint $table) {
            $table->tinyInteger('admin')->default(1);
        });
    }

    public function down()
    {
        Schema::table('discount_cards', function (Blueprint $table) {
            $table->dropColumn('admin');
        });
    }
}
