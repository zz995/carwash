<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountCardCalculationStepsTable extends Migration
{
    public function up()
    {
        Schema::create('discount_card_calculation_steps', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->tinyInteger('disc')->unique();
            $table->decimal('sum', 10, 2)->default(0);
            $table->unsignedInteger('user_id')->nullable();
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
        });

        Schema::table('discount_cards', function (Blueprint $table) {
            $table->decimal('current_step_accrual', 10, 2)->default(0);
        });

        Schema::create('discount_card_type_change_history', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->tinyInteger('disc')->unique();
            $table->tinyInteger('type')->default(0);
            $table->decimal('current_step_accrual', 10, 2)->default(0);
            $table->unsignedInteger('discount_card_id');
            $table
                ->foreign('discount_card_id')
                ->references('id')
                ->on('discount_cards')
                ->onDelete('CASCADE');
            $table->unsignedInteger('user_id');
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::dropIfExists('discount_card_calculation_steps');
        Schema::table('discount_cards', function (Blueprint $table) {
            $table->dropColumn('current_step_accrual');
        });
        Schema::dropIfExists('discount_card_type_change_history');
    }
}
