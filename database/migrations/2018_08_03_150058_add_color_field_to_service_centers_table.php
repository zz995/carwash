<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColorFieldToServiceCentersTable extends Migration
{
    public function up()
    {
        Schema::table('service_centers', function (Blueprint $table) {
            $table->string('color', 10)->default("#343a40");
        });
    }

    public function down()
    {
        Schema::table('service_centers', function (Blueprint $table) {
            $table->dropColumn('color');
        });
    }
}
