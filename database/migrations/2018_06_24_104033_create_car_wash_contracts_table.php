<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarWashContractsTable extends Migration
{
    public function up()
    {
        Schema::create('car_wash_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('number', 256)->unique();
            $table->string('contractor', 512);
            $table->timestamp('from');
            $table->timestamp('to');
        });
    }

    public function down()
    {
        Schema::dropIfExists('car_wash_contracts');
    }
}
