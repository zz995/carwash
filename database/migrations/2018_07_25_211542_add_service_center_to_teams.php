<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddServiceCenterToTeams extends Migration
{
    public function up()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->unsignedInteger('service_center_id');
            $table
                ->foreign('service_center_id')
                ->references('id')
                ->on('service_centers')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->dropForeign(['service_center_id']);
            $table->dropColumn('service_center_id');
        });
    }
}
