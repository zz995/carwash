<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountCardsTable extends Migration
{
    public function up()
    {
        Schema::create('discount_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('number')->unique();

            $table->string('phone', 40)->nullable()->default(null);

            $table->string('name');
            $table->string('surname')->default(null);
            $table->string('patronymic')->nullable()->default(null);

            $table->tinyInteger('type')->default(0);
            $table->tinyInteger('fixed_disc')->default(0);
            $table->tinyInteger('cumulative_disc')->default(0);
            $table->timestamps();
        });

        Schema::table('cars', function (Blueprint $table) {
            $table->unsignedInteger('discount_card_id')->nullable();
            $table
                ->foreign('discount_card_id')
                ->references('id')
                ->on('discount_cards')
                ->onDelete('RESTRICT');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedTinyInteger('discount_size')->default(0);
            $table->unsignedInteger('discount_card_id')->nullable();
            $table
                ->foreign('discount_card_id')
                ->references('id')
                ->on('discount_cards')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['discount_card_id']);
            $table->dropColumn('discount_card_id');
            $table->dropColumn('discount_size');
        });
        Schema::table('cars', function (Blueprint $table) {
            $table->dropForeign(['discount_card_id']);
            $table->dropColumn('discount_card_id');
        });
        Schema::dropIfExists('discount_cards');
    }
}
