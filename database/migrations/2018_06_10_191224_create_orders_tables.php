<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTables extends Migration
{
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('status')->default(0);
            $table->string('comment', 2048)->nullable();
            $table
                ->integer('admin_id')
                ->unsigned();
            $table
                ->integer('cashier_id')
                ->unsigned()
                ->nullable();
            $table
                ->integer('car_id')
                ->unsigned();
            $table->timestamps();

            $table->index('created_at');

            $table
                ->foreign('admin_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
            $table
                ->foreign('cashier_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
            $table
                ->foreign('car_id')
                ->references('id')
                ->on('cars')
                ->onDelete('RESTRICT');
        });

        Schema::create('order-user', function (Blueprint $table) {
            $table->bigInteger('order_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table
                ->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('CASCADE');
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('CASCADE');
            $table->primary(['user_id', 'order_id']);
        });

        Schema::create('order-basket', function (Blueprint $table) {
            $table->bigInteger('order_id')->unsigned();
            $table->integer('work_id')->unsigned();

            $table->decimal('price', 10, 2);
            $table->decimal('cost', 10, 2)->nullable();
            $table->decimal('salary', 10, 2)->nullable();
            $table->timestamps();

            $table
                ->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('CASCADE');
            $table
                ->foreign('work_id')
                ->references('id')
                ->on('car_wash_works')
                ->onDelete('CASCADE');
            $table->primary(['work_id', 'order_id']);
        });

    }

    public function down()
    {
        Schema::dropIfExists('order-user');
        Schema::dropIfExists('order-basket');
        Schema::dropIfExists('orders');
    }
}
