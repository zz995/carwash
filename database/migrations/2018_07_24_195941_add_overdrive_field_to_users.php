<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOverdriveFieldToUsers extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('overdrive')->default(0);
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['cashier_id']);
            $table->dropColumn('cashier_id');
        });

        Schema::create('order_status_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('timestamp');
            $table->index('timestamp');
            $table->tinyInteger('status');
            $table->unsignedBigInteger('order_id');
            $table
                ->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('RESTRICT');
            $table
                ->integer('initiator_id')
                ->unsigned();
            $table
                ->foreign('initiator_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('overdrive');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table
                ->integer('cashier_id')
                ->unsigned()
                ->nullable();
            $table
                ->foreign('cashier_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
        });

        Schema::dropIfExists('order_status_histories');
    }
}
