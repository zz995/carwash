<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFiltrationFieldToOrdersTable extends Migration
{
    public function up()
    {
        Schema::table('order-basket', function (Blueprint $table) {
            $table->decimal('default_price', 10, 2);
        });

        DB::table('order-basket')->update(['default_price' => DB::raw("`price`")]);

        Schema::table('orders', function (Blueprint $table) {
            $table->tinyInteger('services_standard_price')->default(1);
            $table->integer('time_execute')->nullable()->index();
            $table->integer('time_queue')->nullable()->index();
            $table->index('price');
        });

        /** @var \App\Entity\Order\Order $order */
        foreach (\App\Entity\Order\Order::all() as $order) {
            $order->time_execute = $order->getTimeExecuteInSecond();
            $order->time_queue = $order->getTimeBetweenStatusesInSecond(
                \App\Entity\Order\Order::STATUS_QUEUE,
                \App\Entity\Order\Order::STATUS_EXECUTE
            );
            $order->save();
        }
    }
    
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('services_standard_price');
            $table->dropIndex(['time_execute']);
            $table->dropColumn('time_execute');
            $table->dropIndex(['time_queue']);
            $table->dropColumn('time_queue');
            $table->dropIndex(['price']);
        });

        Schema::table('order-basket', function (Blueprint $table) {
            $table->dropColumn('default_price');
        });
    }
}
