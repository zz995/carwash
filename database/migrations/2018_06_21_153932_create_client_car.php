<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientCar extends Migration
{
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->dropForeign(['client_id']);
            $table->dropColumn('client_id');
        });

        Schema::create('client-car', function (Blueprint $table) {
            $table->integer('client_id')->unsigned();
            $table->integer('car_id')->unsigned();

            $table
                ->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('CASCADE');
            $table
                ->foreign('car_id')
                ->references('id')
                ->on('cars')
                ->onDelete('CASCADE');
            $table->primary(['client_id', 'car_id']);
        });
    }

    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->integer('client_id')->unsigned();
            $table
                ->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('RESTRICT');
        });
        Schema::dropIfExists('client-car');
    }
}
