<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFioAndPhone extends Migration
{

    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('email')->nullable()->default(null)->change();
            $table->string('phone', 40)->unique()->nullable()->default(null);
            $table->string('surname');
            $table->string('patronymic')->nullable()->default(null);
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('email')->nullable(false)->change();
            $table->dropColumn('phone');
            $table->dropColumn('surname');
            $table->dropColumn('patronymic');
        });
    }
}
