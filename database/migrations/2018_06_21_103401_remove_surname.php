<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSurname extends Migration
{
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('surname');
            $table->dropColumn('patronymic');
            $table->dropUnique(['phone']);
        });
    }

    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->string('surname')->nullable()->default(null);
            $table->string('patronymic')->nullable()->default(null);
            $table->string('phone', 40)->unique();
        });
    }
}
