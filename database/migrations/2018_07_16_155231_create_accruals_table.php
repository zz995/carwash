<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccrualsTable extends Migration
{
    public function up()
    {
        Schema::create('accruals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('value', 10, 2)->default(0);
            $table->timestamp('timestamp')->index();

            $table->unsignedInteger('user_id');
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');

            $table->tinyInteger('initiator_type');
            $table->unsignedBigInteger('initiator_id')->nullable();
            $table->index(['initiator_type', 'initiator_id']);
        });

        Schema::create('personal_fines', function (Blueprint $table) {
             $table->increments('id');
             $table->timestamps();
             $table->index('created_at');

             $table->decimal('value', 10, 2)->default(0);
             $table->text('create_reason');
             $table->text('update_reason')->nullable();

             $table->unsignedInteger('creator_id');
             $table
                ->foreign('creator_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');

             $table->unsignedInteger('user_id');
             $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');

             $table->unsignedInteger('service_center_id');
             $table
                 ->foreign('service_center_id')
                 ->references('id')
                 ->on('service_centers')
                 ->onDelete('RESTRICT');
        });

        Schema::create('team_fines', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->index('created_at');

            $table->decimal('value', 10, 2)->default(0);
            $table->text('create_reason');
            $table->text('update_reason')->nullable();

            $table->unsignedInteger('creator_id');
            $table
                ->foreign('creator_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');

            $table->unsignedBigInteger('order_id');
            $table
                ->foreign('order_id')
                ->references('id')
                ->on('orders')
                ->onDelete('RESTRICT');
        });

        Schema::create('team_fines_users', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('team_fine_id');
            $table->primary(['user_id', 'team_fine_id']);
            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
            $table
                ->foreign('team_fine_id')
                ->references('id')
                ->on('team_fines')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::dropIfExists('accruals');
        Schema::dropIfExists('personal_fines');
        Schema::dropIfExists('team_fines_users');
        Schema::dropIfExists('team_fines');
    }
}
