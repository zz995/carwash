<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarModelsTable extends Migration
{
    public function up()
    {
        Schema::create('car_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('category');
            $table->timestamps();
        });

        Schema::table('cars', function (Blueprint $table) {
            $table
                ->integer('car_model_id')
                ->unsigned();
            $table
                ->foreign('car_model_id')
                ->references('id')
                ->on('car_models')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->dropColumn('car_model_id');
        });
        Schema::dropIfExists('car_models');
    }
}
