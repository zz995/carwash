<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RequiredOnlyNumberInCars extends Migration
{
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table
                ->unsignedInteger('car_model_id')
                ->nullable()
                ->change();
            $table
                ->unsignedInteger('car_mark_id')
                ->nullable()
                ->change();
        });
    }

    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table
                ->unsignedInteger('car_model_id')
                ->nullable(false)
                ->change();
            $table
                ->unsignedInteger('car_mark_id')
                ->nullable(false)
                ->change();
        });
    }
}
