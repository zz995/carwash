<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarWashWork extends Migration
{

    public function up()
    {
        Schema::create('car_wash_types', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('name', 255);
            $table->timestamps();
        });

        Schema::create('car_wash_works', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->decimal('category1_price', 10, 2);
            $table->decimal('category1_cost', 10, 2)->nullable();
            $table->decimal('category1_salary', 10, 2)->nullable();
            $table->decimal('category2_price', 10, 2);
            $table->decimal('category2_cost', 10, 2)->nullable();
            $table->decimal('category2_salary', 10, 2)->nullable();
            $table->decimal('category3_price', 10, 2);
            $table->decimal('category3_cost', 10, 2)->nullable();
            $table->decimal('category3_salary', 10, 2)->nullable();
            $table->decimal('category4_price', 10, 2);
            $table->decimal('category4_cost', 10, 2)->nullable();
            $table->decimal('category4_salary', 10, 2)->nullable();
            $table->tinyInteger('type_id')->unsigned();
            $table->timestamps();

            $table
                ->foreign('type_id')
                ->references('id')
                ->on('car_wash_types')
                ->onDelete('RESTRICT');

        });
    }

    public function down()
    {
        Schema::dropIfExists('car_wash_works');
        Schema::dropIfExists('car_wash_types');
    }
}
