<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarMarkToCars extends Migration
{
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->integer('car_mark_id')->unsigned()->nullable();
            $table
                ->foreign('car_mark_id')
                ->references('id')
                ->on('car_marks')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->dropForeign(['car_mark_id']);
            $table->dropColumn('car_mark_id');
        });
    }
}
