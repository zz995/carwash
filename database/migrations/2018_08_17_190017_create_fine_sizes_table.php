<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFineSizesTable extends Migration
{
    public function up()
    {
        Schema::create('payment_refusal_fine_size', function (Blueprint $table) {
            $table->unsignedInteger('id')->primary();
            $table->timestamps();
            $table
                ->unsignedInteger('initiator_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
            $table->decimal('admin_fine', 10, 2)->default(0);
            $table->decimal('washer_fine', 10, 2)->default(0);
        });
    }

    public function down()
    {
        Schema::dropIfExists('payment_refusal_fine_size');
    }
}
