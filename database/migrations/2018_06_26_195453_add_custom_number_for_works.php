<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCustomNumberForWorks extends Migration
{
    public function up()
    {
        Schema::table('car_wash_works', function (Blueprint $table) {
            $table->integer('number')->unique();
        });
    }

    public function down()
    {
        Schema::table('car_wash_works', function (Blueprint $table) {
            $table->dropUnique(['number']);
            $table->dropColumn('number');
        });
    }
}
