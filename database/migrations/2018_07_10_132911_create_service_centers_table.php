<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceCentersTable extends Migration
{

    public function up()
    {
        Schema::create('service_centers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->text('desc')->nullable();
            $table->tinyInteger('disabled')->default(0);
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('service_center_id')->nullable();
            $table
                ->foreign('service_center_id')
                ->references('id')
                ->on('service_centers')
                ->onDelete('RESTRICT');
        });

        Schema::table('login_history', function (Blueprint $table) {
            $table->unsignedInteger('service_center_id')->nullable();
            $table
                ->foreign('service_center_id')
                ->references('id')
                ->on('service_centers')
                ->onDelete('RESTRICT');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedInteger('service_center_id')->nullable();
            $table
                ->foreign('service_center_id')
                ->references('id')
                ->on('service_centers')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['service_center_id']);
            $table->dropColumn('service_center_id');
        });

        Schema::table('login_history', function (Blueprint $table) {
            $table->dropForeign(['service_center_id']);
            $table->dropColumn('service_center_id');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['service_center_id']);
            $table->dropColumn('service_center_id');
        });

        Schema::dropIfExists('service_centers');
    }
}
