<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContractNumberApplyingDiscountFieldToOrders extends Migration
{
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table
                ->unsignedInteger('contract_id_applying_discount')
                ->nullable()
                ->references('id')
                ->on('contracts')
                ->onDelete('RESTRICT');
            $table->tinyInteger('applying_discount_size_from_contract')->default(0);
        });
    }

    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign(['contract_id_applying_discount']);
            $table->dropColumn('contract_id_applying_discount');
            $table->dropColumn('applying_discount_size_from_contract');
        });
    }
}
