<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarMarksTable extends Migration
{

    public function up()
    {
        Schema::create('car_marks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::table('car_models', function (Blueprint $table) {
            $table->integer('car_mark_id')->unsigned()->nullable();
            $table
                ->foreign('car_mark_id')
                ->references('id')
                ->on('car_marks')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::table('car_models', function (Blueprint $table) {
            $table->dropForeign(['car_mark_id']);
            $table->dropColumn('car_mark_id');
        });

        Schema::dropIfExists('car_marks');
    }
}
