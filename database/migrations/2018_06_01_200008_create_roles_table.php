<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{

    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->tinyInteger('id')->unsigned()->primary();
            $table->string('name');
            $table->integer('salary')->nullable();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role');
            $table
                ->tinyInteger('role_id')
                ->unsigned();
            $table
                ->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('role', 16);
            $table->dropColumn('role_id');
        });
        Schema::dropIfExists('roles');
    }
}
