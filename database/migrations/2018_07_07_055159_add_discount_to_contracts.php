<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiscountToContracts extends Migration
{
    public function up()
    {
        Schema::rename("car_wash_contracts", "contracts");
        Schema::table('contracts', function (Blueprint $table) {
            $table->tinyInteger('disc')->default(0);
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('car_wash_contract_id', 'contract_id');
        });
    }

    public function down()
    {
        Schema::rename("contracts", "car_wash_contracts");
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('disc');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->renameColumn('contract_id', 'car_wash_contract_id');
        });
    }
}
