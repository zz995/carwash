<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveNameAndSurnameFromDiscountCards extends Migration
{
    public function up()
    {
        Schema::table('discount_cards', function (Blueprint $table) {
            $table->unsignedInteger('client_id')->nullable();
            $table
                ->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('RESTRICT');
        });

        foreach (DB::table('discount_cards')->get() as $card) {
            $client = DB::table('clients')->where('phone', '=', $card->phone)->first();
            if (is_null($client)) {
                DB::table('clients')->insert([
                    'name' => $card->name,
                    'phone' => $card->phone,
                    'created_at' => $card->created_at,
                    'updated_at' => $card->updated_at,
                ]);
                $id = DB::getPdo()->lastInsertId();
            }
            DB::table('discount_cards')->where('id', '=', $card->id)->update(['client_id' => $id]);
        }

        Schema::table('discount_cards', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('phone');
            $table->unsignedInteger('client_id')->nullable(false)->change();
        });
    }

    public function down()
    {
        Schema::table('discount_cards', function (Blueprint $table) {
            $table->string('name');
            $table->string('phone', 40)->unique()->nullable()->default(null);
        });

        foreach (DB::table('discount_cards')->get() as $card) {
            $client = DB::table('clients')->find($card->client_id);
            DB::table('discount_cards')->where('id', '=', $card->id)->update(['name' => $client->name, 'phone' => $client->phone]);
        }

        Schema::table('discount_cards', function (Blueprint $table) {
            $table->dropForeign(['client_id']);
            $table->dropColumn('client_id');
        });
    }
}
