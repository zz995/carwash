<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCafeTable extends Migration
{
    public function up()
    {
        Schema::create('cafe_sale_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->tinyInteger('active')->default(1);
        });

        Schema::create('cafe_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('payment_type');
            $table->timestamps();
            $table->index('created_at');
            $table->decimal('sum', 10, 2)->default(0);
            $table->unsignedInteger('cafe_sale_type_id');
            $table
                ->foreign('cafe_sale_type_id')
                ->references('id')
                ->on('cafe_sale_types')
                ->onDelete('RESTRICT');
            $table->unsignedInteger('service_center_id');
            $table
                ->foreign('service_center_id')
                ->references('id')
                ->on('service_centers')
                ->onDelete('RESTRICT');
            $table->unsignedInteger('cashier_id');
            $table
                ->foreign('cashier_id')
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');
        });
    }

    public function down()
    {
        Schema::dropIfExists('cafe_sales');
        Schema::dropIfExists('cafe_sale_types');
    }
}
