<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToDiscountCardsTable extends Migration
{
    public function up()
    {
        Schema::table('discount_cards', function (Blueprint $table) {
            $table
                ->unsignedInteger('created_id')
                ->nullable()
                ->references('id')
                ->on('users')
                ->onDelete('RESTRICT');

            $table
                ->unsignedInteger('issued_id')
                ->nullable()
                ->references('id')
                ->on('contracts')
                ->onDelete('RESTRICT');

            $table->timestamp('issued_at')->nullable();
            $table->text('comment')->nullable();
        });
    }

    public function down()
    {
        Schema::table('discount_cards', function (Blueprint $table) {
            $table->dropForeign(['created_id']);
            $table->dropForeign(['issued_id']);
            $table->dropColumn('created_id');
            $table->dropColumn('issued_id');
            $table->dropColumn('issued_at');
            $table->dropColumn('comment');
        });
    }
}
