let mix = require('laravel-mix');

mix
    .setPublicPath('public/build')
    .setResourceRoot('/build/')
    .js('resources/assets/js/admin.js', 'js')
    .js('resources/assets/js/order-client-register.js', 'js')
    .js('resources/assets/js/order-ordering.js', 'js')
    .js('resources/assets/js/order-edit-works.js', 'js')
    .js('resources/assets/js/discount-card-form.js', 'js')
    .js('resources/assets/js/order-payment.js', 'js')
    .js('resources/assets/js/order-list-payment.js', 'js')
    .js('resources/assets/js/app.js', 'js')
    .js('resources/assets/js/auth.js', 'js')
    .sass('resources/assets/sass/admin.scss', 'css')
    .sass('resources/assets/sass/app.scss', 'css')
    .sass('resources/assets/sass/auth.scss', 'css');

if (!mix.inProduction()) {
    mix.sourceMaps();
}

mix.version();
