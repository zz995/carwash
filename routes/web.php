<?php



Route::get('/', function () {
    dd(1);
});

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(
    [
        'prefix' => 'employees',
        'as' => 'employees.',
        'namespace' => 'Employees',
        'middleware' => ['auth', 'mustByActive'],
    ],
    function () {
        Route::get('/contract/autocomplete', 'ContractController@autocomplete')->name('contract.autocomplete');
        Route::get('/client/autocomplete', 'ClientController@autocomplete')->name('client.autocomplete');
        Route::get('/car/autocomplete', 'CarController@autocomplete')->name('car.autocomplete');
        Route::get('/car-model', 'CarModelController@byMark')->name('carModel.byMark');

        Route::group(
            [
                'prefix' => 'discount-card',
                'as' => 'discountCard.',
            ],
            function () {
                Route::get('', 'DiscountCardController@list')->name('list');
                Route::get('/create', 'DiscountCardController@createForm')->name('create');
                Route::post('/create', 'DiscountCardController@create');
                Route::get('/check', 'DiscountCardController@check')->name('check');

                Route::get('/{discountCard}/edit', 'DiscountCardController@editForm')->name('edit');
                Route::put('/{discountCard}/edit', 'DiscountCardController@edit');
                Route::get('/{discountCard}/edit-disc', 'DiscountCardController@editDiscForm')->name('editDisc');
                Route::put('/{discountCard}/edit-disc', 'DiscountCardController@editDisc');

                Route::get('/{discountCard}', 'DiscountCardController@show')->name('show');
            }
        );

        Route::group(
            [
                'prefix' => 'cafe',
                'as' => 'cafe.'
            ],
            function () {
                Route::get('/sale', 'CafeController@createSaleForm')->name('createSale');
                Route::post('/sale', 'CafeController@createSale');
            }
        );

        Route::group(
            [
                'prefix' => 'order',
                'as' => 'order.',
                'namespace' => 'Order',
            ],
            function () {
                Route::get('', 'OrderController@list')->name('list');

                Route::get('/service-history', 'ServiceHistoryController@list')->name('serviceHistory');

                Route::get('/check-new-order', 'PaymentController@checkNewOrder')->name('checkNewOrder');
                Route::get('/user-register', 'ClientRegisterController@registerForm')->name('clientRegister');
                Route::post('/user-register', 'ClientRegisterController@register');

                Route::get('/{car}/{client}/ordering', 'OrderingController@orderingForm')->name('ordering');
                Route::post('/{car}/{client}/ordering', 'OrderingController@ordering');

                Route::get('/{order}', 'OrderingController@show')->name('show');

                Route::get('/{order}/edit-works', 'OrderingController@editWorksForm')->name('editWorks');
                Route::patch('/{order}/edit-works', 'OrderingController@editWorks');

                Route::patch('/{order}/admin-comment', 'OrderingController@comment')->name('adminComment');

                Route::get('/list/payment', 'PaymentController@list')->name('payment.list');
                Route::get('/{order}/payment', 'PaymentController@paymentForm')->name('payment');
                Route::post('/{order}/payment', 'PaymentController@payment');
                Route::put('/{order}/repayment', 'PaymentController@repayment')->name('repayment');
                Route::post('/{order}/payment-refusal', 'PaymentController@refusal')->name('paymentRefusal');

                Route::get('/list/overdrive', 'OverdriveController@list')->name('overdrive.list');
                Route::get('/{order}/appoint-team', 'AppointTeamController@appointTeamForm')->name('appointTeam');
                Route::patch('/{order}/appoint-team', 'AppointTeamController@appointTeam');

                Route::get('/{order}/reassign-team', 'AppointTeamController@reassignTeamForm')->name('reassignTeam');
                Route::patch('/{order}/reassign-team', 'AppointTeamController@reassignTeam');

                Route::patch('/{order}/comment', 'PaymentController@comment')->name('comment');

                Route::get('/{order}/fine', 'FineController@createForm')->name('createFine');
                Route::post('/{order}/fine', 'FineController@create');

                Route::get('/{order}/cross', 'FineController@crossForm')->name('cross');
                Route::post('/{order}/cross', 'FineController@cross');

                Route::patch('/{order}/executed', 'OrderingController@executed')->name('executed');

                Route::get('/fine/{fine}/disable', 'FineController@disableForm')->name('disableFine');
                Route::put('/fine/{fine}/disable', 'FineController@disable');
            }
        );

        Route::group(
            [
                'prefix' => 'teams',
                'as' => 'teams.',
            ],
            function () {
                Route::get('', 'TeamController@list')->name('list');
                Route::get('/create', 'TeamController@createForm')->name('create');
                Route::post('/create', 'TeamController@create');
                Route::get('/{team}', 'TeamController@show')->name('show');
                Route::get('/{team}/edit', 'TeamController@editForm')->name('edit');
                Route::put('/{team}/edit', 'TeamController@edit');
                Route::delete('/{team}/delete', 'TeamController@delete')->name('delete');
            }
        );

        Route::group(
            [
                'prefix' => 'balance',
                'as' => 'balance.',
            ],
            function () {
                Route::get('', 'BalanceController@common')->name('common');
            }
        );

        Route::group(
            [
                'prefix' => 'accrual',
                'as' => 'accrual.',
                'namespace' => 'Accrual',
            ],
            function () {
                Route::get('', 'AccrualController@common')->name('common');
                Route::get('personal-fine', 'PersonalFineController@list')->name('personalFines');
                Route::get('team-fine', 'TeamFineController@list')->name('teamFines');
                Route::get('employees', 'EmployeesController@list')->name('employees');
            }
        );

        Route::group(
            [
                'prefix' => 'washer',
                'as' => 'washer.',
                'namespace' => 'Washer',
            ],
            function () {
                Route::get('', 'WasherController@list')->name('list');
                Route::get('/{user}', 'WasherController@show')->name('show');

                Route::get('/{user}/fine', 'PersonalFineController@createForm')->name('createFine');
                Route::post('/{user}/fine', 'PersonalFineController@create');

                Route::get('/personal-fine/{fine}', 'PersonalFineController@show')->name('showPersonalFine');
                Route::get('/personal-fine/{fine}/disable', 'PersonalFineController@disableForm')->name('disablePersonalFine');
                Route::put('/personal-fine/{fine}/disable', 'PersonalFineController@disable');
                Route::get('/{user}/personal-fines', 'PersonalFineController@list')->name('personalFines');
                Route::get('/{user}/team-fines', 'TeamFineController@list')->name('teamFines');
                Route::get('/{user}/salary-breakdown', 'SalaryBreakdownController@index')->name('salaryBreakdown');
            }
        );
    }
);

Route::group(
    [
        'prefix' => 'super_admin',
        'as' => 'superAdmin.',
        'namespace' => 'SuperAdmin',
        'middleware' => ['auth', 'mustByActive', App\Http\Middleware\SuperAdmin::class],
    ],
    function () {
        Route::get('/', 'HomeController@index')->name('home');

        Route::group(
            [
                'prefix' => 'fine',
                'as' => 'fine.',
                'namespace' => 'Fine',
            ],
            function () {
                Route::get('payment-refusal-fine-size', 'PaymentRefusalFineSizeController@form')->name('paymentRefusalFineSize');
                Route::put('payment-refusal-fine-size', 'PaymentRefusalFineSizeController@change');
            }
        );

        Route::group(
            [
                'prefix' => 'service_center',
                'as' => 'serviceCenter.'
            ],
            function () {
                Route::get('', 'ServiceCentersController@list')->name('list');
                Route::get('/create', 'ServiceCentersController@createForm')->name('create');
                Route::post('/create', 'ServiceCentersController@create');
                Route::get('/{serviceCenter}', 'ServiceCentersController@show')->name('show');
                Route::get('/{serviceCenter}/edit', 'ServiceCentersController@editForm')->name('edit');
                Route::put('/{serviceCenter}/edit', 'ServiceCentersController@edit');
                Route::delete('/{serviceCenter}/delete', 'ServiceCentersController@delete')->name('delete');
            }
        );

        Route::group(
            [
                'prefix' => 'car_wash',
                'as' => 'carWash.',
                'namespace' => 'CarWash',
            ],
            function () {
                Route::group(
                    [
                        'prefix' => 'works',
                        'as' => 'works.',
                    ],
                    function () {
                        Route::get('', 'WorksController@index')->name('list');
                        Route::get('/create', 'WorksController@createForm')->name('create');
                        Route::post('/create', 'WorksController@create');
                        Route::get('/{work}', 'WorksController@show')->name('show');
                        Route::get('/{work}/edit', 'WorksController@editForm')->name('edit');
                        Route::put('/{work}/edit', 'WorksController@edit');
                        Route::delete('/{work}/delete', 'WorksController@delete')->name('delete');
                    }
                );

                Route::group(
                    [
                        'prefix' => 'order',
                        'as' => 'order.',
                    ],
                    function () {
                        Route::get('/list', 'OrdersController@list')->name('list');
                        Route::get('/service-history', 'OrdersController@serviceHistory')->name('serviceHistory');
                        Route::get('/{order}/show', 'OrdersController@show')->name('show');
                        Route::put('/{order}/cancel', 'OrdersController@cancel')->name('cancel');
                    }
                );
            }
        );

        Route::group(
            [
                'prefix' => 'contracts',
                'as' => 'contracts.',
            ],
            function () {
                Route::get('', 'ContractsController@list')->name('list');
                Route::get('/create', 'ContractsController@createForm')->name('create');
                Route::post('/create', 'ContractsController@create');
                Route::get('/{contract}', 'ContractsController@show')->name('show');
                Route::get('/{contract}/edit', 'ContractsController@editForm')->name('edit');
                Route::put('/{contract}/edit', 'ContractsController@edit');
                Route::delete('/{contract}/delete', 'ContractsController@delete')->name('delete');
            }
        );

        Route::group(
            [
                'prefix' => 'discount-card',
                'as' => 'discountCard.',
                'namespace' => 'DiscountCard'
            ],
            function () {
                Route::get('', 'DiscountCardController@list')->name('list');
                Route::get('/{discountCard}/show', 'DiscountCardController@show')->name('show');
                Route::get('/{discountCard}/edit', 'DiscountCardController@editForm')->name('edit');
                Route::put('/{discountCard}/edit', 'DiscountCardController@edit');
                Route::get('/{discountCard}/edit-disc', 'DiscountCardController@editDiscForm')->name('editDisc');
                Route::put('/{discountCard}/edit-disc', 'DiscountCardController@editDisc');
                Route::post('/{discountCard}/deny-edit', 'DiscountCardController@denyEdit')->name('denyEdit');
                Route::post('/{discountCard}/allow-edit', 'DiscountCardController@allowEdit')->name('allowEdit');
                Route::group(
                    [
                        'prefix' => 'steps',
                        'as' => 'steps.',
                    ],
                    function () {
                        Route::get('', 'StepsController@list')->name('list');
                        Route::get('/create', 'StepsController@createForm')->name('create');
                        Route::post('/create', 'StepsController@create');
                        Route::get('/{step}/edit', 'StepsController@editForm')->name('edit');
                        Route::put('/{step}/edit', 'StepsController@edit');
                        Route::delete('/{step}/delete', 'StepsController@delete')->name('delete');
                    }
                );
            }
        );

        Route::group(
            [
                'prefix' => 'employees',
                'as' => 'employees.',
                'namespace' => 'Employees',
            ],
            function () {
                Route::group(
                    [
                        'prefix' => 'roles',
                        'as' => 'roles.',
                    ],
                    function () {
                        Route::get('', 'RoleController@index')->name('list');
                        Route::get('/{role}/salary', 'SalaryController@changeSalaryForm')->name('changeSalary');
                        Route::patch('/{role}/salary', 'SalaryController@changeSalary');
                    }
                );

                Route::get('', 'EmployeesController@index')->name('list');
                Route::get('/create', 'EmployeesController@createForm')->name('create');
                Route::post('/create', 'EmployeesController@create');
                Route::get('/{employe}', 'EmployeesController@show')->name('show');
                Route::get('/{employe}/edit', 'EmployeesController@editForm')->name('edit');
                Route::put('/{employe}/edit', 'EmployeesController@edit');
                Route::get('/{employe}/changePassword', 'EmployeesController@changePasswordForm')->name('changePassword');
                Route::patch('/{employe}/changePassword', 'EmployeesController@changePassword');
                Route::delete('/{employe}/delete', 'EmployeesController@delete')->name('delete');
                Route::patch('/{employe}/recovery', 'EmployeesController@recovery')->name('recovery');
                Route::patch('/{employe}/disable', 'EmployeesController@disable')->name('disable');
                Route::get('/{employe}/payout', 'EmployeesController@payoutForm')->name('payout');
                Route::post('/{employe}/payout', 'EmployeesController@payout');
                Route::get('/{employe}/salary-breakdown', 'SalaryBreakdownController@index')->name('salaryBreakdown');
            }
        );
    }
);
