<?php

use App\Entity\Car;
use App\Entity\Contract;
use App\Entity\Client;
use App\Entity\Order\Order;
use App\Entity\CarWash\Work;
use App\Entity\Team;
use App\Entity\User\Fine\PersonalFine;
use App\Entity\User\Fine\TeamFine;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Entity\DiscountCard\DiscountCard;;
use App\Entity\ServiceCenter;
use App\Entity\DiscountCard\CalculationStep;
use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator as Crumbs;

Breadcrumbs::register('superAdmin.home', function (Crumbs $crumbs) {
    $crumbs->push(trans('home.header'), route('superAdmin.home'));
});

Breadcrumbs::register('superAdmin.employees.list', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.home');
    $crumbs->push(trans('user.page.list.header'), route('superAdmin.employees.list'));
});

Breadcrumbs::register('superAdmin.employees.create', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.employees.list');
    $crumbs->push(trans('user.page.create.header'), route('superAdmin.employees.create'));
});

Breadcrumbs::register('superAdmin.employees.show', function (Crumbs $crumbs, User $employe) {
    $crumbs->parent('superAdmin.employees.list');
    $crumbs->push(
        trans('user.page.show.header',['employe' => $employe->getFio()->getFullName()]),
        route('superAdmin.employees.show', ['employe' => $employe])
    );
});

Breadcrumbs::register('superAdmin.employees.edit', function (Crumbs $crumbs, User $employe) {
    $crumbs->parent('superAdmin.employees.show', $employe);
    $crumbs->push(
        trans('user.page.edit.action', ['employe' => $employe->getFio()->getFullName()]),
        route('superAdmin.employees.edit', ['employe' => $employe])
    );
});

Breadcrumbs::register('superAdmin.employees.payout', function (Crumbs $crumbs, User $employe) {
    $crumbs->parent('superAdmin.employees.show', $employe);
    $crumbs->push(
        trans('user.page.payout.action'),
        route('superAdmin.employees.payout', ['employe' => $employe])
    );
});

Breadcrumbs::register('superAdmin.employees.salaryBreakdown', function (Crumbs $crumbs, User $employe) {
    $crumbs->parent('superAdmin.employees.show', $employe);
    $crumbs->push(
        trans('user.page.salaryBreakdown.header'),
        route('superAdmin.employees.salaryBreakdown', ['employe' => $employe])
    );
});

Breadcrumbs::register('superAdmin.employees.changePassword', function (Crumbs $crumbs, User $employe) {
    $crumbs->parent('superAdmin.employees.edit', $employe);
    $crumbs->push(
        trans('user.page.changePassword.action', ['employe' => $employe->getFio()->getFullName()]),
        route('superAdmin.employees.changePassword', ['employe' => $employe])
    );
});

Breadcrumbs::register('superAdmin.employees.roles.list', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.home');
    $crumbs->push(trans('role.page.list.header'), route('superAdmin.employees.roles.list'));
});

Breadcrumbs::register('superAdmin.employees.roles.changeSalary', function (Crumbs $crumbs, Role $role) {
    $crumbs->parent('superAdmin.employees.roles.list');
    $crumbs->push(trans('role.page.changeSalary.header'), route('superAdmin.employees.roles.changeSalary', ['role' => $role]));
});

Breadcrumbs::register('superAdmin.carWash.works.list', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.home');
    $crumbs->push(trans('works.page.list.header'), route('superAdmin.carWash.works.list'));
});

Breadcrumbs::register('superAdmin.carWash.works.create', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.carWash.works.list');
    $crumbs->push(trans('works.page.create.header'), route('superAdmin.carWash.works.create'));
});

Breadcrumbs::register('superAdmin.carWash.works.show', function (Crumbs $crumbs, Work $work) {
    $crumbs->parent('superAdmin.carWash.works.list');
    $crumbs->push(
        trans('works.page.show.header'),
        route('superAdmin.carWash.works.show', ['work' => $work])
    );
});

Breadcrumbs::register('superAdmin.carWash.works.edit', function (Crumbs $crumbs, Work $work) {
    $crumbs->parent('superAdmin.carWash.works.show', $work);
    $crumbs->push(
        trans('works.page.edit.action'),
        route('superAdmin.carWash.works.edit', ['work' => $work])
    );
});

Breadcrumbs::register('superAdmin.carWash.order.list', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.home');
    $crumbs->push(trans('order.page.list.header'), route('superAdmin.carWash.order.list'));
});

Breadcrumbs::register('superAdmin.carWash.order.serviceHistory', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.home');
    $crumbs->push(trans('order.page.serviceHistory.header'), route('superAdmin.carWash.order.serviceHistory'));
});

Breadcrumbs::register('superAdmin.carWash.order.show', function (Crumbs $crumbs, Order $order) {
    $crumbs->parent('superAdmin.carWash.order.list');
    $crumbs->push(trans('order.page.show.header'), route('superAdmin.carWash.order.show', $order));
});

Breadcrumbs::register('superAdmin.contracts.list', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.home');
    $crumbs->push(trans('contracts.page.list.header'), route('superAdmin.contracts.list'));
});
Breadcrumbs::register('superAdmin.contracts.create', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.contracts.list');
    $crumbs->push(trans('contracts.page.create.header'), route('superAdmin.contracts.create'));
});
Breadcrumbs::register('superAdmin.contracts.show', function (Crumbs $crumbs, Contract $contract) {
    $crumbs->parent('superAdmin.contracts.list');
    $crumbs->push(trans('contracts.page.show.header'), route('superAdmin.contracts.show', [$contract]));
});
Breadcrumbs::register('superAdmin.contracts.edit', function (Crumbs $crumbs, Contract $contract) {
    $crumbs->parent('superAdmin.contracts.show', $contract);
    $crumbs->push(
        trans('contracts.page.edit.header'),
        route('superAdmin.contracts.edit', ['contract' => $contract])
    );
});


Breadcrumbs::register('superAdmin.serviceCenter.list', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.home');
    $crumbs->push(trans('serviceCenter.page.list.header'), route('superAdmin.serviceCenter.list'));
});
Breadcrumbs::register('superAdmin.serviceCenter.create', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.serviceCenter.list');
    $crumbs->push(trans('serviceCenter.page.create.header'), route('superAdmin.serviceCenter.create'));
});
Breadcrumbs::register('superAdmin.serviceCenter.show', function (Crumbs $crumbs, ServiceCenter $serviceCenter) {
    $crumbs->parent('superAdmin.serviceCenter.list');
    $crumbs->push(trans('serviceCenter.page.show.header'), route('superAdmin.serviceCenter.show', [$serviceCenter]));
});
Breadcrumbs::register('superAdmin.serviceCenter.edit', function (Crumbs $crumbs, ServiceCenter $serviceCenter) {
    $crumbs->parent('superAdmin.serviceCenter.list');
    $crumbs->push(trans('serviceCenter.page.edit.header'), route('superAdmin.serviceCenter.edit', [$serviceCenter]));
});

Breadcrumbs::register('superAdmin.discountCard.steps.list', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.home');
    $crumbs->push(trans('discountCard.page.stepList.header'), route('superAdmin.discountCard.steps.list'));
});
Breadcrumbs::register('superAdmin.discountCard.steps.create', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.discountCard.steps.list');
    $crumbs->push(trans('discountCard.page.stepCreate.header'), route('superAdmin.discountCard.steps.create'));
});
Breadcrumbs::register('superAdmin.discountCard.steps.edit', function (Crumbs $crumbs, CalculationStep $step) {
    $crumbs->parent('superAdmin.discountCard.steps.list');
    $crumbs->push(trans('discountCard.page.stepEdit.header'), route('superAdmin.discountCard.steps.edit', $step));
});
Breadcrumbs::register('superAdmin.discountCard.list', function (Crumbs $crumbs) {
    $crumbs->push(trans('discountCard.page.list.header'), route('superAdmin.discountCard.list'));
});
Breadcrumbs::register('superAdmin.discountCard.show', function (Crumbs $crumbs, DiscountCard $discountCard) {
    $crumbs->parent('superAdmin.discountCard.list');
    $crumbs->push(trans('discountCard.page.show.header'), route('superAdmin.discountCard.show', [$discountCard]));
});
Breadcrumbs::register('superAdmin.discountCard.edit', function (Crumbs $crumbs, DiscountCard $discountCard) {
    $crumbs->parent('superAdmin.discountCard.show', $discountCard);
    $crumbs->push(trans('discountCard.page.edit.header'), route('superAdmin.discountCard.edit', $discountCard));
});
Breadcrumbs::register('superAdmin.discountCard.editDisc', function (Crumbs $crumbs, DiscountCard $discountCard) {
    $crumbs->parent('superAdmin.discountCard.show', $discountCard);
    $crumbs->push(trans('discountCard.page.editDisc.header'), route('superAdmin.discountCard.editDisc', $discountCard));
});

Breadcrumbs::register('superAdmin.fine.paymentRefusalFineSize', function (Crumbs $crumbs) {
    $crumbs->parent('superAdmin.home');
    $crumbs->push(trans('teamFine.page.paymentRefusalFineSize.header'), route('superAdmin.fine.paymentRefusalFineSize'));
});


Breadcrumbs::register('employees.teams.list', function (Crumbs $crumbs) {
    $crumbs->push(trans('team.page.list.header'), route('employees.teams.list'));
});

Breadcrumbs::register('employees.teams.create', function (Crumbs $crumbs) {
    $crumbs->parent('employees.teams.list');
    $crumbs->push(trans('team.page.create.header'), route('employees.teams.create'));
});

Breadcrumbs::register('employees.teams.show', function (Crumbs $crumbs, Team $team) {
    $crumbs->parent('employees.teams.list');
    $crumbs->push(trans('team.page.show.header', ['num' => $team->id]), route('employees.teams.show', $team));
});

Breadcrumbs::register('employees.teams.edit', function (Crumbs $crumbs, Team $team) {
    $crumbs->parent('employees.teams.show', $team);
    $crumbs->push(trans('team.page.edit.header'), route('employees.teams.edit', $team));
});

Breadcrumbs::register('employees.washer.list', function (Crumbs $crumbs) {
    $crumbs->push(trans('washer.page.list.header'), route('employees.washer.list'));
});
Breadcrumbs::register('employees.washer.show', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('employees.washer.list');
    $crumbs->push(trans('washer.page.show.header'), route('employees.washer.show', $user));
});
Breadcrumbs::register('employees.washer.createFine', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('employees.washer.show', $user);
    $crumbs->push(trans('personalFine.page.create.header'), route('employees.washer.createFine', $user));
});
Breadcrumbs::register('employees.washer.showPersonalFine', function (Crumbs $crumbs, PersonalFine $fine) {
    $crumbs->parent('employees.accrual.personalFines');
    $crumbs->push(trans('personalFine.page.show.header'), route('employees.washer.showPersonalFine', $fine));
});
Breadcrumbs::register('employees.washer.disablePersonalFine', function (Crumbs $crumbs, PersonalFine $fine) {
    $crumbs->parent('employees.washer.showPersonalFine', $fine);
    $crumbs->push(trans('personalFine.page.disable.header'), route('employees.washer.disablePersonalFine', $fine));
});
Breadcrumbs::register('employees.washer.personalFines', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('employees.washer.list');
    $crumbs->push(trans('washer.page.show.header'), route('employees.washer.personalFines', $user));
});
Breadcrumbs::register('employees.washer.teamFines', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('employees.washer.list');
    $crumbs->push(trans('washer.page.show.header'), route('employees.washer.teamFines', $user));
});
Breadcrumbs::register('employees.washer.salaryBreakdown', function (Crumbs $crumbs, User $user) {
    $crumbs->parent('employees.washer.list', $user);
    $crumbs->push(trans('washer.page.show.header'), route('employees.washer.salaryBreakdown', $user));
});

Breadcrumbs::register('employees.accrual.common', function (Crumbs $crumbs) {
    $crumbs->push(trans('accrual.page.common.header'), route('employees.accrual.common'));
});
Breadcrumbs::register('employees.accrual.personalFines', function (Crumbs $crumbs) {
    $crumbs->push(trans('personalFine.page.list.header'), route('employees.accrual.personalFines'));
});
Breadcrumbs::register('employees.accrual.teamFines', function (Crumbs $crumbs) {
    $crumbs->push(trans('teamFine.page.list.header'), route('employees.accrual.teamFines'));
});
Breadcrumbs::register('employees.accrual.employees', function (Crumbs $crumbs) {
    $crumbs->push(trans('accrual.page.employees.header'), route('employees.accrual.employees'));
});

Breadcrumbs::register('employees.balance.common', function (Crumbs $crumbs) {
    $crumbs->push(trans('balance.page.common.header'), route('employees.balance.common'));
});

Breadcrumbs::register('employees.order.serviceHistory', function (Crumbs $crumbs) {
    $crumbs->push(trans('order.page.serviceHistory.header'), route('employees.order.serviceHistory'));
});

Breadcrumbs::register('employees.order.list', function (Crumbs $crumbs) {
    $crumbs->push(trans('order.page.list.header'), route('employees.order.list'));
});

Breadcrumbs::register('employees.order.clientRegister', function (Crumbs $crumbs) {
    $crumbs->parent('employees.order.list');
    $crumbs->push(trans('order.page.create.header'), route('employees.order.clientRegister'));
});

Breadcrumbs::register('employees.order.ordering', function (Crumbs $crumbs, Car $car, Client $client) {
    $crumbs->parent('employees.order.clientRegister');
    $crumbs->push(trans('order.page.ordering.header'), route('employees.order.ordering', ['car' => $car, 'client' => $client]));
});

Breadcrumbs::register('employees.order.show', function (Crumbs $crumbs, Order $order) {
    $crumbs->parent('employees.order.list');
    $crumbs->push(trans('order.page.show.header'), route('employees.order.show', ['order' => $order]));
});

Breadcrumbs::register('employees.order.editWorks', function (Crumbs $crumbs, Order $order) {
    $crumbs->parent('employees.order.show', $order);
    $crumbs->push(trans('order.page.editWorks.header'), route('employees.order.editWorks', ['order' => $order]));
});

Breadcrumbs::register('employees.order.createFine', function (Crumbs $crumbs, Order $order) {
    $crumbs->parent('employees.order.show', $order);
    $crumbs->push(trans('teamFine.page.create.header'), route('employees.order.createFine', ['order' => $order]));
});
Breadcrumbs::register('employees.order.disableFine', function (Crumbs $crumbs, TeamFine $fine) {
    $crumbs->parent('employees.order.show', $fine->order);
    $crumbs->push(trans('teamFine.page.disable.header'), route('employees.order.disableFine', $fine));
});
Breadcrumbs::register('employees.order.cross', function (Crumbs $crumbs, Order $order) {
    $crumbs->parent('employees.order.show', $order);
    $crumbs->push(trans('order.page.cross.header'), route('employees.order.cross', $order));
});

Breadcrumbs::register('employees.order.payment.list', function (Crumbs $crumbs) {
    $crumbs->push(trans('order.page.list.header'), route('employees.order.payment.list'));
});
Breadcrumbs::register('employees.order.payment', function (Crumbs $crumbs, Order $order) {
    $crumbs->parent('employees.order.payment.list');
    $crumbs->push(trans('order.page.payment.header', ['num' => $order->id]), route('employees.order.payment', ['order' => $order]));
});

Breadcrumbs::register('employees.order.overdrive.list', function (Crumbs $crumbs) {
    $crumbs->push(trans('order.page.list.header'), route('employees.order.overdrive.list'));
});
Breadcrumbs::register('employees.order.appointTeam', function (Crumbs $crumbs, Order $order) {
    /** @var User $user */
    $user = Auth::user();
    if ($user->isOverdrive()) {
        $crumbs->parent('employees.order.overdrive.list');
    } else {
        $crumbs->parent('employees.order.show', $order);
    }

    $crumbs->push(trans('order.page.appointTeam.header'), route('employees.order.appointTeam', ['order' => $order]));
});
Breadcrumbs::register('employees.order.reassignTeam', function (Crumbs $crumbs, Order $order) {
    $crumbs->parent('employees.order.show', $order);
    $crumbs->push(trans('order.page.reassignTeam.header'), route('employees.order.reassignTeam', ['order' => $order]));
});

Breadcrumbs::register('employees.cafe.createSale', function (Crumbs $crumbs) {
    $crumbs->push(trans('cafe.page.createSale.header'), route('employees.cafe.createSale'));
});

Breadcrumbs::register('employees.discountCard.list', function (Crumbs $crumbs) {
    $crumbs->push(trans('discountCard.page.list.header'), route('employees.discountCard.list'));
});
Breadcrumbs::register('employees.discountCard.create', function (Crumbs $crumbs) {
    $crumbs->parent('employees.discountCard.list');
    $crumbs->push(trans('discountCard.page.create.header'), route('employees.discountCard.create'));
});
Breadcrumbs::register('employees.discountCard.show', function (Crumbs $crumbs, DiscountCard $discountCard) {
    $crumbs->parent('employees.discountCard.list');
    $crumbs->push(trans('discountCard.page.show.header'), route('employees.discountCard.show', [$discountCard]));
});
Breadcrumbs::register('employees.discountCard.edit', function (Crumbs $crumbs, DiscountCard $discountCard) {
    $crumbs->parent('employees.discountCard.show', $discountCard);
    $crumbs->push(trans('discountCard.page.edit.header'), route('employees.discountCard.edit', $discountCard));
});
Breadcrumbs::register('employees.discountCard.editDisc', function (Crumbs $crumbs, DiscountCard $discountCard) {
    $crumbs->parent('employees.discountCard.show', $discountCard);
    $crumbs->push(trans('discountCard.page.editDisc.header'), route('employees.discountCard.editDisc', $discountCard));
});