<?php

namespace Tests\Unit\User;

use App\ObjectValue\Money;
use Tests\TestCase;

class MoneyTest extends TestCase
{
    public function testMoneyRepresentationAsAStringIncludesDecimals()
    {
        $money = new Money('10.00');
        $this->assertEquals('10.00', (string) $money);
    }

    public function testMoneyAmountsCanBeAddedTogether()
    {
        $money = new Money('10.45');
        $finalMoney = $money->add(new Money('21.55'));
        $this->assertEquals('32.00', (string) $finalMoney);
    }

    public function testMoneyCanBeTakeAPercent()
    {
        $money = new Money('50');
        $finalMoney = $money->percent(5);
        $this->assertEquals('2.50', (string) $finalMoney);
    }

    public function testMoneyCanBeSubtracted()
    {
        $moneyA = new Money('50');
        $moneyB = new Money('15');
        $this->assertEquals('35', (string) $moneyA->sub($moneyB));
        $this->assertEquals('-35', (string) $moneyB->sub($moneyA));
    }

    public function testMoneyCanBeMultipliedForAFactor()
    {
        $money = new Money('54.46');
        $finalMoney = $money->multiply(100);
        $this->assertEquals('5446.00', (string) $finalMoney);
    }

    public function testMoneyCanBeDividedForADivisor()
    {
        $money = new Money('10');
        $finalMoney = $money->division(5);
        $this->assertEquals('2', (string) $finalMoney);
    }

    public function testMoneyCanBeDividedWithRemainderForADivisor()
    {
        $money = new Money('10');
        $finalMoney = $money->division(6);
        $this->assertEquals('1.66', (string) $finalMoney);
    }

    public function testMoneyCanBeMultipliedForAFactorAndMaintainPrecision()
    {
        $money = new Money('54.46');
        $finalMoney = $money->multiply('100000000000000000');
        $this->assertEquals('5446000000000000000.00', (string) $finalMoney);
    }

    public function testMoneyGreat()
    {
        $money = new Money('5');

        $this->assertTrue($money->gt(new Money('1')));
        $this->assertFalse($money->gt(new Money('10')));
        $this->assertFalse($money->gt($money));
    }

    public function testMoneyLess()
    {
        $money = new Money('5');

        $this->assertTrue($money->lt(new Money('10')));
        $this->assertFalse($money->lt(new Money('1')));
        $this->assertFalse($money->lt($money));
    }

    public function testMoneyEqual()
    {
        $money = new Money('5');

        $this->assertFalse($money->equal(new Money('1')));
        $this->assertFalse($money->equal(new Money('10')));
        $this->assertTrue($money->equal(new Money('5')));
        $this->assertTrue($money->equal($money));
    }
}
