<?php

namespace Tests\Unit\User;

use App\Entity\Order\BasketItem;
use App\ObjectValue\MoneyInterface;
use App\Services\SalaryCalculator\AdminCalculator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class AdminCalculatorTest extends TestCase
{
    use DatabaseTransactions;

    public function testSalaryFromItemsPrice(): void
    {
        $basket = [];
        $basket[] = BasketItem::make([
            'price' => 1000,
            'count' => 1,
            'cost' => null,
            'salary' => null
        ]);

        $percent = 5;

        $calc = new AdminCalculator(0, $basket, $percent);
        $salary = $calc->getSalary();

        $this->assertTrue($salary instanceof MoneyInterface);
        self::assertEquals($salary->getAmount(), 1000 / 100 * $percent);
    }

    public function testSalaryFromManyItemsPrice(): void
    {
        $basket = [];
        $basket[] = BasketItem::make([
            'price' => 1000,
            'count' => 1,
            'cost' => null,
            'salary' => null
        ]);
        $basket[] = BasketItem::make([
            'price' => 100,
            'count' => 1,
            'cost' => null,
            'salary' => null
        ]);
        $basket[] = BasketItem::make([
            'price' => 2501,
            'count' => 1,
            'cost' => null,
            'salary' => null
        ]);

        $percent = 5;

        $calc = new AdminCalculator(0, $basket, $percent);
        $salary = $calc->getSalary();

        $this->assertTrue($salary instanceof MoneyInterface);
        self::assertEquals($salary->getAmount(), (1000 + 100 + 2501) / 100 * $percent);
    }

    public function testSalaryFromItemsCost(): void
    {
        $basket = [];
        $basket[] = BasketItem::make([
            'price' => 1000,
            'count' => 1,
            'cost' => 100,
            'salary' => null
        ]);

        $percent = 5;

        $calc = new AdminCalculator(0, $basket, $percent);
        $salary = $calc->getSalary();

        $this->assertTrue($salary instanceof MoneyInterface);
        self::assertEquals($salary->getAmount(), 1000 / 100 * $percent);
    }

    public function testSalaryFromItemsSalary(): void
    {
        $basket = [];
        $basket[] = BasketItem::make([
            'price' => 1000,
            'count' => 1,
            'cost' => 100,
            'salary' => 200
        ]);

        $percent = 5;

        $calc = new AdminCalculator(0, $basket, $percent);
        $salary = $calc->getSalary();

        $this->assertTrue($salary instanceof MoneyInterface);
        self::assertEquals($salary->getAmount(), 1000 / 100 * $percent);
    }

    public function testSalaryFromItemsSalaryWithDisc(): void
    {
        $basket = [];
        $basket[] = BasketItem::make([
            'price' => 1000,
            'count' => 1,
            'cost' => 100,
            'salary' => 200
        ]);

        $percent = 5;

        $calc = new AdminCalculator(12, $basket, $percent);
        $salary = $calc->getSalary();

        $this->assertTrue($salary instanceof MoneyInterface);
        self::assertEquals($salary->getAmount(), 44);
    }

    public function testSalaryFromManyItemsSalaryWithDisc(): void
    {
        $basket = [];
        $basket[] = BasketItem::make([
            'price' => 1000,
            'count' => 1,
            'cost' => 100,
            'salary' => 200
        ]);
        $basket[] = BasketItem::make([
            'price' => 300,
            'count' => 1,
            'cost' => 100,
            'salary' => 200
        ]);
        $basket[] = BasketItem::make([
            'price' => 500,
            'count' => 1,
            'cost' => 100,
            'salary' => 200
        ]);

        $percent = 5;

        $calc = new AdminCalculator(12, $basket, $percent);
        $salary = $calc->getSalary();

        $this->assertTrue($salary instanceof MoneyInterface);
        self::assertEquals($salary->getAmount(), 79.2);
    }
}
