<?php

namespace Tests\Unit\User;

use App\Entity\Order\BasketItem;
use App\ObjectValue\MoneyInterface;
use App\Services\SalaryCalculator\AdminCalculator;
use App\Services\SalaryCalculator\WasherCalculator;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class WasherCalculatorTest extends TestCase
{
    use DatabaseTransactions;

    public function testSalaryFromItemsPrice(): void
    {
        $basket = [];
        $basket[] = BasketItem::make([
            'price' => 1000,
            'count' => 1,
            'cost' => null,
            'salary' => null
        ]);

        $calc = new WasherCalculator(0, $basket, 5, 3);
        $salary = $calc->getSalary();

        $this->assertTrue($salary instanceof MoneyInterface);
        self::assertEquals($salary->getAmount(), 16.66);
    }

    public function testSalaryFromManyItemsPrice(): void
    {
        $basket = [];
        $basket[] = BasketItem::make([
            'price' => 1000,
            'count' => 1,
            'cost' => null,
            'salary' => null
        ]);
        $basket[] = BasketItem::make([
            'price' => 100,
            'count' => 1,
            'cost' => null,
            'salary' => null
        ]);
        $basket[] = BasketItem::make([
            'price' => 2501,
            'count' => 1,
            'cost' => null,
            'salary' => null
        ]);

        $calc = new WasherCalculator(0, $basket, 30, 2);
        $salary = $calc->getSalary();

        $this->assertTrue($salary instanceof MoneyInterface);
        self::assertEquals($salary->getAmount(), 540.15);
    }

    public function testSalaryFromItemsCost(): void
    {
        $basket = [];
        $basket[] = BasketItem::make([
            'price' => 1000,
            'count' => 1,
            'cost' => 100,
            'salary' => null
        ]);

        $calc = new WasherCalculator(0, $basket, 25, 3);
        $salary = $calc->getSalary();

        $this->assertTrue($salary instanceof MoneyInterface);
        self::assertEquals($salary->getAmount(), 75);
    }

    public function testSalaryFromItemsSalary(): void
    {
        $basket = [];
        $basket[] = BasketItem::make([
            'price' => 1000,
            'count' => 1,
            'cost' => 100,
            'salary' => 200
        ]);

        $calc = new WasherCalculator(0, $basket, 5, 4);
        $salary = $calc->getSalary();

        $this->assertTrue($salary instanceof MoneyInterface);
        self::assertEquals($salary->getAmount(), 50);
    }

    public function testSalaryFromManyItemsSalary(): void
    {
        $basket = [];
        $basket[] = BasketItem::make([
            'price' => 700,
            'count' => 1,
            'cost' => 100,
            'salary' => 200
        ]);
        $basket[] = BasketItem::make([
            'price' => 300,
            'count' => 1,
            'cost' => 50,
            'salary' => 100
        ]);
        $basket[] = BasketItem::make([
            'price' => 1000,
            'count' => 1,
            'cost' => 100,
            'salary' => null
        ]);
        $basket[] = BasketItem::make([
            'price' => 800,
            'count' => 1,
            'cost' => null,
            'salary' => null
        ]);

        $calc = new WasherCalculator(0, $basket, 30, 4);
        $salary = $calc->getSalary();

        $this->assertTrue($salary instanceof MoneyInterface);
        self::assertEquals($salary->getAmount(), '202.50');
    }

    public function testSalaryFromManyItemsSalaryWithDisc(): void
    {
        $basket = [];
        $basket[] = BasketItem::make([
            'price' => 700,
            'count' => 1,
            'cost' => 100,
            'salary' => 200
        ]);
        $basket[] = BasketItem::make([
            'price' => 300,
            'count' => 1,
            'cost' => 50,
            'salary' => 100
        ]);
        $basket[] = BasketItem::make([
            'price' => 1000,
            'count' => 1,
            'cost' => 100,
            'salary' => null
        ]);
        $basket[] = BasketItem::make([
            'price' => 800,
            'count' => 1,
            'cost' => null,
            'salary' => null
        ]);

        $calc = new WasherCalculator(12, $basket, 30, 4);
        $salary = $calc->getSalary();

        $this->assertTrue($salary instanceof MoneyInterface);
        self::assertEquals($salary->getAmount(), '186.30');
    }
}
