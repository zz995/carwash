<?php

namespace Tests\Unit\User;

use App\Entity\DiscountCard\Accrual\StepsAccrual;
use App\Entity\DiscountCard\CalculationStep;
use App\Entity\DiscountCard\CalculationSteps;
use App\Entity\DiscountCard\DiscountCard;
use App\ObjectValue\Money;
use Tests\TestCase;

class StepsAccrualTest extends TestCase
{
    public function testCumulativeDiscountCardNotAccrual()
    {
        /** @var DiscountCard $card */
        $card = DiscountCard::make(['type' => DiscountCard::TYPE_FIXED, 'disc' => '0']);
        $card->current_step_accrual = new Money('0');

        $collect = collect([
            CalculationStep::make(['sum' => 1, 'disc' => 5]),
        ]);
        $steps = new CalculationSteps(...$collect);
        $accrual = new StepsAccrual($steps);

        $accrual->calculation($card, new Money('10000'));

        $this->assertEquals($card->getDisc(), 0);
        $this->assertEquals((string)$card->current_step_accrual, '0');
    }

    public function testEveryAccrualIncDiscOnOnePercent()
    {
        /** @var DiscountCard $card */
        $card = DiscountCard::make(['type' => DiscountCard::TYPE_CUMULATIVE, 'disc' => '0']);
        $card->current_step_accrual = new Money('0');

        $collect = collect([
            CalculationStep::make(['sum' => 0, 'disc' => 5]),
        ]);
        $steps = new CalculationSteps(...$collect);
        $accrual = new StepsAccrual($steps);

        $accrual->calculation($card, new Money('2'));
        $accrual->calculation($card, new Money('2'));

        $this->assertEquals($card->getDisc(), 2);
        $this->assertEquals((string)$card->current_step_accrual, '4');
    }

    public function testAccrualDisc()
    {
        /** @var DiscountCard $card */
        $card = DiscountCard::make(['type' => DiscountCard::TYPE_CUMULATIVE, 'disc' => '2']);
        $card->current_step_accrual = new Money('600');

        $collect = collect([
            CalculationStep::make(['sum' => 1000, 'disc' => 5]),
        ]);
        $steps = new CalculationSteps(...$collect);
        $accrual = new StepsAccrual($steps);

        $accrual->calculation($card, new Money('900'));

        $this->assertEquals($card->getDisc(), 3);
        $this->assertEquals((string)$card->current_step_accrual, '500.00');
    }

    public function testManyAccrualDisc()
    {
        /** @var DiscountCard $card */
        $card = DiscountCard::make(['type' => DiscountCard::TYPE_CUMULATIVE, 'disc' => '2']);
        $card->current_step_accrual = new Money('1000');

        $collect = collect([
            CalculationStep::make(['sum' => 1000, 'disc' => 5]),
            CalculationStep::make(['sum' => 2000, 'disc' => 10]),
        ]);
        $steps = new CalculationSteps(...$collect);
        $accrual = new StepsAccrual($steps);

        $accrual->calculation($card, new Money('10000'));

        $this->assertEquals($card->getDisc(), 9);
        $this->assertEquals((string)$card->current_step_accrual, '0');
    }

    public function testNotEnoughToIncDisc()
    {
        /** @var DiscountCard $card */
        $card = DiscountCard::make(['type' => DiscountCard::TYPE_CUMULATIVE, 'disc' => '2']);
        $card->current_step_accrual = new Money('1000');

        $collect = collect([
            CalculationStep::make(['sum' => 2000, 'disc' => 5]),
        ]);
        $steps = new CalculationSteps(...$collect);
        $accrual = new StepsAccrual($steps);

        $accrual->calculation($card, new Money('500'));

        $this->assertEquals($card->getDisc(), 2);
        $this->assertEquals((string)$card->current_step_accrual, '1500');
    }

    public function testMaximumDiscountIsReached()
    {
        /** @var DiscountCard $card */
        $card = DiscountCard::make(['type' => DiscountCard::TYPE_CUMULATIVE, 'disc' => '2']);
        $card->current_step_accrual = new Money('1000');

        $collect = collect([
            CalculationStep::make(['sum' => 1000, 'disc' => 5]),
            CalculationStep::make(['sum' => 2000, 'disc' => 10]),
        ]);
        $steps = new CalculationSteps(...$collect);
        $accrual = new StepsAccrual($steps);

        $accrual->calculation($card, new Money('15000'));

        $this->assertEquals($card->getDisc(), 10);
        $this->assertEquals((string)$card->current_step_accrual, '3000');
    }
}