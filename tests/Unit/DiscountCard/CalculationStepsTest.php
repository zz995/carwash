<?php

namespace Tests\Unit\User;

use App\Entity\DiscountCard\CalculationStep;
use App\Entity\DiscountCard\CalculationSteps;
use Tests\TestCase;

class CalculationStepsTest extends TestCase
{
    public function testNextStepNotFoundInEmptyCollection()
    {
        $collection = collect([]);
        $disc = 5;

        $steps = new CalculationSteps(...$collection);
        $this->assertNull($steps->getStep($disc));
    }

    public function testFoundNextStep()
    {
        $collection = collect([
            CalculationStep::make(['sum' => 20000, 'disc' => 15]),
            CalculationStep::make(['sum' => 10000, 'disc' => 5]),
            CalculationStep::make(['sum' => 15000, 'disc' => 10]),
        ]);
        $steps = new CalculationSteps(...$collection);

        $this->assertNotNull($steps->getStep(0));
        $this->assertEquals((string)$steps->getStep(0)->sum, '10000');

        $this->assertNotNull($steps->getStep(5));
        $this->assertEquals((string)$steps->getStep(5)->sum, '15000');

        $this->assertNotNull($steps->getStep(7));
        $this->assertEquals((string)$steps->getStep(7)->sum, '15000');

        $this->assertNotNull($steps->getStep(11));
        $this->assertEquals((string)$steps->getStep(11)->sum, '20000');

        $this->assertNull($steps->getStep(15));
        $this->assertNull($steps->getStep(20));
    }
}