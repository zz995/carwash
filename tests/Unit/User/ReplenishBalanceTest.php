<?php

namespace Tests\Unit\User;

use App\Entity\Order\Order;
use App\Entity\User\Role;
use App\Entity\User\Fine\TeamFine;
use App\Entity\User\User;
use App\ObjectValue\Money;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ReplenishBalanceTest extends TestCase
{
    use DatabaseTransactions;

    /** @var User $user */
    private $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->make([
            'login' => 'watcher',
            'role_id' => Role::WASHER,
            'status' => User::STATUS_ACTIVE,
        ]);
        $this->user->balance = new Money('0');
    }

    public function tearDown()
    {
        $this->user = null;
        DB::rollBack();
    }

    public function testReplenishBalanceByOrder()
    {
        $order = $this->getOrder();
        $this->user->replenishBalanceByOrder(new Money('10'), $order);
        $this->user->replenishBalanceByOrder(new Money('15'), $order);

        self::assertEquals((string)$this->user->balance, '25');
        self::assertEquals((string)$this->user->totalAccrual(), '25');
    }

    public function testReplenishBalanceByDailyAccrual()
    {
        $this->user->replenishBalanceByDailyAccrual(new Money('10'));
        $this->user->replenishBalanceByDailyAccrual(new Money('15'));

        self::assertEquals((string)$this->user->balance, '25');
        self::assertEquals((string)$this->user->totalAccrual(), '25');
    }

    public function testReduceBalanceByTeamFine()
    {
        $fine = $this->getTeamFine();
        $this->user->reduceBalanceByTeamFine(new Money('10'), $fine);

        self::assertEquals((string)$this->user->balance, '-10');
        self::assertEquals((string)$this->user->totalAccrual(), '-10');
    }

    private function getTeamFine(): TeamFine
    {
        $fine = new TeamFine;
        $fine->id = 1;

        return $fine;
    }

    private function getOrder(): Order
    {
        $order = new Order;
        $order->id = 1;

        return $order;
    }
}