<?php

namespace App\Services;

use App\Entity\Order\Order;
use App\Entity\User\Accrual;
use App\Entity\User\Fine\TeamFine;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Http\Requests\Order\CreateFineRequest;
use App\Http\Requests\Order\CrossRequest;
use App\Http\Requests\Order\DisableFineRequest;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use App\Services\SalaryCalculator\AdminCalculator;
use App\Services\SalaryCalculator\WasherCalculator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;

class TeamFineService
{
    public function create(Order $order, User $creator, CreateFineRequest $request)
    {
        DB::transaction(function () use($order, $creator, $request) {
            $sum = new Money($request->get('value'));
            $reason = $request->get('reason');

            $employees = $order->employees()->lockForUpdate()->get();
            if ($order->hasTeamFine()) {
                /** @var TeamFine $teamFine */
                $teamFine = $order->getBaseTeamFine();
                $teamFine->value = $sum;
                $teamFine->save();
            } else {
                $teamFine = new TeamFine();
                $teamFine->value = $sum;
                $teamFine->order()->associate($order);
                $teamFine->save();
                $teamFine->users()->sync($employees);
            }

            $teamFine->saveToHistory($reason, $creator);

            $employeesCount = count($employees);
            $value = $sum->division($employeesCount);
            /** @var User $employee */
            foreach ($employees as $employee) {
                $employee->reduceBalanceByTeamFine($value, $teamFine);
            }
        });
    }

    public function disable(TeamFine $fine, User $initiator, DisableFineRequest $request)
    {
        DB::transaction(function () use($fine, $initiator, $request) {
            $fine->disable($request->get('reason'), $initiator);
        });
    }

    public function cross(Order $order, User $initiator, CrossRequest $request)
    {
        DB::transaction(function () use($order, $initiator, $request) {
            $accruals = $order->accrual;

            $sum = new Money(0);
            /** @var Accrual $accrual */
            foreach ($accruals as $accrual) {
                $sum = $sum->add($accrual->value);
            }

            $employees = User::whereIn('id', array_unique(array_pluck($accruals, 'user_id')))->lockForUpdate()->get();

            $reason = $request->get('reason');
            $teamFine = TeamFine::register($order, $employees, $sum, $initiator, $reason, TeamFine::TYPE_CROSS);

            /** @var Accrual $accrual */
            foreach ($accruals as $accrual) {
                $employee = $accrual->user;
                $employee->reduceBalanceByTeamFine(
                    $accrual->value,
                    $teamFine
                );
            }
        });
    }
}