<?php

namespace App\Services;

use App\Entity\User\Salary;
use App\Entity\User\User;
use App\ObjectValue\MoneyInterface;
use Illuminate\Support\Facades\DB;

class PayoutService
{
    public function payout(MoneyInterface $sum, User $employee, User $initiator)
    {
        DB::transaction(function () use($sum, $employee, $initiator) {
            /** @var User $employee */
            $employee = User::where('id', $employee->id)->lockForUpdate()->first();

            /** @var Salary $salary */
            $salary = Salary::make(['value' => $sum]);
            $salary->initiator()->associate($employee);
            $salary->save();

            $employee->reduceBalanceByPayout($sum, $salary);
        });
    }
}