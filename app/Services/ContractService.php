<?php

namespace App\Services;

use App\Entity\Contract;
use Illuminate\Database\Eloquent\Collection;

class ContractService
{
    public function getMapDisc(Collection $contracts): array
    {
        $map = [];
        /** @var Contract $contract */
        foreach ($contracts as $contract) {
            $map[$contract->number] = $contract->disc;
        }

        return $map;
    }
}