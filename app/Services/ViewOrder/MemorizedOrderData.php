<?php

namespace App\Services\ViewOrder;

use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use Illuminate\Support\Facades\Cookie;

class MemorizedOrderData implements ViewOrderInterface
{
    private const COOKIE_NAME = 'order-ordering';
    private $exist;
    private $orderData = null;

    public function __construct()
    {
        $orderDataRaw = Cookie::get(self::COOKIE_NAME);
        $this->exist = isset($orderDataRaw);
        if ($this->exist) {
            $this->orderData = json_decode($orderDataRaw);
        }
    }

    public function has()
    {
        return $this->exist;
    }

    public function getHref()
    {
        if ($this->has()) {
            return $this->orderData->href;
        } else {
            return null;
        }
    }

    public function delete()
    {
        Cookie::forget(self::COOKIE_NAME);
        Cookie::queue(Cookie::forget(self::COOKIE_NAME));
    }

    public function getWorks(): array
    {
        if (!$this->has()) {
            return [];
        }

        $works = @$this->orderData->form->works;
        if (is_null($works)) {
            return [];
        }

        return $this->orderData->form->works;
    }

    public function getPrice($workId): ?MoneyInterface
    {
        if (!$this->has()) {
            return null;
        }

        if (!isset($this->orderData->form->prices->{$workId})) {
            return null;
        }

        return new Money($this->orderData->form->prices->{$workId});
    }

    public function getCount($workId): ?string
    {
        if (!$this->has()) {
            return null;
        }

        return @$this->orderData->form->counts->{$workId};
    }

    public function getTeam(): ?string
    {
        if (!$this->has()) {
            return null;
        }

        return @$this->orderData->form->team;
    }

    public function getEmployees(): array
    {
        if (!$this->has()) {
            return [];
        }

        $employees = @$this->orderData->form->employees;
        if (is_null($employees)) {
            return [];
        }

        return $this->orderData->form->employees;
    }

    public function getKey(): string
    {
        if (!$this->has()) {
            return '';
        }

        $key = @$this->orderData->form->key;
        if (is_null($key)) {
            return '';
        }

        return $key;
    }

    public function getComment(): string
    {
        if (!$this->has()) {
            return '';
        }

        $comment = @$this->orderData->form->comment;
        if (is_null($comment)) {
            return '';
        }

        return $this->orderData->form->comment;
    }
}