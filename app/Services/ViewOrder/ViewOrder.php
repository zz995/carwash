<?php

namespace App\Services\ViewOrder;

use App\Entity\Order\BasketItem;
use App\Entity\Order\Order;
use App\ObjectValue\MoneyInterface;

class ViewOrder implements ViewOrderInterface
{
    private $order;
    /** @var BasketItem[] $basketItems */
    private $basketItems;
    private $employeesIsEmpty = false;

    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->basketItems = $order->basketItems;
    }

    public function getWorks(): array
    {
        return array_pluck($this->basketItems, ['work_id']);
    }

    public function getPrice($workId): ?MoneyInterface
    {
        $price = null;
        $basketItem = $this->getBasketItemByWorkId($workId);
        if (isset($basketItem)) {
            $price = $basketItem->price;
        }

        return $price;
    }

    public function getCount($workId): ?string
    {
        $count = null;
        $basketItem = $this->getBasketItemByWorkId($workId);
        if (isset($basketItem)) {
            $count = (string)$basketItem->count;
        }

        return $count;
    }

    public function getEmployees(): array
    {
        if ($this->employeesIsEmpty) {
            return [];
        }

        return array_pluck($this->order->employees, ['id']);
    }

    public function employeeIdsToNull()
    {
        $this->employeesIsEmpty = true;
    }

    public function getKey(): string
    {
        return $this->order->number_key;
    }

    public function getComment(): string
    {
        return $this->order->admin_comment ?: '';
    }

    public function getTeam(): ?string
    {
        return null;
    }

    private function getBasketItemByWorkId($workId): ?BasketItem
    {
        /** @var BasketItem $basketItem */
        foreach ($this->basketItems as $basketItem) {
            if ($basketItem->work_id == $workId) {
                return $basketItem;
            }
        }

        return null;
    }
}