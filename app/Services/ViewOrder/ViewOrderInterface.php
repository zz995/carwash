<?php

namespace App\Services\ViewOrder;

use App\ObjectValue\MoneyInterface;

interface ViewOrderInterface
{
    public function getWorks(): array;
    public function getPrice($workId): ?MoneyInterface;
    public function getCount($workId): ?string;
    public function getEmployees(): array;
    public function getKey(): string;
    public function getComment(): string;
    public function getTeam(): ?string;
}