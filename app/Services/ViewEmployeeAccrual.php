<?php

namespace App\Services;

use App\Entity\User\User;
use App\ObjectValue\MoneyInterface;

class ViewEmployeeAccrual
{
    private $name;
    private $employee;
    private $salary;
    private $fine;

    public function __construct(string $name, User $employee, MoneyInterface $salary, MoneyInterface $fine)
    {
        $this->name = $name;
        $this->employee = $employee;
        $this->salary = $salary;
        $this->fine = $fine;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmployee(): User
    {
        return $this->employee;
    }

    public function getSalary(): MoneyInterface
    {
        return $this->salary;
    }

    public function getFine(): MoneyInterface
    {
        return $this->fine;
    }

    public function getResult(): MoneyInterface
    {
        return $this->salary->sub($this->fine);
    }
}