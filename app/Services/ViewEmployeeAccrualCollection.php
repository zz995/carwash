<?php

namespace App\Services;

use App\Entity\User\Accrual;
use App\Entity\User\User;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use App\Services\ViewEmployeeAccrual;

class ViewEmployeeAccrualCollection
{
    private $employees;
    private $accruals = [];

    public function __construct($employees)
    {
        $this->employees = $employees;

        /** @var User $employee */
        foreach ($employees as $employee) {
            $this->accruals[$employee->id] = new ViewEmployeeAccrual(
                $employee->getFio()->getFullName(),
                $employee,
                new Money(0),
                new Money(0)
            );
        }
    }

    public function add($employeeId, MoneyInterface $value, $type)
    {
        $fine = new Money(0);
        $salary = new Money(0);
        if ($type == Accrual::TYPE_ACCRUAL_BY_ORDER) {
            $salary = $value;
        } elseif ($type == Accrual::TYPE_DAILY_ACCRUAL) {
            $salary = $value;
        } elseif ($type == Accrual::TYPE_PERSONAL_FINE) {
            $fine = $value->multiply(-1);
        } elseif ($type == Accrual::TYPE_TEAM_FINE) {
            $fine = $value->multiply(-1);
        }

        if (isset($this->accruals[$employeeId])) {
            /** @var ViewEmployeeAccrual $old */
            $old = $this->accruals[$employeeId];
            $this->accruals[$employeeId] = new ViewEmployeeAccrual(
                $old->getName(),
                $old->getEmployee(),
                $salary->add($old->getSalary()),
                $fine->add($old->getFine())
            );
        }
    }

    public function getAccruals()
    {
        return $this->accruals;
    }


    public function getEmployees()
    {
        return $this->employees;
    }
}