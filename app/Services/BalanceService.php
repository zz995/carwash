<?php

namespace App\Services;

use App\Entity\Cafe\Sale;
use App\Entity\Cafe\SaleType;
use App\Entity\Contract;
use App\Entity\Order\Order;
use App\Entity\ServiceCenter;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use App\Services\Dto\BalanceCommonDataDto;
use App\Services\Dto\ContractBalanceDto;
use App\Services\Dto\DateRangeDto;
use App\Services\Dto\PointBalanceDto;
use App\Services\Dto\DateRangeLastMonthDto;
use App\Services\Dto\ReportDto;
use App\Services\Dto\SaleTypeBalanceDto;
use App\Services\Dto\TypePaymentDto;
use Illuminate\Support\Facades\DB;

class BalanceService
{
    private $serviceCenter;
    private $typeAggregator = 'sum';

    public const POINT_CAR_WASH = 1;
    public const POINT_CAFE = 2;

    public function __construct(ServiceCenter $serviceCenter)
    {
        $this->serviceCenter = $serviceCenter;
    }

    public function common(DateRangeDto $dateRangeDto, $paymentType, $point, $isStart): BalanceCommonDataDto
    {
        if ($paymentType == Order::PAYMENT_CONTRACT) {
            $balance = BalanceCommonDataDto::byContracts(
                $point == self::POINT_CAFE ? [] : $this->byContracts($dateRangeDto)
            );
        } elseif ($point == self::POINT_CAFE) {
            $balance = BalanceCommonDataDto::byCafeSaleTypes($this->byCafeSaleTypes($dateRangeDto, $paymentType));
        } elseif ($isStart) {
            $report = new ReportDto(
                $this->byCafeWithDifferentPaymentType(DateRangeDto::today()),
                $this->byContractsWithPayment(DateRangeDto::today()),
                $this->paymentPoints(DateRangeDto::today(), null, self::POINT_CAR_WASH),
                $this->paymentPoints(DateRangeDto::today(), null, null)->getCommon(),
                $this->paymentPoints(
                    DateRangeDto::startMonth(),
                    null,
                    null
                )->getCommon()
            );

            $this->typeAggregator = 'count';
            $countReport = new ReportDto(
                $this->byCafeWithDifferentPaymentType(DateRangeDto::today()),
                $this->byContractsWithPayment(DateRangeDto::today()),
                $this->paymentPoints(DateRangeDto::today(), null, self::POINT_CAR_WASH),
                $this->paymentPoints(DateRangeDto::today(), null, null)->getCommon(),
                $this->paymentPoints(
                    DateRangeDto::startMonth(),
                    null,
                    null
                )->getCommon()
            );

            $this->typeAggregator = 'sum';

            $balance = BalanceCommonDataDto::byReport($report, $countReport);
        } else {
            $balance = BalanceCommonDataDto::base(
                $this->paymentPoints($dateRangeDto, $paymentType, $point)
            );
        }

        return $balance;
    }


    private function byCafeWithDifferentPaymentType($dateRangeDto): array
    {
        $cafe = [];
        $cafeByCard = $this->byCafeSaleTypes($dateRangeDto, Order::PAYMENT_CARD);
        $cafeByCash = $this->byCafeSaleTypes($dateRangeDto, Order::PAYMENT_CASH);
        for($i = 0; $i < count($cafeByCard); $i++) {
            $pointBalanceDto = new PointBalanceDto();
            $pointBalanceDto->card($cafeByCard[$i]->sum);
            $pointBalanceDto->cash($cafeByCash[$i]->sum);
            $cafe[] = new TypePaymentDto(
                $cafeByCard[$i]->id,
                $cafeByCard[$i]->name,
                $pointBalanceDto
            );
        }

        return $cafe;
    }

    private function byCafeSaleTypes(DateRangeDto $dateRangeDto, $paymentType): array
    {
        $query = Sale::byServiceCenter($this->serviceCenter);
        $query = $query->whereBetween('created_at', $dateRangeDto->getRange());

        if (isset($paymentType)) {
            $query = $query->paymentType($paymentType);
        }

        $query = $query->groupBy('cafe_sale_type_id');

        $sales = [];

        if ($this->typeAggregator == 'sum') {
            $data = $query->select(DB::raw('sum(`sum`) as `sum`, cafe_sale_type_id'))->get();
        } else {
            $data = $query->select(DB::raw('count(*) as `sum`, cafe_sale_type_id'))->get();
        }

        foreach ($data as $sum) {
            $sales[$sum->cafe_sale_type_id] = $sum->sum;
        };

        $saleTypes = [];
        foreach (SaleType::active()->orderBy('name')->get(['id', 'name'])->toArray() as $sale) {
            $saleTypes[] = new SaleTypeBalanceDto($sale['id'], $sale['name'], @$sales[$sale['id']]);
        }

        return $saleTypes;
    }

    private function byContractsWithPayment(DateRangeDto $dateRangeDto): array
    {
        $contracts = $this->byContracts($dateRangeDto);
        $contractsWithPayment = [];
        /** @var ContractBalanceDto $contract */
        foreach ($contracts as $contract) {
            if (! $contract->sum->equal(new Money(0))) {
                $contractsWithPayment[] = $contract;
            }
        }

        return $contractsWithPayment;
    }

    private function byContracts(DateRangeDto $dateRangeDto): array
    {
        $query = Order::byServiceCenter($this->serviceCenter);
        $query = $query->whereBetween('created_at', $dateRangeDto->getRange());

        if (isset($paymentType)) {
            $query = $query->paymentType($paymentType);
        }

        $query = $query->whereNotNull('contract_id');
        $query = $query->groupBy('contract_id');

        $contractPrices = [];

        if ($this->typeAggregator == 'sum') {
            $data = $query->select(DB::raw('sum(price) as price, contract_id'))->get();
        } else {
            $data = $query->select(DB::raw('count(*) as price, contract_id'))->get();
        }

        foreach ($data as $contractPrice) {
            $contractPrices[$contractPrice->contract_id] = $contractPrice->price;
        };

        $contracts = [];
        foreach (Contract::active()->orderBy('number')->get(['id', 'number', 'contractor'])->toArray() as $contract) {
            $contracts[] = new ContractBalanceDto($contract['id'], $contract['number'], $contract['contractor'], @$contractPrices[$contract['id']]);
        }

        return $contracts;
    }

    private function paymentPoints(DateRangeDto $dateRangeDto, $paymentType, $point): PointBalanceDto
    {
        $pointBalance = new PointBalanceDto();

        if (is_null($point) || $point == self::POINT_CAR_WASH) {
            $pointBalance = $pointBalance->add($this->carWash($dateRangeDto, $paymentType));
        }

        if (is_null($point) || $point == self::POINT_CAFE) {
            $pointBalance = $pointBalance->add($this->cafe($dateRangeDto, $paymentType));
        }

        return $pointBalance;
    }

    private function cafe(DateRangeDto $dateRangeDto, $paymentType): PointBalanceDto
    {
        $point = new PointBalanceDto();

        if(is_null($paymentType) || $paymentType == Sale::PAYMENT_CASH) {
            $point->cash($this->toMoney($this->paymentCafe($dateRangeDto, Sale::PAYMENT_CASH)));
        }
        if(is_null($paymentType) || $paymentType == Sale::PAYMENT_CARD) {
            $point->card($this->toMoney($this->paymentCafe($dateRangeDto, Sale::PAYMENT_CARD)));
        }

        return $point;
    }

    private function carWash(DateRangeDto $dateRangeDto, $paymentType): PointBalanceDto
    {
        $point = new PointBalanceDto();

        if (is_null($paymentType) || $paymentType == Order::PAYMENT_CONTRACT) {
            $point->contract($this->toMoney($this->paymentCarWash($dateRangeDto, Order::PAYMENT_CONTRACT)));
        }
        if(is_null($paymentType) || $paymentType == Order::PAYMENT_CASH) {
            $point->cash($this->toMoney($this->paymentCarWash($dateRangeDto, Order::PAYMENT_CASH)));
        }
        if(is_null($paymentType) || $paymentType == Order::PAYMENT_CARD) {
            $point->card($this->toMoney($this->paymentCarWash($dateRangeDto, Order::PAYMENT_CARD)));
        }

        return $point;
    }

    private function paymentCarWash(DateRangeDto $dateRangeDto, $paymentType): MoneyInterface
    {
        $query = Order::byServiceCenter($this->serviceCenter);

        if (isset($paymentType)) {
            $query = $query->paymentType($paymentType);
        }
        $query = $query->whereBetween('created_at', $dateRangeDto->getRange());

        return $this->toMoney($this->typeAggregator == 'sum' ? $query->sum('price') : $query->count());
    }

    private function paymentCafe(DateRangeDto $dateRangeDto, $paymentType): MoneyInterface
    {
        $query = Sale::byServiceCenter($this->serviceCenter);

        if (isset($paymentType)) {
            $query = $query->paymentType($paymentType);
        }
        $query = $query->whereBetween('created_at', $dateRangeDto->getRange());

        return $this->toMoney($this->typeAggregator == 'sum' ? $query->sum('sum') : $query->count());
    }

    private function toMoney($sum): MoneyInterface
    {
        if (!isset($sum)) {
            $sum = '0';
        }

        return new Money($sum);
    }
}