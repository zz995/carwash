<?php

namespace App\Services;

use Illuminate\Support\Collection;

class ViewEmployeesSep
{
    private $our;
    private $foreign;

    public function __construct(Collection $our, Collection $foreign)
    {
        $this->our = $our;
        $this->foreign = $foreign;
    }

    public function getOur(): Collection
    {
        return $this->our;
    }

    public function getForeign(): Collection
    {
        return $this->foreign;
    }

    public function isOurEmpty()
    {
        return $this->our->isEmpty();
    }

    public function isForeignEmpty()
    {
        return $this->foreign->isEmpty();
    }

    public function isEmpty()
    {
        return $this->isOurEmpty() && $this->isForeignEmpty();
    }
}