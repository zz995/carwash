<?php

namespace App\Services;

use App\Entity\User\Fine\PersonalFine;
use App\Entity\User\User;
use App\Http\Requests\Washer\CreateFineRequest;
use App\Http\Requests\Washer\DisableFineRequest;
use App\ObjectValue\Money;
use Illuminate\Support\Facades\DB;

class PersonalFineService
{
    public function create(User $user, User $creator, CreateFineRequest $request)
    {
        DB::transaction(function () use($user, $creator, $request) {
            $sum = new Money($request->get('value'));
            $reason = $request->get('reason');

            /** @var User $user */
            $user = User::where('id', '=', $user->id)->lockForUpdate()->first();

            $personalFine = new PersonalFine();
            $personalFine->value = $sum;
            $personalFine->serviceCenter()->associate($creator->serviceCenter);
            $personalFine->user()->associate($user);
            $personalFine->save();

            $personalFine->saveToHistory($reason, $creator);

            $user->reduceBalanceByPersonalFine($sum, $personalFine);
        });
    }

    public function disable(PersonalFine $fine, User $initiator, DisableFineRequest $request)
    {
        DB::transaction(function () use($fine, $initiator, $request) {
             $fine->disable($request->get('reason'), $initiator);
        });
    }
}