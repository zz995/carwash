<?php

namespace App\Services;

use App\Entity\User\Accrual;
use App\Entity\User\User;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use App\Services\Dto\DateRangeDto;
use Illuminate\Pagination\LengthAwarePaginator;

class SalaryBreakdownService
{
    public function getAccruals(User $user, ?DateRangeDto $dataRange): LengthAwarePaginator
    {
        $accruals = Accrual::byEmployee($user, $dataRange)->paginate(20);
        //$accruals->load(['initiator']);

        return $accruals;
    }

    public function getAccrualsSum(User $user, ?DateRangeDto $dataRange): MoneyInterface
    {
        $sum = Accrual::byEmployee($user, $dataRange)->sum('value') ?: '0';
        $sum = new Money($sum);
        return $sum;
    }
}