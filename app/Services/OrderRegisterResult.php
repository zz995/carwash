<?php

namespace App\Services;

use App\Entity\Car;
use App\Entity\Client;

class OrderRegisterResult
{
    public $car;
    public $client;

    public function __construct(Car $car, Client $client)
    {
        $this->car = $car;
        $this->client = $client;
    }
}