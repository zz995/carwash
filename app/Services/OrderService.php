<?php

namespace App\Services;

use App\Entity\Car;
use App\Entity\CarMark;
use App\Entity\CarModel;
use App\Entity\CarWash\Type;
use App\Entity\CarWash\Work;
use App\Entity\Contract;
use App\Entity\Client;
use App\Entity\DiscountCard\DiscountCard;;

use App\Entity\Order\BasketItem;
use App\Entity\Order\Order;
use App\Entity\ServiceCenter;
use App\Entity\Team;
use App\Entity\TemporaryTeamUser;
use App\Entity\User\Fine\PaymentRefusalFineSize;
use App\Entity\User\Fine\TeamFine;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use App\Services\Dto\CarDto;
use App\Services\Dto\ClientDto;
use App\Services\Dto\PaymentDto;
use App\Services\Exception\NeedCommentForOtherOrderPrice;
use App\Services\Exception\NotExistDiscountCardNumber;
use App\Services\Exception\NotFoundEmployees;
use App\Services\SalaryCalculator\AdminCalculator;
use App\Services\SalaryCalculator\WasherCalculator;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

class OrderService
{
    private $teamService;

    public function __construct(TeamService $teamService)
    {

        $this->teamService = $teamService;
    }

    public function carWashWorks(Car $car)
    {
        $carWashWorks = Type::with('works')->orderBy('id')->get();
        foreach ($carWashWorks as $type) {
            /** @var Work $work */
            foreach ($type->works as $work) {
                $work->defaultCategory($car->carModel->category);
            }
        }

        return $carWashWorks;
    }

    private function findOrRegisterCar(CarDto $carDto): Car
    {
        /** @var CarModel $carModal */
        $carModal = CarModel::findOrFail($carDto->carModel);

        /** @var CarMark $carMark */
        $carMark = CarMark::findOrFail($carDto->carMark);

        /** @var null|Car $car */
        $car = Car::where('number', '=', $carDto->number)->first();
        if (is_null($car)) {
            $car = new Car();
            $car->number = $carDto->number;
        }
        $car->carModel()->associate($carModal);
        $car->carMark()->associate($carMark);

        $car->save();

        return $car;
    }

    public function findOrRegisterClient(ClientDto $clientDto): Client
    {
        /** @var null|Client $client */
        $client = null;

        $client = Client::where('phone', '=', $clientDto->phone)->first();
        if (isset($client)) {
            $client->name = $clientDto->name;
            $client->save();
            return $client;
        }

        $newClient = new Client();
        $newClient->phone = $clientDto->phone;
        $newClient->name = $clientDto->name;
        $newClient->save();

        return $newClient;
    }

    public function register(CarDto $carDto, ClientDto $clientDto): OrderRegisterResult
    {
        return DB::transaction(function () use ($carDto, $clientDto) {
            $car = $this->findOrRegisterCar($carDto);
            $client = $this->findOrRegisterClient($clientDto);
            $client->cars()->syncWithoutDetaching($car);
            $client->save();

            return new OrderRegisterResult($car, $client);
        });
    }

    public function appointTeam(Order $order, int $teamId, User $user)
    {
        $team = Team::findOrFail($teamId);
        DB::transaction(function () use ($team, $order, $user) {

            /** @var Team $team */
            $employeIds = $team->employees()->pluck('id')->toArray();
            if (empty($employeIds)) {
                throw new NotFoundEmployees();
            }

            $teamName = $team->name;
            $order->team_name = $teamName;

            if ($team->isTemporary()) {
                $order->team_type = Team::TYPE_TEMPORARY;
            }

            $order->save();

            $order->changeStatus(Order::STATUS_EXECUTE, $user);

            $order->employees()->sync($employeIds);
            $order->save();

            TemporaryTeamUser::whereIn('user_id', $employeIds)->update(['order_id' => $order->id]);
        });
    }

    public function reassignTeam(Order $order, int $teamId, User $user)
    {
        $team = Team::findOrFail($teamId);
        DB::transaction(function () use ($team, $order, $user) {
            /** @var Team $team */
            $employeIds = $team->employees()->pluck('id')->toArray();
            if (empty($employeIds)) {
                throw new NotFoundEmployees();
            }

            $teamName = $team->name;
            $order->team_name = $teamName;

            if ($team->isTemporary()) {
                $order->team_type = Team::TYPE_TEMPORARY;
            } else {
                $order->team_type = Team::TYPE_CONSTANT;
            }

            $order->save();

            $order->employees()->sync($employeIds);
            $order->save();

            TemporaryTeamUser::whereIn('user_id', $employeIds)->update(['order_id' => $order->id]);
        });
    }

    public function create(Client $client, Car $car, array $prices, ?DiscountCard $discountCard, ?array $counts, $numberKey, int $teamId, ?array $workIds, User $admin, $comment, ?ServiceCenter $serviceCenter): Order
    {

        $team = null;
        if ($teamId != 0) {
            $team = Team::findOrFail($teamId);
        }

        return DB::transaction(function () use ($client, $car, $prices, $discountCard, $counts, $workIds, $numberKey, $team, $admin, $comment, $serviceCenter) {

            if (isset($team)) {
                /** @var Team $team */
                $employeIds = $team->employees()->pluck('id')->toArray();
                if (empty($employeIds)) {
                    throw new NotFoundEmployees();
                }

                $teamName = $team->name;
            } else {
                $employeIds = [];
                $teamName = trans('team.withoutTeam');
            }

            /** @var Order $order */
            $order = Order::make([
                'team_name' => $teamName,
                'number_key' => $numberKey,
                'admin_comment' => $comment,
                'discount_size' => isset($discountCard) ? $discountCard->getDisc() : 0
            ]);

            if (isset($team) && $team->isTemporary()) {
                $order->team_type = Team::TYPE_TEMPORARY;
            }

            $order->car()->associate($car);
            $order->client()->associate($client);
            $order->admin()->associate($admin);
            $order->serviceCenter()->associate($serviceCenter);

            if (isset($discountCard)) {
                $order->discountCard()->associate($discountCard);
            }

            $order->save();

            $order->employees()->sync($employeIds);
            $order->save();

            $order->changeStatus(!isset($team) ? Order::STATUS_QUEUE : Order::STATUS_EXECUTE, $admin);

            $carModelCategory = $car->carModel->category;
            $this->setWorks($order, $workIds, $prices, $counts, $carModelCategory);

            if (isset($team)) {
                TemporaryTeamUser::whereIn('user_id', $employeIds)->update(['order_id' => $order->id]);
            }

            return $order;
        });
    }

    public function changeWorks(Order $order, ?array $workIds, array $prices, ?array $counts)
    {
        DB::transaction(function () use ($order, $prices, $counts, $workIds) {

            $order->basketItems()->delete();
            $carModelCategory = $order->car->carModel->category;
            $this->setWorks($order, $workIds, $prices, $counts, $carModelCategory);
        });
    }

    public function execute(Order $order, User $admin)
    {
        DB::transaction(function () use ($order, $admin) {
            $order->changeStatus(Order::STATUS_EXECUTED, $admin);

            $this->teamService->deleteEmployeesTemporaryTeam($order->employees);
        });
    }

    public function cancel(Order $order, User $superAdmin): Order
    {
        DB::transaction(function () use ($order, $superAdmin) {
            $order->cancel($superAdmin);
            $this->teamService->deleteEmployeesTemporaryTeam($order->employees);
        });

        return $order;
    }

    public function refusal(Order $order, User $cashier, string $comment): Order
    {
        DB::transaction(function () use ($order, $cashier, $comment) {
            $order->refusal($comment, $cashier);

            /** @var User $admin */
            $admin = $order->admin()->lockForUpdate()->first();
            $washers = $order->employees()->lockForUpdate()->get();

            $fineSize = PaymentRefusalFineSize::getInstance();
            $sum = $fineSize->totalSum($washers->count());
            $teamFine = TeamFine::register($order, $washers->concat([$admin]), $sum, $cashier, $comment, TeamFine::TYPE_REFUSAL);

            $admin->reduceBalanceByTeamFine(
                $fineSize->admin_fine,
                $teamFine
            );

            /** @var User $washer */
            foreach ($washers as $washer) {
                $washer->reduceBalanceByTeamFine(
                    $fineSize->washer_fine,
                    $teamFine
                );
            }

            $this->teamService->deleteEmployeesTemporaryTeam($order->employees);
        });

        return $order;
    }

    public function repaid(Order $order, User $cashier, PaymentDto $paymentDto): Order
    {
        DB::transaction(function () use($order, $cashier, $paymentDto) {
            $contract = Contract::where('number', '=', $paymentDto->numberContract)->first();
            $order->payment($paymentDto, $cashier, $contract);

            $paymentRation = $order->getPaymentRation();

            /** @var User $admin */
            $admin = $order->admin()->lockForUpdate()->first();
            $admin->resetReplenishBalanceByOrder(
                $this->getAdminSalary($order, $paymentRation),
                $order
            );

            $employees = $order->employees()->lockForUpdate()->get();
            $employeSalary = $this->getWasherSalary($order, $paymentRation, $employees->count());

            /** @var User $employee */
            foreach ($employees as $employee) {
                $employee->resetReplenishBalanceByOrder(
                    $employeSalary,
                    $order
                );
            }
        });

        return $order;
    }

    public function paid(Order $order, User $cashier, PaymentDto $paymentDto, ?string $discountNumber): Order
    {
        DB::transaction(function () use($order, $cashier, $paymentDto, $discountNumber) {

            $contract = Contract::where('number', '=', $paymentDto->numberContract)->first();
            $order->payment($paymentDto, $cashier, $contract);

            if (!$order->price()->equal($paymentDto->sum) && is_null($paymentDto->comment)) {
                throw new NeedCommentForOtherOrderPrice();
            }

            if ($order->hasDiscountCard() && !$order->isContractPayment() && !$order->isApplyingDiscountFromContract()) {
                $order->discountCard->accrual($order->price);
            }

            if ($order->hasDiscountCard() && !($order->discountCard->hasNumber() || $order->isContractPayment())) {
                if (isset($discountNumber)) {
                    $discountCard = $order->discountCard;
                    $discountCard->fillNumber($discountNumber);
                    $discountCard->issued()->associate($cashier);
                    $discountCard->issued_at = Carbon::now();
                    $discountCard->save();
                } else {
                    throw new NotExistDiscountCardNumber();
                }
            }

            $paymentRation = $order->getPaymentRation();

            /** @var User $admin */
            $admin = $order->admin()->lockForUpdate()->first();
            $admin->replenishBalanceByOrder(
                $this->getAdminSalary($order, $paymentRation),
                $order
            );

            $employees = $order->employees()->lockForUpdate()->get();
            $employeSalary = $this->getWasherSalary($order, $paymentRation, $employees->count());

            /** @var User $employee */
            foreach ($employees as $employee) {
                $employee->replenishBalanceByOrder(
                    $employeSalary,
                    $order
                );
            }

            $this->teamService->deleteEmployeesTemporaryTeam($order->employees);
        });

        return $order;
    }

    private function getAdminSalary(Order $order, string $paymentRation): MoneyInterface
    {
        $adminPercent = Role::adminSalary();
        return (new AdminCalculator($order->getDisc(), $order->basketItems, $adminPercent))
            ->getSalary()
            ->multiply($paymentRation);
    }

    private function getWasherSalary(Order $order, string $paymentRation, int $employeesCount): MoneyInterface
    {
        return (new WasherCalculator($order->getDisc(), $order->basketItems, Role::washerSalary(), $employeesCount))
            ->getSalary()
            ->multiply($paymentRation);
    }

    private function setWorks(Order $order, array $workIds, array $prices, ?array $counts, $carModelCategory)
    {
        $works = Work::whereIn('id', $workIds)->get();

        /** @var Work $work */
        foreach ($works as $work) {
            $work->defaultCategory($carModelCategory);
            $count = 1;
            if ($work->resolutionSeveral() && isset($counts[$work->id])) {
                $count = $counts[$work->id];
            }

            $order->addBasketItem($work->id, $count, new Money($prices[$work->id]), $work->getDefaultPrice(), $work->getDefaultCost(), $work->getDefaultSalary());
        }

        if (!$this->orderHasStandardPrice($order)) {
            $order->services_standard_price = false;
            $order->save();
        }
    }

    private function orderHasStandardPrice(Order $order)
    {
        /** @var BasketItem $basketItem */
        foreach ($order->basketItems as $basketItem) {
            if (!$basketItem->price->equal($basketItem->default_price)) {
                return false;
            }
        }

        return true;
    }
}