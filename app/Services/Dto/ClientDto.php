<?php

namespace App\Services\Dto;

class ClientDto
{
    public $id;
    public $name;
    public $phone;

    public function __construct(?string $id, string $name, string $phone)
    {
        $this->id = $id;
        $this->name = $name;
        $this->phone = preg_replace('/\D/', '', $phone);
    }
}