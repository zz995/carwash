<?php

namespace App\Services\Dto;

class DiscountCardDto
{
    public $number;
    public $type;
    public $disc;
    public $comment;

    public function __construct(?string $number, ?string $comment, $type = null, $disc = null)
    {
        if (isset($number)) {
            $this->number = mb_strtoupper($number);
        }

        $this->type = intval($type);
        $this->disc = intval($disc);
        $this->comment = $comment;
    }
}