<?php

namespace App\Services\Dto;

use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;

class ReportDto
{
    public $cafe;
    public $contracts;
    public $carwash;
    public $common;
    public $sum;

    public function __construct(array $cafe, array $contracts, PointBalanceDto $carwash, MoneyInterface $common, MoneyInterface $sum)
    {
        $this->cafe = $cafe;
        $this->contracts = $contracts;
        $this->carwash = $carwash;
        $this->common = $common;
        $this->sum = $sum;
    }
}