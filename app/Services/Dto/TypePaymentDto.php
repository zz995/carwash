<?php

namespace App\Services\Dto;

class TypePaymentDto
{
    public $id;
    public $name;
    public $balance;

    public function __construct($id, $name, PointBalanceDto $balance)
    {
        $this->id = $id;
        $this->name = $name;
        $this->balance = $balance;
    }
}