<?php

namespace App\Services\Dto;

use App\ObjectValue\MoneyInterface;

class AccrualDataDto
{
    private $personalFine;
    private $teamFine;
    private $salary;
    private $orderCount;
    private $receipt;

    public function __construct(int $orderCount, MoneyInterface $receipt, MoneyInterface $personalFine, MoneyInterface $teamFine, MoneyInterface $salary)
    {
        $this->orderCount = $orderCount;
        $this->personalFine = $personalFine;
        $this->teamFine = $teamFine;
        $this->salary = $salary;
        $this->receipt = $receipt;
    }

    public function getOrderCount(): int
    {
        return $this->orderCount;
    }

    public function getReceipt(): MoneyInterface
    {
        return $this->receipt;
    }

    public function getFine(): MoneyInterface
    {
        return $this->personalFine->add($this->teamFine);
    }

    public function getSalary(): MoneyInterface
    {
        return $this->salary;
    }

    public function getResult(): MoneyInterface
    {
        return $this->receipt->sub($this->salary->sub($this->getFine()));
    }
}