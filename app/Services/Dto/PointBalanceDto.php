<?php

namespace App\Services\Dto;

use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;

class PointBalanceDto
{
    private $cash;
    private $contract;
    private $card;

    public function __construct()
    {
    }

    public function getCash(): ?MoneyInterface
    {
        return $this->cash;
    }

    public function getContract(): ?MoneyInterface
    {
        return $this->contract;
    }

    public function getCard(): ?MoneyInterface
    {
        return $this->card;
    }

    public function getCommon(): MoneyInterface
    {
        $common = new Money(0);
        if ($this->hasCard()) {
            $common = $common->add($this->getCard());
        }
        if ($this->hasCash()) {
            $common = $common->add($this->getCash());
        }
        if ($this->hasContract()) {
            $common = $common->add($this->getContract());
        }
        return $common;
    }


    public function hasCash()
    {
        return isset($this->cash);
    }

    public function hasContract()
    {
        return isset($this->contract);
    }

    public function hasCard()
    {
        return isset($this->card);
    }

    public function cash(?MoneyInterface $value)
    {
        $this->cash = $value;
    }

    public function contract(?MoneyInterface $value)
    {
        $this->contract = $value;
    }

    public function card(?MoneyInterface $value)
    {
        $this->card = $value;
    }

    public function add(PointBalanceDto $point): self
    {
        $new = new self;
        $new->cash(self::addTwoBalance($point->cash, $this->cash));
        $new->contract(self::addTwoBalance($point->contract, $this->contract));
        $new->card(self::addTwoBalance($point->card, $this->card));

        return $new;
    }

    static private function addTwoBalance(?MoneyInterface $a, ?MoneyInterface $b)
    {
        $result = null;

        if (isset($a) && isset($b)) {
            $result = $a->add($b);
        } elseif (isset($a)) {
            $result = $a;
        } elseif (isset($b)) {
            $result = $b;
        }

        return $result;
    }
}