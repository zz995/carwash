<?php

namespace App\Services\Dto;

use Carbon\Carbon;

class DateRangeStartLastMonthDto extends DateRangeDto
{

    public function __construct(?string $range)
    {
        parent::__construct($range);

        $min = Carbon::now()->subMonth()->startOfMonth();
        if ($this->getFrom()->lt($min)) {
            $this->from = $min;
        }

        if (!$this->getTo()->gt($this->getFrom())) {
            $this->to = Carbon::now();
        }
    }
}