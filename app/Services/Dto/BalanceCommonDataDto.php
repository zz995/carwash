<?php

namespace App\Services\Dto;

use App\ObjectValue\MoneyInterface;

class BalanceCommonDataDto
{
    public $cafeSaleTypes;
    public $contracts;
    public $pointBalance;
    public $report;
    public $countReport;

    private function __construct()
    {
    }

    public function isBase()
    {
        return isset($this->pointBalance);
    }

    public function isReport()
    {
        return isset($this->report);
    }

    public function isContract()
    {
        return isset($this->contracts);
    }

    public function isCafeSaleType()
    {
        return isset($this->cafeSaleTypes);
    }


    static public function base(PointBalanceDto $pointBalance): BalanceCommonDataDto
    {
        $self = new self;
        $self->pointBalance = $pointBalance;
        return $self;
    }

    static public function byReport($report, $countReport): BalanceCommonDataDto
    {
        $self = new self;
        $self->report = $report;
        $self->countReport = $countReport;
        return $self;
    }

    static public function byContracts($contracts): BalanceCommonDataDto
    {
        $self = new self;
        $self->contracts = $contracts;

        return $self;
    }

    static public function byCafeSaleTypes($cafeSaleTypes): BalanceCommonDataDto
    {
        $self = new self;
        $self->cafeSaleTypes = $cafeSaleTypes;

        return $self;
    }
}