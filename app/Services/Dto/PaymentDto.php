<?php

namespace App\Services\Dto;

use App\ObjectValue\Money;

class PaymentDto
{
    public $type;
    public $numberContract;
    public $comment;
    /** @var Money $sum */
    public $sum;

    public function __construct(string $type, ?string $numberContract, ?string $comment, string $sum)
    {
        $this->type = intval($type);
        $this->numberContract = $numberContract;
        $this->comment = $comment;
        $this->sum = new Money($sum);
    }
}