<?php

namespace App\Services\Dto;

use Carbon\Carbon;

class DateRangeDto
{
    protected $sep = ' - ';

    protected $from;
    protected $to;

    public function __construct(?string $range)
    {
        if (isset($range)) {
            $from = @explode($this->sep, $range)[0];
            $to = @explode($this->sep, $range)[1];
        } else {
            $from = null;
            $to = null;
        }

        if(is_null($from)) {
            $from = Carbon::today();
        } else {
            $from = new Carbon($from);
        }

        if (is_null($to)) {
            $to = $from->copy();
        } else {
            $to = new Carbon($to);
        }

        $this->from = $from;
        $this->to = $to;
    }

    public function getFrom(): Carbon
    {
        return $this->from->copy();
    }

    public function getTo(): Carbon
    {
        return $this->to->copy()->addDay(1)->subSecond(1);
    }

    public function getRange(): array
    {
        return [$this->getFrom()->toDateString(), $this->getTo()->toDateTimeString()];
    }

    public function toString(): string
    {
        return "{$this->getFrom()->toDateString()}{$this->sep}{$this->getTo()->toDateTimeString()}";
    }

    static public function startMonth(): DateRangeDto
    {
        $date = new self(null);
        $date->from = Carbon::today()->startOfMonth();
        $date->to = Carbon::now();

        return $date;
    }

    static public function lastMonth(): DateRangeDto
    {
        $date = new self(null);
        $date->from = Carbon::today()->subMonth();
        $date->to = Carbon::now();

        return $date;
    }

    static public function today(): DateRangeDto
    {
        $date = new self(null);
        $date->from = Carbon::today();
        $date->to = Carbon::now();

        return $date;
    }
}