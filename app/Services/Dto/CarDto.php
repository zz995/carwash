<?php

namespace App\Services\Dto;

class CarDto
{
    public $number;
    public $carModel;
    public $carMark;
    public $id;

    public function __construct(?string $id, string $number, string $carMark, string $carModel)
    {
        $this->id = $id;
        $this->number = mb_strtoupper($number);
        $this->carModel = $carModel;
        $this->carMark = $carMark == 0 ? null : $carMark;
    }
}