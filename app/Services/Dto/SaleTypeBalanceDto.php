<?php

namespace App\Services\Dto;

use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;

class SaleTypeBalanceDto
{
    public $id;
    public $name;
    public $sum;

    public function __construct($id, $name, $sum)
    {
        $this->id = $id;
        $this->name = $name;
        $this->sum = new Money(is_null($sum) ? '0' : $sum);
    }
}