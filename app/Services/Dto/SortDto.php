<?php

namespace App\Services\Dto;

class SortDto
{
    private const SEPARATION = '_';
    private $field;
    private $direction;

    public function __construct(?string $sort, ?string $default = null, array $allowField = [])
    {

        @list($field, $direction) = $this->reconstruct($sort);
        @list($defaultField, $defaultDirection) = $this->reconstruct($default);

        if (! in_array($field, $allowField)) {
            $field = $defaultField;
            $direction = $defaultDirection;
        }

        if (! in_array($direction, ['asc', 'desc'])) {
            $direction = $this->getDefaultDirection();
        }

        $this->direction = (string) $direction;
        $this->field = (string) $field;
    }

    public function getField(): string
    {
        return $this->field;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }

    public function getNextValue(string $field): string
    {
        $direction = $this->getDefaultDirection();
        if ($this->equal($field)) {
            $direction = $this->getNextDirection();
        }

        return implode(self::SEPARATION, [$field, $direction]);
    }

    public function equal(string $field, ?string $direction = null)
    {
        if ($field != $this->getField()) {
            return false;
        }

        if (isset($direction) && $direction != $this->direction) {
            return false;
        }

        return true;
    }

    private function getDefaultDirection(): string
    {
        return 'desc';
    }

    private function getNextDirection(): string
    {
        if ($this->direction == 'asc') {
            return 'desc';
        }

        return 'asc';
    }

    private function reconstruct(?string $sort): array
    {
        if (is_null($sort)) {
            return [];
        }

        $explodeSort = explode(self::SEPARATION, $sort);
        $field = implode(self::SEPARATION, array_slice($explodeSort, 0, count($explodeSort) - 1));
        $direction = implode(self::SEPARATION, array_slice($explodeSort, -1));

        return [$field, $direction];
    }
}