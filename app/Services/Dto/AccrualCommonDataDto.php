<?php

namespace App\Services\Dto;

class AccrualCommonDataDto
{
    public $today;
    public $currentMonth;
    public $lastMonth;

    public function __construct(AccrualDataDto $today, AccrualDataDto $currentMonth, AccrualDataDto $lastMonth)
    {
        $this->today = $today;
        $this->currentMonth = $currentMonth;
        $this->lastMonth = $lastMonth;
    }
}