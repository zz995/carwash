<?php

namespace App\Services\Dto;

use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;

class ContractBalanceDto
{
    public $id;
    public $number;
    public $contractor;
    public $sum;

    public function __construct($id, $number, $contractor, $sum)
    {
        $this->id = $id;
        $this->number = $number;
        $this->contractor = $contractor;
        $this->sum = new Money(is_null($sum) ? '0' : $sum);
    }
}