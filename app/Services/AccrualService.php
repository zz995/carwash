<?php

namespace App\Services;

use App\Entity\Order\BasketItem;
use App\Entity\Order\Order;
use App\Entity\ServiceCenter;
use App\Entity\User\Accrual;
use App\Entity\User\Fine\PersonalFine;
use App\Entity\User\Fine\TeamFine;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Http\Requests\Washer\CreateFineRequest;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use App\Services\Dto\AccrualCommonDataDto;
use App\Services\Dto\AccrualDataDto;
use App\Services\Dto\DateRangeDto;
use App\Services\ViewEmployeeAccrual;
use App\Services\ViewEmployeeAccrualCollection;
use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

class AccrualService
{
    private const DATE_TODAY = 'today';
    private const DATE_CURRENT_MONTH = 'currentMonth';
    private const DATE_LAST_MONTH = 'lastMonth';

    private $serviceCenter;

    public function __construct(ServiceCenter $serviceCenter)
    {
        $this->serviceCenter = $serviceCenter;
    }

    public function employeesSalary(DateRangeDto $dateRange, User $admin): ViewEmployeeAccrualCollection
    {
        $employees = User::byServiceCenter($this->serviceCenter)
            ->withoutDeleted()
            ->where(function (Builder $query) use ($admin) {
                $query->where('role_id', Role::WASHER)
                    ->orWhere('id', '=', $admin->id);
            })
            ->paginate(20);


        $accruals = Accrual::whereBetween('timestamp', $dateRange->getRange())
                        ->whereIn('user_id', array_pluck($employees, 'id'))
                        ->selectRaw('user_id, initiator_type, sum(value) as sum')
                        ->groupBy('user_id', 'initiator_type')
                        ->get();

        $accrualCollection = new ViewEmployeeAccrualCollection($employees);
        foreach ($accruals as $accrual) {
            $accrualCollection->add(
                $accrual->user_id,
                new Money($accrual->sum),
                $accrual->initiator_type
            );
        }

        return $accrualCollection;
    }

    public function common(): AccrualCommonDataDto
    {
        return new AccrualCommonDataDto(
            new AccrualDataDto(
                Order::byServiceCenter($this->serviceCenter)->today()->count(),
                $this->receipt(self::DATE_TODAY),
                $this->personalFine(self::DATE_TODAY),
                $this->teamFine(self::DATE_TODAY),
                $this->salary(self::DATE_TODAY)
            ),
            new AccrualDataDto(
                Order::byServiceCenter($this->serviceCenter)->currentMonth()->count(),
                $this->receipt(self::DATE_CURRENT_MONTH),
                $this->personalFine(self::DATE_CURRENT_MONTH),
                $this->teamFine(self::DATE_CURRENT_MONTH),
                $this->salary(self::DATE_CURRENT_MONTH)
            ),
            new AccrualDataDto(
                Order::byServiceCenter($this->serviceCenter)->lastMonth()->count(),
                $this->receipt(self::DATE_LAST_MONTH),
                $this->personalFine(self::DATE_LAST_MONTH),
                $this->teamFine(self::DATE_LAST_MONTH),
                $this->salary(self::DATE_LAST_MONTH)
            )
        );
    }

    private function receipt($date): MoneyInterface
    {
        return $this->toMoney(Order::with(BasketItem::class)->byServiceCenter($this->serviceCenter)->{$date}()->sum('price'));
    }

    private function personalFine($date): MoneyInterface
    {
        return $this->toMoney(PersonalFine::byServiceCenter($this->serviceCenter)->{$date}()->sum('value'));
    }

    private function teamFine($date): MoneyInterface
    {
        return $this->toMoney(TeamFine::byServiceCenter($this->serviceCenter)->{$date}()->sum('value'));
    }

    private function salary($date): MoneyInterface
    {
        return $this->toMoney(Accrual::salaryByServiceCenter($this->serviceCenter)->{$date}()->sum('value'));
    }

    private function toMoney($sum): MoneyInterface
    {
        if (!isset($sum)) {
            $sum = '0';
        }

        return new Money($sum);
    }
}