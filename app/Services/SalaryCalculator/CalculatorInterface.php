<?php

namespace App\Services\SalaryCalculator;

use App\ObjectValue\MoneyInterface;

interface CalculatorInterface
{
    public function getSalary(): MoneyInterface;
}