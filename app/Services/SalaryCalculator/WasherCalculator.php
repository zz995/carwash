<?php

namespace App\Services\SalaryCalculator;

use App\Entity\Order\BasketItem;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;

class WasherCalculator implements CalculatorInterface
{
    private $basketItems;
    private $percent;
    private $countEmployees;
    private $discountSize;

    public function __construct($discountSize, $basketItems, int $percent, int $countEmployees)
    {
        $this->basketItems = $basketItems;
        $this->percent = $percent;
        $this->countEmployees = $countEmployees;
        $this->discountSize = $discountSize;
    }

    public function getSalary(): MoneyInterface
    {
        $salary = new Money(0);

        /** @var BasketItem $item */
        foreach ($this->basketItems as $item) {
            if ($item->hasFixedSalary()) {
                $salary = $salary->add($item->salary->division($this->countEmployees)->multiply($item->count));
                continue;
            }

            $price = $item->price;
            $price = $price->percent(100 - $this->discountSize);
            if ($item->hasCost()) {
                $price = $price->sub($item->cost);
            }

            $price = $price->multiply($item->count);

            $salary = $salary->add($price->percent($this->percent)->division($this->countEmployees));
        }

        return $salary;
    }
}