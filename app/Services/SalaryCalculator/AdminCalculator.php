<?php

namespace App\Services\SalaryCalculator;

use App\Entity\Order\BasketItem;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;

class AdminCalculator implements CalculatorInterface
{
    private $basketItems;
    private $percent;
    private $discountSize;

    public function __construct($discountSize, $basketItems, int $percent)
    {
        $this->basketItems = $basketItems;
        $this->percent = $percent;
        $this->discountSize = $discountSize;
    }

    public function getSalary(): MoneyInterface
    {
        $salary = new Money(0);

        /** @var BasketItem $item */
        foreach ($this->basketItems as $item) {
            /** @var MoneyInterface $price */
            $price = $item->price;
            $price = $price->multiply($item->count);
            $salary = $salary->add($price->percent($this->percent));
        }

        $salary = $salary->percent(100 - $this->discountSize);
        return $salary;
    }
}