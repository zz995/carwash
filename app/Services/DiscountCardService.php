<?php

namespace App\Services;

use App\Entity\Car;
use App\Entity\CarMark;
use App\Entity\CarModel;
use App\Entity\CarWash\Type;
use App\Entity\CarWash\Work;
use App\Entity\Contract;
use App\Entity\Client;
use App\Entity\DiscountCard\DiscountCard;;
use App\Entity\Order\Order;
use App\Entity\Team;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use App\Services\Dto\CarDto;
use App\Services\Dto\ClientDto;
use App\Services\Dto\DiscountCardDto;
use App\Services\Dto\PaymentDto;
use App\Services\Exception\ClientAlreadyHasDiscountCard;
use App\Services\Exception\NotFoundEmployees;
use App\Services\SalaryCalculator\AdminCalculator;
use App\Services\SalaryCalculator\WasherCalculator;
use Carbon\Carbon;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class DiscountCardService
{
    public function register(DiscountCardDto $discountCardDto, ClientDto $clientDto, ?array $newCarNumbers, ?array $markAndModel, User $user): DiscountCard
    {
        return DB::transaction(function () use($discountCardDto, $clientDto, $newCarNumbers, $markAndModel, $user) {

            $orderService = new OrderService(new TeamService);
            /** @var Client $client */
            $client = $orderService->findOrRegisterClient($clientDto);
            if ($client->hasDiscount()) {
                throw new ClientAlreadyHasDiscountCard();
            }

            /** @var DiscountCard $discountCard */
            $discountCard = DiscountCard::make([
                'number' => $discountCardDto->number,
                'type' => $discountCardDto->type,
                'disc' => $discountCardDto->disc,
                'comment' => $discountCardDto->comment
            ]);

            $discountCard->creator()->associate($user);

            if (isset($discountCardDto->number)) {
                $discountCard->issued()->associate($user);
                $discountCard->issued_at = Carbon::now();
            }

            $discountCard->client()->associate($client);
            $discountCard->save();

            $this->newCar($discountCard, $client, $newCarNumbers, $markAndModel);

            return $discountCard;
        });
    }

    public function edit(DiscountCard $discountCard, DiscountCardDto $discountCardDto, ClientDto $clientDto, ?array $newCarNumbers, ?array $savedCarIds, ?array $markAndModel, User $user)
    {
        return DB::transaction(function () use($discountCard, $discountCardDto, $clientDto, $newCarNumbers, $savedCarIds, $markAndModel, $user) {

            $orderService = new OrderService(new TeamService);
            /** @var Client $client */
            $client = $orderService->findOrRegisterClient($clientDto);
            if ($client->hasDiscount() && !$client->equal($discountCard->client)) {
                throw new ClientAlreadyHasDiscountCard();
            }

            $discountCard->client()->associate($client);

            if (! isset($discountCard->number) && isset($discountCardDto->number)) {
                $discountCard->issued()->associate($user);
                $discountCard->issued_at = Carbon::now();
            }

            $discountCard->number = $discountCardDto->number;
            $discountCard->comment = $discountCardDto->comment;
            $discountCard->save();

            if (isset($savedCarIds)) {
                Car::where('discount_card_id', '=', $discountCard->id)
                    ->whereNotIn('id', $savedCarIds)
                    ->update(['discount_card_id' => null]);
            }

            $this->newCar($discountCard, $client, $newCarNumbers, $markAndModel);

            return $discountCard;
        });
    }

    private function newCar(DiscountCard $discountCard, Client $client, ?array $newCarNumbers, ?array $markAndModel)
    {
        if (is_null($newCarNumbers)) {
            return;
        }

        if (is_null($markAndModel)) {
            $markAndModel = [];
        }

        foreach ($newCarNumbers as $carNumber) {

            /** @var Car $car */
            $car = Car::firstOrNew(['number' => $carNumber]);
            $car->discountCard()->associate($discountCard);


            if (isset($markAndModel[$carNumber])) {
                $carMark = $this->getMark($markAndModel[$carNumber]);
                $carModel = $this->getModel($carMark, $markAndModel[$carNumber]);

                if (isset($carModel) && isset($carMark)) {
                    $car->carModel()->associate($carModel);
                    $car->carMark()->associate($carMark);
                }
            }

            $car->save();

            $car->clients()->sync([$client->id], false);
        }
    }

    private function getModel(?CarMark $markObject, $markAndModel): ?CarModel
    {
        if (is_null($markObject)) {
            return null;
        }

        @list(
            'model' => $model,
            'model_category' => $modelCategory,
            'new_model' => $isNewModel
        ) = $markAndModel;

        if (is_null($model)) {
            return null;
        }

        if (+$isNewModel) {
            $modelObject = CarModel::create([
                'name' => $model,
                'category' => CarModel::findOrFail($modelCategory)->category,
                'car_mark_id' => $markObject->id
            ]);
        } else {
            $modelObject = CarModel::findOrFail($model);
        }

        return $modelObject;
    }

    private function getMark($markAndModel): ?CarMark
    {
        @list(
            'mark' => $mark,
            'new_mark' => $isNewMark
        ) = $markAndModel;

        if (is_null($mark)) {
            return null;
        }

        if (+$isNewMark) {
            $markObject = CarMark::create(['name' => $mark]);
        } else {
            $markObject = CarMark::findOrFail($mark);
        }

        return $markObject;
    }
}