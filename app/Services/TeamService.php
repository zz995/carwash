<?php

namespace App\Services;

use App\Entity\ServiceCenter;
use App\Entity\Team;
use App\Entity\TemporaryTeamUser;
use App\Entity\User\Role;
use App\Entity\User\User;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;

class TeamService
{
    public function getAllFreeEmployees(User $user, $base = null): ViewEmployeesSep
    {
        if (is_null($base)) {
            $base = collect();
        }

        $employees = User::with(['teams.serviceCenter'])->ourOrder($user)->orderBy('name')
            ->withoutDeleted()
            ->where('role_id', Role::WASHER)
            ->whereNotIn('id', function (Builder $q) {
                $q
                    ->select('team-user.user_id')
                    ->from('team-user')
                    ->join('teams', 'team-user.team_id', '=', 'teams.id')
                    ->where('teams.type', '<>', Team::TYPE_TEMPORARY);
            })
            ->get();

        return $this->separateEmployees($base->merge($employees), $user);
    }

    public function getEmployees(User $user): ViewEmployeesSep
    {
        $employees = User::with(['teams.serviceCenter', 'serviceCenter'])
            ->withoutDeleted()
            ->orderBy('surname')
            ->orderBy('name')
            ->where('role_id', Role::WASHER)
            ->get();

        return $this->separateEmployees($employees, $user);
    }

    private function separateEmployees($employees, $user): ViewEmployeesSep
    {
        $our = [];
        $foreign = [];

        /** @var User $employee */
        foreach ($employees as $employee) {
            $serviceCenter = $employee->getCurrentServiceCenter();
            if ($serviceCenter->id == $user->service_center_id) {
                $our[] = $employee;
            } else {
                $foreign[] = $employee;
            }
        }

        return new ViewEmployeesSep(collect($our), collect($foreign));
    }

    public function create(string $name, array $employees, ServiceCenter $serviceCenter): Team
    {
        return DB::transaction(function () use($name, $employees, $serviceCenter) {
            /** @var Team $team */
            $team = Team::make(['name' => $name]);
            $team->serviceCenter()->associate($serviceCenter);
            $team->save();

            $this->detachEmployeesTeam($this->findEmployees($employees));

            $team->employees()->sync($employees);
            $team->activeEmployees();

            return $team;
        });
    }

    public function createForce(string $name, array $employees, ServiceCenter $serviceCenter): Team
    {
        return DB::transaction(function () use($name, $employees, $serviceCenter) {
            /** @var User $user */
            foreach(User::whereIn('id', $employees)->get() as $user) {
                /** @var TemporaryTeamUser $temporaryTeamUser */
                $temporaryTeamUser = $user->temporaryTeamUser;

                if (is_null($temporaryTeamUser)) {
                    $temporaryTeamUser = TemporaryTeamUser::make();
                    $temporaryTeamUser->employee()->associate($user);

                    /** @var Team $team */
                    $team = $user->teams()->first();
                    if (isset($team)) {
                        $temporaryTeamUser->team()->associate($team);
                    }

                    $temporaryTeamUser->save();
                }

                $user->teams()->detach();
            }

            /** @var Team $team */
            $team = Team::make(['name' => $name]);
            $team->type = Team::TYPE_TEMPORARY;
            $team->serviceCenter()->associate($serviceCenter);
            $team->save();

            $team->employees()->sync($employees);

            $team->activeEmployees();

            /** @var User $user */
            foreach(User::whereIn('id', $employees)->get() as $user) {
                /** @var TemporaryTeamUser $temporaryTeamUser */
                $temporaryTeamUser = $user->temporaryTeamUser;
                if (is_null($temporaryTeamUser)) {
                    $temporaryTeamUser = TemporaryTeamUser::make();
                    $temporaryTeamUser->employee()->associate($user);
                    $temporaryTeamUser->team()->associate($team);
                    $temporaryTeamUser->save();
                }
            }

            return $team;
        });
    }

    public function edit(Team $team, string $name, array $employees): Team
    {
        return DB::transaction(function () use($team, $name, $employees) {
            $team->disableEmployees();
            $team->name = $name;

            $oldEmployeeIds = array_pluck($team->employees, ['id']);
            $detachEmployeeIds = array_diff($oldEmployeeIds, $employees);
            $attachEmployeeIds = array_diff($employees, $oldEmployeeIds);

            $this->detachEmployeesTeam($this->findEmployees($attachEmployeeIds));
            $this->deleteEmployeesTeam($this->findEmployees($detachEmployeeIds));

            $team->employees()->sync($employees);
            $team->save();

            $team->activeEmployees();
            return $team;
        });
    }

    public function delete(Team $team)
    {
        return DB::transaction(function () use($team) {
            if ($team->is)

            $team->disableEmployees();
            $this->deleteEmployeesTemporaryTeam($team->employees);

            $team->delete();
        });
    }

    private function findEmployees(array $employees): Collection
    {
        return User::whereIn('id', $employees)->get();
    }

    public function detachEmployeesTeam(Collection $employees)
    {
        /** @var User $employee */
        foreach($employees as $employee) {
            $this->detachEmployeeTeam($employee);
        }
    }

    public function detachEmployeeTeam(User $employee)
    {
        $temporaryTeamUser = $employee->temporaryTeamUser;
        if (isset($temporaryTeamUser)) {
            $temporaryTeamUser->delete();
        }
        $employee->teams()->detach();
    }

    public function deleteEmployeesTeam(Collection $employees)
    {
        /** @var User $employee */
        foreach($employees as $employee) {
            $this->detachTeam($employee);
            $this->deleteTemporaryTeam($employee);
        }
    }

    public function deleteEmployeesTemporaryTeam(Collection $employees)
    {
        /** @var User $employee */
        foreach($employees as $employee) {
            $this->deleteTemporaryTeam($employee);
        }
    }

    private function detachTeam(User $employee)
    {
        $employee->status = User::STATUS_DISABLED;
        $employee->save();

        $employee->teams()->detach();
    }

    private function deleteTemporaryTeam(User $employee)
    {
        /** @var TemporaryTeamUser|null $temporaryTeamUser */
        $temporaryTeamUser = $employee->temporaryTeamUser;
        if (isset($temporaryTeamUser)) {
            $employee->teams()->detach();
        }
        $this->returnEmployeeFromTemporaryTeam($employee, $temporaryTeamUser);
        $this->deleteEmptyTemporaryTeam($employee->teams);
    }

    private function returnEmployeeFromTemporaryTeam(User $employee, ?TemporaryTeamUser $temporaryTeamUser)
    {
        if (is_null($temporaryTeamUser)) {
            return;
        }

        $team = $temporaryTeamUser->team;
        if (isset($team)) {
            $team->employees()->attach($employee);
            $employee->status = User::STATUS_ACTIVE;
            $employee->save();
        }

        $temporaryTeamUser->delete();
    }

    private function deleteEmptyTemporaryTeam(Collection $teams)
    {
        /** @var Team $team */
        foreach ($teams as $team) {
            if ($team->isTemporary() && $team->employees->isEmpty()) {
                $team->delete();
            }
        }
    }
}