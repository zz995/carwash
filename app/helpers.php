<?php

use App\Entity\User\User;

if (! function_exists('dataTimeFormat')) {

    function dataTimeFormat(Carbon\Carbon $date)
    {
        return $date->format("d.m.Y H:i:s");
    }
}

if (! function_exists('getCurrentDay')) {

    function getCurrentDay()
    {
        return dateToPickerFormat(Carbon\Carbon::now());
    }
}

if (! function_exists('getCurrentMonth')) {

    function getCurrentMonth()
    {
        return dateToPickerFormat(Carbon\Carbon::now()->startOfMonth());
    }
}

if (! function_exists('phonesFormat')) {
    function phonesFormat(?array $phones, $noBr = false)
    {
        if (empty($phones)) {
            return null;
        }

        $phones = array_map('phoneFormat', $phones);
        $phones = array_map('e', $phones);

        if ($noBr) {
            $phones = array_map(function ($phone) {
                return "<nobr>$phone</nobr>";
            }, $phones);
        }

        return implode(', ', $phones);
    }
}

if (! function_exists('phoneFormat')) {

    function phoneFormat(?string $phone)
    {
        if (is_null($phone)) {
            return $phone;
        }

        return preg_replace('/(\d)(\d{3})(\d{3})(\d{2})(\d{2})/iu', '$1 ($2) $3-$4-$5', $phone);
    }
}

if (! function_exists('numberFormat')) {

    function numberFormat(\App\ObjectValue\MoneyInterface $money): int
    {
        return (int)(string)$money->round();
    }
}

if (! function_exists('moneyFormat')) {

    function moneyFormat(\App\ObjectValue\MoneyInterface $money): string
    {
        return (string)$money->round();
    }
}

if (! function_exists('moneyInputFormat')) {

    function moneyInputFormat(?\App\ObjectValue\MoneyInterface $money): ?string
    {
        if (is_null($money)) {
            return null;
        }

        return (string)$money->round();
    }
}

if (! function_exists('dateToPickerFormat')) {

    function dateToPickerFormat(?Carbon\Carbon $date)
    {
        if (is_null($date)) {
            return '';
        }

        return $date->format("d.m.Y");
    }
}

if (! function_exists('dateToInputFormat')) {

    function dateToInputFormat(?Carbon\Carbon $date)
    {
        if (is_null($date)) {
            return '';
        }

        return $date->format("Y-m-d");
    }
}

if (! function_exists('listEmployees')) {

    function listEmployees($employees, ?string $link = null)
    {
        $fios = [];
        /** @var App\Entity\User\User $employee */
        foreach ($employees as $employee) {
            $name = e(trim($employee->getFio()->getFullName()));
            if (isset($link)) {
                $href = e(route($link, $employee));
                $name = "<a href='$href'>$name</a>";
            }
            $fios[] = $name;
        }

        return implode(', ', $fios);
    }
}

if (! function_exists('fontColorStyle')) {

    function fontColorStyle(float $visible): string
    {
        $rgba = fontColorRgba($visible);
        return "style=\"color: $rgba !important;\"";
    }
}

if (! function_exists('fontColorRgba')) {

    function fontColorRgba(float $visible): string
    {
        $hex = "#ffffff";
        /** @var User $user */
        $user = Auth::user();
        if (isset($user->serviceCenter) && isset($user->serviceCenter->font_color)) {
            $hex = $user->serviceCenter->font_color;
        }

        $r = $hex[1] . $hex[2];
        $g = $hex[3] . $hex[4];
        $b = $hex[5] . $hex[6];

        return "rgba(" . implode(', ', array_merge(
            array_map("hexdec", [$r, $g, $b]),
            [$visible]
        )) . ")";
    }
}

if (! function_exists('backgroundColor')) {

    function backgroundColor(): string
    {
        /** @var User $user */
        $user = Auth::user();
        if (isset($user->serviceCenter) && isset($user->serviceCenter->background_color)) {
            return $user->serviceCenter->background_color;
        } else {
            return "#343a40";
        }
    }
}

if (! function_exists('declension')) {
    function declension($digit, $expr, $onlyword) {
        $res = '';
        $digit = '' . $digit;
        $expr_list = explode(' ', $expr);
        $reg = '/[^0-9]+/';
        $i = preg_replace($reg, '', $digit);

        if($onlyword) {
            $digit = '';
        }

        if($i >= 5 && $i <= 20) {
            $res = $digit . ' ' . $expr_list[2];
        } else {
            $i %= 10;
            if($i == 1) {
                $res = $digit . ' ' . $expr_list[0];
            } else if($i >= 2 && $i <= 4) {
                $res = $digit . ' ' . $expr_list[1];
            } else {
                $res = $digit . ' ' . $expr_list[2];
            }
        }

        return trim($res);
    }
}

if (! function_exists('dataUpdate')) {
    function dataUpdate(Carbon\Carbon $date) {
        $time = $date->timestamp;
        $timeNow = time();
        if ($timeNow < $time) {
            $timeNow = $time;
        }

        $diffTime = $timeNow  - $time;

        return dataUpdateFromTime($diffTime);
    }
}

if (! function_exists('dataUpdateFromTime')) {
    function dataUpdateFromTime(int $diffTime) {
        $days = trans('time.days');
        $hours = trans('time.hours');
        $mins = trans('time.mins');

        $day = ~~($diffTime / (24 * 60 * 60));
        $hour = ~~($diffTime / (60 * 60) % 24);
        $min = ~~($diffTime / 60 % 60);

        $dateStr = [];

        if (!($day || $hour || $min)) {
            $min = 1;
        }

        if ($day) {
            $dateStr[] = declension($day, $days, false) . ' ';
        }

        if ($hour) {
            $dateStr[] = declension($hour, $hours, false) . ' ';
        }

        if ($min) {
            $dateStr[] = declension($min, $mins, false) . ' ';
        }

        return implode(' ', $dateStr);
    }
}

if (! function_exists('timePassed')) {
    function timePassed(?int $second): string
    {
        if (! isset($second)) {
            return '';
        }

        return dataUpdateFromTime($second);
    }
}

if (! function_exists('orderPaymentType')) {
    function orderPaymentType($type): string
    {
        $map = [
            \App\Entity\Order\Order::PAYMENT_CASH => trans('order.paymentType.cash'),
            \App\Entity\Order\Order::PAYMENT_CARD => trans('order.paymentType.card'),
            \App\Entity\Order\Order::PAYMENT_CONTRACT => trans('order.paymentType.contract'),
        ];

        $title = @$map[$type] ?: '';
        //$title = mb_strtolower($title);

        return $title;
    }
}