<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int $id
 * @property string $number
 */
class CarAutocomplete extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'number' => $this->number,
            'carModel' => [
                'id' => $this->carModel ? $this->carModel->id : null
            ],
            'carMark' => [
                'id' =>  $this->carModel ? ($this->carMark ? $this->carMark->id : 0) : null
            ],
            'clients' => ClientAutocomplete::collection($this->clients),
            'discountCard' => isset($this->discountCard) ? new DiscountCard($this->discountCard) : null
        ];
    }
}
