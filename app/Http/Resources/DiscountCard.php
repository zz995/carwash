<?php

namespace App\Http\Resources;

use App\ObjectValue\MoneyInterface;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int $id
 * @property string|null $number
 * @property string|null $phone
 * @property string $name
 * @property integer $type
 * @property integer $disc
 * @property MoneyInterface $current_step_accrual
 */
class DiscountCard extends JsonResource
{
    public function toArray($request)
    {
        return [
            'number' => $this->number,
            'disc' => $this->disc,
        ];
    }
}
