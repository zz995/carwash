<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int $id
 * @property string $number
 * @property string $contractor
 * @property Carbon $from
 * @property Carbon $to
 */
class ContractAutocomplete extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'number' => $this->number,
            'contractor' => $this->contractor
        ];
    }
}
