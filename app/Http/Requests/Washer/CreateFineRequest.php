<?php

namespace App\Http\Requests\Washer;

use App\Entity\CarModel;
use App\Rules\Money;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateFineRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'value' => ['required', new Money],
            'reason' => 'required|string',
        ];
    }
}
