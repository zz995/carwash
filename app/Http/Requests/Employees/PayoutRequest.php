<?php

namespace App\Http\Requests\Employees;

use App\Rules\Money;
use Illuminate\Foundation\Http\FormRequest;

class PayoutRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'sum' => ['required', new Money],
        ];
    }
}
