<?php

namespace App\Http\Requests\Employees;

use App\Entity\User\Role;
use App\Entity\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class ChangeSalaryRequest extends CreateRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return ['salary' => 'required|integer'];
    }
}
