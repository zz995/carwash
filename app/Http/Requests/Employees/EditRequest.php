<?php

namespace App\Http\Requests\Employees;

use App\Entity\User\Role;
use App\Entity\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'surname' => 'nullable|string|max:255',
            'patronymic' => 'nullable|string|max:255',
            'email' => ['nullable', 'string', 'email', 'max:255', Rule::unique('users')->ignore($this->employe->email, 'email')],
            'phones.*' => ['required', 'string', 'max:20'],
            'role' => ['required', Rule::in([Role::CASHIER, Role::WASHER, Role::ADMIN])],
            'login' => ['nullable', 'string', Rule::unique('users')->ignore($this->employe->login, 'login')]
        ];
    }
}
