<?php

namespace App\Http\Requests\Employees;

use App\Entity\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChangePasswordRequest extends CreateRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return array_only(parent::rules(), ['password']);
    }
}
