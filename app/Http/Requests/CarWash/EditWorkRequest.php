<?php

namespace App\Http\Requests\CarWash;


class EditWorkRequest extends CreateWorkRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return parent::rules();
    }
}
