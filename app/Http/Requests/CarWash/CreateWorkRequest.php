<?php

namespace App\Http\Requests\CarWash;

use App\Entity\CarWash\Type;
use App\Rules\Money;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateWorkRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return array_merge(
            [
                'number' => ['required', 'string', 'max:255', Rule::unique('car_wash_works')->ignore($this->number, 'number')],
                'name' => 'required|string|max:255',
                'type' => ['required', Rule::exists((new Type())->getTable(), 'id')],
                'several' => 'nullable|integer',
            ],
            array_merge(...array_map(function ($num) {
                return [
                    "category{$num}_price" => ['required', new Money],
                    "category{$num}_cost" => [new Money],
                    "category{$num}_salary" => [new Money],
                ];
            }, [1, 2, 3, 4]))
        );
    }
}
