<?php

namespace App\Http\Requests\ServiceCenter;


class EditServiceCenterRequest extends CreateServiceCenterRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return parent::rules();
    }
}
