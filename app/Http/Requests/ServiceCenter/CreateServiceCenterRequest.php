<?php

namespace App\Http\Requests\ServiceCenter;

use App\Entity\User\Role;
use App\Entity\User\User;
use App\Rules\Rgb;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateServiceCenterRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'background_color' => ['required', new Rgb],
            'font_color' => ['required', new Rgb],
            'desc' => 'nullable',
            'hidden' => 'nullable'
        ];
    }
}
