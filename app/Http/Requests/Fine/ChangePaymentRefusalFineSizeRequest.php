<?php

namespace App\Http\Requests\Fine;

use App\Rules\Money;
use Illuminate\Foundation\Http\FormRequest;

class ChangePaymentRefusalFineSizeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'admin_fine' => ['required', new Money()],
            'washer_fine' => ['required', new Money()],
        ];
    }
}
