<?php

namespace App\Http\Requests\Cafe;

use App\Entity\Cafe\Sale;
use App\Entity\Cafe\SaleType;
use App\Rules\Money;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateSaleRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'sum' => ['required', new Money],
            'sale-type' => ['required', Rule::exists((new SaleType())->getTable(), 'id')],
            'payment-type' => ['required', Rule::in([Sale::PAYMENT_CARD, Sale::PAYMENT_CASH])]
        ];
    }
}
