<?php

namespace App\Http\Requests\Contract;

use App\Entity\CarWash\Type;
use App\Http\Requests\Employees\CreateRequest;
use App\Rules\Money;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditContractRequest extends CreateContractRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return array_only(parent::rules(), ['number', 'contractor', 'from', 'to', 'disc']);
    }

    protected function uniqueExceptNumber(): ?string
    {
        return $this->contract->number;
    }
}
