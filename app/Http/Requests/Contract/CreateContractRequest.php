<?php

namespace App\Http\Requests\Contract;

use App\Entity\CarWash\Type;
use App\Rules\Money;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateContractRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'number' => ['required', 'string', 'max:255', Rule::unique('contracts')->ignore($this->uniqueExcept(), 'number')],
            'contractor' => 'required|string|max:512',
            'from' => 'required|date',
            'to' => 'required|date',
            'disc' => 'nullable|integer|between:0,20',
        ];
    }

    protected function uniqueExceptNumber(): ?string
    {
        return null;
    }

    private function uniqueExcept(): string
    {
        $number = $this->uniqueExceptNumber();
        if (is_null($number)) {
            return '';
        }

        return $number;
    }
}
