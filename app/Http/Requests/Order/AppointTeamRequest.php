<?php

namespace App\Http\Requests\Order;

use App\Entity\CarWash\Work;
use App\Entity\Team;
use App\Rules\Money;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AppointTeamRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'team' => ['nullable', Rule::exists((new Team())->getTable(), 'id')],
        ];
    }
}
