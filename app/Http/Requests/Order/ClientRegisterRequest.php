<?php

namespace App\Http\Requests\Employees;

use App\Entity\CarModel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClientRegisterRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:20',
            'number' => 'required|string',
            //'model' => ['required', Rule::exists((new CarModel())->getTable(), 'id')],
        ];
    }
}
