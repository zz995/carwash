<?php

namespace App\Http\Requests\Employees;

use App\Entity\CarWash\Work;
use App\Entity\Team;
use App\Rules\Money;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderingRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'team' => ['nullable'],
            'works' => 'required',
            'counts.*' => ['required', 'integer', 'min:1', 'max:100'],
            'works.*' => ['required', Rule::exists((new Work())->getTable(), 'id')],
            'prices' => 'required',
            'prices.*' => ['required', new Money],
            'key' => ['required', 'integer']
        ];
    }
}
