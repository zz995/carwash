<?php

namespace App\Http\Requests\DiscountCard\CalculationStep;

use App\Rules\Money;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'disc' => ['required', 'numeric', 'min:0', 'max:100', Rule::unique('discount_card_calculation_steps')->ignore($this->step->id)],
            'sum' => ['required', new Money()]
        ];
    }
}
