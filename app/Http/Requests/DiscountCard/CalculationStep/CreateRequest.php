<?php

namespace App\Http\Requests\DiscountCard\CalculationStep;

use App\Rules\Money;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'disc' => 'required|numeric|min:0|max:100|unique:discount_card_calculation_steps,disc',
            'sum' => ['required', new Money()]
        ];
    }
}
