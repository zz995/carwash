<?php

namespace App\Http\Requests\DiscountCard;

use App\Entity\Car;
use App\Entity\DiscountCard\DiscountCard;;
use App\Rules\CarNotHaveDisc;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'number' => ['nullable', 'string', 'max:255', Rule::unique('discount_cards')->ignore($this->number, 'number')],
            'phone' => 'required|string|max:20',
            'name' => 'required|max:255',
            'comment' => 'required',
            'new_cars.*' => ['required', 'string', new CarNotHaveDisc],
            'saved_cars.*' => [Rule::exists((new Car)->getTable(), 'id')]
        ];
    }
}
