<?php

namespace App\Http\Requests\DiscountCard;

use App\Entity\Car;
use App\Entity\DiscountCard\DiscountCard;;
use App\Rules\CarNotHaveDisc;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditDiscRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type' => ['required', Rule::in([DiscountCard::TYPE_CUMULATIVE, DiscountCard::TYPE_FIXED])],
            'disc' => 'required|integer|between:0,20'
        ];
    }
}
