<?php

namespace App\Http\Requests\DiscountCard;

use App\Entity\Car;
use App\Entity\DiscountCard\DiscountCard;;
use App\Rules\CarNotHaveDisc;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'number' => 'nullable|string|max:255|unique:discount_cards,number',
            'phone' => 'required|string|max:20',
            'name' => 'required|max:255',
            'comment' => 'required',
            'type' => ['required', Rule::in([DiscountCard::TYPE_CUMULATIVE, DiscountCard::TYPE_FIXED])],
            'disc' => 'required|integer|between:0,20',
            'new_cars.*' => ['required', 'string', new CarNotHaveDisc],
        ];
    }
}
