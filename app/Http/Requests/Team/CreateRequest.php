<?php

namespace App\Http\Requests\Team;

use App\Entity\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'employees' => 'required',
            'employees.*' => ['required', Rule::exists((new User())->getTable(), 'id')],
        ];
    }
}
