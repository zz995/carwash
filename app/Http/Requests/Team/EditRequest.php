<?php

namespace App\Http\Requests\Team;

use App\Entity\User\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EditRequest extends CreateRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return parent::rules();
    }
}
