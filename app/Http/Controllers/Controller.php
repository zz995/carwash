<?php

namespace App\Http\Controllers;

use App\Entity\User\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function fieldValidationError(Request $request, array $errors)
    {
        $validator = Validator::make($request->all(), []);
        foreach ($errors as $field => $msg) {
            $validator->errors()->add($field, $msg);
        }

        throw new \Illuminate\Validation\ValidationException($validator);
    }

    protected function getAuthUser(): ?User
    {
        /** @var User $user */
        $user = Auth::user();
        return $user;
    }

}
