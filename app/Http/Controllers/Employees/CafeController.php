<?php

namespace App\Http\Controllers\Employees;

use App\Entity\Cafe\Sale;
use App\Entity\Cafe\SaleType;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Cafe\CreateSaleRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CafeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-sale-cafe']);
    }

    public function createSaleForm(Request $request)
    {
        $pageTitle = trans('cafe.page.createSale.header');

        $saleTypes = SaleType::active()->orderBy('name')->orderBy('id')->get();
        $sale = new Sale();

        return view('employees.cafe.createSale', compact('pageTitle', 'saleTypes', 'sale'));
    }

    public function createSale(CreateSaleRequest $request)
    {
        /** @var User $user */
        $user = Auth::user();
        Sale::register(
            $request->get('sum'),
            $request->get('sale-type'),
            $request->get('payment-type'),
            $user,
            $user->serviceCenter
        );

        return redirect()
            ->to(route('employees.cafe.createSale'))
            ->with('success', trans('cafe.message.successCreateSale'));
    }
}
