<?php

namespace App\Http\Controllers\Employees\Order;

use App\Entity\CarModel;
use App\Entity\Client;
use App\Entity\Order\Order;
use App\Entity\Team;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Order\AppointTeamRequest;
use App\Services\Exception\NotFoundEmployees;
use App\Services\OrderService;
use App\Services\TeamService;
use App\Services\ViewOrder\ViewOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppointTeamController extends Controller
{
    private $service;
    private $teamService;

    public function __construct(OrderService $service, TeamService $teamService)
    {
        $this->middleware(['auth', 'can:manage-overdrive-order']);

        $this->service = $service;
        $this->teamService = $teamService;
    }

    public function appointTeamForm(Order $order)
    {
        if (!$order->isQueue()) {
            return back()
                ->with('error', trans('order.message.appointTeam'));
        }

        /** @var User $user */
        $user = Auth::user();
        $pageTitle = trans('order.page.appointTeam.header');

        $teams = Team::fullTeam($user->serviceCenter);
        $employees = $this->teamService->getEmployees($user);

        $viewOrder = new ViewOrder($order);

        return view('employees.order.appointTeam', compact('pageTitle', 'teams', 'employees', 'viewOrder'));

    }

    public function appointTeam(Order $order, AppointTeamRequest $request)
    {
        if (!$order->isQueue()) {
            return back()
                ->with('error', trans('order.message.appointTeam'));
        }

        /** @var User $user */
        $user = Auth::user();

        $teamId = $request->get('team');
        $employeeIds = $request->get('employees');

        if (! empty($employeeIds)) {
            $teamId = $this->teamService->createForce(trans('team.defaultName', ['num' => $order->car->number]), $employeeIds, $user->serviceCenter)->id;
        }

        if (is_null($teamId)) {
            $this->fieldValidationError($request, ['team' => trans('order.message.needSelectTeamOrEmployees')]);
        }

        try {
            $this->service->appointTeam($order, $teamId, $user);
        } catch (NotFoundEmployees $err) {
            $this->fieldValidationError($request, ['team' => trans('order.message.notFoundEmployees')]);
        }

        return redirect()
            ->to($user->isOverdrive() ? route('employees.order.overdrive.list') : route('employees.order.list'))
            ->with('success', trans('order.message.successAppointTeam'));
    }

    public function reassignTeamForm(Order $order)
    {

        if (!$order->isExecute()) {
            return back()
                ->with('error', trans('order.message.reassignTeam'));
        }

        /** @var User $user */
        $user = Auth::user();
        $pageTitle = trans('order.page.reassignTeam.header');

        $teams = Team::fullTeam($user->serviceCenter);
        $employees = $this->teamService->getEmployees($user);

        $viewOrder = new ViewOrder($order);
        $viewOrder->employeeIdsToNull();

        return view('employees.order.reassignTeam', compact('pageTitle', 'teams', 'employees', 'viewOrder'));
    }

    public function reassignTeam(Order $order, AppointTeamRequest $request)
    {
        if (!$order->isExecute()) {
            return back()
                ->with('error', trans('order.message.reassignTeam'));
        }

        /** @var User $user */
        $user = Auth::user();

        $teamId = $request->get('team');
        $employeeIds = $request->get('employees');

        $this->teamService->deleteEmployeesTemporaryTeam($order->employees);

        if (! empty($employeeIds)) {
            $teamId = $this->teamService->createForce(trans('team.defaultName', ['num' => $order->car->number]), $employeeIds, $user->serviceCenter)->id;
        }

        if (is_null($teamId)) {
            $this->fieldValidationError($request, ['team' => trans('order.message.needSelectTeamOrEmployees')]);
        }

        try {
            $this->service->reassignTeam($order, $teamId, $user);
        } catch (NotFoundEmployees $err) {
            $this->fieldValidationError($request, ['team' => trans('order.message.notFoundEmployees')]);
        }

        return redirect()
            ->to($user->isOverdrive() ? route('employees.order.overdrive.list') : route('employees.order.list'))
            ->with('success', trans('order.message.successAppointTeam'));
    }
}
