<?php

namespace App\Http\Controllers\Employees\Order;

use App\Entity\Car;
use App\Entity\CarMark;
use App\Entity\CarModel;
use App\Entity\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests\Employees\ClientRegisterRequest;
use App\Services\Dto\CarDto;
use App\Services\Dto\ClientDto;
use App\Services\ViewOrder\MemorizedOrderData;
use App\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request as RequestFacade;

class ClientRegisterController extends Controller
{
    private $service;
    private $memorizedOrderData;

    public function __construct(OrderService $service, MemorizedOrderData $memorizedOrderData)
    {
        $this->service = $service;
        $this->memorizedOrderData = $memorizedOrderData;
        $this->middleware(['auth', 'can:manage-order']);
    }

    public function registerForm(Request $request)
    {
        $this->memorizedOrderData->delete();

        $pageTitle = trans('order.page.create.header');
        $referer = $request->headers->get('referer');
        $route = Route::getRoutes()->match(RequestFacade::create($referer, 'GET'));
        $routeName = $route->getName();

        $car = $client = null;
        if ($routeName === 'employees.order.ordering') {
            $carId = $route->parameter('car');
            $car = Car::findOrFail($carId);

            $clientId = $route->parameter('client');
            $client = Client::findOrFail($clientId);
        }

        $carMarks = CarMark::all();
        $markId = old('mark', isset($car) ? $car->carMark->id : null);
        $carModels = isset($markId) ? CarModel::forMark($markId)->get() : [];
        if (isset($markId)) {
            $carModels = $carModels->concat([CarModel::make(['id' => 0, 'name' => trans('order.createModel')])]);
        }


        return view('employees.order.clientRegister', compact('pageTitle', 'carMarks', 'carModels', 'car', 'client'));
    }

    public function register(ClientRegisterRequest $request)
    {
        if ($request->get('car_id') === null) {
            $car = Car::where('number', '=', $request->get('number'))->first();
            if (isset($car)) {
                $this->fieldValidationError($request, ['number' => trans('car.message.exist')]);
            }
        }

        if ($request->get('new-mark') !== null) {
            $mark = CarMark::create(['name' => $request->get('new-mark')]);
            $markId = $mark->id;
        } else {
            $markId = $request->get('mark');
        }

        if ($request->get('new-model') !== null) {
            $model = CarModel::create([
                'name' => $request->get('new-model'),
                'category' => CarModel::findOrFail($request->get('model'))->category,
                'car_mark_id' => $markId
            ]);
            $modelId = $model->id;
        } else {
            $modelId = $request->get('model');
        }

        $clientDto = new ClientDto(
            $request->get('client_id'),
            $request->get('name'),
            $request->get('phone')
        );

        $carDto = new CarDto(
            $request->get('car_id'),
            $request->get('number'),
            $markId,
            $modelId
        );

        $registerResult = $this->service->register($carDto, $clientDto);

        return redirect()
            ->to(route('employees.order.ordering', ['car' => $registerResult->car, 'client' => $registerResult->client]));
    }
}
