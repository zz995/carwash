<?php

namespace App\Http\Controllers\Employees\Order;

use App\Entity\CarModel;
use App\Entity\Client;
use App\Entity\Order\Order;
use App\Entity\Team;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Services\ViewOrder\ViewOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OverdriveController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-overdrive-order-list']);
    }

    public function list()
    {
        /** @var User $user */
        $user = Auth::user();

        $pageTitle = trans('order.page.list.header');
        $orders = Order::with('car', 'client', 'teamFines')->our($user)->sortForAppointTeam()->actual()->get();
        return view('employees.order.overdrive.list', compact('pageTitle', 'orders'));
    }
}
