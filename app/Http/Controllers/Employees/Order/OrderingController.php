<?php

namespace App\Http\Controllers\Employees\Order;

use App\Entity\Car;
use App\Entity\CarModel;
use App\Entity\CarWash\Type;
use App\Entity\CarWash\Work;
use App\Entity\Client;
use App\Entity\DiscountCard\DiscountCard;;
use App\Entity\Order\Order;
use App\Entity\Team;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Order\EditWorksRequest;
use App\Http\Requests\Employees\OrderingRequest;
use App\ObjectValue\Money;
use App\Services\Dto\CarDto;
use App\Services\Dto\ClientDto;
use App\Services\Exception\NotFoundEmployees;
use App\Services\ViewOrder\MemorizedOrderData;
use App\Services\OrderService;
use App\Services\TeamService;
use App\Services\ViewOrder\ViewOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class OrderingController extends Controller
{
    private $service;
    private $teamService;
    private $memorizedOrderData;

    public function __construct(OrderService $service, TeamService $teamService, MemorizedOrderData $memorizedOrderData)
    {
        $this->service = $service;
        $this->teamService = $teamService;
        $this->memorizedOrderData = $memorizedOrderData;
        $this->middleware(['auth', 'can:manage-order']);
    }

    public function orderingForm(Car $car, Client $client)
    {
        /** @var User $user */
        $user = Auth::user();

        $this->memorizedOrderData->delete();
        $memorizedOrderData = $this->memorizedOrderData;

        $pageTitle = trans('order.page.ordering.header');

        $carWashWorks = $this->service->carWashWorks($car);
        $teams = Team::fullTeam($user->serviceCenter);

        $employees = $this->teamService->getEmployees($user);

        $clientsWithDiscount = $car->clients->filter(function (Client $client) {
            return $client->hasDiscount();
        });

        $discountCard = null;
        if ($car->hasDiscountCard()) {
            $discountCard = $car->discountCard;
        }

        $clientCars = $client->cars()->get();
        $clientCars->load(['carMark', 'carModel', 'clients']);

        $lastOrders = $client
            ->orders()
            ->orderByDesc('id')
            ->limit(10)
            ->get();
        $lastOrders->load(['employees', 'car', 'car.carMark', 'car.carModel', 'basketItems', 'basketItems.work']);

        return view('employees.order.ordering', compact('pageTitle', 'carWashWorks', 'employees', 'discountCard', 'teams', 'car', 'clientsWithDiscount', 'client', 'memorizedOrderData', 'lastOrders', 'clientCars'));
    }

    public function ordering(Car $car, Client $client, OrderingRequest $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $this->memorizedOrderData->delete();

        $discountCard = $car->discountCard;

        $prices = $request->get('prices');
        $counts = $request->get('counts');
        $teamId = $request->get('team');
        $employeeIds = $request->get('employees');
        $workIds = $request->get('works');
        $numberKey = $request->get('key');
        $comment = $request->get('comment');
        /** @var User $admin */
        $admin = Auth::user();

        if (! empty($employeeIds)) {
            $teamId = $this->teamService->createForce(trans('team.defaultName', ['num' => $car->number]), $employeeIds, $user->serviceCenter)->id;
        }

        try {
            $this->service->create($client, $car, $prices, $discountCard, $counts, $numberKey, $teamId, $workIds, $admin, $comment, $user->serviceCenter);
        } catch (NotFoundEmployees $err) {
            $this->fieldValidationError($request, ['team' => trans('order.message.notFoundEmployees')]);
        }

        return redirect()
            ->to(route('employees.order.list'));
    }

    public function show(Order $order)
    {
        return view('employees.order.show', compact('order'));
    }

    public function editWorksForm(Order $order)
    {
        $pageTitle = trans('order.page.editWorks.header');

        $carWashWorks = $this->service->carWashWorks($order->car);
        $viewOrder = new ViewOrder($order);

        return view('employees.order.editWorks', compact('pageTitle', 'carWashWorks', 'viewOrder', 'order'));
    }

    public function editWorks(Order $order, EditWorksRequest $request)
    {
        if (!$order->isExecute() && !$order->isQueue()) {
            return back()
                ->with('error', trans('order.message.orderPaid'));
        }

        $prices = $request->get('prices');
        $counts = $request->get('counts');
        $workIds = $request->get('works');

        $this->service->changeWorks($order, $workIds, $prices, $counts);

        return redirect()
            ->to(route('employees.order.list'))
            ->with('success', trans('order.message.successEditWorks'));
    }

    public function executed(Order $order)
    {
        if (!$order->isExecute() && $order->hasTemporaryTeam()) {
            return back()
                ->with('error', trans('order.message.canNotBeExecuted'));
        }

        $this->service->execute($order, $this->getAuthUser());

        return redirect()
            ->to(route('employees.order.show', $order))
            ->with('success', trans('order.message.successChangeStatusToExecuted'));
    }

    public function comment(Order $order, Request $request)
    {
        $order->admin_comment = $request->get('comment');
        $order->save();

        return back()->with('success', trans('order.message.successAdminComment'));
    }
}
