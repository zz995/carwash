<?php

namespace App\Http\Controllers\Employees\Order;

use App\Entity\User\Fine\TeamFine;
use App\Entity\Order\Order;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Order\CreateFineRequest;
use App\Http\Requests\Order\CrossRequest;
use App\Http\Requests\Order\DisableFineRequest;
use App\Services\TeamFineService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class FineController extends Controller
{
    private $service;

    public function __construct(TeamFineService $service)
    {
        $this->middleware(['auth']);
        $this->service = $service;
    }

    public function createForm(Order $order)
    {
        Gate::denies('manage-team-fine', $order) and abort(403);

        $pageTitle = trans('teamFine.page.create.header');
        $teamFine = new TeamFine();
        return view('employees.order.fine', compact('pageTitle', 'teamFine'));
    }

    public function create(Order $order, CreateFineRequest $request)
    {
        Gate::denies('manage-team-fine', $order) and abort(403);

        /** @var User $creator */
        $creator = Auth::user();

        $this->service->create($order, $creator, $request);

        return redirect()
            ->to(route('employees.order.show', $order))
            ->with('success', trans('teamFine.message.successFine'));
    }

    public function disableForm(TeamFine $fine)
    {
        Gate::denies('manage-disable-team-fine', $fine->order) and abort(403);
        if(! $fine->isActive()) {
            abort(400);
        }

        $pageTitle = trans('teamFine.page.disable.header');
        $fine = new TeamFine();
        return view('employees.fine.team.disable', compact('pageTitle', 'fine'));
    }

    public function disable(DisableFineRequest $request, TeamFine $fine)
    {
        Gate::denies('manage-disable-team-fine', $fine->order) and abort(403);
        if(! $fine->isActive()) {
            abort(400);
        }

        /** @var User $user */
        $user = Auth::user();
        $this->service->disable($fine, $user, $request);

        return redirect()
            ->to(route('employees.order.show', $fine->order))
            ->with('success', trans('teamFine.message.successDisabledFine'));
    }

    public function crossForm(Order $order)
    {
        Gate::denies('manage-cross-order', $order) and abort(403);

        $pageTitle = trans('order.page.cross.header');
        $fine = new TeamFine();
        return view('employees.order.cross', compact('pageTitle', 'fine'));
    }

    public function cross(CrossRequest $request, Order $order)
    {
        Gate::denies('manage-cross-order', $order) and abort(403);

        /** @var User $user */
        $user = Auth::user();
        $this->service->cross($order, $user, $request);

        return redirect()
            ->to(route('employees.order.show', $order))
            ->with('success', trans('order.message.successCross'));
    }

}
