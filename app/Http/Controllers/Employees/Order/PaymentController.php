<?php

namespace App\Http\Controllers\Employees\Order;

use App\Entity\CarModel;
use App\Entity\Contract;
use App\Entity\Client;
use App\Entity\Order\Exception\ContractNotExist;
use App\Entity\Order\Order;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Services\ContractService;
use App\Services\Dto\PaymentDto;
use App\Services\Exception\NeedCommentForOtherOrderPrice;
use App\Services\Exception\NotExistDiscountCardNumber;
use App\Services\OrderService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    private $service;
    private $contractService;

    public function __construct(OrderService $service, ContractService $contractService)
    {
        $this->service = $service;
        $this->contractService = $contractService;
        $this->middleware(['auth', 'can:manage-payment-order']);
    }

    public function checkNewOrder(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $timestamp = $request->get('timestamp');
        $date = Carbon::createFromTimestamp($timestamp);
        $newOrderCount = Order::sortForPaidOrder()
            ->our($user)
            ->where('created_at', '>', $date->toDateTimeString())
            ->count();

        return ['hasNewOrder' => !!$newOrderCount, 'count' => $newOrderCount, 'timestamp' => Carbon::now()->timestamp];
    }

    public function list(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $pageTitle = trans('order.page.list.header');

        $query = Order::with('car', 'car.carMark', 'car.carModel', 'client')
            ->sortForPaidOrder();
        if (!empty($value = $request->get('key'))) {
            $query->where('number_key', $value);
        }
        if (!empty($value = $request->get('status')) && in_array($value, [Order::STATUS_EXECUTE, Order::STATUS_PAID])) {
            if ($value == Order::STATUS_EXECUTE) {
                $query->whereIn('status', [Order::STATUS_EXECUTE, Order::STATUS_EXECUTED]);
            } elseif ($value == Order::STATUS_PAID) {
                $query->whereIn('status', [Order::STATUS_PAID, Order::STATUS_PAYMENT_REFUSAL, Order::STATUS_CANCELED]);
            } else {
                $query->where('status', $value);
            }
        } else {
            $query->whereIn('status', [Order::STATUS_EXECUTE, Order::STATUS_EXECUTED]);
        }

        $orders = $query->our($user)->actual()->get();
        $statuses = Order::getStatuses();

        return view('employees.order.payment.list', compact('pageTitle', 'orders', 'statuses'));
    }

    public function paymentForm(Order $order)
    {
        $pageTitle = trans('order.page.payment.header', ['num' => $order->id]);
        $contracts = Contract::active()->orderBy('number')->get();
        $mapDiscForContract = $this->contractService->getMapDisc($contracts);
        return view('employees.order.payment.payment', compact('pageTitle', 'order', 'contracts', 'mapDiscForContract'));
    }

    public function repayment(Order $order, Request $request)
    {
        if (!$order->isPaid()) {
            abort(400);
        }

        try {
            $this->service->repaid(
                $order,
                $this->getAuthUser(),
                new PaymentDto(
                    $request->get('type'),
                    $request->get('number'),
                    $order->comment,
                    (string) $order->price
                )
            );
        } catch (ContractNotExist $e) {
            $this->fieldValidationError($request, ['number' => trans('order.message.contractNotExist')]);
        }

        return redirect()
            ->to(route('employees.order.payment.list'));
    }

    public function payment(Order $order, Request $request)
    {
        if (!$order->isExecute() && !$order->isExecuted()) {
            abort(400);
        }

        try {
            $this->service->paid(
                $order,
                $this->getAuthUser(),
                new PaymentDto(
                    $request->get('type'),
                    intval($request->get('type')) === Order::PAYMENT_CONTRACT
                        ? $request->get('number')
                        : $request->get('contract_number_applying_discount'),
                    $request->get('comment'),
                    $request->get('sum')
                ),
                $request->get('discount_card_number')
            );
        } catch (ContractNotExist $e) {
            $this->fieldValidationError($request, ['number' => trans('order.message.contractNotExist')]);
        } catch (NotExistDiscountCardNumber $e) {
            $this->fieldValidationError($request, ['discount_card_number' => trans('order.message.discountCardNumberNotSet')]);
        } catch (NeedCommentForOtherOrderPrice $e) {
            $this->fieldValidationError($request, ['comment' => trans('order.message.needCommentForOtherOrderPrice')]);
        }

        return redirect()
            ->to(route('employees.order.payment.list'));
    }

    public function refusal(Order $order, Request $request)
    {
        if (!$order->isExecute() && !$order->isExecuted()) {
            abort(400);
        }

        $this->service->refusal($order, $this->getAuthUser(), $request->get('comment'));

        return redirect()
            ->to(route('employees.order.payment.list'));
    }

    public function comment(Order $order, Request $request)
    {
        $order->comment = $request->get('comment');
        $order->save();

        return back()->with('success', trans('order.message.successPaymentComment'));
    }
}
