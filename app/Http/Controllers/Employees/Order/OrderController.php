<?php

namespace App\Http\Controllers\Employees\Order;

use App\Entity\CarModel;
use App\Entity\Client;
use App\Entity\Order\Order;
use App\Entity\Team;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class OrderController extends Controller
{
    private $service;

    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-order']);
    }

    public function list(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $pageTitle = trans('order.page.list.header');

        $query = Order::with('car', 'car.carMark', 'car.carModel', 'client', 'teamFines');

        $status = $request->get('status', Order::STATUS_EXECUTE);
        if (in_array($status, [Order::STATUS_EXECUTE, Order::STATUS_QUEUE, Order::STATUS_PAID])) {
            if ($status == Order::STATUS_EXECUTE) {
                $query->whereIn('status', [Order::STATUS_EXECUTE, Order::STATUS_EXECUTED]);
            } elseif ($status == Order::STATUS_PAID) {
                $query->whereIn('status', [Order::STATUS_PAID, Order::STATUS_PAYMENT_REFUSAL, Order::STATUS_CANCELED]);
            } else {
                $query->where('status', $status);
            }
        }

        $query = $query
            ->our($user)
            ->sortForAppointTeam()
            ->actual();

        $orders = $query->get();

        $teamExist = !!Team::fullTeam($user->serviceCenter)->count();
        $statuses = Order::getFullStatuses();

        return view('employees.order.list', compact('pageTitle', 'orders', 'teamExist', 'statuses', 'status'));
    }
}
