<?php

namespace App\Http\Controllers\Employees\Order;

use App\Entity\Car;
use App\Entity\CarModel;
use App\Entity\Contract;
use App\Entity\Client;
use App\Entity\DiscountCard\DiscountCard;
use App\Entity\Order\Exception\ContractNotExist;
use App\Entity\Order\Order;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Sanitizes\Sanitize;
use App\Services\Dto\PaymentDto;
use App\Services\Exception\NotExistDiscountCardNumber;
use App\Services\OrderService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Query\Builder;

class ServiceHistoryController extends Controller
{
    private $sanitize;

    public function __construct(Sanitize $sanitize)
    {
        $this->middleware(['auth', 'can:manage-service-history']);
        $this->sanitize = $sanitize;
    }

    public function list(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $pageTitle = trans('order.page.serviceHistory.header');

        $query = Order::with(['contract', 'discountCard', 'client', 'car', 'car.carMark', 'car.carModel', 'basketItems', 'basketItems.work']);
        $query = $query->our($user);

        if (!empty($value = $request->get('discount_card'))) {
            $query->whereIn('discount_card_id', function (Builder $q) use ($value) {
                $q
                    ->select('id')
                    ->from((new DiscountCard())->getTable())
                    ->where('number', 'like', $value . '%')
                    ->get();
            })->where('payment_type', '<>', Order::PAYMENT_CONTRACT);
        }

        if (!empty($value = $this->sanitize->phone($request->get('phone')))) {
            $query->whereIn('client_id', function (Builder $q) use ($value) {
                $q
                    ->select('id')
                    ->from((new Client())->getTable())
                    ->where('phone', 'like',$value . '%')
                    ->get();
            });
        }

        if (!empty($value = $request->get('car_number'))) {
            $query->whereIn('car_id', function (Builder $q) use ($value) {
                $q
                    ->select('id')
                    ->from((new Car())->getTable())
                    ->where('number', 'like', mb_strtoupper($value) . '%')
                    ->get();
            });
        }

        $orders = $query->paginate(20);
        $statuses = Order::getFullStatuses();

        return view('employees.order.serviceHistory', compact('pageTitle', 'orders', 'statuses'));
    }
}
