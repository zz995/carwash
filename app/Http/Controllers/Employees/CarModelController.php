<?php

namespace App\Http\Controllers\Employees;

use App\Entity\Car;
use App\Entity\CarModel;
use App\Http\Controllers\Controller;
use App\Http\Resources\CarModel as CarModelResources;
use Illuminate\Http\Request;

class CarModelController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-order']);
    }

    public function byMark(Request $request)
    {
        $markId = intval($request->get('mark'));

        $carModels = CarModel::forMark($markId)->get();

        return CarModelResources::collection($carModels);
    }
}
