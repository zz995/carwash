<?php

namespace App\Http\Controllers\Employees;

use App\Entity\Team;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Team\CreateRequest;
use App\Http\Requests\Team\EditRequest;
use App\Services\TeamService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeamController extends Controller
{
    private $service;

    public function __construct(TeamService $service)
    {
        $this->service = $service;
        $this->middleware(['auth', 'can:manage-teams']);
    }

    public function list(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $pageTitle = trans('team.page.list.header');
        $teams = Team::orderBy('name')->constant()->our($user)->get();

        return view('employees.teams.list', compact('pageTitle', 'teams'));
    }

    public function createForm()
    {
        $pageTitle = trans('team.page.create.header');
        $team = new Team();
        /** @var User $user */
        $user = Auth::user();
        $employees = $this->service->getAllFreeEmployees($user);
        return view('employees.teams.create', compact('pageTitle', 'employees', 'team'));
    }

    public function create(CreateRequest $request)
    {
        /** @var User $user */
        $user = Auth::user();

        $this->service->create(
            $request->get('name'),
            $request->get('employees'),
            $user->serviceCenter
        );

        return redirect()
            ->to(route('employees.teams.list'))
            ->with('success', trans('team.message.successCreate'));
    }

    public function show(Team $team)
    {
        return view('employees.teams.show', compact('team'));
    }

    public function editForm(Team $team)
    {
        if ($team->isTemporary()) {
            return back()->with('error', trans('team.message.notEditTemporaryTeam'));
        }

        $pageTitle = trans('team.page.edit.header');
        /** @var User $user */
        $user = Auth::user();
        $employees = $this->service->getAllFreeEmployees($user, $team->employees);
        return view('employees.teams.edit', compact('pageTitle', 'employees', 'team'));
    }

    public function edit(Team $team, EditRequest $request)
    {
        if ($team->isTemporary()) {
            return back()->with('error', trans('team.message.notEditTemporaryTeam'));
        }

        $this->service->edit(
            $team,
            $request->get('name'),
            $request->get('employees')
        );

        return redirect()
            ->to(route('employees.teams.list'))
            ->with('success', trans('team.message.successEdit'));
    }

    public function delete(Team $team)
    {
        $this->service->delete($team);

        return redirect()
            ->to(route('employees.teams.list'))
            ->with('success', trans('team.message.successDelete'));
    }
}
