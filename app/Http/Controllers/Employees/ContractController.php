<?php

namespace App\Http\Controllers\Employees;

use App\Entity\Contract;
use App\Http\Controllers\Controller;
use App\Http\Resources\ContractAutocomplete;
use Illuminate\Http\Request;

class ContractController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-payment-order']);
    }

    public function autocomplete(Request $request)
    {
        $value = $request->get('value');
        $cars = Contract::active()
            ->where(function ($query) use ($value) {
                return $query
                    ->where('number', 'like', $value . '%')
                    ->orWhere('contractor', 'like', '%' . $value . '%');
            })
            ->limit(20)
            ->get();

        return ContractAutocomplete::collection($cars);
    }
}
