<?php

namespace App\Http\Controllers\Employees;

use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Services\BalanceService;
use App\Services\Dto\DateRangeLastMonthDto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BalanceController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-balance']);
    }

    public function common(Request $request)
    {
        $pageTitle = trans('balance.page.common.header');

        /** @var User $user */
        $user = Auth::user();
        $service = new BalanceService($user->serviceCenter);

        $dataRange = new DateRangeLastMonthDto($request->get('range'));
        $balance = $service->common(
            $dataRange,
            $request->get('payment'),
            $request->get('point'),
            count($request->only(['range', 'payment', 'point'])) == 0
        );


        return view('employees.balance.common', compact('pageTitle', 'balance', 'dataRange'));
    }
}
