<?php

namespace App\Http\Controllers\Employees;

use App\Entity\Client;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClientAutocomplete;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-order']);
    }

    public function autocomplete(Request $request)
    {
        $value = $request->get('value');
        $value = preg_replace('/\D/', '', $value);
        $clients = Client::where('phone', 'like', $value . '%')->limit(10)->get();

        return ClientAutocomplete::collection($clients);
    }
}
