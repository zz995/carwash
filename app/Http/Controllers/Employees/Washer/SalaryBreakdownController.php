<?php

namespace App\Http\Controllers\Employees\Washer;

use App\Entity\User\Accrual;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Services\Dto\DateRangeDto;
use App\Services\Dto\DateRangeStartLastMonthDto;
use App\Services\SalaryBreakdownService;
use Illuminate\Http\Request;

class SalaryBreakdownController extends Controller
{
    private $service;

    public function __construct(SalaryBreakdownService $service)
    {
        $this->middleware(['auth', 'can:manage-salary-breakdown']);
        $this->service = $service;
    }

    public function index(User $user, Request $request)
    {
        $pageTitle = trans('user.page.salaryBreakdown.header');
        !$user->isWasher() and abort(403);

        $dataRange = new DateRangeStartLastMonthDto($request->get('range'));

        $accruals = $this->service->getAccruals($user, $dataRange);
        $accrualsSum = $this->service->getAccrualsSum($user, $dataRange);

        return view('employees.washers.salaryBreakdown', compact('pageTitle', 'user', 'accruals', 'accrualsSum', 'dataRange'));
    }
}
