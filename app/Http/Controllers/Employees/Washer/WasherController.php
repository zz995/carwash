<?php

namespace App\Http\Controllers\Employees\Washer;

use App\Entity\Team;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Team\CreateRequest;
use App\Http\Requests\Team\EditRequest;
use App\Services\TeamService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WasherController extends Controller
{
    private $teamService;

    public function __construct(TeamService $teamService)
    {
        $this->middleware(['auth', 'can:manage-washers']);
        $this->teamService = $teamService;
    }

    public function list(Request $request)
    {
        $pageTitle = trans('washer.page.list.header');

        /** @var User $user */
        $user = Auth::user();
        $washers = $this->teamService->getEmployees($user);

        return view('employees.teams.washers', compact('pageTitle', 'washers'));
    }

    public function show(User $user)
    {
        $pageTitle = trans('washer.page.show.header');
        !$user->isWasher() and abort(403);

        return view('employees.washers.show', compact('user', 'pageTitle'));
    }
}
