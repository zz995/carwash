<?php

namespace App\Http\Controllers\Employees\Washer;

use App\Entity\Team;
use App\Entity\User\Fine\PersonalFine;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Team\CreateRequest;
use App\Http\Requests\Team\EditRequest;
use App\Http\Requests\Washer\CreateFineRequest;
use App\Http\Requests\Washer\DisableFineRequest;
use App\Services\PersonalFineService;
use App\Services\TeamService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class PersonalFineController extends Controller
{
    private $service;

    public function __construct(PersonalFineService $service)
    {
        $this->middleware(['auth']);
        $this->service = $service;
    }

    public function createForm(User $user)
    {
        Gate::denies('manage-personal-fine', $user) and abort(403);

        $pageTitle = trans('personalFine.page.create.header');
        $personalFine = new PersonalFine();

        return view('employees.fine.personal.create', compact('pageTitle', 'personalFine', 'user'));
    }

    public function create(CreateFineRequest $request, User $user)
    {
        Gate::denies('manage-personal-fine', $user) and abort(403);

        /** @var User $creator */
        $creator = Auth::user();

        $this->service->create($user, $creator, $request);

        return redirect()
            ->to(route('employees.washer.show', $user))
            ->with('success', trans('personalFine.message.successFine'));
    }

    public function show(PersonalFine $fine)
    {
        Gate::denies('manage-personal-fine', $fine->user) and abort(403);

        $pageTitle = trans('personalFine.page.show.header');
        return view('employees.fine.personal.show', compact('pageTitle', 'fine'));
    }

    public function disableForm(PersonalFine $fine)
    {
        Gate::denies('manage-personal-fine', $fine->user) and abort(403);
        if(! $fine->isActive()) {
            abort(400);
        }

        $pageTitle = trans('personalFine.page.disable.header');
        return view('employees.fine.personal.disable', compact('pageTitle', 'fine'));
    }

    public function disable(DisableFineRequest $request, PersonalFine $fine)
    {
        Gate::denies('manage-personal-fine', $fine->user) and abort(403);
        if(! $fine->isActive()) {
            abort(400);
        }

        /** @var User $user */
        $user = Auth::user();
        $this->service->disable($fine, $user, $request);

        return redirect()
            ->to(route('employees.washer.show', $fine->user))
            ->with('success', trans('personalFine.message.successDisabledFine'));
    }

    public function list(User $user)
    {
        Gate::denies('manage-personal-fine', $user) and abort(403);

        $pageTitle = trans('washer.page.show.header');
        $personalFines = PersonalFine::with('user', 'user.serviceCenter')->orderByDesc('id')->where('user_id', '=', $user->id)->paginate(20);
        return view('employees.washers.personalFines', compact('pageTitle', 'personalFines', 'user'));
    }
}
