<?php

namespace App\Http\Controllers\Employees\Washer;

use App\Entity\Team;
use App\Entity\User\Fine\PersonalFine;
use App\Entity\User\Fine\TeamFine;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Team\CreateRequest;
use App\Http\Requests\Team\EditRequest;
use App\Http\Requests\Washer\CreateFineRequest;
use App\Http\Requests\Washer\DisableFineRequest;
use App\Services\PersonalFineService;
use App\Services\TeamService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class TeamFineController extends Controller
{
    private $service;

    public function __construct(PersonalFineService $service)
    {
        $this->middleware(['auth']);
        $this->service = $service;
    }

    public function list(User $user)
    {
        Gate::denies('manage-personal-fine', $user) and abort(403);

        $pageTitle = trans('washer.page.show.header');
        $teamFines = $user->teamFines()->with(['users', 'users.serviceCenter'])->orderByDesc('id')->paginate(20);
        return view('employees.washers.teamFines', compact('pageTitle', 'teamFines', 'user'));
    }
}
