<?php

namespace App\Http\Controllers\Employees\Accrual;

use App\Entity\ServiceCenter;
use App\Entity\User\Fine\TeamFine;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TeamFineController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-team-fines']);
    }

    public function list()
    {
        $pageTitle = trans('personalFine.page.list.header');

        /** @var User $user */
        $user = Auth::user();
        /** @var ServiceCenter $serviceCenter */
        $serviceCenter = $user->serviceCenter;

        $teamFines = TeamFine::with('users', 'order')->byServiceCenter($serviceCenter)->orderByDesc('id')->paginate(20);

        return view('employees.accrual.teamFines', compact('pageTitle', 'teamFines'));
    }
}
