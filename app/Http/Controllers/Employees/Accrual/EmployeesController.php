<?php

namespace App\Http\Controllers\Employees\Accrual;

use App\Entity\ServiceCenter;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Services\AccrualService;
use App\Services\Dto\DateRangeDto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmployeesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-accrual']);
    }

    public function list(Request $request)
    {
        $pageTitle = trans('accrual.page.employees.header');
        $dataRange = new DateRangeDto($request->get('range'));
        $accrualCollection = $this->getService()->employeesSalary($dataRange, Auth::user());

        return view('employees.accrual.employees', compact('pageTitle', 'accrualCollection', 'dataRange'));
    }

    private function getService(): AccrualService
    {
        /** @var User $user */
        $user = Auth::user();
        /** @var ServiceCenter $serviceCenter */
        $serviceCenter = $user->serviceCenter;
        return new AccrualService($serviceCenter);
    }
}
