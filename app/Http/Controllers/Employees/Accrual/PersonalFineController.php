<?php

namespace App\Http\Controllers\Employees\Accrual;

use App\Entity\ServiceCenter;
use App\Entity\User\Fine\PersonalFine;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PersonalFineController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-personal-fines']);
    }

    public function list()
    {
        $pageTitle = trans('personalFine.page.list.header');

        /** @var User $user */
        $user = Auth::user();
        /** @var ServiceCenter $serviceCenter */
        $serviceCenter = $user->serviceCenter;
        $personalFines = PersonalFine::with(['user', 'user.serviceCenter'])->byServiceCenter($serviceCenter)->orderByDesc('id')->paginate(20);

        return view('employees.accrual.personalFines', compact('pageTitle', 'personalFines'));
    }
}
