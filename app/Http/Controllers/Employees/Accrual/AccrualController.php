<?php

namespace App\Http\Controllers\Employees\Accrual;

use App\Entity\ServiceCenter;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Services\AccrualService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccrualController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-accrual']);
    }

    public function common()
    {
        $pageTitle = trans('accrual.page.common.header');
        $commonAccrual = $this->getService()->common();

        return view('employees.accrual.common', compact('pageTitle', 'commonAccrual'));
    }

    private function getService(): AccrualService
    {
        /** @var User $user */
        $user = Auth::user();
        /** @var ServiceCenter $serviceCenter */
        $serviceCenter = $user->serviceCenter;
        return new AccrualService($serviceCenter);
    }
}
