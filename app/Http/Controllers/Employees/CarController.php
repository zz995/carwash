<?php

namespace App\Http\Controllers\Employees;

use App\Entity\Car;
use App\Http\Controllers\Controller;
use App\Http\Resources\CarAutocomplete;
use Illuminate\Http\Request;

class CarController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-order']);
    }

    public function autocomplete(Request $request)
    {
        $value = $request->get('value');
        $cars = Car::with('clients', 'carModel', 'carModel.mark')
            ->where('number', 'like', $value . '%')
            ->limit(20)
            ->get();

        return CarAutocomplete::collection($cars);
    }
}
