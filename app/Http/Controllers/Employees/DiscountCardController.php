<?php

namespace App\Http\Controllers\Employees;

use App\Entity\Car;
use App\Entity\CarMark;
use App\Entity\Client;
use App\Entity\DiscountCard\DiscountCard;;

use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\DiscountCard\CreateRequest;
use App\Http\Requests\DiscountCard\EditDiscRequest;
use App\Http\Requests\DiscountCard\EditRequest;
use App\Services\DiscountCardService;
use App\Services\Dto\ClientDto;
use App\Services\Dto\DiscountCardDto;
use App\Services\Exception\ClientAlreadyHasDiscountCard;
use App\Services\ViewOrder\MemorizedOrderData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request as RequestFacade;

class DiscountCardController extends Controller
{
    private $service;
    private $memorizedOrderData;

    public function __construct(DiscountCardService $service, MemorizedOrderData $memorizedOrderData)
    {
        $this->service = $service;
        $this->memorizedOrderData = $memorizedOrderData;
        $this->middleware(['auth', 'can:manage-discount-card']);
    }

    public function list(Request $request)
    {
        $pageTitle = trans('discountCard.page.list.header');

        $query = DiscountCard::with('cars', 'client')->orderByDesc('id');
        if (!empty($value = $request->get('number'))) {
            $query->where('number', $value);
        }
        if (!empty($value = $request->get('car_number'))) {
            $discountCardIds = Car::where('number', '=', mb_strtoupper($value))->pluck('discount_card_id');
            $query->whereIn('id', $discountCardIds);
        }

        $discountCards = $query->paginate(20);
        return view('employees.discountCard.list', compact('pageTitle', 'discountCards'));
    }

    public function createForm(Request $request)
    {
        $pageTitle = trans('discountCard.page.create.header');

        $referer = $request->headers->get('referer');
        $route = Route::getRoutes()->match(RequestFacade::create($referer, 'GET'));
        $routeName = $route->getName();

        $car = $client = null;
        if ($routeName === 'employees.order.ordering') {
            $carId = $route->parameter('car');
            /** @var Car $car */
            $car = Car::findOrFail($carId);

            $clientId = $route->parameter('client');
            /** @var Client $client */
            $client = Client::findOrFail($clientId);
        }

        $discountCard = new DiscountCard();
        if (isset($client)) {
            $discountCard->client()->associate($client);
        }

        $carMarks = CarMark::all();

        return view('employees.discountCard.create', compact('pageTitle', 'discountCard', 'car', 'carMarks'));
    }

    public function create(CreateRequest $request)
    {
        $clientDto = new ClientDto(
            null,
            $request->get('name'),
            $request->get('phone')
        );
        $discountCardDto = new DiscountCardDto(
            $request->get('number'),
            $request->get('comment'),
            $request->get('type'),
            $request->get('disc')
        );
        $newCarNumbers = $request->get('new_cars');
        if (isset($newCarNumbers)) {
            $newCarNumbers = array_map('mb_strtoupper', $newCarNumbers);
        }

        $markAndModel = $request->get('markAndModel');

        try {
            $this->service->register($discountCardDto, $clientDto, $newCarNumbers, $markAndModel, $this->getAuthUser());
        } catch (ClientAlreadyHasDiscountCard $e) {
            $this->fieldValidationError($request, ['phone' => trans('discountCard.message.clientAlreadyHasDiscountCard')]);
        }

        $href = $this->memorizedOrderData->getHref();
        if (is_null($href)) {
            $href = route('employees.discountCard.list');
        }

        return redirect()
            ->to($href)
            ->with('success', trans('discountCard.message.successCreate'));
    }

    public function check(Request $request)
    {
        $value = $request->get('value');
        $old = $request->get('old');
        $carId = $request->get('car_id');

        $discountCards = DiscountCard::where('number', '=', $value)->first();

        $message = null;
        if (isset($discountCards) && ($old != $value || is_null($old))) {
            $message = trans('discountCard.message.discountCardExist') . '. <a href="' . route('employees.discountCard.edit', [$discountCards]) . '?car_id=' . $carId . '">' . trans('discountCard.action.edit') . '</a>';
        }

        return [
            'message' => $message
        ];
    }

    public function show(DiscountCard $discountCard)
    {
        $pageTitle = trans('discountCard.page.show.header');
        return view('employees.discountCard.show', compact('pageTitle', 'discountCard'));
    }

    public function editForm(DiscountCard $discountCard, Request $request)
    {
        ! $discountCard->canAdminChange() and abort(403);

        $carId = $request->get('car_id');
        $car = null;
        if (isset($carId)) {
            $car = Car::findOrFail($carId);
        }

        $pageTitle = trans('discountCard.page.edit.header');

        $carMarks = CarMark::all();

        return view('employees.discountCard.edit', compact('pageTitle', 'discountCard', 'car', 'carMarks'));
    }

    public function edit(EditRequest $request, DiscountCard $discountCard)
    {
        ! $discountCard->canAdminChange() and abort(403);

        $clientDto = new ClientDto(
            null,
            $request->get('name'),
            $request->get('phone')
        );
        $discountCardDto = new DiscountCardDto(
            $request->get('number'),
            $request->get('comment'),
            $request->get('type'),
            $request->get('disc')
        );

        $newCarNumbers = $request->get('new_cars');
        if (isset($newCarNumbers)) {
            $newCarNumbers = array_map('mb_strtoupper', $newCarNumbers);
        }

        $savedCarIds = $request->get('saved_cars');

        $markAndModel = $request->get('markAndModel');

        try {
            $this->service->edit($discountCard, $discountCardDto, $clientDto, $newCarNumbers, $savedCarIds, $markAndModel, $this->getAuthUser());
        } catch (ClientAlreadyHasDiscountCard $e) {
            $this->fieldValidationError($request, ['phone' => trans('discountCard.message.clientAlreadyHasDiscountCard')]);
        }

        $href = $this->memorizedOrderData->getHref();
        if (is_null($href)) {
            $href = route('employees.discountCard.list');
        }

        return redirect()
            ->to($href)
            ->with('success', trans('discountCard.message.successEdit'));
    }

    public function editDiscForm(DiscountCard $discountCard)
    {
        ! $discountCard->canAdminChange() and abort(403);

        $pageTitle = trans('discountCard.page.editDisc.header');

        return view('employees.discountCard.editDisc', compact('pageTitle', 'discountCard'));
    }

    public function editDisc(EditDiscRequest $request, DiscountCard $discountCard)
    {
        ! $discountCard->canAdminChange() and abort(403);

        /** @var User $user */
        $user = Auth::user();
        $discountCard->changeDisc($request->get('disc'), $request->get('type'), $user);

        $href = $this->memorizedOrderData->getHref();
        if (is_null($href)) {
            $href = route('employees.discountCard.list');
        }

        return redirect()
            ->to($href)
            ->with('success', trans('discountCard.message.successEditDisc'));
    }
}
