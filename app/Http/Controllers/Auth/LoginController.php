<?php

namespace App\Http\Controllers\Auth;

use App\Entity\ServiceCenter;
use App\Entity\User\Exception\NeedSelectServiceCenterException;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $serviceCenters = ServiceCenter::active()->orderBy('name')->get();
        return view('auth.login', compact('serviceCenters'));
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $authenticate = Auth::attempt(
            $request->only(['login', 'password']),
            false //$request->filled('remember')
        );

        if ($authenticate) {
            $request->session()->regenerate();
            $this->clearLoginAttempts($request);

            /** @var User $user */
            $user = Auth::user();

            if ($user->isDelete()) {
                Auth::logout();
                return back()->with('error', trans('auth.delete'));
            }

            if (! $user->canLogin()) {
                Auth::logout();
                return back()->with('error', trans('auth.denied'));
            }

            try {
                $user->enter($request->get('service_center'));
            } catch (NeedSelectServiceCenterException $e) {
                Auth::logout();
                $this->fieldValidationError($request, ['service_center' => trans('user.message.needServiceCenter')]);
            }

            if ($user->isAdmin()) {
                return redirect()->route('employees.teams.list');
            } elseif ($user->isCashier()) {
                return redirect()->route('employees.order.payment.list');
            } elseif ($user->isOverdrive()) {
                return redirect()->route('employees.order.overdrive.list');
            } else {
                return redirect()->route('superAdmin.home');
            }
        }

        $this->incrementLoginAttempts($request);

        throw ValidationException::withMessages(['login' => [trans('auth.failed')]]);
    }

    public function logout(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        $user->exit();

        $this->guard()->logout();
        $request->session()->invalidate();

        return redirect()->route('login');
    }

    protected function username()
    {
        return 'login';
    }
}
