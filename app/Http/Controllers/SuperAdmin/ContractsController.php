<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Entity\Car;
use App\Entity\CarWash\Type;
use App\Entity\CarWash\Work;
use App\Entity\Contract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Contract\CreateContractRequest;
use App\Http\Requests\CarWash\CreateWorkRequest;
use App\Http\Requests\Contract\EditContractRequest;
use App\Http\Requests\CarWash\EditWorkRequest;
use Illuminate\Http\Request;

class ContractsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-contracts']);
    }

    public function list()
    {
        $pageTitle = trans('contracts.page.list.header');
        $contracts = Contract::orderByDesc('id')->paginate(20);
        return view('superAdmin.contracts.list', compact('pageTitle', 'contracts'));
    }

    public function show(Contract $contract)
    {
        $pageTitle = trans('contracts.page.show.header');
        return view('superAdmin.contracts.show', compact('pageTitle', 'contract'));
    }

    public function createForm()
    {
        $pageTitle = trans('contracts.page.create.header');
        $contract = new Contract();
        return view('superAdmin.contracts.create', compact('pageTitle', 'contract'));
    }

    public function create(CreateContractRequest $request)
    {
        Contract::create($request->only(['number', 'contractor', 'from', 'to', 'disc']));

        return redirect()
            ->to(route('superAdmin.contracts.list'))
            ->with('success', trans('contracts.message.successCreate'));
    }

    public function editForm(Contract $contract)
    {
        $pageTitle = trans('contracts.page.edit.header');
        return view('superAdmin.contracts.edit', compact('pageTitle', 'contract'));
    }

    public function edit(EditContractRequest $request, Contract $contract)
    {
        $contract->update($request->only(['number', 'contractor', 'from', 'to', 'disc']));

        return redirect()
            ->to(route('superAdmin.contracts.list'))
            ->with('success', trans('contracts.message.successEdit'));
    }

    public function delete(Contract $contract)
    {
        if ($contract->hasOrder()) {
            abort(400);
        }

        $contract->delete();
        return redirect()
            ->to(route('superAdmin.contracts.list'))
            ->with('success', trans('contracts.message.successDelete'));
    }
}
