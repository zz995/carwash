<?php

namespace App\Http\Controllers\SuperAdmin\DiscountCard;

use App\Entity\Car;
use App\Entity\CarMark;
use App\Entity\Client;
use App\Entity\DiscountCard\DiscountCard;;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\DiscountCard\CreateRequest;
use App\Http\Requests\DiscountCard\EditDiscRequest;
use App\Http\Requests\DiscountCard\EditRequest;
use App\Services\DiscountCardService;
use App\Services\Dto\ClientDto;
use App\Services\Dto\DiscountCardDto;
use App\Services\Exception\ClientAlreadyHasDiscountCard;
use App\Services\ViewOrder\MemorizedOrderData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request as RequestFacade;

class DiscountCardController extends Controller
{
    private $service;
    private $memorizedOrderData;

    public function __construct(DiscountCardService $service, MemorizedOrderData $memorizedOrderData)
    {
        $this->service = $service;
        $this->memorizedOrderData = $memorizedOrderData;
        $this->middleware(['auth', 'can:administration-discount-card']);
    }

    public function list(Request $request)
    {
        $pageTitle = trans('discountCard.page.list.header');

        $query = DiscountCard::with('cars', 'client')->orderByDesc('id');
        if (!empty($value = $request->get('number'))) {
            $query->where('number', $value);
        }
        if (!empty($value = $request->get('car_number'))) {
            $discountCardIds = Car::where('number', '=', mb_strtoupper($value))->pluck('discount_card_id');
            $query->whereIn('id', $discountCardIds);
        }
        if (!empty($value = $request->get('phone'))) {
            $clientIds = Client::where('phone', 'like', $value . '%')->pluck('id');
            $query->whereIn('client_id', $clientIds);
        }

        $discountCards = $query->paginate(20);
        return view('superAdmin.discountCard.list', compact('pageTitle', 'discountCards'));
    }

    public function show(DiscountCard $discountCard)
    {
        $pageTitle = trans('discountCard.page.show.header');
        return view('superAdmin.discountCard.show', compact('pageTitle', 'discountCard'));
    }

    public function editForm(DiscountCard $discountCard, Request $request)
    {
        $carId = $request->get('car_id');
        $car = null;
        if (isset($carId)) {
            $car = Car::findOrFail($carId);
        }

        $pageTitle = trans('discountCard.page.edit.header');

        $carMarks = CarMark::all();

        return view('superAdmin.discountCard.edit', compact('pageTitle', 'discountCard', 'car', 'carMarks'));
    }

    public function edit(EditRequest $request, DiscountCard $discountCard)
    {
        $clientDto = new ClientDto(
            null,
            $request->get('name'),
            $request->get('phone')
        );
        $discountCardDto = new DiscountCardDto(
            $request->get('number'),
            $request->get('comment'),
            $request->get('type'),
            $request->get('disc')
        );

        $newCarNumbers = $request->get('new_cars');
        if (isset($newCarNumbers)) {
            $newCarNumbers = array_map('mb_strtoupper', $newCarNumbers);
        }

        $savedCarIds = $request->get('saved_cars');

        $markAndModel = $request->get('markAndModel');

        try {
            $this->service->edit($discountCard, $discountCardDto, $clientDto, $newCarNumbers, $savedCarIds, $markAndModel, $this->getAuthUser());
        } catch (ClientAlreadyHasDiscountCard $e) {
            $this->fieldValidationError($request, ['phone' => trans('discountCard.message.clientAlreadyHasDiscountCard')]);
        }

        $href = $this->memorizedOrderData->getHref();
        if (is_null($href)) {
            $href = route('superAdmin.discountCard.list');
        }

        return redirect()
            ->to($href)
            ->with('success', trans('discountCard.message.successEdit'));
    }

    public function editDiscForm(DiscountCard $discountCard)
    {
        $pageTitle = trans('discountCard.page.editDisc.header');

        return view('superAdmin.discountCard.editDisc', compact('pageTitle', 'discountCard'));
    }

    public function editDisc(EditDiscRequest $request, DiscountCard $discountCard)
    {
        /** @var User $user */
        $user = Auth::user();
        $discountCard->changeDisc($request->get('disc'), $request->get('type'), $user);

        $href = $this->memorizedOrderData->getHref();
        if (is_null($href)) {
            $href = route('superAdmin.discountCard.list');
        }

        return redirect()
            ->to($href)
            ->with('success', trans('discountCard.message.successEditDisc'));
    }

    public function denyEdit(DiscountCard $discountCard)
    {
        $discountCard->admin = 0;
        $discountCard->save();

        return redirect()->to(route('superAdmin.discountCard.show', $discountCard));
    }

    public function allowEdit(DiscountCard $discountCard)
    {
        $discountCard->admin = 1;
        $discountCard->save();

        return redirect()->to(route('superAdmin.discountCard.show', $discountCard));
    }
}
