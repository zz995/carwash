<?php

namespace App\Http\Controllers\SuperAdmin\DiscountCard;

use App\Entity\CarWash\Type;
use App\Entity\CarWash\Work;
use App\Entity\DiscountCard\CalculationStep;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\CarWash\CreateWorkRequest;
use App\Http\Requests\CarWash\EditWorkRequest;
use App\Http\Requests\DiscountCard\CalculationStep\CreateRequest;
use App\Http\Requests\DiscountCard\CalculationStep\EditRequest;
use App\ObjectValue\Money;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StepsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-discount-card-steps']);
    }

    public function list()
    {
        $pageTitle = trans('discountCard.page.stepList.header');
        $steps = CalculationStep::orderBy('disc')->get();

        return view('superAdmin.discountCard.steps.list', compact('pageTitle', 'steps'));
    }

    public function createForm()
    {
        $pageTitle = trans('discountCard.page.stepCreate.header');
        $step = new CalculationStep();

        return view('superAdmin.discountCard.steps.create', compact('pageTitle', 'step'));
    }

    public function create(CreateRequest $request)
    {
        /** @var User $user */
        $user = Auth::user();

        /** @var CalculationStep $step */
        $step = CalculationStep::make($request->only(['disc', 'sum']));
        $step->user()->associate($user);
        $step->save();

        return redirect()
            ->to(route('superAdmin.discountCard.steps.list'))
            ->with('success', trans('discountCard.message.successCreateStep'));
    }

    public function editForm(CalculationStep $step)
    {
        $pageTitle = trans('discountCard.page.stepEdit.header');

        return view('superAdmin.discountCard.steps.edit', compact('pageTitle', 'step'));
    }

    public function edit(EditRequest $request, CalculationStep $step)
    {
        /** @var User $user */
        $user = Auth::user();

        /** @var CalculationStep $step */
        $step->disc = $request->get('disc');
        $step->sum = new Money($request->get('sum'));
        $step->user()->associate($user);
        $step->save();

        return redirect()
            ->to(route('superAdmin.discountCard.steps.list'))
            ->with('success', trans('discountCard.message.successEditStep'));
    }

    public function delete(CalculationStep $step)
    {
        $step->delete();

        return redirect()
            ->to(route('superAdmin.discountCard.steps.list'))
            ->with('success', trans('discountCard.message.successDeleteStep'));
    }
}
