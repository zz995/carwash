<?php

namespace App\Http\Controllers\SuperAdmin\Fine;

use App\Entity\User\Fine\PaymentRefusalFineSize;
use App\Entity\User\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\Fine\ChangePaymentRefusalFineSizeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PaymentRefusalFineSizeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:administration-fine']);
    }

    public function form()
    {
        $pageTitle = trans('teamFine.page.paymentRefusalFineSize.header');

        $fineSize = PaymentRefusalFineSize::getInstance();



        return view('superAdmin.fine.paymentRefusalFineSize', compact('pageTitle', 'fineSize'));
    }

    public function change(ChangePaymentRefusalFineSizeRequest $request)
    {
        $fineSize = PaymentRefusalFineSize::getInstance()->fill($request->only(['admin_fine', 'washer_fine']));
        $fineSize->initiator()->associate($this->getAuthUser());
        $fineSize->save();

        return redirect()
            ->to(route('superAdmin.fine.paymentRefusalFineSize'))
            ->with('success', trans('teamFine.message.successChangePaymentRefusalFineSize'));
    }
}
