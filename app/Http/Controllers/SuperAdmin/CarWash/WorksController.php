<?php

namespace App\Http\Controllers\SuperAdmin\CarWash;

use App\Entity\CarWash\Type;
use App\Entity\CarWash\Work;
use App\Http\Controllers\Controller;
use App\Http\Requests\CarWash\CreateWorkRequest;
use App\Http\Requests\CarWash\EditWorkRequest;
use Illuminate\Http\Request;

class WorksController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-car-wash-works']);
    }

    public function index(Request $request)
    {
        $pageTitle = trans('works.page.list.header');

        $query = Work::with('type')->orderBy('type_id')->orderBy('id');

        if (!empty($value = $request->get('id'))) {
            $query->where('number', $value);
        }

        if (!empty($value = $request->get('name'))) {
            $query->where('name', 'like', '%' . $value . '%');
        }

        if (!empty($value = $request->get('type'))) {
            $query->where('type_id', $value);
        }

        $works = $query->paginate(20);
        $types = Type::all();

        return view('superAdmin.carWash.works.list', compact('pageTitle', 'works', 'types'));
    }

    public function show(Work $work)
    {
        $pageTitle = trans('works.page.show.header');
        return view('superAdmin.carWash.works.show', compact('pageTitle', 'work'));
    }

    public function createForm()
    {
        $pageTitle = trans('works.page.create.header');
        $types = Type::all();

        $work = new Work();
        return view('superAdmin.carWash.works.create', compact('pageTitle', 'work', 'types'));
    }

    public function create(CreateWorkRequest $request)
    {
        Work::create(
            array_merge(
                $request->only('name'),
                $request->only('number'),
                ['type_id' => $request->get('type')],
                ['several' => !!$request->get('several')],
                array_merge(...array_map(function ($num) use($request) {
                    return $request->only("category{$num}_price", "category{$num}_cost", "category{$num}_salary");
                }, [1, 2, 3, 4]))
            )
        );

        return redirect()
            ->to(route('superAdmin.carWash.works.list'))
            ->with('success', trans('works.message.successCreate'));
    }

    public function editForm(Work $work)
    {
        $pageTitle = trans('works.page.edit.header');
        $types = Type::all();

        return view('superAdmin.carWash.works.edit', compact('pageTitle', 'work', 'types'));
    }

    public function edit(EditWorkRequest $request, Work $work)
    {
        $work->update(
            array_merge(
                $request->only('name'),
                $request->only('number'),
                ['type_id' => $request->get('type')],
                ['several' => !!$request->get('several')],
                array_merge(...array_map(function ($num) use($request) {
                    return $request->only("category{$num}_price", "category{$num}_cost", "category{$num}_salary");
                }, [1, 2, 3, 4]))
            )
        );

        return redirect()
            ->to(route('superAdmin.carWash.works.list'))
            ->with('success', trans('works.message.successEdit'));
    }

    public function delete(Work $work)
    {
        $work->delete();
        return redirect()
            ->to(route('superAdmin.carWash.works.list'))
            ->with('success', trans('works.message.successDelete'));
    }
}
