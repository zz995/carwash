<?php

namespace App\Http\Controllers\SuperAdmin\CarWash;

use App\Entity\Car;
use App\Entity\Client;
use App\Entity\Contract;
use App\Entity\DiscountCard\DiscountCard;
use App\Entity\Order\BasketItem;
use App\Entity\Order\Order;
use App\Entity\ServiceCenter;
use App\Http\Controllers\Controller;
use App\Sanitizes\Sanitize;
use App\Services\Dto\DateRangeDto;
use App\Services\Dto\SortDto;
use App\Services\OrderService;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    private $service;
    private $sanitize;

    public function __construct(OrderService $service, Sanitize $sanitize)
    {
        $this->service = $service;
        $this->sanitize = $sanitize;
        $this->middleware(['auth', 'can:administration-order']);
    }

    public function list(Request $request)
    {
        $pageTitle = trans('order.page.list.header');

        $sort = new SortDto(
            $request->get('sort'),
            'id_desc',
            ['id', 'price', 'created_at', 'time_execute', 'time_queue']
        );

        $dateRaw = $request->get('date');
        $date = null;
        if (isset($dateRaw)) {
            $date = new DateRangeDto($dateRaw);
        }

        $query = Order::with('basketItems.work', 'serviceCenter', 'teamFines');
        $query = $query->orderBy($sort->getField(), $sort->getDirection())->orderByDesc('id');
        if (!is_null($value = $request->get('work'))) {
            $ordersIds = BasketItem::where('work_id', $value)->pluck('order_id');
            $query->whereIn('id', $ordersIds);
        }
        if (!is_null($value = $request->get('status'))) {
            $query->where('status', $value);
        }
        if (!is_null($value = $request->get('payment'))) {
            $query->where('payment_type', $value);
        }
        if (!is_null($value = $request->get('service_center'))) {
            $query->where('service_center_id', $value);
        }
        if (!is_null($value = $request->get('services_standard_price'))) {
            $query->where('services_standard_price', $value);
        }
        if (isset($date)) {
            $query = $query->whereBetween('created_at', $date->getRange());
        }
        if (!is_null($value = $request->get('price_from'))) {
            $query->where('price', '>=', $value);
        }
        if (!is_null($value = $request->get('price_to')) && is_int($value)) {
            $query->where('price', '<=', $value);
        }
        if (!is_null($value = $request->get('time_execute_from'))) {
            $query->where('time_execute', '>=', intval($value) * 60);
        }
        if (!is_null($value = $request->get('time_execute_to'))) {
            $query->where('time_execute', '<=', intval($value) * 60 + 59);
        }
        if (!is_null($value = $request->get('time_queue_from'))) {
            $query->where('time_queue', '>=', intval($value) * 60);
        }
        if (!is_null($value = $request->get('time_queue_to'))) {
            $query->where('time_queue', '<=', intval($value) * 60 + 59);
        }

        $orders = $query->paginate(20);

        $statuses = Order::getAllStatuses();
        $serviceCenters = ServiceCenter::orderBy('name')->get();

        return view('superAdmin.carWash.orders.list', compact('pageTitle', 'orders', 'date', 'sort', 'statuses', 'serviceCenters'));
    }

    public function serviceHistory(Request $request)
    {
        $pageTitle = trans('order.page.serviceHistory.header');
        $query = Order::with(['contract', 'discountCard', 'client', 'car', 'car.carMark', 'car.carModel', 'serviceCenter', 'teamFines']);
        $query->orderByDesc('id');
        if (!is_null($value = $request->get('contract_number'))) {
            $query->whereIn('contract_id', function (Builder $q) use ($value) {
                $q
                    ->select('id')
                    ->from((new Contract())->getTable())
                    ->where('number', 'like', $value . '%')
                    ->get();
            });
        }
        if (!is_null($value = $request->get('discount_card'))) {
            $query->whereIn('discount_card_id', function (Builder $q) use ($value) {
                $q
                    ->select('id')
                    ->from((new DiscountCard())->getTable())
                    ->where('number', 'like', $value . '%')
                    ->get();
            })->where('payment_type', '<>', Order::PAYMENT_CONTRACT);
        }
        if (!is_null($value = $this->sanitize->phone($request->get('phone')))) {
            $query->whereIn('client_id', function (Builder $q) use ($value) {
                $q
                    ->select('id')
                    ->from((new Client())->getTable())
                    ->where('phone', 'like',$value . '%')
                    ->get();
            });
        }
        if (!is_null($value = $request->get('car_number'))) {
            $query->whereIn('car_id', function (Builder $q) use ($value) {
                $q
                    ->select('id')
                    ->from((new Car())->getTable())
                    ->where('number', 'like', mb_strtoupper($value) . '%')
                    ->get();
            });
        }
        if (!is_null($value = $request->get('service_center'))) {
            $query->where('service_center_id', $value);
        }

        $serviceCenters = ServiceCenter::orderBy('name')->get();
        $orders = $query->paginate(20);

        return view('superAdmin.carWash.orders.serviceHistories', compact('pageTitle', 'orders', 'serviceCenters'));
    }

    public function show(Order $order)
    {
        $pageTitle = trans('order.page.show.header');
        $statusHistories = $order->statusHistory()->orderByDesc('id')->get();
        return view('superAdmin.carWash.orders.show', compact('pageTitle', 'order', 'statusHistories'));
    }

    public function cancel(Order $order)
    {
        $this->service->cancel($order, $this->getAuthUser());

        return redirect()
            ->to(route('superAdmin.carWash.order.show', $order))
            ->with('success', trans('order.message.successCancel'));
    }
}
