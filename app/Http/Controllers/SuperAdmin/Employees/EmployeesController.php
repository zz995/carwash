<?php

namespace App\Http\Controllers\SuperAdmin\Employees;

use App\Entity\ServiceCenter;
use App\Entity\User\Exception\ImpossibleOverdriveException;
use App\Entity\User\Exception\NeedLoginException;
use App\Entity\User\Exception\NeedPasswordException;
use App\Entity\User\Exception\NeedSelectServiceCenterException;
use App\Entity\User\Exception\WrongPasswordException;
use App\Entity\User\Fio;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Employees\ChangePasswordRequest;
use App\Http\Requests\Employees\CreateRequest;
use App\Http\Requests\Employees\EditRequest;
use App\Http\Requests\Employees\PayoutRequest;
use App\ObjectValue\Money;
use App\Sanitizes\Sanitize;
use App\Services\PayoutService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class EmployeesController extends Controller
{
    private $sanitizeService;
    private $payoutService;

    public function __construct(PayoutService $payoutService, Sanitize $sanitizeService)
    {
        $this->middleware('auth');
        $this->sanitizeService = $sanitizeService;
        $this->payoutService = $payoutService;
    }

    public function index(Request $request)
    {
        Gate::denies('manage-employe-view') and abort(403);

        $pageTitle = trans('user.page.list.header');

        $query = User::with('role', 'serviceCenter')->defaultOrder();

        if (!empty($value = $request->get('id'))) {
            $query->where('id', $value);
        }

        if (!empty($value = $request->get('surname'))) {
            $query->where('surname', 'like', $value . '%');
        }

        if (!empty($value = $request->get('phone'))) {
            $query->whereRaw("JSON_SEARCH(phones, 'one', ?) IS NOT NULL", [$this->sanitizeService->phone($value) . '%']);
        }

        if (!empty($value = $request->get('status'))) {
            $query->where('status', $value);
        }

        if (!empty($value = $request->get('service_center'))) {
            $query->where('service_center_id', $value);
        }

        if (!empty($value = $request->get('role'))) {
            $query->where('role_id', $value);
        }

        $employees = $query->paginate(20);

        $serviceCenters = ServiceCenter::active()->get();
        $statuses = User::statusesList();
        $roles = Role::allList();

        return view('superAdmin.employees.list', compact('pageTitle', 'serviceCenters', 'employees', 'statuses', 'roles'));
    }

    public function createForm()
    {
        Gate::denies('manage-employe-create') and abort(403);

        $pageTitle = trans('user.page.create.header');
        $statuses = User::statusesList();
        $roles = Role::baseList();
        $serviceCenters = ServiceCenter::active()->orderBy('name')->get();

        $employe = new User();
        return view('superAdmin.employees.create', compact('pageTitle', 'serviceCenters', 'employe', 'statuses', 'roles'));
    }

    public function create(CreateRequest $request)
    {
        Gate::denies('manage-employe-create') and abort(403);

        try {
            $employe = User::register(
                new Fio(
                    $request->get('surname'),
                    $request->get('name'),
                    $request->get('patronymic')
                ),
                $request->get('role'),
                $request->get('overdrive') == 1,
                $this->sanitizeService->phones($request->get('phones')),
                $request->get('email'),
                $request->get('login'),
                $request->get('password'),
                $request->get('service_center')
            );
        } catch (NeedLoginException $e) {
            $this->fieldValidationError($request, ['login' => trans('user.message.needLogin')]);
        } catch (NeedPasswordException $e) {
            $this->fieldValidationError($request, ['password' => trans('user.message.needPassword')]);
        } catch (NeedSelectServiceCenterException $e) {
            $this->fieldValidationError($request, ['service_center' => trans('user.message.needServiceCenter')]);
        } catch (ImpossibleOverdriveException $e) {
            $this->fieldValidationError($request, ['overdrive' => trans('user.message.impossibleOverdrive')]);
        }

        return redirect()
            ->to(route('superAdmin.employees.list'))
            ->with('success', trans('user.message.successCreate'));
    }

    public function editForm(User $employe)
    {
        Gate::denies('manage-employe-edit', [$employe]) and abort(403);

        $pageTitle = trans('user.page.edit.header', ['employe' => $employe->getFio()->getFullName()]);
        $statuses = User::statusesList();
        $roles = Role::baseList();
        $serviceCenters = ServiceCenter::active()->orderBy('name')->get();

        return view('superAdmin.employees.edit', compact('pageTitle', 'serviceCenters', 'employe', 'statuses', 'roles'));
    }

    public function edit(EditRequest $request, User $employe)
    {
        Gate::denies('manage-employe-edit', [$employe]) and abort(403);

        try {
            $employe->edit(
                new Fio($request->get('surname'), $request->get('name'), $request->get('patronymic')),
                $request->get('role'),
                $request->get('overdrive') == 1,
                $this->sanitizeService->phones($request->get('phones')),
                $request->get('email'),
                $request->get('login'),
                $request->get('service_center')
            );
        } catch (NeedLoginException $e) {
            $this->fieldValidationError($request, ['login' => trans('user.message.needLogin')]);
        } catch (NeedSelectServiceCenterException $e) {
            $this->fieldValidationError($request, ['service_center' => trans('user.message.needServiceCenter')]);
        } catch (ImpossibleOverdriveException $e) {
            $this->fieldValidationError($request, ['overdrive' => trans('user.message.impossibleOverdrive')]);
        }

        return redirect()
            ->to(route('superAdmin.employees.list'))
            ->with('success', trans('user.message.successEdit'));
    }

    public function show(User $employe)
    {
        Gate::denies('manage-employe-view') and abort(403);

        $pageTitle = trans('user.page.show.header', ['employe' => $employe->getFio()->getFullName()]);
        $loginHistories = $employe->loginHistory()->orderByDesc('created_at')->limit(10)->get();
        return view('superAdmin.employees.show', compact('pageTitle', 'loginHistories', 'employe'));
    }

    public function changePasswordForm(User $employe)
    {
        Gate::denies('manage-employe-edit', [$employe]) and abort(403);

        $pageTitle = trans('user.page.changePassword.header', ['employe' => $employe->getFio()->getFullName()]);
        return view('superAdmin.employees.changePassword', compact('pageTitle', 'employe'));
    }

    public function changePassword(ChangePasswordRequest $request, User $employe)
    {
        Gate::denies('manage-employe-edit', [$employe]) and abort(403);

        try {
            $employe->changePassword(
                $request->get('password'),
                $request->get('current')
            );
        } catch (WrongPasswordException $e) {
            $error = \Illuminate\Validation\ValidationException::withMessages([
                'current' => trans('user.message.wrongPassword'),
            ]);
            throw $error;
        }

        return redirect()
            ->to(route('superAdmin.employees.edit', ['employe' => $employe]))
            ->with('success', trans('user.message.successChangePassword'));
    }

    public function delete(User $employe)
    {
        Gate::denies('manage-employe-delete', [$employe]) and abort(403);

        $employe->remove();

        return redirect()
            ->to(route('superAdmin.employees.show', ['employe' => $employe]))
            ->with('success', trans('user.message.successDelete'));
    }

    public function recovery(User $employe)
    {
        Gate::denies('manage-employe-delete', [$employe]) and abort(403);

        $employe->recovery();

        return redirect()
            ->to(route('superAdmin.employees.show', ['employe' => $employe]))
            ->with('success', trans('user.message.successRecovery'));
    }

    public function disable(User $employe)
    {
        Gate::denies('manage-employe-edit', [$employe]) and abort(403);

        $employe->disable();

        return redirect()
            ->to(route('superAdmin.employees.show', ['employe' => $employe]))
            ->with('success', trans('user.message.successDisable'));
    }

    public function payoutForm(User $employe)
    {
        Gate::denies('manage-employe-payout', [$employe]) and abort(403);

        $pageTitle = trans('user.page.payout.header');

        $payoutSum = $employe->balance->gt(new Money(0)) ? $employe->balance : new Money(0);

        return view('superAdmin.employees.payout', compact('pageTitle', 'employe', 'payoutSum'));
    }

    public function payout(User $employe, PayoutRequest $request)
    {
        Gate::denies('manage-employe-payout', [$employe]) and abort(403);

        $this->payoutService->payout(
            new Money($request->get('sum')),
            $employe,
            $this->getAuthUser()
        );

        return redirect()
            ->to(route('superAdmin.employees.show', ['employe' => $employe]))
            ->with('success', trans('user.message.successPayout'));
    }
}
