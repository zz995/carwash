<?php

namespace App\Http\Controllers\SuperAdmin\Employees;

use App\Entity\User\Accrual;
use App\Entity\User\Exception\SalaryIncorectException;
use App\Entity\User\Role;
use App\Entity\User\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Employees\ChangeSalaryRequest;
use App\Services\Dto\DateRangeDto;
use App\Services\SalaryBreakdownService;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class SalaryBreakdownController extends Controller
{
    private $service;

    public function __construct(SalaryBreakdownService $service)
    {
        $this->middleware(['auth', 'can:manage-salary-breakdown']);
        $this->service = $service;
    }

    public function index(User $employe, Request $request)
    {
        $pageTitle = trans('user.page.salaryBreakdown.header');

        $dataRange = null;
        if ($range = $request->get('range')) {
            $dataRange = new DateRangeDto($range);
        }

        $accruals = $this->service->getAccruals($employe, $dataRange);
        $accrualsSum = $this->service->getAccrualsSum($employe, $dataRange);

        return view('superAdmin.employees.salaryBreakdown', compact('pageTitle', 'employe', 'accruals', 'accrualsSum', 'dataRange'));
    }
}
