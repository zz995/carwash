<?php

namespace App\Http\Controllers\SuperAdmin\Employees;

use App\Entity\User\Exception\SalaryIncorectException;
use App\Entity\User\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\Employees\ChangeSalaryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class SalaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function changeSalaryForm(Role $role)
    {
        Gate::denies('manage-role-change-salary', $role) and abort(403);

        $pageTitle = trans('role.page.changeSalary.header');

        return view('superAdmin.roles.changeSalary', compact('pageTitle', 'role'));
    }

    public function changeSalary(ChangeSalaryRequest $request, Role $role)
    {
        Gate::denies('manage-role-change-salary', $role) and abort(403);

        try {
            $role->changeSalary($request->get('salary'));
        } catch (SalaryIncorectException $err) {
            $this->fieldValidationError($request, ['salary' => trans('role.message.salaryIncorect')]);
        }

        return redirect()
            ->to(route('superAdmin.employees.roles.list'))
            ->with('success', trans('role.message.changeSalary'));
    }


}
