<?php

namespace App\Http\Controllers\SuperAdmin\Employees;

use App\Entity\User\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        Gate::denies('manage-role-view') and abort(403);

        $pageTitle = trans('role.page.list.header');

        $roles = Role::orderBy('id')->paginate(20);

        return view('superAdmin.roles.list', compact('pageTitle', 'roles'));
    }
}
