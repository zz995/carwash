<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Entity\ServiceCenter;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceCenter\CreateServiceCenterRequest;
use App\Http\Requests\ServiceCenter\EditServiceCenterRequest;
use Illuminate\Http\Request;

class ServiceCentersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'can:manage-service-centers']);
    }

    public function list(Request $request)
    {
        $pageTitle = trans('serviceCenter.page.list.header');
        $serviceCenters = ServiceCenter::orderByDesc('id')->paginate(20);

        return view('superAdmin.serviceCenters.list', compact('pageTitle', 'serviceCenters'));
    }

    public function createForm()
    {
        $pageTitle = trans('serviceCenter.page.list.header');
        $serviceCenter = new ServiceCenter();

        return view('superAdmin.serviceCenters.create', compact('pageTitle', 'serviceCenter'));
    }

    public function create(CreateServiceCenterRequest $request)
    {
        ServiceCenter::create($request->only(['name', 'background_color', 'font_color', 'desc', 'disabled']));

        return redirect()
            ->to(route('superAdmin.serviceCenter.list'))
            ->with('success', trans('serviceCenter.message.successCreate'));
    }

    public function show(ServiceCenter $serviceCenter)
    {
        $pageTitle = trans('serviceCenter.page.show.header');
        return view('superAdmin.serviceCenters.show', compact('pageTitle', 'serviceCenter'));
    }

    public function editForm(ServiceCenter $serviceCenter)
    {
        $pageTitle = trans('serviceCenter.page.edit.header');
        return view('superAdmin.serviceCenters.edit', compact('pageTitle', 'serviceCenter'));
    }

    public function edit(EditServiceCenterRequest $request, ServiceCenter $serviceCenter)
    {
        $serviceCenter->update($request->only(['name', 'background_color', 'font_color', 'desc', 'disabled']));

        return redirect()
            ->to(route('superAdmin.serviceCenter.list'))
            ->with('success', trans('serviceCenter.message.successEdit'));
    }
}