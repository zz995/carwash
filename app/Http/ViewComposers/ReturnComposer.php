<?php

namespace App\Http\ViewComposers;

use App\Entity\User\User;
use App\Services\ViewOrder\MemorizedOrderData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class ReturnComposer
{
    public function compose(View $view): void
    {
        /** @var User $user */
        $user = Auth::user();

        $memorizedOrderData = new MemorizedOrderData;

        $data = null;
        $exist =
            $user->isAdmin()
            && !Route::currentRouteNamed('employees.order.ordering')
            && !Route::currentRouteNamed('employees.order.clientRegister')
            && $memorizedOrderData->has();

        if ($exist) {
            $data = [
                'message' => trans('order.action.return'),
                'href' => $memorizedOrderData->getHref()
            ];
        }

        $view->with('exist', $exist);
        $view->with('data', $data);
    }
}
