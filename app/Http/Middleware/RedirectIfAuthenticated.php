<?php

namespace App\Http\Middleware;

use App\Entity\User\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            /** @var User $user */
            $user = Auth::user();

            if ($user->isAdmin()) {
                return redirect()->route('employees.teams.list');
            } elseif ($user->isCashier()) {
                return redirect()->route('employees.order.payment.list');
            } elseif ($user->isOverdrive()) {
                return redirect()->route('employees.order.overdrive.list');
            } else {
                return redirect()->route('superAdmin.home');
            }
        }

        return $next($request);
    }
}
