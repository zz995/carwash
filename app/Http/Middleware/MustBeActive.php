<?php

namespace App\Http\Middleware;

use App\Entity\User\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\Auth;

class MustBeActive
{
    public function handle($request, \Closure $next)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user && $user->isActive()) {
            return $next($request);
        }

        if (isset($user)) {
            Auth::logout();
        }

        throw new AuthenticationException;
    }
}
