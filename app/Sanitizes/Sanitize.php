<?php

namespace App\Sanitizes;


class Sanitize
{
    public function __construct()
    {

    }

    public function phone(?string $val): ?string
    {
        if (is_null($val)) {
            return null;
        }

        return preg_replace('/\D/', '', $val);
    }

    public function phones(?array $phones): ?array
    {
        if (empty($phones)) {
            return null;
        }

        return array_map([$this, 'phone'], $phones);
    }

}
