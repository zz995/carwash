<?php

namespace App\ObjectValue;

interface MoneyInterface
{
    public function add(MoneyInterface $another): MoneyInterface;
    public function sub(MoneyInterface $another): MoneyInterface;
    public function multiply(string $factor): MoneyInterface;
    public function division(int $divisor): MoneyInterface;
    public function percent(int $percent): MoneyInterface;
    public function ration(MoneyInterface $money): string;
    public function round(): MoneyInterface;
    public function gt(MoneyInterface $money): bool;
    public function lt(MoneyInterface $money): bool;
    public function equal(MoneyInterface $money): bool;
    public function getAmount(): string;
    public function __toString(): string;
}