<?php

namespace App\ObjectValue;

class Money implements MoneyInterface
{
    private $amount;
    public const SCALE = 2;
    public const LARGE_SCALE = 20;

    public function __construct(string $amount)
    {
        $this->amount = $amount;
    }

    public function percent(int $percent): MoneyInterface
    {
        $factor = (string) ($percent / 100);
        return new Money(bcmul($this->amount, $factor, self::SCALE));
    }

    public function ration(MoneyInterface $another): string
    {
        return bcdiv($this->amount, $another->getAmount(), self::LARGE_SCALE);
    }

    public function add(MoneyInterface $another): MoneyInterface
    {
        return new Money(bcadd($this->amount, $another->getAmount(), self::SCALE));
    }

    public function sub(MoneyInterface $another): MoneyInterface
    {
        return new Money(bcsub($this->amount, $another->getAmount(), self::SCALE));
    }

    public function multiply(string $factor): MoneyInterface
    {
        $factor = (string) $factor;
        return new Money(bcmul($this->amount, $factor, self::SCALE));
    }

    public function division(int $divisor): MoneyInterface
    {
        $divisor = (string) $divisor;
        return new Money(bcdiv($this->amount, $divisor, self::SCALE));
    }

    public function round(): MoneyInterface
    {
        return new Money((string)round((float)$this->amount));
    }

    public function gt(MoneyInterface $money): bool
    {
        return bccomp($this->amount, $money->getAmount()) == 1;
    }

    public function lt(MoneyInterface $money): bool
    {
        return bccomp($this->amount, $money->getAmount()) == -1;
    }

    public function equal(MoneyInterface $money): bool
    {
        return bccomp($this->amount, $money->getAmount()) == 0;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function __toString(): string
    {
        return $this->amount;
    }
}