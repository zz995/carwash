<?php

namespace App\Providers;

use App\Http\ViewComposers\ReturnComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        View::composer('partials.return', ReturnComposer::class);
    }
}
