<?php

namespace App\Providers;

use App\Entity\Order\Order;
use App\Entity\User\Role;
use App\Entity\User\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('administration-fine', function (User $user) {
            return $user->isSuperAdmin();
        });

        Gate::define('administration-order', function (User $user) {
            return $user->isSuperAdmin();
        });

        Gate::define('manage-employe-payout', function (User $user) {
            return $user->isSuperAdmin();
        });

        Gate::define('manage-salary-breakdown', function (User $user) {
            return $user->isSuperAdmin() || $user->isAdmin();
        });

        Gate::define('manage-employe-view', function (User $user) {
            return $user->isSuperAdmin();
        });
        Gate::define('manage-employe-create', function (User $user) {
            return  $user->isSuperAdmin();
        });
        Gate::define('manage-employe-edit', function (User $user, User $employe) {
            return $user->isSuperAdmin() && (!$employe->isSuperAdmin() || $user->id === $employe->id);
        });
        Gate::define('manage-employe-delete', function (User $user, User $employe) {
            return $user->isSuperAdmin() && !$employe->isSuperAdmin();
        });

        Gate::define('manage-role-view', function (User $user) {
            return $user->isSuperAdmin();
        });
        Gate::define('manage-role-change-salary', function (User $user, Role $role) {
            return $user->isSuperAdmin() && $role->hasSalary();
        });

        Gate::define('manage-car-wash-works', function (User $user) {
            return $user->isSuperAdmin();
        });

        Gate::define('manage-teams', function (User $user) {
            return $user->isAdmin();
        });

        Gate::define('manage-order', function (User $user) {
            return $user->isAdmin();
        });

        Gate::define('manage-service-history', function (User $user) {
            return $user->isAdmin();
        });

        Gate::define('manage-payment-order', function (User $user) {
            return $user->isCashier();
        });

        Gate::define('manage-overdrive-order-list', function (User $user) {
            return $user->isOverdrive();
        });
        Gate::define('manage-overdrive-order', function (User $user) {
            return $user->isOverdrive() || $user->isAdmin();
        });

        Gate::define('manage-sale-cafe', function (User $user) {
            return $user->isCashier();
        });

        Gate::define('manage-contracts', function (User $user) {
            return $user->isSuperAdmin();
        });

        Gate::define('manage-discount-card', function (User $user) {
            return $user->isAdmin();
        });


        Gate::define('administration-discount-card', function (User $user) {
            return $user->isSuperAdmin();
        });

        Gate::define('manage-discount-card-steps', function (User $user) {
            return $user->isSuperAdmin();
        });

        Gate::define('manage-service-centers', function (User $user) {
            return $user->isSuperAdmin();
        });


        Gate::define('manage-accrual', function (User $user) {
            return $user->isAdmin();
        });

        Gate::define('manage-balance', function (User $user) {
            return $user->isCashier();
        });


        Gate::define('manage-team-fines', function (User $user) {
            return $user->isAdmin();
        });
        Gate::define('manage-team-fine', function (User $user, Order $order) {
            return $user->isAdmin() && $order->isExecute() && !($order->hasTeamFine() && $order->getBaseTeamFine()->isActive());
        });
        Gate::define('manage-disable-team-fine', function (User $user, Order $order) {
            return $user->isAdmin();
        });
        Gate::define('manage-cross-order', function (User $user, Order $order) {
            return $user->isAdmin() && ($order->isPaid()) && !$order->hasCrossTeamFine();
        });

        Gate::define('manage-personal-fines', function (User $user) {
            return $user->isAdmin();
        });
        Gate::define('manage-personal-fine', function (User $user, User $washer) {
            return $user->isAdmin() && $washer->isWasher() && !$washer->isDelete();
        });

        Gate::define('manage-washers', function (User $user) {
            return $user->isAdmin();
        });
    }
}
