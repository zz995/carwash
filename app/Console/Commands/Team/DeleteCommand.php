<?php

namespace App\Console\Commands\Team;

use App\Entity\Team;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DeleteCommand extends Command
{
    protected $signature = 'teams:delete';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): bool
    {
        DB::table((new Team())->getTable())->delete();

        return true;
    }
}
