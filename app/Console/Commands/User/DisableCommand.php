<?php

namespace App\Console\Commands\User;

use App\Entity\Team;
use App\Entity\User\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DisableCommand extends Command
{
    protected $signature = 'users:disable';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): bool
    {
        $users = User::all();

        /** @var User $user */
        foreach ($users as $user) {
            if (
                $user->isCashier()
                || $user->isAdmin()
                || $user->isOverdrive()
                || $user->isSuperAdmin()
            ) {
                $user->disable();
            }
        }

        return true;
    }
}
