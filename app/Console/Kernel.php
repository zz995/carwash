<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        \App\Console\Commands\Team\DeleteCommand::class,
        \App\Console\Commands\User\DisableCommand::class
    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('teams:delete')->daily();
        $schedule->command('users:disable')->daily();
    }

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
