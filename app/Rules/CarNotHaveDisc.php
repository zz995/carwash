<?php

namespace App\Rules;

use App\Entity\Car;
use App\Entity\DiscountCard\DiscountCard;;
use Illuminate\Contracts\Validation\Rule;

class CarNotHaveDisc implements Rule
{
    /** @var null|Car $car */
    private $car = null;

    public function __construct()
    {

    }

    public function passes($attribute, $value)
    {
        if(is_null($value)) {
            return true;
        }

        /** @var Car|null $car */
        $car = Car::where('number', '=', $value)->first();
        if (is_null($car)) {
            return true;
        }

        $this->car = $car;
        return !$car->hasDiscountCard();
    }

    public function message()
    {
        /** @var DiscountCard $discountCard */
        $discountCard = $this->car->discountCard;
        return trans('validation.carHaveDisc', [
            'number' => $discountCard->number,
            'disc' => $discountCard->disc
        ]);
    }
}
