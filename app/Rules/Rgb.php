<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Rgb implements Rule
{
    public function __construct()
    {

    }

    public function passes($attribute, $value)
    {
        if(is_null($value)) {
            return true;
        }

        return preg_match('/^#[a-zA-Z0-9]{6}$/', $value);
    }

    public function message()
    {
        return trans('validation.color');
    }
}
