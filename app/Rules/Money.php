<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Money implements Rule
{
    public function __construct()
    {

    }

    public function passes($attribute, $value)
    {
        if(is_null($value)) {
            return true;
        }

        return preg_match('/^\d+$/', $value);
    }

    public function message()
    {
        return trans('validation.money');
    }
}
