<?php

namespace App\Entity;

use App\Entity\Order\Order;
use App\Entity\User\LoginHistory;
use App\Entity\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property string $name
 * @property string $desc
 * @property integer $disabled
 * @property string $background_color
 * @property string $font_color
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class ServiceCenter extends Model
{
    protected $fillable = [
        'name', 'background_color', 'font_color', 'desc', 'disabled'
    ];

    protected $casts = [
        'disabled' => 'integer',
    ];

    public function loginHistories()
    {
        return $this->hasMany(LoginHistory::class, 'service_center_id', 'id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'service_center_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'service_center_id', 'id');
    }

    public function teams()
    {
        return $this->hasMany(Team::class, 'service_center_id', 'id');
    }

    public function scopeActive(Builder $query)
    {
        return $query
            ->where('disabled', '=', 0);
    }
}