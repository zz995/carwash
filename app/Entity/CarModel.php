<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property string $name
 * @property int $category
 */
class CarModel extends Model
{
    public const CATEGORY_1 = 1;
    public const CATEGORY_2 = 2;
    public const CATEGORY_3 = 3;
    public const CATEGORY_4 = 4;
    public const CATEGORY_DEFAULT = self::CATEGORY_1;

    protected $fillable = [
        'id', 'name', 'category', 'car_mark_id'
    ];

    public function cars()
    {
        return $this->hasMany(Car::class, 'car_model_id', 'id');
    }

    public function mark()
    {
        return $this->belongsTo(CarMark::class, 'car_mark_id', 'id');
    }

    public function getNameAttribute($value)
    {
        return mb_strtoupper($value);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_strtoupper($value);
    }

    public function scopeForMark(Builder $query, $markId)
    {
        if (isset($markId) && $markId == 0) {
            $carModels = self::orderBy('name')->whereNull('car_mark_id');
        } else {
            $carModels = self::orderByDesc('car_mark_id')
                ->orderBy('name')
                ->where('car_mark_id', '=', $markId)
                ->orWhereNull('car_mark_id');
        }
        return $carModels;
    }

    public function getCategoryName(): string
    {
        $map = $this->getCategoryMapName();
        $name = @$map[$this->category] ?: '';
        return $name;
    }

    public function getCategoryMapName(): array
    {
        return [
            self::CATEGORY_1 => trans('works.field.category1'),
            self::CATEGORY_2 => trans('works.field.category2'),
            self::CATEGORY_3 => trans('works.field.category3'),
            self::CATEGORY_4 => trans('works.field.category4'),
        ];
    }
}