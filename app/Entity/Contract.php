<?php

namespace App\Entity;

use App\Entity\Order\Order;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property string $number
 * @property string $contractor
 * @property Carbon $from
 * @property Carbon $to
 * @property int $disc
 */
class Contract extends Model
{
    protected $fillable = [
        'number', 'contractor', 'from', 'to', 'disc'
    ];

    protected $casts = [
        'from' => 'datetime',
        'to' => 'datetime',
        'disc' => 'integer',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class, 'contract_id', 'id');
    }

    public function hasOrder()
    {
        return !is_null($this->orders()->first());
    }

    public function scopeActive(Builder $query)
    {
        $now = new Carbon('now');
        return $query
            ->where('from', '<=', $now->toDateTimeString())
            ->where('to', '>=', $now->toDateTimeString());
    }

    static public function existContract(string $number): bool
    {
        return !!self::active()->where('number', '=', $number)->count();
    }
}