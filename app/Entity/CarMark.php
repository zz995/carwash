<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */
class CarMark extends Model
{
    protected $fillable = [
        'name', 'category'
    ];

    public function models()
    {
        return $this->hasMany(CarModel::class, 'car_mark_id', 'id');
    }

    public function cars()
    {
        return $this->hasMany(Car::class, 'car_mark_id', 'id');
    }

    public function getNameAttribute($value)
    {
        return mb_strtoupper($value);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = mb_strtoupper($value);
    }
}