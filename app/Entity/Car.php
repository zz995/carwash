<?php

namespace App\Entity;

use App\Entity\DiscountCard\DiscountCard;
use App\Entity\Order\Order;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $number
 * @property integer $car_model_id
 * @property integer $car_mark_id
 * @property integer|null $discount_card_id
 */
class Car extends Model
{
    protected $fillable = [
        'number', 'car_model_id', 'car_mark_id', 'discount_card_id'
    ];

    public function discountCard()
    {
        return $this->belongsTo(DiscountCard::class, 'discount_card_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'car_id', 'id');
    }

    public function clients()
    {
        return $this->belongsToMany(Client::class, 'client-car', 'car_id', 'client_id');
    }

    public function carModel()
    {
        return $this->belongsTo(CarModel::class, 'car_model_id', 'id');
    }

    public function carMark()
    {
        return $this->belongsTo(CarMark::class, 'car_mark_id', 'id');
    }

    public function hasDiscountCard(): bool
    {
        return isset($this->discount_card_id);
    }

    public function isForeignNumber(): bool
    {
        return self::isForeignNumberStatic($this->number);
    }

    static public function isForeignNumberStatic($number): bool
    {
        return !preg_match('/[АВЕКМНОРСТУХ][0-9]{3}[АВЕКМНОРСТУХ]{2}[0-9]{2,3}/ui', $number);
    }
}