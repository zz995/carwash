<?php

namespace App\Entity\CarWash;

use App\Entity\MoneyTrait;
use App\Entity\Order\BasketItem;
use App\Entity\Order\Order;
use App\ObjectValue\MoneyInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property bool $several
 * @property integer $number
 * @property MoneyInterface $category1_price
 * @property MoneyInterface $category1_cost
 * @property MoneyInterface $category1_salary
 * @property MoneyInterface $category2_price
 * @property MoneyInterface $category2_cost
 * @property MoneyInterface $category2_salary
 * @property MoneyInterface $category3_price
 * @property MoneyInterface $category3_cost
 * @property MoneyInterface $category3_salary
 * @property MoneyInterface $category4_price
 * @property MoneyInterface $category4_cost
 * @property MoneyInterface $category4_salary
 */
class Work extends Model
{
    use MoneyTrait;

    public const CATEGORY_1 = 1;
    public const CATEGORY_2 = 2;
    public const CATEGORY_3 = 3;
    public const CATEGORY_4 = 4;

    public $defaultCategoryPrice = self::CATEGORY_1;

    protected $table = 'car_wash_works';

    protected $fillable = [
        'name',
        'number',
        'type_id',
        'several',
        'category1_price', 'category1_cost', 'category1_salary',
        'category2_price', 'category2_cost', 'category2_salary',
        'category3_price', 'category3_cost', 'category3_salary',
        'category4_price', 'category4_cost', 'category4_salary',
    ];

    public function basketItems()
    {
        return $this->hasMany(BasketItem::class, 'work_id', 'id');
    }

    public function type()
    {
        return $this->belongsTo(Type::class, 'type_id', 'id');
    }

    protected function getFieldListToMoney(): array
    {
        return array_merge(...array_map(function($num) {
            return ["category{$num}_price", "category{$num}_cost", "category{$num}_salary",];
        }, [1, 2, 3, 4]));
    }

    public function getDefaultPrice()
    {
        return $this->{'category' . $this->defaultCategoryPrice . '_price'};
    }

    public function getDefaultCost()
    {
        return $this->{'category' . $this->defaultCategoryPrice . '_cost'};
    }

    public function getDefaultSalary()
    {
        return $this->{'category' . $this->defaultCategoryPrice . '_salary'};
    }

    public function resolutionSeveral()
    {
        return $this->several == 1;
    }

    public function defaultCategory($category)
    {
        $this->defaultCategoryPrice = $category;
    }
}