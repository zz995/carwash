<?php

namespace App\Entity\CarWash;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */
class Type extends Model
{
    protected $table = 'car_wash_types';

    protected $fillable = [
        'name'
    ];

    public function works()
    {
        return $this->hasMany(Work::class, 'type_id', 'id');
    }
}