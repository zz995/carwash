<?php

namespace App\Entity;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait DateTrait
{
    public function scopeToday(Builder $query)
    {
        return $query->where($this->timestampField, '>', Carbon::today()->toDateString());
    }

    public function scopeCurrentMonth(Builder $query)
    {
        return $query->where($this->timestampField, '>', Carbon::today()->startOfMonth()->toDateString());
    }

    public function scopeLastMonth(Builder $query)
    {
        return $query
            ->where($this->timestampField, '>', Carbon::today()->startOfMonth()->subMonth()->toDateString())
            ->where($this->timestampField, '<', Carbon::today()->startOfMonth()->toDateString());
    }
}