<?php

namespace App\Entity\DiscountCard;

use App\Entity\Car;
use App\Entity\Client;
use App\Entity\DiscountCard\Accrual\StepsAccrual;
use App\Entity\MoneyTrait;
use App\Entity\Order\Order;
use App\Entity\User\User;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string|null $number
 * @property string|null $phone
 * @property string $name
 * @property integer $admin
 * @property integer $type
 * @property integer $disc
 * @property string $comment
 * @property Carbon|null $issued_at
 * @property MoneyInterface $current_step_accrual
 */
class DiscountCard extends Model
{
    use MoneyTrait;

    public const TYPE_CUMULATIVE = 0;
    public const TYPE_FIXED = 1;

    protected $fillable = [
        'number', 'phone', 'name', 'type', 'disc', 'comment'
    ];

    protected $casts = [
        'disc' => 'integer',
        'issued_at' => 'datetime'
    ];

    protected function getFieldListToMoney(): array
    {
        return ['current_step_accrual'];
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'created_id', 'id');
    }

    public function issued()
    {
        return $this->belongsTo(User::class, 'issued_id', 'id');
    }

    public function cars()
    {
        return $this->hasMany(Car::class, 'discount_card_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'discount_card_id', 'id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    public function typeChangeHistories()
    {
        return $this->hasMany(TypeChangeHistory::class, 'discount_card_id', 'id');
    }

    public function getClientName(): ?string
    {
        if ($this->hasClient()) {
            return $this->client->name;
        }

        return null;
    }

    public function getClientPhone(): ?string
    {
        if ($this->hasClient()) {
            return $this->client->phone;
        }

        return null;
    }

    public function hasClient()
    {
        return isset($this->client_id);
    }

    public function getDisc(): int
    {
        $disc = $this->disc;
        return is_null($disc) ? 0 : $disc;
    }

    public function isCumulative()
    {
        return $this->type == self::TYPE_CUMULATIVE;
    }

    public function isFixed()
    {
        return $this->type == self::TYPE_FIXED;
    }

    public function hasNumber()
    {
        return !!$this->number;
    }

    public function accrual(MoneyInterface $sum)
    {
        $steps = new CalculationSteps(...CalculationStep::all());
        $accrual = new StepsAccrual($steps);
        $accrual->calculation($this, $sum);

        $this->save();
    }

    public function canAdminChange()
    {
        return $this->admin == 1;
    }

    public function fillNumber(string $discountNumber)
    {
        $this->number = mb_strtoupper($discountNumber);
        $this->save();
    }

    public function changeDisc($disc, $type, User $user)
    {
        /** @var TypeChangeHistory $typeChangeHistory */
        $typeChangeHistory = TypeChangeHistory::make([
            'disc' => $this->disc,
            'type' => $this->type,
            'current_step_accrual' => $this->current_step_accrual
        ]);
        $typeChangeHistory->user()->associate($user);
        $typeChangeHistory->discountCard()->associate($this);
        $typeChangeHistory->save();

        $this->disc = $disc;
        $this->type = $type;
        if ($this->isFixed()) {
            $this->current_step_accrual = new Money('0');
        }
        $this->save();
    }
}