<?php

namespace App\Entity\DiscountCard\Accrual;

use App\Entity\DiscountCard\CalculationSteps;
use App\Entity\DiscountCard\DiscountCard;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;

class StepsAccrual implements AccrualInterface
{
    private $steps;

    public function __construct(CalculationSteps $calculationSteps)
    {
        $this->steps = $calculationSteps;
    }

    public function calculation(DiscountCard $discountCard, MoneyInterface $value)
    {
        if (! $discountCard->isCumulative()) {
            return;
        }

        $discountCard->current_step_accrual = $discountCard->current_step_accrual->add($value);
        $this->incDisc($discountCard);
    }

    private function incDisc(DiscountCard $discountCard)
    {
        $step = $this->steps->getStep($discountCard->getDisc());
        $currentAccrual = $discountCard->current_step_accrual;
        if (isset($step) && ($currentAccrual->gt($step->sum) || $currentAccrual->equal($step->sum))) {
            $discountCard->current_step_accrual = $currentAccrual->sub($step->sum);
            $discountCard->disc = $discountCard->getDisc() + 1;

            //dd($discountCard->disc);

            if ($step->sum->gt(new Money('0'))) {
                $this->incDisc($discountCard);
            }
        }
    }
}