<?php

namespace App\Entity\DiscountCard\Accrual;

use App\Entity\DiscountCard\DiscountCard;
use App\ObjectValue\MoneyInterface;

interface AccrualInterface {
    public function calculation(DiscountCard $discountCard, MoneyInterface $value);
}