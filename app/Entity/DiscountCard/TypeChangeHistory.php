<?php

namespace App\Entity\DiscountCard;

use App\Entity\MoneyTrait;
use App\Entity\Order\Order;
use App\Entity\User\User;
use App\ObjectValue\MoneyInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property integer $disc
 * @property integer $type
 * @property MoneyInterface $current_step_accrual
 * @property User $user
 * @property DiscountCard $discountCard
 */
class TypeChangeHistory extends Model
{
    use MoneyTrait;

    protected $table = 'discount_card_type_change_history';

    protected $fillable = [
        'disc', 'type', 'current_step_accrual'
    ];

    protected $casts = [
        'disc' => 'integer',
        'type' => 'integer',
    ];

    protected function getFieldListToMoney(): array
    {
        return ['current_step_accrual'];
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function discountCard()
    {
        return $this->belongsTo(DiscountCard::class, 'discount_card_id', 'id');
    }
}