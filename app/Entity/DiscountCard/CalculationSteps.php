<?php

namespace App\Entity\DiscountCard;


class CalculationSteps
{
    private $steps;

    public function __construct(?CalculationStep ...$calculationSteps)
    {
        $this->steps = $calculationSteps;
    }

    public function getStep(int $disc): ?CalculationStep
    {
        /** @var CalculationStep|null $possibleStep */
        $possibleStep = null;
        foreach ($this->steps as $step) {
            if ($disc >= $step->disc) continue;

            if (is_null($possibleStep)) {
                $possibleStep = $step;
            } elseif ($possibleStep->disc > $step->disc) {
                $possibleStep = $step;
            }
        }

        return $possibleStep;
    }
}