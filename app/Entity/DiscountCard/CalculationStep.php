<?php

namespace App\Entity\DiscountCard;

use App\Entity\MoneyTrait;
use App\Entity\Order\Order;
use App\Entity\User\User;
use App\ObjectValue\MoneyInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property integer $disc
 * @property MoneyInterface $sum
 * @property integer $user_id
 */
class CalculationStep extends Model
{
    use MoneyTrait;

    protected $table = 'discount_card_calculation_steps';

    protected $fillable = [
        'sum', 'disc'
    ];

    protected $casts = [
        'disc' => 'integer',
    ];

    protected function getFieldListToMoney(): array
    {
        return ['sum'];
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}