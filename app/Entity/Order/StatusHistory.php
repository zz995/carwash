<?php

namespace App\Entity\Order;

use App\Entity\User\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property int $status
 * @property Carbon $timestamp
 */
class StatusHistory extends Model
{
    public $timestamps = false;

    protected $table = 'order_status_histories';

    protected $casts = [
        'timestamp' => 'datetime',
    ];

    protected $fillable = [
        'status', 'timestamp'
    ];

    public function initiator()
    {
        return $this->belongsTo(User::class, 'initiator_id', 'id');
    }

    public function scopeDefaultOrder(Builder $query)
    {
        return $query->orderBy('id');
    }
}