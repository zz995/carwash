<?php

namespace App\Entity\Order;

use App\Entity\DateTrait;
use App\Entity\MoneyTrait;
use App\Entity\Team;
use App\Entity\User\Accrual;
use App\Entity\Car;
use App\Entity\CarWash\Work;
use App\Entity\Contract;
use App\Entity\Client;
use App\Entity\DiscountCard\DiscountCard;
use App\Entity\Order\Exception\ContractNotExist;
use App\Entity\ServiceCenter;
use App\Entity\TemporaryTeam;
use App\Entity\TemporaryTeamUser;
use App\Entity\User\Fine\TeamFine;
use App\Entity\User\User;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use App\Services\Dto\PaymentDto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;


/**
 * @property int $id
 * @property int $status
 * @property string $team_name
 * @property string $comment
 * @property string $admin_comment
 * @property int $number_key
 * @property int $payment_type
 * @property int $discount_size
 * @property int|null $contract_id
 * @property int|null $contract_id_applying_discount
 * @property int $contract_discount_size
 * @property int $applying_discount_size_from_contract
 * @property int $service_center_id
 * @property int $user_appoint_id
 * @property MoneyInterface $price
 * @property DiscountCard|null $discountCard
 * @property int $team_type;
 * @property Collection $employees;
 * @property bool $services_standard_price;
 * @property int|null $time_execute;
 * @property int|null $time_queue;
 */
class Order extends Model
{
    use DateTrait;
    use MoneyTrait;

    private $timestampField = 'created_at';

    public const STATUS_EXECUTE = 0;
    public const STATUS_PAID = 1;
    public const STATUS_QUEUE = 2;
    public const STATUS_EXECUTED = 3;
    public const STATUS_PAYMENT_REFUSAL = 4;
    public const STATUS_CANCELED = 5;

    public const PAYMENT_CASH = 1;
    public const PAYMENT_CARD = 2;
    public const PAYMENT_CONTRACT = 3;

    protected function getFieldListToMoney(): array
    {
        return ['price'];
    }

    protected $fillable = [
        'number_key', 'team_name', 'admin_comment', 'discount_size'
    ];

    protected $casts = [
        'discount_size' => 'integer',
        'contract_discount_size' => 'integer',
        'applying_discount_size_from_contract' => 'integer',
        'time_queue' => 'integer',
        'time_execute' => 'integer',
        'services_standard_price' => 'bool',
    ];

    public function discountCard()
    {
        return $this->belongsTo(DiscountCard::class, 'discount_card_id', 'id');
    }

    public function contract()
    {
        return $this->belongsTo(Contract::class, 'contract_id', 'id');
    }

    public function contractApplyingDiscount()
    {
        return $this->belongsTo(Contract::class, 'contract_id_applying_discount', 'id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id', 'id');
    }

    public function car()
    {
        return $this->belongsTo(Car::class, 'car_id', 'id');
    }

    public function admin()
    {
        return $this->belongsTo(User::class, 'admin_id', 'id');
    }

    public function employees()
    {
        return $this->belongsToMany(User::class, 'order-user', 'order_id', 'user_id');
    }

    public function basketItems()
    {
        return $this->hasMany(BasketItem::class, 'order_id', 'id');
    }

    public function serviceCenter()
    {
        return $this->belongsTo(ServiceCenter::class, 'service_center_id', 'id');
    }

    public function temporaryTeamUsers()
    {
        return $this->hasMany(TemporaryTeamUser::class, 'order_id', 'id');
    }

    public function accrual()
    {
        return $this->morphMany(Accrual::class, 'initiator');
    }

    public function teamFines()
    {
        return $this->hasMany(TeamFine::class, 'order_id', 'id');
    }

    public function statusHistory()
    {
        return $this->hasMany(StatusHistory::class, 'order_id', 'id');
    }

    public function getBaseTeamFine(): ?TeamFine
    {
        return $this->teamFines()->where('type', '=', TeamFine::TYPE_BASE)->first();
    }

    public function getCrossTeamFine(): ?TeamFine
    {
        return $this->teamFines->where('type', '=', TeamFine::TYPE_CROSS)->first();
    }

    public function hasTeamFine()
    {
        return $this->getBaseTeamFine() !== null;
    }

    public function hasCrossTeamFine()
    {
        return $this->getCrossTeamFine() !== null;
    }

    public function changeStatus($statusConst, User $initiator)
    {
        /** @var StatusHistory $status */
        $status = $this->statusHistory()->make(['status' => $statusConst, 'timestamp' => Carbon::now()->toDateTimeString()]);
        $status->initiator()->associate($initiator);
        $status->save();

        $oldStatus = $this->status;
        $this->status = $statusConst;

        if ($oldStatus != self::STATUS_PAID && $this->status == self::STATUS_PAID) {
            $this->time_execute = $this->getTimeExecuteInSecond();
        }

        if ($oldStatus == self::STATUS_QUEUE && $this->status == self::STATUS_EXECUTE) {
            $this->time_queue = $this->getTimeBetweenStatusesInSecond(self::STATUS_QUEUE, self::STATUS_EXECUTE);
        }

        $this->save();
    }

    public function hasTemporaryTeam()
    {
        return $this->team_type == Team::TYPE_TEMPORARY;
    }

    public function isExecute()
    {
        return $this->status == self::STATUS_EXECUTE;
    }

    public function isExecuted()
    {
        return $this->status == self::STATUS_EXECUTED;
    }

    public function isPaid()
    {
        return $this->status == self::STATUS_PAID;
    }

    public function isQueue()
    {
        return $this->status == self::STATUS_QUEUE;
    }

    public function isPaymentRefusal()
    {
        return $this->status == self::STATUS_PAYMENT_REFUSAL;
    }

    public function isCanceled()
    {
        return $this->status == self::STATUS_CANCELED;
    }

    public function isCompleted()
    {
        return $this->isPaid() || $this->isPaymentRefusal() || $this->isCanceled();
    }

    public function addBasketItem(int $workId, int $count, MoneyInterface $price, MoneyInterface $defaultPrice, ?MoneyInterface $cost, ?MoneyInterface $salary)
    {
        $this->basketItems()->create([
            'work_id' => $workId,
            'count' => $count,
            'price' => $price,
            'default_price' => $defaultPrice,
            'cost' => $cost,
            'salary' => $salary,
        ]);
    }

    public function hasDiscountCard()
    {
        return isset($this->discount_card_id);
    }

    public function hasContract()
    {
        return isset($this->contract_id);
    }

    public function basePrice($round = true): MoneyInterface
    {
        $price = new Money(0);
        /** @var BasketItem $item */
        foreach ($this->basketItems as $item) {
            $price = $price->add($item->price->multiply($item->count));
        }

        if ($round) {
            $price = $price->round();
        }

        return $price;
    }

    public function price(): MoneyInterface
    {
        $price = $this->basePrice(false);
        $price = $price->percent(100 - $this->getDisc());
        $price = $price->round();
        return $price;
    }

    public function paymentPrice(): MoneyInterface
    {
        if (is_null($this->price)) {
            return new Money('0');
        }

        return $this->price;
    }

    public function isOtherPaymentPrice()
    {
        return !$this->paymentPrice()->equal($this->price());
    }

    public function scopeCurrentDay(Builder $query)
    {
        return $query->where('created_at', '>', Carbon::today()->toDateString());
    }

    public function scopeActual(Builder $query)
    {
        return $query
            ->where(function (Builder $query) {
                $query
                    ->currentDay()
                    ->orWhereNotIn('status', [Order::STATUS_PAID, Order::STATUS_CANCELED, Order::STATUS_PAYMENT_REFUSAL]);
            });
    }

    public function scopeOur(Builder $query, User $user)
    {
        return $query->where('service_center_id', '=', $user->service_center_id);
    }

    public function scopeByServiceCenter(Builder $query, ServiceCenter $serviceCenter)
    {
        return $query->where('service_center_id', '=', $serviceCenter->id);
    }

    public function scopeSortForAppointTeam(Builder $query)
    {
        $queue = intval(self::STATUS_QUEUE);
        $paid = intval(self::STATUS_PAID);
        $execute = intval(self::STATUS_EXECUTE);
        $executed = intval(self::STATUS_EXECUTED);
        $paymentRefusal = intval(self::STATUS_PAYMENT_REFUSAL);
        $canceled = intval(self::STATUS_CANCELED);

        $rawSortSql = "
            CASE 
                WHEN status=$queue THEN 1 
                WHEN status=$execute THEN 5
                WHEN status=$executed THEN 7
                WHEN status=$paid OR status=$paymentRefusal OR status=$canceled THEN 10
            END,
            id
        ";
        $rawSortSql = str_replace(PHP_EOL, ' ', $rawSortSql);

        return $query->orderByRaw($rawSortSql);
    }

    public function scopeSortForPaidOrder(Builder $query)
    {
        $queue = intval(self::STATUS_QUEUE);
        $paid = intval(self::STATUS_PAID);
        $execute = intval(self::STATUS_EXECUTE);
        $executed = intval(self::STATUS_EXECUTED);
        $paymentRefusal = intval(self::STATUS_PAYMENT_REFUSAL);
        $canceled = intval(self::STATUS_CANCELED);

        $rawSortSql = "
            CASE 
                WHEN status=$execute OR status=$executed THEN 1 
                WHEN status=$queue THEN 5
                WHEN status=$paid OR status=$paymentRefusal OR status=$canceled THEN 10
            END,
            id DESC
        ";
        $rawSortSql = str_replace(PHP_EOL, ' ', $rawSortSql);

        return $query->orderByRaw($rawSortSql);
    }

    public function scopePaymentType(Builder $query, int $paymentType)
    {
        return $query->where('payment_type', '=', $paymentType);
    }

    public function isContractPayment()
    {
        return $this->payment_type === self::PAYMENT_CONTRACT;
    }

    public function isApplyingDiscountFromContract()
    {
        return isset($this->contract_id_applying_discount) && !$this->isContractPayment();
    }

    public function getDisc(): int
    {
        if ($this->isContractPayment()) {
            return $this->contract_discount_size;
        }

        if ($this->isApplyingDiscountFromContract()) {
            return $this->applying_discount_size_from_contract;
        }

        return $this->discount_size;
    }

    public function cancel(User $superAdmin)
    {
        $this->price = new Money(0);
        $this->save();

        $this->changeStatus(Order::STATUS_CANCELED, $superAdmin);
    }

    public function refusal(string $comment, User $cashier)
    {
        $this->price = new Money(0);
        $this->comment = $comment;
        $this->save();

        $this->changeStatus(Order::STATUS_PAYMENT_REFUSAL, $cashier);
    }

    public function getPaymentRation(): string
    {
        $paymentPrice = $this->price;
        $setPrice = $this->price();

        if ($setPrice->equal(new Money(0))) {
            return 1;
        }

        return $paymentPrice->ration($setPrice);
    }

    public function payment(PaymentDto $paymentDto, User $cashier, ?Contract $contract, bool $isRepayment = false)
    {
        if ($paymentDto->type === self::PAYMENT_CONTRACT) {
            if (is_null($contract)) {
                throw new ContractNotExist();
            }

            $this->contract_discount_size = $contract->disc;
            $this->contract()->associate($contract);
        } elseif (isset($contract)) {
            $this->applying_discount_size_from_contract = $contract->disc;
            $this->contractApplyingDiscount()->associate($contract);
        }

        $oldPaymentType = $this->payment_type;
        $this->payment_type = $paymentDto->type;
        $this->comment = $paymentDto->comment;

        if (
            isset($oldPaymentType)
            && (
                (
                    in_array($this->payment_type, [self::PAYMENT_CASH, self::PAYMENT_CARD])
                    && $oldPaymentType == self::PAYMENT_CONTRACT
                )
                || (
                    $this->payment_type == self::PAYMENT_CONTRACT
                )
            )
        ) {
            $this->price = $this->price();
        } else {
            $this->price = $paymentDto->sum;
        }

        $this->save();

        $this->changeStatus(Order::STATUS_PAID, $cashier);
    }

    public function getTimeExecuteInSecond(): ?int
    {
        $timeExecute = $this->getTimeBetweenStatusesInSecond(
            [self::STATUS_QUEUE, self::STATUS_EXECUTE],
            [self::STATUS_PAID, self::STATUS_PAYMENT_REFUSAL, self::STATUS_CANCELED]
        );

        return $timeExecute;
    }

    public function getTimeBetweenStatusesInSecond($firstStatus, $secondStatus): ?int
    {
        $startStatus = $this->getStatusFromHistory($firstStatus);
        $endStatus = $this->getStatusFromHistory($secondStatus);

        if (is_null($startStatus) || is_null($endStatus)) {
            return null;
        }

        $startTime = $startStatus->timestamp;
        $endTime = $endStatus->timestamp;

        return $endTime->diffInSeconds($startTime);
    }

    public function getBasketItemsWithNonStandardPrice(): Collection
    {
        return self::basketItems()->where('price', '<>', DB::raw("`default_price`"))->get();
    }

    public function temporaryReturnToHistoryStatus(StatusHistory $statusHistory): Order
    {
        $order = clone $this;
        $order->status = $statusHistory->status;
        return $order;
    }

    private function getStatusFromHistory($statuses): ?StatusHistory
    {
        if (! is_array($statuses)) {
            $statuses = [$statuses];
        }

        foreach ($statuses as $status) {
            $history = $this
                ->statusHistory()
                ->orderBy('id')
                ->where('status', $status)
                ->first();

            if (isset($history)) {
                return $history;
            }
        }

        return null;
    }

    static public function getStatuses()
    {
        return [
            ['label' => trans('order.statuses.execute'), 'value' => self::STATUS_EXECUTE],
            ['label' => trans('order.statuses.paid'), 'value' => self::STATUS_PAID],
        ];
    }

    static public function getFullStatuses()
    {
        return [
            ['label' => trans('order.statuses.execute'), 'value' => self::STATUS_EXECUTE],
            ['label' => trans('order.statuses.queue'), 'value' => self::STATUS_QUEUE],
            ['label' => trans('order.statuses.paid'), 'value' => self::STATUS_PAID],
            ['label' => trans('order.statuses.all'), 'value' => null],
        ];
    }

    static public function getAllStatuses()
    {
        return [
            ['label' => trans('order.status.execute'), 'value' => self::STATUS_EXECUTE],
            ['label' => trans('order.status.paid'), 'value' => self::STATUS_PAID],
            ['label' => trans('order.status.queue'), 'value' => self::STATUS_QUEUE],
            ['label' => trans('order.status.executed'), 'value' => self::STATUS_EXECUTED],
            ['label' => trans('order.status.paymentRefusal'), 'value' => self::STATUS_PAYMENT_REFUSAL],
            ['label' => trans('order.status.canceled'), 'value' => self::STATUS_CANCELED],
        ];
    }
}