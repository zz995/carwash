<?php

namespace App\Entity\Order;

use App\Entity\Car;
use App\Entity\CarWash\Work;
use App\Entity\MoneyTrait;
use App\Entity\User\User;
use App\ObjectValue\MoneyInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property int $order_id
 * @property int $work_id
 * @property int $count
 * @property MoneyInterface $price
 * @property MoneyInterface $default_price
 * @property MoneyInterface $cost
 * @property MoneyInterface $salary
 */
class BasketItem extends Model
{
    use MoneyTrait;

    protected $table = 'order-basket';

    protected $fillable = [
        'order_id', 'work_id', 'count', 'price', 'default_price', 'cost', 'salary'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function work()
    {
        return $this->belongsTo(Work::class, 'work_id', 'id');
    }

    public function hasCost(): bool
    {
        return isset($this->cost);
    }

    public function hasFixedSalary(): bool
    {
        return isset($this->salary);
    }

    protected function getFieldListToMoney(): array
    {
        return ['price', 'default_price', 'cost', 'salary'];
    }
}