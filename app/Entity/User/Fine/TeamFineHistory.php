<?php

namespace App\Entity\User\Fine;

use App\Entity\DateTrait;
use App\Entity\MoneyTrait;
use App\Entity\ServiceCenter;
use App\Entity\User\Accrual;
use App\Entity\User\User;
use App\ObjectValue\MoneyInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property MoneyInterface $value
 * @property Carbon $timestamp
 * @property string $comment
 * @property int $initiator_id
 * @property User $initiator
 * @property int $team_fine_id
 */
class TeamFineHistory extends Model
{
    use MoneyTrait;

    public $timestamps = false;

    protected $fillable = [
        'value', 'timestamp', 'comment'
    ];

    protected $casts = [
        'timestamp' => 'datetime',
    ];

    protected function getFieldListToMoney(): array
    {
        return ['value'];
    }

    public function initiator()
    {
        return $this->belongsTo(User::class, 'initiator_id', 'id');
    }
}