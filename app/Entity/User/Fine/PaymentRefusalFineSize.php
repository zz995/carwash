<?php

namespace App\Entity\User\Fine;

use App\Entity\DateTrait;
use App\Entity\MoneyTrait;
use App\Entity\Order\Order;
use App\Entity\ServiceCenter;
use App\Entity\User\Accrual;
use App\Entity\User\User;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property MoneyInterface $admin_fine
 * @property MoneyInterface $washer_fine
 * @property int $initiator_id
 */
class PaymentRefusalFineSize extends Model
{
    use MoneyTrait;

    protected $table = 'payment_refusal_fine_size';

    protected $fillable = [
        'admin_fine', 'washer_fine'
    ];

    private const ACTIVE_ID = 1;

    protected $attributes = [
        'id' => self::ACTIVE_ID,
    ];

    protected function getFieldListToMoney(): array
    {
        return ['admin_fine', 'washer_fine'];
    }

    public function initiator()
    {
        return $this->belongsTo(User::class, 'initiator_id', 'id');
    }

    public function getAdminFineAttribute($value)
    {
        if (is_null($value)) {
            return new Money(0);
        }

        return $value;
    }

    public function getWasherFineAttribute($value)
    {
        if (is_null($value)) {
            return new Money(0);
        }

        return $value;
    }

    public function totalSum(int $washerCount = 1): MoneyInterface
    {
        return (new Money(0))
            ->add($this->admin_fine)
            ->add($this->washer_fine->multiply($washerCount));
    }

    static public function getInstance(): PaymentRefusalFineSize
    {
        /** @var PaymentRefusalFineSize $fineSize */
        $fineSize = PaymentRefusalFineSize::findOrNew(PaymentRefusalFineSize::ACTIVE_ID);
        return $fineSize;
    }
}