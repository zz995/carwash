<?php

namespace App\Entity\User\Fine;

use App\Entity\DateTrait;
use App\Entity\MoneyTrait;
use App\Entity\Order\Order;
use App\Entity\ServiceCenter;
use App\Entity\User\Accrual;
use App\Entity\User\User;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * @property int $id
 * @property MoneyInterface $value
 * @property integer $type
 * @property integer $user_id
 */
class TeamFine extends Model
{
    use MoneyTrait;
    use DateTrait;

    public const TYPE_BASE = 0;
    public const TYPE_CROSS = 1;
    public const TYPE_REFUSAL = 2;

    private $timestampField = 'created_at';

    protected $fillable = [
        'value'
    ];

    protected function getFieldListToMoney(): array
    {
        return ['value'];
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'team_fines_users', 'team_fine_id', 'user_id');
    }

    public function accrual()
    {
        return $this->morphMany(Accrual::class, 'initiator');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function histories()
    {
        return $this->hasMany(TeamFineHistory::class, 'team_fine_id', 'id');
    }

    public function scopeByServiceCenter(Builder $query, ServiceCenter $serviceCenter)
    {
        return $query->whereIn('order_id', function ($query) use ($serviceCenter) {
            $query
                ->select('id')
                ->from((new Order())->getTable())
                ->where('service_center_id', $serviceCenter->id);
        });
    }

    public function disable(string  $comment, User $initiator)
    {
        $employees = $this->users()->get();
        $employeesCount = count($employees);
        $value = $this->value->division($employeesCount);
        /** @var User $employee */
        foreach ($employees as $employee) {
            $employee->increaseBalance($value);
        }
        $this->value = new Money(0);
        $this->save();
        $this->accrual()->delete();
        $this->saveToHistory($comment, $initiator);
    }

    public function saveToHistory(string $comment, User $initiator)
    {
        /** @var TeamFineHistory $history */
        $history = $this->histories()->make();
        $history->value = $this->value;
        $history->timestamp = Carbon::now()->toDateTimeString();
        $history->comment = $comment;
        $history->initiator()->associate($initiator);
        $history->save();
    }

    public function lastComment(): ?string
    {
        $this->loadMissing('histories');
        /** @var TeamFineHistory $history */
        $history = $this->histories->last();
        if (isset($history)) {
            return $history->comment;
        } else {
            return null;
        }
    }

    public function isActive(): bool
    {
        return !$this->value->equal(new Money(0));
    }

    public function isBase(): bool
    {
        return $this->type = self::TYPE_BASE;
    }

    public function isCross(): bool
    {
        return $this->type = self::TYPE_CROSS;
    }

    public function isRefusal(): bool
    {
        return $this->type = self::TYPE_REFUSAL;
    }

    static public function register(Order $order, Collection $employees, MoneyInterface $sum, User $initiator, string $reason, int $type): self
    {
        $teamFine = new self();
        $teamFine->type = $type;
        $teamFine->order()->associate($order);
        $teamFine->value = $sum;
        $teamFine->save();
        $teamFine->users()->sync($employees);
        $teamFine->saveToHistory($reason, $initiator);

        return $teamFine;
    }
}