<?php

namespace App\Entity\User\Fine;

use App\Entity\DateTrait;
use App\Entity\MoneyTrait;
use App\Entity\ServiceCenter;
use App\Entity\User\Accrual;
use App\Entity\User\User;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property MoneyInterface $value
 * @property int $user_id
 * @property User $user
 * @property int $service_center_id
 */
class PersonalFine extends Model
{
    use MoneyTrait;
    use DateTrait;

    private $timestampField = 'created_at';

    protected $fillable = [
        'value'
    ];

    protected function getFieldListToMoney(): array
    {
        return ['value'];
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function accrual()
    {
        return $this->morphMany(Accrual::class, 'initiator');
    }

    public function serviceCenter()
    {
        return $this->belongsTo(ServiceCenter::class, 'service_center_id', 'id');
    }

    public function histories()
    {
        return $this->hasMany(PersonalFineHistory::class, 'personal_fine_id', 'id');
    }

    public function scopeByServiceCenter(Builder $query, ServiceCenter $serviceCenter)
    {
        return $query->where('service_center_id', '=', $serviceCenter->id);
    }

    public function disable(string  $comment, User $initiator)
    {
        $this->user->increaseBalance($this->value);
        $this->value = new Money(0);
        $this->save();
        $this->accrual()->delete();
        $this->saveToHistory($comment, $initiator);
    }

    public function saveToHistory(string $comment, User $initiator)
    {
        /** @var PersonalFineHistory $history */
        $history = $this->histories()->make();
        $history->value = $this->value;
        $history->timestamp = Carbon::now()->toDateTimeString();
        $history->comment = $comment;
        $history->initiator()->associate($initiator);
        $history->save();
    }

    public function lastComment(): ?string
    {
        $this->loadMissing('histories');
        /** @var PersonalFineHistory $history */
        $history = $this->histories->last();
        if (isset($history)) {
            return $history->comment;
        } else {
            return null;
        }
    }

    public function isActive(): bool
    {
        return !$this->value->equal(new Money(0));
    }
}