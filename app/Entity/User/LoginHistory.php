<?php

namespace App\Entity\User;

use App\Entity\ServiceCenter;
use App\Entity\User\Exception\SalaryIncorectException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;
use Carbon\Carbon;

/**
 * @property int $id
 * @property integer $type
 * @property integer $service_center_id
 * @property Carbon $created_at
 */
class LoginHistory extends Model
{
    public const TYPE_LOGIN = 1;
    public const TYPE_LOGOUT = 2;

    public $timestamps = false;

    protected $table = 'login_history';

    protected $fillable = [
        'type'
    ];

    protected $casts = [
        'created_at' => 'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function serviceCenter()
    {
        return $this->belongsTo(ServiceCenter::class, 'service_center_id', 'id');
    }

    public function isLogin()
    {
        return $this->type === self::TYPE_LOGIN;
    }

    public function isLogout()
    {
        return $this->type === self::TYPE_LOGOUT;
    }
}