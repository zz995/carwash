<?php

namespace App\Entity\User;

use App\Entity\DateTrait;
use App\Entity\MoneyTrait;
use App\Entity\Order\Order;
use App\Entity\ServiceCenter;
use App\Entity\User\Fine\PersonalFine;
use App\Entity\User\Fine\TeamFine;
use App\ObjectValue\MoneyInterface;
use App\Services\Dto\DateRangeDto;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property MoneyInterface $value
 * @property Carbon $timestamp
 * @property integer $user_id
 * @property integer $initiator_type
 * @property integer|null $initiator_id
 * @property User $user
 */
class Accrual extends Model
{
    use DateTrait;
    use MoneyTrait;

    private $timestampField = 'timestamp';

    public const TYPE_ACCRUAL_BY_ORDER = 1;
    public const TYPE_PERSONAL_FINE = 2;
    public const TYPE_TEAM_FINE = 3;
    public const TYPE_DAILY_ACCRUAL = 4;
    public const TYPE_SALARY = 5;

    public $timestamps = false;

    protected $fillable = [
        'value'
    ];

    protected $casts = [
        'timestamp' => 'datetime',
    ];

    protected function getFieldListToMoney(): array
    {
        return ['value'];
    }

    static protected function boot()
    {
        Relation::morphMap([
            self::TYPE_ACCRUAL_BY_ORDER => Order::class,
            self::TYPE_PERSONAL_FINE => PersonalFine::class,
            self::TYPE_TEAM_FINE => TeamFine::class,
            self::TYPE_SALARY => Salary::class,
        ]);
    }

    public function initiator()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function scopeSalaryByServiceCenter(Builder $query, ServiceCenter $serviceCenter)
    {
        return $query
            ->where('initiator_type', '=', self::TYPE_ACCRUAL_BY_ORDER)
            ->whereIn('initiator_id', function ($query) use ($serviceCenter) {
                $query
                    ->select('id')
                    ->from((new Order())->getTable())
                    ->where('service_center_id', $serviceCenter->id);
            });
    }

    public function isAccrualByOrder()
    {
        return $this->initiator_type == self::TYPE_ACCRUAL_BY_ORDER;
    }

    public function isAccrualByPersonalFine()
    {
        return $this->initiator_type == self::TYPE_PERSONAL_FINE;
    }

    public function isAccrualByTeamFine()
    {
        return $this->initiator_type == self::TYPE_TEAM_FINE;
    }

    public function isAccrualByDaily()
    {
        return $this->initiator_type == self::TYPE_DAILY_ACCRUAL;
    }

    public function isAccrualBySalary()
    {
        return $this->initiator_type == self::TYPE_SALARY;
    }

    public function scopeByEmployee(Builder $query, User $employee, ?DateRangeDto $dataRange)
    {
        $query = $query->where('user_id', $employee->id);
        if (isset($dataRange)) {
            $query = $query->whereBetween('timestamp', $dataRange->getRange());
        }
        $query = $query->orderByDesc('id');
    }

    static public function byOrder(MoneyInterface $value, User $user, Order $order)
    {
        $accrual = new Accrual();
        $accrual->value = $value;
        $accrual->timestamp = new Carbon('now');
        $accrual->user()->associate($user);
        $accrual->initiator_type = Accrual::TYPE_ACCRUAL_BY_ORDER;
        $accrual->initiator()->associate($order);
        $accrual->save();
    }

    static public function byDailyAccrual(MoneyInterface $value, User $user)
    {
        $accrual = new Accrual();
        $accrual->value = $value;
        $accrual->timestamp = new Carbon('now');
        $accrual->user()->associate($user);
        $accrual->initiator_type = Accrual::TYPE_DAILY_ACCRUAL;
        $accrual->save();
    }

    static public function byTeamFine(MoneyInterface $value, User $user, TeamFine $fine)
    {
        $accrual = new Accrual();
        $accrual->value = $value;
        $accrual->timestamp = new Carbon('now');
        $accrual->user()->associate($user);
        $accrual->initiator_type = Accrual::TYPE_TEAM_FINE;
        $accrual->initiator()->associate($fine);
        $accrual->save();
    }

    static public function byPersonalFine(MoneyInterface $value, User $user, PersonalFine $fine)
    {
        $accrual = new Accrual();
        $accrual->value = $value;
        $accrual->timestamp = new Carbon('now');
        $accrual->user()->associate($user);
        $accrual->initiator_type = Accrual::TYPE_PERSONAL_FINE;
        $accrual->initiator()->associate($fine);
        $accrual->save();
    }

    static public function byPayout(MoneyInterface $value, User $user, Salary $salary)
    {
        $accrual = new Accrual();
        $accrual->value = $value;
        $accrual->timestamp = new Carbon('now');
        $accrual->user()->associate($user);
        $accrual->initiator_type = Accrual::TYPE_SALARY;
        $accrual->initiator()->associate($salary);
        $accrual->save();
    }

    static public function getTypeMap(): array
    {
        return [
            self::TYPE_ACCRUAL_BY_ORDER => trans('salaryBreakdown.type.by_order'),
            self::TYPE_PERSONAL_FINE => trans('salaryBreakdown.type.by_personal_fine'),
            self::TYPE_TEAM_FINE => trans('salaryBreakdown.type.by_team_fine'),
            self::TYPE_DAILY_ACCRUAL => trans('salaryBreakdown.type.by_daily'),
            self::TYPE_SALARY => trans('salaryBreakdown.type.by_salary'),
        ];
    }
}