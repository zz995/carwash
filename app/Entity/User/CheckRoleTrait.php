<?php

namespace App\Entity\User;

trait CheckRoleTrait
{
    public function isCashier()
    {
        return $this->getRoleId() == Role::CASHIER;
    }

    public function isWasher()
    {
        return $this->getRoleId() == Role::WASHER;
    }

    public function isOverdrive()
    {
        return $this->isWasher() && $this->overdrive;
    }

    public function isAdmin()
    {
        return $this->getRoleId() == Role::ADMIN;
    }

    public function isSuperAdmin()
    {
        return $this->getRoleId() == Role::SUPER_ADMIN;
    }

    protected function getRoleId()
    {
        return $this->id;
    }
}