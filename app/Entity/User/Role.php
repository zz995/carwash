<?php

namespace App\Entity\User;

use App\Entity\User\Exception\SalaryIncorectException;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;

/**
 * @property int $id
 * @property string $name
 * @property integer $salary
 */
class Role extends Model
{
    use CheckRoleTrait;

    public const WASHER = 4;
    public const CASHIER = 3;
    public const ADMIN = 2;
    public const SUPER_ADMIN = 1;

    protected $fillable = [
        'name'
    ];

    public function users()
    {
        return $this->hasMany(User::class, 'role_id', 'id');
    }

    public function hasSalary()
    {
        return isset($this->salary);
    }

    public function changeSalary(int $salary)
    {
        if (
            $this->isSuperAdmin()
            || $this->isAdmin() && ($salary < 0 || $salary > 100)
            || $this->isWasher() && ($salary < 0 || $salary > 100)
            || $this->isCashier() && ($salary < 0 || $salary > 20000)
        ) {
            throw new SalaryIncorectException();
        }

        $this->salary = $salary;
        $this->save();
    }

    public static function allList(): array
    {
        $roles = Role::whereIn('id', [Role::WASHER, Role::CASHIER, Role::ADMIN, Role::SUPER_ADMIN])->get();
        $map = [];

        foreach ($roles as $role) {
            $map[$role->id] = $role->name;
        }

        return $map;
    }

    public static function baseList(): array
    {
        $roles = Role::whereIn('id', [Role::WASHER, Role::CASHIER, Role::ADMIN])->get();
        $map = [];

        foreach ($roles as $role) {
            $map[$role->id] = $role->name;
        }

        return $map;
    }

    public static function washerSalary()
    {
        return self::find(Role::WASHER)->salary;
    }

    public static function adminSalary()
    {
        return self::find(Role::ADMIN)->salary;
    }

    public static function cashierSalary(): MoneyInterface
    {
        return new Money(self::find(Role::CASHIER)->salary);
    }
}