<?php

namespace App\Entity\User;

use App\Entity\CarWash\Type;
use App\Entity\MoneyTrait;
use App\Entity\Order\Order;
use App\Entity\ServiceCenter;
use App\Entity\Team;
use App\Entity\TemporaryTeamUser;
use App\Entity\User\Exception\ImpossibleOverdriveException;
use App\Entity\User\Exception\NeedLoginException;
use App\Entity\User\Exception\NeedPasswordException;
use App\Entity\User\Exception\NeedSelectServiceCenterException;
use App\Entity\User\Exception\UserDeletedException;
use App\Entity\User\Exception\WrongPasswordException;
use App\Entity\User\Fine\PersonalFine;
use App\Entity\User\Fine\TeamFine;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property string $email
 * @property array $phones
 * @property string $password
 * @property string $login
 * @property integer $role_id
 * @property integer $service_center_id
 * @property string $status
 * @property bool $overdrive
 * @property MoneyInterface $balance
 */
class User extends Authenticatable
{
    use Notifiable;
    use CheckRoleTrait;
    use MoneyTrait;

    public const STATUS_DISABLED = 'disabled';
    public const STATUS_ACTIVE = 'active';
    public const STATUS_DELETE = 'delete';

    protected $fillable = [
        'name', 'surname', 'patronymic', 'phones', 'email', 'login', 'password', 'status', 'role_id', 'overdrive'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'overdrive' => 'boolean',
        'phones' => 'array',
    ];

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order-user', 'user_id', 'order_id');
    }

    public function teams()
    {
        return $this->belongsToMany(Team::class, 'team-user', 'user_id', 'team_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id', 'id');
    }

    public function loginHistory()
    {
        return $this->hasMany(LoginHistory::class, 'user_id', 'id');
    }

    public function serviceCenter()
    {
        return $this->belongsTo(ServiceCenter::class, 'service_center_id', 'id');
    }

    public function teamFines()
    {
        return $this->belongsToMany(TeamFine::class, 'team_fines_users', 'user_id', 'team_fine_id');
    }

    public function temporaryTeamUser()
    {
        return $this->hasOne(TemporaryTeamUser::class, 'user_id', 'id');
    }

    public function accrual()
    {
        return $this->hasMany(Accrual::class, 'user_id', 'id');
    }

    public function canLogin()
    {
        return $this->isSuperAdmin() || $this->isAdmin() || $this->isCashier() || $this->isOverdrive();
    }

    public function isDisabled()
    {
        return $this->status === self::STATUS_DISABLED;
    }

    public function isActive()
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public function isDelete()
    {
        return $this->status === self::STATUS_DELETE;
    }

    protected function getRoleId()
    {
        return $this->role_id;
    }

    public function getFio(): Fio
    {
        return new Fio($this->surname, $this->name, $this->patronymic);
    }

    public function getTeam(): ?Team
    {
        return $this->teams->first();
    }

    public function getCurrentServiceCenter(): ?ServiceCenter
    {
        if ($this->getTeam() !== null) {
            /** @var Team $team */
            $team = $this->getTeam();
            $serviceCenter = $team->serviceCenter;
        } else {
            $serviceCenter = $this->serviceCenter;
        }

        return $serviceCenter;
    }

    public function currentEqualConstantServiceCenter(): bool
    {
        $current = $this->getCurrentServiceCenter();
        $constant = $this->serviceCenter;

        if (is_null($current) && is_null($constant)) {
            return true;
        }

        if (is_null($current) || is_null($constant)) {
            return false;
        }

        return $current->id == $constant->id;
    }

    public function remove()
    {
        $this->teams()->detach();

        $this->status = self::STATUS_DELETE;
        $this->save();

        event('auth.logout', [$this]);
    }

    public function recovery()
    {
        $this->status = self::STATUS_DISABLED;
        $this->save();
    }

    public function disable()
    {
        $this->status = self::STATUS_DISABLED;
        $this->save();

        event('auth.logout', [$this]);
    }

    public function changePassword(string $new, ?string $current)
    {
        if ($this->isSuperAdmin() && $this->comparePassword($current)) {
            throw new WrongPasswordException();
        }

        $this->newPassword($new);
        $this->status = self::STATUS_DISABLED;
        $this->save();

        event('auth.logout', [$this]);
    }

    public function enter(?int $serviceCenterId)
    {
        $serviceCenter = null;
        if (isset($serviceCenterId)) {
            $serviceCenter = ServiceCenter::findOrFail($serviceCenterId);
        }

        if($this->isDelete()) {
            throw new UserDeletedException();
        }

        if (($this->isAdmin() || $this->isCashier() || $this->isOverdrive()) && is_null($serviceCenter)) {
            throw new NeedSelectServiceCenterException();
        }

        DB::transaction(function () use ($serviceCenter) {
            if ($this->isCashier() && $this->firstTodayLogin()) {
                $this->replenishBalanceByDailyAccrual(Role::cashierSalary());
            }
            $this->status = self::STATUS_ACTIVE;

            isset($serviceCenter) && $this->serviceCenter()->associate($serviceCenter);

            /** @var LoginHistory $loginHistory */
            $loginHistory = LoginHistory::make(['type' => LoginHistory::TYPE_LOGIN]);
            isset($serviceCenter) && $loginHistory->serviceCenter()->associate($serviceCenter);
            $this->loginHistory()->save($loginHistory);
            $this->save();
        });
    }

    public function exit()
    {
        DB::transaction(function () {
            if ($this->isActive()) {
                $this->status = self::STATUS_DISABLED;
            }

            $this->loginHistory()->save(LoginHistory::make(['type' => LoginHistory::TYPE_LOGOUT]));

            $this->save();
        });
    }

    public function scopeDefaultOrder(Builder $query)
    {
        return $query->orderByRaw('IF(status = "' . self::STATUS_DELETE . '", 1, 0), id DESC');
    }

    public function scopeWithoutDeleted(Builder $query)
    {
        return $query->where("status", "<>", self::STATUS_DELETE);
    }

    public function scopeOurOrder(Builder $query, User $user)
    {
        $serviceCenterId = intval($user->service_center_id);
        return $query->orderByRaw('IF(service_center_id = ' . $serviceCenterId . ', 0, 1), id DESC');
    }

    public function scopeByServiceCenter(Builder $query, ServiceCenter $serviceCenter)
    {
        return $query->where('service_center_id', '=', $serviceCenter->id);
    }

    public function edit(Fio $fio, string $role, bool $overdrive, ?array $phones, ?string $email, ?string $login, ?int $serviceCenterId): self
    {
        $serviceCenter = null;
        if (isset($serviceCenterId)) {
            $serviceCenter = ServiceCenter::findOrFail($serviceCenterId);
        }

        $this->fill([
            'name' => $fio->getName(),
            'surname' => $fio->getSurname(),
            'patronymic' => $fio->getPatronymic(),
            'phones' => $phones,
            'email' => $email,
            'login' => $login,
            'role_id' => $role,
            'overdrive' => $overdrive,
        ]);

        isset($serviceCenter) && $this->serviceCenter()->associate($serviceCenter);

        if (!($this->isAdmin() || $this->isCashier()) && is_null($serviceCenter)) {
            throw new NeedSelectServiceCenterException();
        }

        if ($overdrive && !$this->isWasher()) {
            throw new ImpossibleOverdriveException();
        }

        if (!$this->isAllowLogin()) {
            throw new NeedLoginException();
        }

        $this->save();

        return $this;
    }

    public function totalAccrual(): MoneyInterface
    {
        return new Money($this->accrual()->sum('value'));
    }

    public function getAccrualByOrder(Order $order): Accrual
    {
        /** @var Accrual $accrual */
        $accrual = $this
            ->accrual()
            ->where('initiator_type', Accrual::TYPE_ACCRUAL_BY_ORDER)
            ->where('initiator_id', $order->id)
            ->first();
        return $accrual;
    }

    public function resetReplenishBalanceByOrder(MoneyInterface $value, Order $order)
    {
        $accrual = $this->getAccrualByOrder($order);
        $oldValue = $accrual->value;
        $this->balance = $this->balance->sub($oldValue)->add($value);
        $this->save();

        $accrual->value = $value;
        $this->save();
    }

    public function replenishBalanceByOrder(MoneyInterface $value, Order $order)
    {
        $this->balance = $this->balance->add($value);
        $this->save();

        Accrual::byOrder($value, $this, $order);
    }

    public function replenishBalanceByDailyAccrual(MoneyInterface $value)
    {
        $this->balance = $this->balance->add($value);
        $this->save();

        Accrual::byDailyAccrual($value, $this);
    }

    public function reduceBalanceByTeamFine(MoneyInterface $value, TeamFine $fine)
    {
        $value = $value->multiply(-1);
        $this->balance = $this->balance->add($value);
        $this->save();

        Accrual::byTeamFine($value, $this, $fine);
    }

    public function reduceBalanceByPersonalFine(MoneyInterface $value, PersonalFine $fine)
    {
        $value = $value->multiply(-1);
        $this->balance = $this->balance->add($value);
        $this->save();

        Accrual::byPersonalFine($value, $this, $fine);
    }

    public function reduceBalanceByPayout(MoneyInterface $value, Salary $salary)
    {
        $value = $value->multiply(-1);
        $this->balance = $this->balance->add($value);
        $this->save();

        Accrual::byPayout($value, $this, $salary);
    }

    public function increaseBalance($value)
    {
        $this->balance = $this->balance->add($value);
        $this->save();
    }

    public function firstTodayLogin(): bool
    {
        return !$this->loginHistory()->where('type', LoginHistory::TYPE_LOGIN)->where('created_at', '>', Carbon::today()->toDateString())->lockForUpdate()->count();
    }

    private function isAllowLogin(): bool
    {
        return (!$this->overdrive && $this->role_id == Role::WASHER) || !is_null($this->login);
    }

    private function comparePassword(string $password): bool
    {
        return !password_verify($password, $this->password);
    }

    private function newPassword(string $new)
    {
        $this->password = bcrypt($new);
    }

    protected function getFieldListToMoney(): array
    {
        return ['balance'];
    }

    public static function statusesList()
    {
        return [
            self::STATUS_DISABLED => trans('user.status.disabled'),
            self::STATUS_ACTIVE => trans('user.status.active'),
            self::STATUS_DELETE => trans('user.status.delete'),
        ];
    }

    public static function register(Fio $fio, string $role, bool $overdrive, ?array $phones, ?string $email, ?string $login, ?string $password, ?int $serviceCenterId): self
    {
        $serviceCenter = null;
        if (isset($serviceCenterId)) {
            $serviceCenter = ServiceCenter::findOrFail($serviceCenterId);
        }

        /** @var User $user */
        $user = static::make([
            'name' => $fio->getName(),
            'surname' => $fio->getSurname(),
            'patronymic' => $fio->getPatronymic(),
            'phones' => $phones,
            'email' => $email,
            'login' => $login,
            'overdrive' => $overdrive,
            'role_id' => $role,
            'status' => self::STATUS_DISABLED,
        ]);

        isset($serviceCenter) && $user->serviceCenter()->associate($serviceCenter);

        if (!($user->isAdmin() || $user->isCashier()) && is_null($serviceCenter)) {
            throw new NeedSelectServiceCenterException();
        }

        if ($overdrive && !$user->isWasher()) {
            throw new ImpossibleOverdriveException();
        }

        if (!$user->isAllowLogin()) {
            throw new NeedLoginException();
        }

        if (!isset($password) && !($user->isWasher() && !$user->overdrive)) {
            throw new NeedPasswordException();
        }

        if (isset($password)) {
            $user->newPassword($password);
        }

        $user->save();
        return $user;
    }

}