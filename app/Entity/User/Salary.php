<?php

namespace App\Entity\User;

use App\Entity\MoneyTrait;
use App\Entity\User\Exception\SalaryIncorectException;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Notifications\Notifiable;

/**
 * @property int $id
 * @property MoneyInterface $value
 * @property int $initiator_id
 * @property User $initiator
 */
class Salary extends Model
{
    use MoneyTrait;

    protected $fillable = [
        'value'
    ];

    protected function getFieldListToMoney(): array
    {
        return ['value'];
    }

    public function initiator()
    {
        return $this->belongsTo(User::class, 'initiator_id', 'id');
    }

    public function accrual()
    {
        return $this->morphMany(Accrual::class, 'initiator');
    }
}