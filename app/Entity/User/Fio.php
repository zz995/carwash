<?php

namespace App\Entity\User;


class Fio
{
    private $name;
    private $surname;
    private $patronymic;

    public function __construct(?string $surname, string $name, ?string $patronymic)
    {

        $this->name = $name;
        $this->surname = $surname;
        $this->patronymic = $patronymic ?: '';
    }

    public function getPatronymic(): ?string
    {
        return $this->patronymic;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getFullName(): string
    {
        return $this->surname . ' ' . $this->name . ' ' . $this->patronymic;
    }
}