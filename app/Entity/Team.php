<?php

namespace App\Entity;

use App\Entity\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property string $name
 * @property int $type
 * @property int $service_center_id
 */
class Team extends Model
{
    public const TYPE_CONSTANT = 0;
    public const TYPE_TEMPORARY = 1;

    protected $fillable = [
        'name', 'service_center_id'
    ];

    public static $bindTableUser = 'team-user';

    public function employees()
    {
        return $this->belongsToMany(User::class, self::$bindTableUser, 'team_id', 'user_id');
    }

    public function temporaryTeamUsers()
    {
        return $this->hasMany(TemporaryTeamUser::class, 'user_id', 'id');
    }

    public function serviceCenter()
    {
        return $this->belongsTo(ServiceCenter::class, 'service_center_id', 'id');
    }

    public function activeEmployees()
    {
        $this->employees()->update(['status' => User::STATUS_ACTIVE]);
    }

    public function disableEmployees()
    {
        $this->employees()->update(['status' => User::STATUS_DISABLED]);
    }

    public function isTemporary()
    {
        return $this->type == self::TYPE_TEMPORARY;
    }

    public function scopeOur(Builder $query, User $user)
    {
        return $query->where('service_center_id', '=', $user->service_center_id);
    }

    public function scopeConstant(Builder $query)
    {
        return $query->where('type', '=', self::TYPE_CONSTANT);
    }

    public function scopeByServiceCenter(Builder $query, ServiceCenter $serviceCenter)
    {
        return $query->where('service_center_id', '=', $serviceCenter->id);
    }

    static public function fullTeam(ServiceCenter $serviceCenter)
    {
        $teams = Team::with('employees', 'employees.serviceCenter')
            ->byServiceCenter($serviceCenter)
            ->constant()
            ->orderBy('name')
            ->get()
            ->filter(function ($team) {
                return !empty($team->employees->toArray());
            });

        return $teams;
    }
}