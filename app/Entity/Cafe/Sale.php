<?php

namespace App\Entity\Cafe;

use App\Entity\CarWash\Type;
use App\Entity\MoneyTrait;
use App\Entity\Order\Order;
use App\Entity\ServiceCenter;
use App\Entity\Team;
use App\Entity\TemporaryTeamUser;
use App\Entity\User\Exception\NeedLoginException;
use App\Entity\User\Exception\NeedPasswordException;
use App\Entity\User\Exception\NeedSelectServiceCenterException;
use App\Entity\User\Exception\UserDeletedException;
use App\Entity\User\Exception\WrongPasswordException;
use App\Entity\User\User;
use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * @property int $id
 * @property MoneyInterface $sum
 * @property int $payment_type
 * @property integer $cafe_sale_type_id
 * @property integer $service_center_id
 * @property integer $cashier_id
 */
class Sale extends Model
{
    use MoneyTrait;

    public const PAYMENT_CASH = 1;
    public const PAYMENT_CARD = 2;

    protected $table = 'cafe_sales';

    protected $fillable = [
        'sum', 'payment_type'
    ];

    protected function getFieldListToMoney(): array
    {
        return ['sum'];
    }

    public function type()
    {
        return $this->belongsTo(SaleType::class, 'cafe_sale_type_id', 'id');
    }

    public function cashier()
    {
        return $this->belongsTo(User::class, 'cashier_id', 'id');
    }

    public function serviceCenter()
    {
        return $this->belongsTo(ServiceCenter::class, 'service_center_id', 'id');
    }

    public function scopeByServiceCenter(Builder $query, ServiceCenter $serviceCenter)
    {
        return $query->where('service_center_id', '=', $serviceCenter->id);
    }

    public function scopePaymentType(Builder $query, int $paymentType)
    {
        return $query->where('payment_type', '=', $paymentType);
    }

    static public function register(string $sum, int $typeId, int $paymentType, User $cashier, ServiceCenter $serviceCenter)
    {
        $type = SaleType::findOrFail($typeId);

        /** @var Sale $sale */
        $sale = self::make(['sum' => $sum, 'payment_type' => $paymentType]);
        $sale->type()->associate($type);
        $sale->cashier()->associate($cashier);
        $sale->serviceCenter()->associate($serviceCenter);

        $sale->save();
    }
}