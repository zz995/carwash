<?php

namespace App\Entity\Cafe;

use App\Entity\MoneyTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * @property int $id
 * @property string $name
 * @property bool $active
 */
class SaleType extends Model
{
    use MoneyTrait;

    protected $table = 'cafe_sale_types';

    protected $fillable = [
        'name'
    ];

    protected $casts = [
        'active' => 'boolean',
    ];

    public function sale()
    {
        return $this->hasMany(Sale::class, 'cafe_sale_type_id', 'id');
    }

    public function scopeActive(Builder $query)
    {
        return $query
            ->where('active', '=', 1);
    }

}