<?php

namespace App\Entity;

use App\Entity\DiscountCard\DiscountCard;
use App\Entity\Order\Order;
use App\Entity\User\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string|null $phone
 */
class Client extends Model
{
    protected $fillable = [
        'name', 'phone'
    ];

    public function cars()
    {
        return $this->belongsToMany(Car::class, 'client-car', 'client_id', 'car_id');
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'client_id', 'id');
    }

    public function discountCard()
    {
        return $this->hasOne(DiscountCard::class, 'client_id', 'id');
    }

    public function hasDiscount()
    {
        return isset($this->discountCard);
    }

    public function hasPhone()
    {
        return isset($this->phone);
    }

    public function equal(?Client $client): bool
    {
        if (is_null($client)) {
            return false;
        }

        return $this->id == $client->id;
    }
}