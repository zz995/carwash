<?php

namespace App\Entity;

use App\ObjectValue\Money;
use App\ObjectValue\MoneyInterface;


trait MoneyTrait
{

    public function __get($key)
    {
        $value = parent::__get($key);
        if (in_array($key, $this->getFieldListToMoney())) {
            return $this->toMoney($value);
        }
        return $value;
    }

    public function __set($key, $value)
    {
        if (in_array($key, $this->getFieldListToMoney())) {
            parent::__set($key, $this->fromMoney($value));
            return $this->toMoney($value);
        } else {
            parent::__set($key, $value);
        }
    }

    protected function getFieldListToMoney(): array
    {
        return [];
    }

    private function toMoney($value): ?MoneyInterface
    {
        if (is_null($value)) {
            return null;
        }

        return new Money($value);
    }

    private function fromMoney(?MoneyInterface $value)
    {
        if (is_null($value)) {
            return null;
        }

        return $value->getAmount();
    }
}