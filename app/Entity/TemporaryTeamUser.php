<?php

namespace App\Entity;

use App\Entity\Order\Order;
use App\Entity\User\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $user_id
 * @property int $order_id
 * @property int $team_id
 */
class TemporaryTeamUser extends Model
{
    protected $primaryKey = 'user_id';

    public $incrementing = false;

    public function employee()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id', 'id');
    }
}