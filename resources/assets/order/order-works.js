
fastAccessNumber();
worksNumber();
calcPrice();

$(".works-list__item input").on("change", function () {
    worksNumber();
    calcPrice();
    fastAccessNumber();
});

$(".works-list__price").on("change", function () {
    calcPrice();
    fastAccessNumber();
});

$(".works-fast-access__number").on("click", function () {
    const elem = $(".works-list__work[value='" + $(this).data('number') + "']");
    const oldValue = elem.prop("checked");
    elem.prop("checked", !oldValue).trigger("change");
});


function fastAccessNumber() {
    const numbers = getWorksNumber();

    $(".works-fast-access__number").removeClass("badge-light, badge-primary").addClass("badge-light");
    numbers.forEach(function (number) {
        $(".works-fast-access__number[data-number='" + number + "']").removeClass("badge-light").addClass("badge-primary");
    });
}

function getWorksNumber() {
    return [].map.call($(".works-list__item input:checked"), function (elem) {
        return $(elem).data('number');
    });
}

function worksNumber() {
    const numbers = getWorksNumber().join(', ');

    $(".works-numbers").text(numbers);
}

function calcPrice() {
    const worksElem = $(".works-list");
    const disc = getDisc() / 100;
    const input = worksElem.find('input:checked');
    const sum = [].reduce.call(input, function (a, elem) {
        const price = +worksElem.find(".works-list__price[data-work_id='" + $(elem).val() + "']").val();
        const count = +worksElem.find(".works-list__count[data-work_id='" + $(elem).val() + "']").val() || 1;
        return a + (price * count);
    }, 0);

    $("[name='price']").val(sum);
    $(".ordering-sum").text(sum);

    if (disc > 0) {
        $(".ordering-sum-with-disc").removeClass('hidden');
        $(".ordering-sum-with-disc__value").text(Math.round(sum * (1 - disc)));
    } else {
        $(".ordering-sum-with-disc").addClass('hidden');
    }
}

function getDisc() {
    return $("#currentDiscountSize").val();
}
