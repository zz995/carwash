require('jquery-mask-plugin');

$(function () {
    $(".phone-input").mask("8 (000) 000-00-00", {
        placeholder: "8 (___) ___-__-__",
    });
});