$(function () {
    'use strict';

    $(".order-filter-status").on('click', function (e) {
        const form = $(this).closest("form");
        const value = $(this).data('value');

        form.find('[name="status"]').val(value);
        form.trigger("submit");
    })
});