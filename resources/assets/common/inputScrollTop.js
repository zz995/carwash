
$('input:not([type="checkbox"]):not([type="radio"]), select, textarea').on('focus', function() {
    $(this)[0].scrollIntoView();
});