
class Autocomplete {

    constructor(elem, source) {
        this.elem = elem;
        this.minLength = 3;
        this.delay = 300;
        this.template = _.template(this.elem.find(".autocomplete__items-template").html());
        this.source = source;
        this.active = false;
        this.init();
    }

    init() {
        const $this = this;
        const trigger = (function () {
            let timerId = null;
            return function() {
                $this.active = false;
                clearTimeout(timerId);
                timerId = setTimeout(function () {
                    timerId = null;
                    $this.handel();
                }, $this.delay);
            };
        })();
        const fistSelect = function (e) {
            if (e.keyCode == 13) {
                const pickElem = $this.elem
                    .find(".autocomplete__items")
                    .find(".autocomplete__item")
                    .eq(0);
                if (pickElem && pickElem.length && $this.active) {
                    pickElem.click();
                } else {
                    e.stopImmediatePropagation();
                }
            }
        };

        this.input = this.elem.find(".autocomplete__input");
        this.input.on({
            focus: this.show.bind(this),
            keyup: trigger,
            keypress: fistSelect
        });

        this.input.on('click', (e) => {
            e.preventDefault();
            return false;
        });
        $("body").on('click', this.hide.bind(this));
    }

    handel() {
        const old = this.value;
        this.value = this.input.val().toString();

        if (this.value.length < this.minLength) {
            return this.hide();
        }

        if (this.value !== old) {
            this.request();
        }

        return this;
    }

    request() {
        const $this = this;
        this.source.call(this, function(data) {
            $this.active = true;
            if (data) {
                $this.render(data);
            }
        });
    }

    render(data) {
        let appendBeforeElem = this.elem.find(".autocomplete__items");
        if (! appendBeforeElem.length) {
            appendBeforeElem = this.elem.find(".autocomplete__items-template");
        }

        $(this.template({items: data})).insertBefore( appendBeforeElem );
        appendBeforeElem.remove();

        let items = this.elem.find(".autocomplete__items").addClass('autocomplete__items_hidden');

        setTimeout(() => {
            this.show();
            $(".autocomplete__item").on('click', this.select.bind(this))
        }, 0);

    }

    hide() {
        this.elem.find(".autocomplete__items").addClass('autocomplete__items_hidden');
    }

    show() {
        if (this.value && this.value.length >= this.minLength && this.input.get(0) === document.activeElement) {
            this.elem.find(".autocomplete__items_hidden").removeClass('autocomplete__items_hidden');
        }
    }

    select(e) {
        const data = $(e.currentTarget).data();
        this.elem.trigger('autocomplete.pick', data);
        this.hide();
    }
}

module.exports = Autocomplete;

