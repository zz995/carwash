module.exports = (function () {
    'use strict';

    const container = $(".server-notifications");
    const template = _.template(container.find(".server-notifications__example-alert").html());
    const items = [];
    const maxItem = 5;

    function add(message) {
        const itemElem = $(template({message}));
        items.push(itemElem);
        container.append(itemElem);

        if (items.length > maxItem) {
            items.shift().remove();
        }
    }

    return {
        add
    }
})();