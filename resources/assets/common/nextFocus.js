
$("input").keypress(function (e) {
    if (e.keyCode == 13) {
        e.preventDefault();
        setTimeout(() => {
            var self = $(this)
                , form = self.parents('form:eq(0)')
                , focusable
                , next
            ;

            focusable = form.find('input:not(.skip-focus),a,select,button,textarea').filter(':visible');
            next = focusable.eq(focusable.index(this) + 1);
            if (next.length) {
                next.focus();
            }
        }, 0);
    }
});
