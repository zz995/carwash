'use strict';

$(function () {
    $(".input-count__inc").on('click', function () {
        const inputData = getInputData($(this));
        if (inputData.val < inputData.max) {
            inputData.elem.val(inputData.val + 1).trigger('change');
            inputData.activeElem.val(inputData.val + 1).trigger('change');
        }
    });
    $(".input-count__dec").on('click', function () {
        const inputData = getInputData($(this));
        if (inputData.val > inputData.min) {
            inputData.elem.val(inputData.val - 1).trigger('change');
            inputData.activeElem.val(inputData.val - 1).trigger('change');
        }
    });

    function getInputData(incOrDecElem) {
        const elem = incOrDecElem.closest(".input-count").find(".input-count__input");
        const activeElem = incOrDecElem.closest(".input-count").find(".input-count__active-input");

        const max = elem.attr('max') || 999;
        const min = elem.attr('min') || 1;
        const val = +elem.val() || min;

        return {activeElem, elem, max, min, val};
    }
});