'use strict';

module.exports = (function () {

    $(function () {
        $("body").on("change", "[name='mark']", function () {
            const val = $(this).val();
            const markElem = getMarkElem($(this));
            const modelElem = getModelElemByMark(markElem);

            if (val === '') {
                carModelElem.addClass('hidden');
            } else if(val === '-1') {
                markElem.select.addClass('hidden');
                markElem.input.removeClass('hidden');
                modelElem.input.removeClass('hidden');
                modelElem.select.addClass('offset-md-4');

                setTimeout(function () {
                    markElem.input.get(0).focus();
                }, 0);

                loadAndAppendModels($(this), $("[name='model']"), val);
            } else {
                loadAndAppendModels($(this), $("[name='model']"), val);
            }
        });

        $("body").on("blur", "[name='new-mark']", function () {
            const markElem = getMarkElem($(this));
            const modelElem = getModelElemByMark(markElem);

            if ($(this).val() === '') {
                markElem.input.addClass('hidden');
                markElem.select.removeClass('hidden');
                modelElem.input.addClass('hidden');
                modelElem.select.removeClass('offset-md-4');

                loadAndAppendModels(markElem.select.find('select'), $("[name='model']"), undefined);
            }
        });

        $("body").on("change", "[name='model']", function () {
            const val = $(this).val();
            const modelElem = getModelElem($(this));
            const markElem = getMarkElemByModel(modelElem);

            if (val === '0') {
                modelElem.input.removeClass('hidden');
                modelElem.select.addClass('offset-md-4');

                setTimeout(function () {
                    modelElem.input.get(0).focus();
                }, 0);

                loadAndAppendModels(markElem.select.find('select'), $("[name='model']"), -1);
            }
        });

        $("body").on("blur", "[name='new-model']", function () {
            const modelElem = getModelElem($(this));
            const markElem = getMarkElemByModel(modelElem);

            if ($(this).val() === '' && markElem.input.hasClass('hidden')) {
                modelElem.input.addClass('hidden');
                modelElem.select.removeClass('offset-md-4');
                recreateModel(modelElem.select);

                loadAndAppendModels(markElem.select.find('select'), $("[name='model']"), markElem.select.find('select').val());
            }
        });
    });

    function getMarkElem($this) {
        const carMarkElem = $this.closest(".car-mark");
        const carMarkSelectElem = carMarkElem.find(".car-mark__select");
        const carMarkInputElem = carMarkElem.find(".car-mark__input");

        return {
            'elem': carMarkElem,
            'select': carMarkSelectElem,
            'input': carMarkInputElem,
        };
    }

    function getModelElem($this) {
        const model = $this.closest(".car-model");
        const select = model.find(".car-model__select");
        const input = model.find(".car-model__input");

        return {
            'elem': model,
            'select': select,
            'input': input,
        };
    }

    function getModelElemByMark(markElem)
    {
        const markAndModel = markElem.elem.closest(".mark-and-model");
        const model = markAndModel.find(".car-model");
        const select = model.find(".car-model__select");
        const input = model.find(".car-model__input");

        return {
            'elem': model,
            'select': select,
            'input': input,
        };
    }

    function getMarkElemByModel(modelElem)
    {
        const markAndModel = modelElem.elem.closest(".mark-and-model");
        const model = markAndModel.find(".car-mark");
        const select = model.find(".car-mark__select");
        const input = model.find(".car-mark__input");

        return {
            'elem': model,
            'select': select,
            'input': input,
        };
    }

    function loadAndAppendModels(markElem, modelElem, markId, modelId) {
        const url = markElem.data('location_car_model');

        if (markId === undefined) {
            recreateMark(markElem);

            return modelElem.empty();
        }

        $.ajax({url: url + '?mark=' + (markId == '-1' ? 0 : markId)}).done(function (jsonResponse) {
            let items = jsonResponse.data;
            if (markId != -1) {
                items = items.concat({'id': 0, 'name': 'Создать модель'});
            }
            const template = _.template(modelElem.closest(".car-model").find(".model__items-template").html());
            modelElem.empty().append(template({items: items, modelId}));
            modelElem.closest(".car-model").removeClass('hidden');
        });
    }

    function recreateMark(markElem) {
        const html = markElem.get(0).outerHTML;
        $(html).insertBefore(markElem);
        markElem.remove();
    }

    function recreateModel(modelElem) {
        const html = modelElem.get(0).outerHTML;
        $(html).insertBefore(modelElem);
        modelElem.remove();
    }

    return {
        loadAndAppendModels,
        recreateMark,
        recreateModel
    };
})();