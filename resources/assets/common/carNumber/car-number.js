require('jquery-mask-plugin');

class CarNumber {
    constructor(carNumberElem) {
        carNumberElem = $(carNumberElem);

        this.carNumberElem = carNumberElem;
        this.inputElem = carNumberElem.find(".car-number__input");
        this.foreignElem = carNumberElem.find(".car-number__foreign");

        if (! this.foreignElem.prop('checked')) {
            this.setMask();
        }
        this.bindEvents();
    }

    bindEvents() {
        this.foreignElem.on("change", this.changeForeign.bind(this));
        this.inputElem.on("carNumber.input", this.input.bind(this));
    }

    input(e, data) {
        const val = $(e.target);
        if (CarNumber.checkPattern(data.value)) {
            this.unsetForeign();
        } else {
            this.setForeign();
        }

        this.setNumber(data.value);
    }

    changeForeign(e) {
        if ($(e.target).prop('checked')) {
            this.unsetMask();
        } else {
            this.setMask();
        }
    }

    setNumber(value) {
        this.inputElem.val(value).trigger("change");
    }

    setMask() {
        this.inputElem.attr('pattern', CarNumber.pattern);

        this.inputElem.mask("X000XX000", {
            placeholder: "X000XX000",
            'translation': {
                X: {pattern: /[авекмнорстух]/i}
            },
            onKeyPress: function (value, event) {
                event.currentTarget.value = value.toUpperCase();
            }
        });
    }

    unsetMask() {
        this.inputElem.attr('pattern', ".*");
        this.inputElem.unmask().trigger('change');
    }

    setForeign() {
        this.foreignElem.prop("checked", true).trigger("change");
    }

    unsetForeign() {
        this.foreignElem.prop("checked", false).trigger("change");
    }

    static get pattern() {
        return '[АВЕКМНОРСТУХ][0-9]{3}[АВЕКМНОРСТУХ]{2}[0-9]{2,3}';
    }

    static checkPattern(value) {
        const reg = new RegExp(CarNumber.pattern);
        return reg.test(value);
    }
}

module.exports = CarNumber;

$(function () {
     [].forEach.call($(".car-number"), function (carNumberElem) {
         new CarNumber(carNumberElem);
     });
});

