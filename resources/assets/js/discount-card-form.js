
require('./bootstrap');
require('../common/inputScrollTop');
require('../common/nextFocus');
require('../common/carNumber/car-number');
require('../common/phone-input');

const markAndModel = require('../common/mark-and-model');

$(function () {

    $(".discount-card-number__input").on("change", function () {
        const discountCardNumberElem = $(this).closest(".discount-card-number");
        const url = $(this).data('location');
        const value = $(this).val();
        const old = $(this).data('number');
        const carId = $("#car_id").val() || '';

        if (value !== '') {
            $.ajax({url: url + '?value=' + encodeURIComponent(value) + '&car_id=' + carId + '&old=' + old}).done(jsonResponse => {
                discountCardNumberElem.find(".discount-card-number__error").remove();

                const template = _.template($(".discount-card-number__error-template").html());
                const html = template({data: {message: jsonResponse.message}});
                $(this).after(html);
            });
        }
    });

    $(".discount-card-form__new-car-number").on("input", function () {
         const val = $(this).val().toString();
         const addCarButton = $(".discount-card-form__add-new-car");

         if (val.length === 0) {
             addCarButton.prop('disabled', true).removeClass('btn-primary');
         } else {
             addCarButton.prop('disabled', false).addClass('btn-primary');
         }
    });

    $(".discount-card-form__new-car-number").on('keypress', function (e) {
        if (e.keyCode === 13) {
            addNewNumber();
            e.preventDefault();
            e.stopImmediatePropagation();
            return false;
        }
    });

    $(".discount-card-form__add-new-car").on('click', function () {
        addNewNumber();
    });

    function addNewNumber() {
        const newCarNumberElem = $(".discount-card-form__new-car-number");

        const markAndModelData = {
            'mark': '',
            'new_mark': '',
            'model': '',
            'model_category': '',
            'new_model': '',
        };

        const newMarkElem = $("#new-mark");
        const markElem = $("#mark");
        const newMark = newMarkElem.val();
        const mark = markElem.val();

        let titleCarMark = undefined;
        if (newMark) {
            titleCarMark = newMark;
            markAndModelData.mark = newMark;
            markAndModelData.new_mark = 1;
        } else {
            titleCarMark = markElem.find("[value='" + mark +"']").text();
            markAndModelData.mark = mark;
            markAndModelData.new_mark = 0;
        }

        const newModelElem = $("#new-model");
        const modelElem = $("#model");
        const newModel = newModelElem.val();
        const model = modelElem.val();

        let titleCarModel = undefined;
        if (newModel) {
            titleCarModel = newModel;
            markAndModelData.model = newModel;
            markAndModelData.model_category = model;
            markAndModelData.new_model = 1;
        } else {
            titleCarModel = modelElem.find("[value='" + model +"']").text();
            markAndModelData.model = model;
            markAndModelData.new_model = 0;
        }

        if (!titleCarMark || !titleCarModel) {
            titleCarMark = undefined;
            titleCarModel = undefined;
        }

        let number = newCarNumberElem.val();
        if (number === '' || number === undefined) {
            return false;
        }
        number = number.toString().toUpperCase();

        const newCarNumberBlock = $(".discount-card-form__new-cars-block");
        newCarNumberBlock.removeClass('hidden');

        newCarNumberElem.val('').trigger('input');
        markAndModel.recreateMark(markElem);
        newMarkElem.val('').trigger('blur');
        newModelElem.val('');
        modelElem.empty();

        const template = _.template($(".discount-card-form__new-car-template").html());
        $(".discount-card-form__new-cars").prepend(template({data: {number, titleCarMark, titleCarModel, markAndModel: markAndModelData}}));
    }

    $("body").on('click', ".discount-card-form__delete-car", function () {
        const newCarsElem = $(this).closest(".discount-card-form__new-cars");
        if (newCarsElem.find(".discount-card-form__new-car").length <= 1) {
            $(".discount-card-form__new-cars-block").addClass('hidden');
        }
        $(this).closest(".discount-card-form__new-car").remove();
    });

    $("body").on('click', ".discount-card-form__delete-saved-car", function () {
        const savedCars = $(this).closest(".discount-card-form__saved-cars");
        if (savedCars.find(".discount-card-form__saved-car").length <= 1) {
            $(".discount-card-form__saved-cars-block").addClass('hidden');
        }
        $(this).closest(".discount-card-form__saved-car").remove();
    });
});