
require('./bootstrap');
require('../common/order-filter-status');

const serverNotification = require('../common/server-notifications');

$(function () {
    'use strict';

    const orderListElem = $(".orders-list");
    const location = orderListElem.data('location');
    let timestamp = orderListElem.data('timestamp');

    checkNewOrderByInterval();

    function checkNewOrderByInterval() {
        setTimeout(checkNewOrder(function () {
            checkNewOrderByInterval();
        }), 1000);
    }

    function checkNewOrder(cb) {
        $.ajax({'url': location + '?timestamp=' + encodeURIComponent(timestamp)})
            .done(function (jsonResponse) {
                if (jsonResponse.hasNewOrder) {
                    timestamp = jsonResponse.timestamp;

                    for(let i = 0; i < jsonResponse.count; i++) {
                        serverNotification.add('Новый заказ, <a href="">обновите страницу</a>');
                    }
                }
            })
            .always(function () {
                cb();
            });
    }
});