
require('./bootstrap');

require('../common/inputScrollTop');
require('../common/nextFocus');
require('../common/phone-input');

const CarNumber = require('../common/carNumber/car-number');
const Autocomplete = require('../common/autocomplete');
const markAndModel = require('../common/mark-and-model');

const carNumberSearchElem = $(".car-number-search");
const clientPhoneSearchElem = $(".client-phone-search");
const clientNameElem = $(".client-name");
const clientIdElem = $("[name='client_id']");
const carIdElem = $("[name='car_id']");
const carMarkElem = $(".car-mark");
const carModelElem = $(".car-model");
const submitElem = $(".submit");

let showNewClient = true;

carNumberSearchElem.on('autocomplete.pick', function (elem, data) {
     showNewClient = true;

     if (data.id && data.client_id) {
         clientIdElem.val(data.client_id);
         carIdElem.val(data.id);

         carNumberSearchElem.find("input").eq(0).trigger("carNumber.input", {value: data.number});
         clientPhoneSearchElem.removeClass('hidden').find("input").eq(0).val(data.phone).trigger('input');
         clientNameElem.removeClass('hidden').find("input").eq(0).val(data.name);
         carModelElem.removeClass('hidden').find("select").eq(0).val(data.car_model_id);
         carMarkElem.removeClass('hidden').find("select").eq(0).val(data.car_mark_id);
         submitElem.removeClass('hidden');

         markAndModel.loadAndAppendModels($("[name='mark']"), $("[name='model']"), data.car_mark_id, data.car_model_id);
     } else if (data.id) {
         showNewClient = false;

         carIdElem.val(data.id);
         clientIdElem.val('');

         carNumberSearchElem.find("input").eq(0).trigger("carNumber.input", {value: data.number});
         carModelElem.removeClass('hidden').find("select").eq(0).val(data.car_model_id);
         carMarkElem.removeClass('hidden').find("select").eq(0).val(data.car_mark_id);

         clientPhoneSearchElem.removeClass('hidden').find("input").eq(0).val('');
         clientNameElem.removeClass('hidden').find("input").eq(0).val('');
         submitElem.removeClass('hidden');

         markAndModel.loadAndAppendModels($("[name='mark']"), $("[name='model']"), data.car_mark_id, data.car_model_id);
     } else {
         clientIdElem.val('');
         carIdElem.val('');

         clientPhoneSearchElem.removeClass('hidden').find("input").eq(0).val('').trigger('input');
         clientNameElem.removeClass('hidden').find("input").eq(0).val('');

         carModelElem.removeClass('hidden').find("select").eq(0).val(data.car_model_id);
         carMarkElem.removeClass('hidden').find("select").eq(0).val(data.car_mark_id);

         markAndModel.loadAndAppendModels($("[name='mark']"), $("[name='model']"), undefined, undefined);

         clientPhoneSearchElem.removeClass('hidden');
         submitElem.removeClass('hidden');
     }
});
const numberSearch = new Autocomplete(carNumberSearchElem, function (cb) {
    const url = this.elem.data('location');

    $.ajax({
        url: url + '?value=' + encodeURIComponent(this.value)
    }).done(function (jsonResponse) {
        cb(jsonResponse.data);
    });
});
numberSearch.minLength = 1;

clientPhoneSearchElem.on('autocomplete.pick', function (elem, data) {
    if (data.id) {
        clientIdElem.val(data.id);
        clientPhoneSearchElem.find("input").eq(0).val(data.phone).trigger('input');
    } else {
        clientIdElem.val('');
    }

    clientNameElem.removeClass('hidden').find("input").eq(0).val(data.name);

    carModelElem.removeClass('hidden');
    carMarkElem.removeClass('hidden');
    //markAndModel.loadAndAppendModels($("[name='mark']"), $("[name='model']"), undefined, undefined);

    submitElem.removeClass('hidden');
});
const phoneSearch = new Autocomplete(clientPhoneSearchElem, function (cb) {
    const url = this.elem.data('location');
    $.ajax({
        url: url + '?value=' + encodeURIComponent(this.value)
    }).done(function (jsonResponse) {
        cb({items: jsonResponse.data, showNewClient});
    });
});