
require('./bootstrap');
require('../common/inputScrollTop');
require('../common/nextFocus');

const contractNumberElem = $(".contract-number");
const contractNumberSearch = $(".contract-number__input");

const contractNumberApplyingDiscount = $(".contract-number-applying-discount");

$(function () {

    let oldType = $(".payment-types [name='type']").val();
    $(".payment-types [name='type']").on('change', function () {
        const type = $(this).val();

        if (type == 3) {
            contractNumberSearch.prop('required', true);
            contractNumberElem.removeClass('hidden');

            contractNumberApplyingDiscount.addClass('hidden');
        } else {
            contractNumberSearch.prop('required', false);
            contractNumberElem.addClass('hidden');

            contractNumberApplyingDiscount.removeClass('hidden');
        }

        if (oldType == 3) {
            $(".contract-number select").val('');
            changePrice(getPrices().price);
        } else if (type == 3) {
            $(".contract-number-applying-discount select").val('');
            changePrice(getPrices().basePrice);
        }
        oldType = type;
    });

    $(".order-closing-form-toggle__item").on('click', function () {
        const toggleElem = $(this).closest(".order-closing-form-toggle");

        [].forEach.call(toggleElem.find(".order-closing-form-toggle__item"), function (elem) {
            $(elem).find('.nav-link').removeClass('active');
            $("." + $(elem).data('class')).addClass('hidden');
        });

        $(this).find('.nav-link').addClass('active');
        $("." + $(this).data('class')).removeClass('hidden');
    });

    $(".payment-list-works__button-work").on('click', function () {
        const state = $(this).data('state');
        const sum = +$(this).data('sum');
        const title = $(this).data('title');
        const buttonElem = $(this);
        const inputElem = buttonElem.closest(".payment-list-works__work").find(".payment-list-works__work-state");
        const orderSumElem = buttonElem.closest('form').find("[name='sum']");
        let orderSum = +orderSumElem.val();

        if (state == 'in') {
            buttonElem
                .removeClass('btn-danger')
                .addClass('btn-success')
                .text(title['in'])
                .data('state', 'out');
            inputElem.val('out');

            orderSum -= sum;
        } else if (state == 'out')  {
            buttonElem
                .removeClass('btn-success')
                .addClass('btn-danger')
                .text(title['out'])
                .data('state', 'in');
            inputElem.val('in');

            orderSum += sum;
        }

        if (orderSum < 0) {
            orderSum = 0;
        }

        orderSumElem.val(orderSum);
    });

    $(".payment-order-sum__reduction-percentage").on('click', function (e) {
        const reductionPercentage = +$(this).data('percent');
        const orderSum = $(this).closest(".payment-order-sum");
        const sumElem = orderSum.find(".payment-order-sum__sum");
        let sum = getPrices().currentPrice;
        sum = sum * ((100 - reductionPercentage) / 100);
        sum = Math.round(sum);
        sumElem.val(sum);
    });

    $(".contract-number-applying-discount select, .contract-number select").on('change', function () {

        const mapDisc = $(this).data('map_disc');
        const contractNumber = $(this).val();
        const contractDisc = mapDisc[contractNumber] || 0;

        let price = 0;
        if (contractNumber === '') {
            price = getPrices().price;
        } else {
            price = getPrices().basePrice;
            price = Math.round(price * ((100 - contractDisc) / 100));
        }

        changePrice(price);
    });

    function resetPaymentListWorks() {
        $(".payment-list-works__work-state").val('in');
        const title = $(".payment-list-works__button-work").data('title');
        $(".payment-list-works__button-work")
            .removeClass('btn-success')
            .addClass('btn-danger')
            .text(title['out']);
    }

    function changePrice($price) {
        const priceElem = $("#price");
        priceElem.val($price);

        $("#sum").val($price);

        resetPaymentListWorks();
    }

    function getPrices() {
        const priceElem = $("#price");
        return {
            currentPrice: +priceElem.val(),
            price: +priceElem.data('price'),
            basePrice: +priceElem.data('base_price'),
        };
    }
});


