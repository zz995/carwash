
require('./bootstrap');
require('../superAdmin/employees.js');
require('../superAdmin/orders.js');
require('../superAdmin/car-wash-works.js');
require('../superAdmin/car-wash-contracts.js');
require('../superAdmin/service-centers.js');
require('../superAdmin/spreadsheet.js');
require('../superAdmin/employee-phones.js');
require('../superAdmin/sort-field.js');

require('air-datepicker');

require('../common/phone-input');
require('../common/carNumber/car-number');

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});