
require('./bootstrap');
require('../common/inputScrollTop');
require('../common/nextFocus');
require('../common/order-filter-status');

require('air-datepicker');

require('../common/phone-input');
const CarNumber = require('../common/carNumber/car-number');

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});

$(function () {
   $(".change-select-data").on('click', function () {
        const target = $(this).data('target');
        const date = $(this).data('date');

        $(target).val(date);
        $(this).closest('form').submit();
   });
});
