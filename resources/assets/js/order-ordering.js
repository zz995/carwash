require('jquery.cookie');

require('./bootstrap');

require('../common/inputScrollTop');
require('../common/nextFocus');
require('../common/input-count');
require('../order/order-works');

deleteFromDataCookie();

window.onbeforeunload = function (e) {
    setFormDataCookie();
};

$(".discount-card__title").on('click', function (e) {
    $(".discount-card__body").toggleClass('hidden');
    $(".discount-card__angle").toggleClass('hidden');
});

$(".client-information__title").on('click', function (e) {
    $(".client-information__body").toggleClass('hidden');
    $(".client-information__angle").toggleClass('hidden');
});

function setFormDataCookie() {
    const date = new Date();
    date.setTime(date.getTime() + (60 * 60 * 1000));
    $.cookie('order-ordering', JSON.stringify({
        form: formData($(".order-form")),
        href: window.location.href,
    }), {expires: date, path: '/'});
}

function deleteFromDataCookie() {
    const date = new Date();
    $.cookie('order-ordering', '', {expires: date, path: '/'});
}

function formData($form) {
    const unindexed = $form.serializeArray();
    const indexed = {};

    $.map(unindexed, function(n, i){
        if (/\w+\[\d+\]/.test(n['name'])) {
            const matches = n['name'].match(/(\w+)\[(\d+)\]/);
            if (indexed[matches[1]] === undefined) {
                indexed[matches[1]] = {};
            }
            indexed[matches[1]][matches[2]] = n['value'];
        } else if(/\w+\[\]/.test(n['name'])) {
            const matches = n['name'].match(/(\w+)\[\]/);
            if (indexed[matches[1]] === undefined) {
                indexed[matches[1]] = [];
            }
            indexed[matches[1]].push(n['value']);
        } else {
            indexed[n['name']] = n['value'];
        }
    });

    return indexed;
}
