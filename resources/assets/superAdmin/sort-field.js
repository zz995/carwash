
$(function () {
   $('.sort-field__title').on('click', function (e) {
       const $this = $(this);
        setTimeout(function () {
            const formElem = $this.closest('form');
            if (formElem && formElem.length) {
                formElem.trigger('submit');
            }
        }, 0);
   });
});