$(".service-centers__service-center").on('click', function (e) {
    const $this = $(this);
    window.location = $this.data('location');
    return true;
});