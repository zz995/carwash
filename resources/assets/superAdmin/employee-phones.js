
'use strict';

$(function () {
    $(".employee-phones__input").on('input', function (e) {
        const buttonElem = $(this).closest(".employee-phones").find(".employee-phones__button");
        buttonElem.prop('disabled', $(this).val().toString().length !== 17);
    });

    $(".employee-phones__button").on('click', function (e) {
        const phonesElem = $(this).closest(".employee-phones");
        const inputElem = phonesElem.find(".employee-phones__input");
        const phone = inputElem.val();
        inputElem.val('');
        $(this).prop('disabled', true);

        const templateElem = phonesElem.find(".employee-phones__template-phone");
        const template = _.template(templateElem.html());
        const html = template({phone});
        $(html).insertBefore( templateElem );
    });

    $(".employee-phones").on('click', ".employee-phones__phone", function (e) {
        $(this).remove();
    });
});