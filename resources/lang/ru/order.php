<?php

return [
    'field' => [
        'id' => '№',
        'phone' => 'Номер телефона',
        'number' => 'Номер машины',
        'model' => 'Модель машины',
        'mark' => 'Марка машины',
        'client' => [
            'name' => 'Имя',
        ],
        'statusHistory' => [
            'timestamp' => 'Время',
            'initiator' => 'Инициатор',
        ],
        'clientName' => 'Имя клиента',
        'teamName' => 'Бригада',
        'works' => 'Услуги',
        'work' => 'Услуга',
        'team' => 'Бригады',
        'date' => 'Время оформления',
        'key' => 'Номер ключа',
        'status' => 'Статус',
        'numberKey' => 'Номер ключа',
        'comment' => 'Комментарий',
        'adminComment' => 'Комментарий администратора',
        'cashierComment' => 'Комментарий кассира',
        'serviceCenter' => 'Автомойка',
        'discountCardCumulative' => 'Номер :disc% накопительной дисконтной карты',
        'discountCardFixed' => 'Номер :disc% фиксированой дисконтной карты',
        'paymentType' => 'Заказ оплачен',
        'services_standard_price' => 'Стандартна цена за услуги',
    ],
    'action' => [
        'create' => 'Новый заказ',
        'cancel' => 'Отменить заказ',
        'payment' => 'Заказ оплачен',
        'repayment' => 'Изменить оплату',
        'refusal' => 'Отказ от оплаты',
        'comment' => 'Отправить',
        'return' => 'Вернуться к оформлению заказа',
        'editWorks' => 'Редактировать список работ',
        'appointTeam' => 'Назначить бригаду',
        'reassignTeam' => 'Сменить бригаду',
        'crossItOut' => 'Вычеркнуть заказ',
        'executed' => 'Заказ выполнен',
    ],
    'goto' => [
        'team' => 'Перейти к выбору бригады',
        'key' => 'Далее',
    ],
    'page' => [
        'list' => [
            'header' => 'Заказы'
        ],
        'payment' => [
            'header' => 'Заказ №:num'
        ],
        'create' => [
            'header' => 'Регистрация пользователя'
        ],
        'ordering' => [
            'header' => 'Оформление заказа'
        ],
        'show' => [
            'header' => 'Информация о заказе'
        ],
        'editWorks' => [
            'header' => 'Редактирование списка работ'
        ],
        'appointTeam' => [
            'header' => 'Назначение команды'
        ],
        'reassignTeam' => [
            'header' => 'Смена бригады'
        ],
        'cross' => [
            'header' => 'Вычеркивание заказа'
        ],
        'serviceHistory' => [
            'header' => 'История обращений',
            'notFound' => 'Обращения не найдены',
        ],
    ],
    'message' => [
        'successCreate' => 'Заказ успешно создан',
        'successCancel' => 'Заказ успешно отменен',
        'successPaymentComment' => 'Комментарий сохранен',
        'successAdminComment' => 'Комментарий сохранен',
        'contractNotExist' => 'Договора не существует',
        'needCommentForOtherOrderPrice' => 'Необходимо указать причину именения цены',
        'discountCardNumberNotSet' => 'Необходимо указать номер дисконтной карты',
        'notFoundEmployees' => 'Выбраная бригада не содержит сотрудников',
        'needSelectTeamOrEmployees' => 'Необходимо выбрать бригаду или сотрудников',
        'orderPaid' => 'Заказ уже оплачен',
        'successEditWorks' => 'Список работ успешно изменен',
        'appointTeam' => 'У заказа уже указаны работники',
        'reassignTeam' => 'Сменить работников можно только у выполняющегося заказа',
        'successAppointTeam' => 'Успешно указаны работники',
        'successCross' => 'Заказ успешно вычеркнут',
        'canNotBeExecuted' => 'Заказ не может быть выполненным',
        'successChangeStatusToExecuted' => 'Статус заказа успешно изменен на выполнен',
    ],
    'phrase' => [
        'newCar' => 'Новая машина',
        'newClient' => 'Новый клиент',
        'newDriver' => 'Новый водитель',
        'submit' => 'Отправить',
        'sum' => 'Сумма',
        'sumWithDisc' => 'Сумма со скидкой',
        'discountFromContract' => 'Скидка :disc%, с договора',
        'sumByContract' => 'Сумма по контракту',
        'paymentSum' => 'Оплаченная сумма',
        'discount' => 'Скидка :disc%',
        'registerNewDiscountCard' => 'Зарегистрировать новую',
        'needTakeDiscountCard' => 'Необходимо выдать :disc%, дисконтную карту',
        'clientInformation' => 'Информация о клиенте',
        'mainInformation' => 'Основная информация',
        'cars' => 'Автомобили',
        'serviceHistory' => 'История посещений',
        'orderStatusHistory' => 'История изменения статуса заказа',
        'drivers' => 'Водители',
        'openOrderCard' => 'Открыть карточку заказа',
        'firstTime' => 'Новый клиент',
        'showListWorks' => 'Показать список услуг',
        'worksState' => [
            'in' => 'Включить в сумму',
            'out' => 'Исключить из суммы',
        ],
        'timeExecute' => 'Длительность выполнения',
        'timeExecuteInMinute' => 'Длительность выполнения (мин.)',
        'timeInQueue' => 'В очереди',
        'timeInQueueInMinute' => 'В очереди (мин.)',
        'sumRange' => 'Оплаченная сумма',
        'statistic' => 'Статистика выполнения',

    ],
    'notExist' => 'Заказы отсутствуют',
    'status' => [
        'execute' => 'Выполняется',
        'executed' => 'Выполнен',
        'paid' => 'Оплаченный',
        'queue' => 'В очереди',
        'cross' => 'Вычеркнутый',
        'paymentRefusal' => 'Отказ оплаты',
        'canceled' => 'Отмененный',
    ],
    'statuses' => [
        'all' => 'Все',
        'execute' => 'В работе',
        'executed' => 'Выполненные',
        'paid' => 'Завершенные',
        'queue' => 'В очереди',
        'cross' => 'Вычеркнутые',
    ],
    'anotherMark' => 'Другая марка',
    'createMark' => 'Создать марку',
    'createModel' => 'Создать модель',
    'selectTeam' => 'Выберите бригаду',
    'selectNumberWorks' => 'Выбранные услуги',
    'createTeam' => 'Или составьте ее',
    'paymentForm' => 'Форма оплаты',
    'refusalForm' => 'Форма отказа',
    'repaymentForm' => 'Форма изменения способа оплаты',
    'ordered' => 'Оформлен',
    'selectTypePayment' => 'Выбирите способ оплаты',
    'contractTypePayment' => 'Номер договора',
    'applyingDiscountFromContract' => 'Скидка с договора',
    'paymentType' => [
        'cash' => 'Наличными',
        'card' => 'По карте',
        'contract' => 'По договору',
    ],
    'confirm' => [
        'payment' => 'Вы подтверждаете оплату?',
        'repayment' => 'Вы подтверждаете изменение способа оплаты?',
        'refusal' => 'Вы подтверждаете отказ от оплаты?',
        'execute' => 'Вы подтверждаете что заказ выполнен?',
        'canceled' => 'Вы подтверждаете что заказ ошибочный?',
    ],
    'orderTitle' => 'Заказ №:num',
    'notFoundTeam' => 'Нет сформированных бригад',
    'yes' => 'Да',
    'no' => 'Нет',
    'tooltip' => [
        'show' => 'Информация о заказе'
    ],
    'statusHistoriesNotFound' => 'Статус не изменялся'
];
