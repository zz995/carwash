<?php

return [
    'field' => [
        'id' => '№',
        'name' => 'Роль',
        'salary' => 'Зарплата'
    ],
    'salaryType' => [
        'teamPercent' => '% от заказа на бригаду',
        'selfPercent' => '% от заказа',
        'byDay' => '&#8381; за день',
    ],
    'action' => [
        'changeSalary' => 'Изменить запрплату',
    ],
    'message' => [
        'changeSalary' => 'Зарплата успешно изменена',
        'salaryIncorect' => 'Неверное значение',
    ],
    'page' => [
        'list' => [
            'header' => 'Список ролей'
        ],
        'changeSalary' => [
            'header' => 'Изменение зарплаты'
        ]
    ],
    'operation' => 'Операция',
];
