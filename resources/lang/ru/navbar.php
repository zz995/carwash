<?php

return [
    'logout' => 'Выйти',
    'superAdmin' => [
        'employees' => 'Сотрудники',
        'carWashOrder' => 'Заказы',
        'carWashServiceHistory' => 'История обращений',
        'roles' => 'Роли',
        'carWash' => 'Автомойка',
        'carWashWorks' => 'Виды работ',
        'carWashCarsModels' => 'Категории автомобилей',
        'contracts' => 'Договора',
        'serviceCenters' => 'Центры оказания услуг',
        'discountCards' => 'Дисконтные карты',
        'discountCardSteps' => 'Шаги дисконтной карты',
        'fines' => [
            'fines' => 'Штрафы',
            'paymentRefusalFineSize' => 'Размер штрафа',
        ]
    ],
    'employees' => [
        'teams' => 'Бригады',
        'washers' => 'Сотрудники',
        'order' => 'Заказы',
        'serviceHistory' => 'История',
        'cafe' => 'Кафе',
        'discountCards' => 'Дисконтные карты',
        'accruals' => 'Начисления',
        'accrual' => [
            'common' => 'Общие данные',
            'teamFine' => 'Командные штрафы',
            'personalFine' => 'Персональные штрафы',
            'employees' => 'Зарплаты сотрудников',
        ],
        'washer' => [
            'common' => 'Общая информация'
        ],
        'balance' => 'Балансы',
    ],
];
