<?php

return [
    'field' => [
        'id' => '№',
        'name' => 'Наименование',
        'type' => 'Тип',
        'several' => 'Несколько в корзине',
        'category1' => '1 категория',
        'category2' => '2 категория',
        'category3' => '3 категория',
        'category4' => '4 категория',
        'categoryN_price' => 'Стоимость',
        'categoryN_cost' => 'Расходы',
        'categoryN_salary' => 'Зарплата',
    ],
    'page' => [
        'list' => [
            'header' => 'Список работ автомойки',
        ],
        'create' => [
            'header' => 'Создание работы'
        ],
        'show' => [
            'header' => 'Информация о работе'
        ],
        'edit' => [
            'header' => 'Редактирование информации о работе',
            'action' => 'Редактирование'
        ],
    ],
    'tooltip' => [
        'show' => 'Информация о работе',
    ],
    'action' => [
        'create' => 'Создать новую работу',
        'edit' => 'Редактировать',
        'delete' => 'Удалить',
    ],
    'message' => [
        'successCreate' => 'Успешно создано',
        'successEdit' => 'Успешно изменено',
        'successDelete' => 'Успешно удалено',
    ],
    'confirm' => [
        'delete' => 'Вы действительно хотите удалить?',
    ],
    'several' => [
        'yes' => 'Да',
        'no' => 'Нет',
    ]
];
