<?php

return [
    'field' => [
        'date' => 'Дата',
        'type' => 'Тип',
        'value' => 'Сумма',
    ],
    'type' => [
        'by_order' => 'Начисление по заказу',
        'by_personal_fine' => 'Персональный штраф',
        'by_team_fine' => 'Командный штраф',
        'by_daily' => 'Начисление за выход на работу',
        'by_salary' => 'Выдана зарплата',
    ],
    'message' => [
        'range' => 'За период с :from по :to',
    ],
    'title' => 'Начисление №:num',
    'notExist' => 'Начисления отсутствуют',
    'sum' => 'Итого',
];
