<?php

return [
    'field' => [
        'sum' => 'Сумма',
        'employee' => 'Сотрудник',
        'value' => 'Сумма штрафа',
        'create_reason' => 'Комментарий',
        'date_create' => 'Дата создания',
        'comment' => 'Комментарий',
        'status' => 'Статус',
        'date' => 'Дата',
    ],
    'page' => [
        'create' => [
            'header' => 'Создание персонального штрафа'
        ],
        'list' => [
            'header' => 'Персональные штрафы'
        ],
        'show' => [
            'header' => 'Информация о персональном штрафе'
        ],
        'disable' => [
            'header' => 'Отменить штраф'
        ]
    ],
    'action' => [
        'create' => 'Оштрафовать',
        'disable' => 'Отменить штраф',
    ],
    'status' => [
        'active' => 'Активный',
        'disable' => 'Отмененый',
    ],
    'message' => [
        'successFine' => 'Сотрудник оштрафован',
        'successDisabledFine' => 'Штраф сотрудника отменен',
    ],
    'notExist' => 'Персональные штрафы не найдены',
    'fineTitle' => 'Персональный штраф №:num',
    'historyBlock' => [
        'title' => 'История',
        'notFound' => 'История отсутствует'
    ],
];
