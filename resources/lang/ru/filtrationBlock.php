<?php

return [
    'header' => 'Фильтрация',
    'search' => 'Искать',
    'reset' => 'Сбросить',
    'currentDay' => 'За сегодня',
    'currentMonth' => 'За текущий месяц'
];