<?php

return [
    'status' => [
        'disabled' => 'Отключен',
        'active' => 'Активный',
        'delete' => 'Уволен',
    ],
    'field' => [
        'id' => '№',
        'name' => 'Имя',
        'surname' => 'Фамилия',
        'patronymic' => 'Отчество',
        'phone' => 'Телефон',
        'phones' => 'Телефоны',
        'email' => 'Почта',
        'status' => 'Статус',
        'role' => 'Роль',
        'overdrive' => 'Перегонщик',
        'login' => 'Логин',
        'password' => 'Пароль',
        'confirmPassword' => 'Подтвердите пароль',
        'currentPassword' => 'Текущий пароль',
        'serviceCenter' => 'Автомойка',
        'team' => 'Бригада',
        'currentServiceCenter' => 'Находятся на'
    ],
    'loginHistoryField' => [
        'id' => '№',
        'type' => 'Тип',
        'created_at' => 'Дата',
    ],
    'action' => [
        'create' => 'Создать сотрудника',
        'edit' => 'Изменить данные',
        'changePassword' => 'Изменить пароль',
        'delete' => 'Уволить',
        'recovery' => 'Восстановить',
        'disable' => 'Отключить',
        'payout' => 'Выдать зарплату',
    ],
    'tooltip' => [
        'update' => 'Редактировать информацию о сотруднике',
        'show' => 'Информация о сотруднике',
    ],
    'fio' => 'ФИО',
    'message' => [
        'successCreate' => 'Сотрудник успешно создан',
        'successEdit' => 'Информация о сотруднике успешно обновлена',
        'successChangePassword' => 'Пароль успешно изменен',
        'successDelete' => 'Сотрудник успешно уволен',
        'successRecovery' => 'Сотрудник успешно восстановлен',
        'successDisable' => 'Сотрудник успешно отключен',
        'successPayout' => 'Зарплата успешно выдана',
        'wrongPassword' => 'Неверный пароль',
        'needLogin' => 'Необходимо заполнить поле',
        'needPassword' => 'Необходимо заполнить поле',
        'needServiceCenter' => 'Необходимо указать севистный центр',
        'impossibleOverdrive' => 'Перегонщиком может быть только мойщик',
    ],
    'page' => [
        'create' => [
            'header' => 'Создание сотрудника'
        ],
        'payout' => [
            'header' => 'Выдача запрлаты',
            'commonInfo' => 'Общая информация',
            'action' => 'Выдача зарплаты',
            'field' => [
                'sum' => 'Сумма'
            ],
        ],
        'edit' => [
            'header' => 'Изменение данных сотрудника :employe',
            'action' => 'Изменение данных',
        ],
        'show' => [
            'header' => 'Cотрудник :employe',
            'userInfo' => 'Информация о сотруднике',
            'userLoginHistory' => 'Информация последних входах и выходах',
        ],
        'list' => [
            'header' => 'Сотрудники',
        ],
        'salaryBreakdown' => [
            'header' => 'Разбивка зарплат',
        ],
        'changePassword' => [
            'header' => 'Изменение пароля сотрудника :employe',
            'action' => 'Изменение пароля',
        ],
    ],
    'confirm' => [
        'delete' => 'Вы действительно хотите уволить сотрудника :employe?',
        'recovery' => 'Вы действительно хотите востановить сотрудника :employe?',
        'disable' => 'Вы действительно хотите отключить сотрудника :employe?',
    ],
    'loginHistoriesNotFound' => 'История входов отсутствует',
    'loginHistories' => [
        'status' => [
            'login' => 'Вход',
            'logout' => 'Выход',
        ],
    ],
    'balance' => 'Баланс',
];
