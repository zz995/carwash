<?php

return [
    'field' => [
        'foreignNumber' => 'Иностранный номер',
    ],
    'message' => [
        'exist' => 'Машина с таким номером уже существует'
    ],
    'action' => [
        'addCar' => 'Добавить машину',
    ],
];
