<?php

return [
    'failed' => 'Неверный логин или пароль.',
    'delete' => 'Невозможно выполнить вход, обратитесь к администрации',
    'denied' => 'Недостаточно прав',
    'throttle' => 'Слишком много попыток. Попробуйте через :seconds секунд.',
    'login.header' => 'Вход',
    'login.button' => 'Войти',
    'link.forgot' => 'Забыли пароль?',
    'field.login' => 'Логин',
    'field.password' => 'Пароль',
    'field.remember' => 'Запомнить',
];
