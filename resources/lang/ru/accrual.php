<?php

return [
    'field' => [
        'orderCount' => 'Количество заказов',
        'receipt' => 'Выручка',
        'salary' => 'Зарплаты',
        'fine' => 'Штрафы',
        'result' => 'Итого',
        'today' => 'За сегодня',
        'currentMonth' => 'За текущий месяц',
        'lastMonth' => 'За прошлый месяц',
    ],
    'page' => [
        'common' => [
            'header' => 'Общие данные'
        ],
        'employees' => [
            'header' => 'Зарплаты сотрудников',
            'notExist' => 'Не найдены',
            'field' => [
                'salary' => 'Зарплата',
                'fine' => 'Штраф',
                'result' => 'Итого',
            ],
        ],
    ],
    'detailed' => 'Подробние',
];
