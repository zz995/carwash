<?php

return [
    'field' => [
        'sum' => 'Сумма',
        'employees' => 'Сотрудники',
        'value' => 'Сумма штрафа',
        'create_reason' => 'Комментарий',
        'date_create' => 'Дата создания',
        'status' => 'Статус',
    ],
    'paymentRefusalFineSize' => [
        'admin_fine' => 'Штраф для администратора',
        'washer_fine' => 'Штраф для мойщика',
    ],
    'page' => [
        'create' => [
            'header' => 'Штраф на команду'
        ],
        'list' => [
            'header' => 'Командные штрафы'
        ],
        'disable' => [
            'header' => 'Отменить штраф'
        ],
        'paymentRefusalFineSize' => [
            'header' => 'Изменение размера штрафа при отказе от оплаты заказа',
            'submit' => 'Изменить'
        ],
    ],
    'action' => [
        'create' => 'Назначить штраф',
        'disable' => 'Отменить штраф',
        'changePaymentRefusalFineSize' => 'Изменить размера штрафа при отказе от оплаты заказа'
    ],
    'message' => [
        'successFine' => 'Команда оштрафована',
        'successDisabledFine' => 'Штраф сотрудника отменен',
        'successChangePaymentRefusalFineSize' => 'Успешно изменен размер штрафа при отказе от оплаты заказа',
    ],
    'block' => 'Штраф на команду',
    'notExist' => 'Командные штрафы не найдены',
    'fineTitle' => 'Командный штраф №:num',
    'historyBlock' => [
        'notFound' => 'История отсутствует'
    ],
];
