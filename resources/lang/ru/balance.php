<?php

return [
    'field' => [
        'date' => 'Дата',
        'payment' => 'Методы оплаты',
        'point' => 'Отделы',
    ],
    'message' => [
        'range' => 'За период с :from по :to',
    ],
    'page' => [
        'common' => [
            'header' => 'Общие данные'
        ]
    ],
    'point' => [
        'carWash' => 'Автомойка',
        'cafe' => 'Кафе',
    ],
    'action' => [
        'showStatistic' => 'Показать'
    ],
    'block' => [
        'group' => 'Параметры'
    ],
    'report' => [
        'card' => 'безнал',
        'cash' => 'нал',
        'carwash' => 'Мойка',
        'common' => 'Общая',
        'sum' => 'Всего',
    ]
];
