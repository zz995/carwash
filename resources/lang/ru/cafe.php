<?php

return [
    'field' => [
        'sum' => 'Сумма',
        'type' => 'Тип',
    ],
    'page' => [
        'createSale' => [
            'header' => 'Продажа в кафе'
        ]
    ],
    'message' => [
        'successCreateSale' => 'Оплата принята'
    ],
    'action' => [
        'createSale' => 'Продажа',
    ]
];
