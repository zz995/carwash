
<div class="modal fade" id="{{ $target }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('modalWindow.header') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{ $body }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('modalWindow.no') }}</button>

                <form method="POST" action="{{ $action }}" class="mr-1">
                    @csrf
                    @method($method)
                    <button type="submit" class="btn btn-primary">{{ __('modalWindow.yes') }}</button>
                </form>
            </div>
        </div>
    </div>
</div>