{{ config('app.name', 'Laravel') }}
@if (isset(Auth::user()->service_center_id))
    <sup>
        <span class="badge badge-warning">{{ Auth::user()->serviceCenter->name }}</span>
    </sup>
@endif