
<div class="d-flex flex-row align-items-center">
    <div>
        {{ __('salaryBreakdown.sum') }}:&nbsp;
    </div>
    <div class="">
        <span class="rub h3">
            {{ moneyFormat($accrualsSum) }}
        </span>
    </div>
</div>
