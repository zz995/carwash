@if ($exist)
    <div class="return">
        <div class="container">
            <a href="{{ $data['href'] }}">
                <i class="fa fa-long-arrow-left"></i>
                {{ $data['message'] }}
            </a>
        </div>
    </div>
@endif