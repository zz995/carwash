@extends('layouts.auth')

@section('content')
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">{{ __('auth.login.header') }}</div>

            <div class="card-body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="service_center" class="col-md-4 col-form-label text-md-right">
                            {{ __('user.field.serviceCenter') }}
                        </label>

                        <div class="col-md-6">
                            <select id="service_center" class="form-control{{ $errors->has('service_center') ? ' is-invalid' : '' }}" name="service_center">
                                <option value=""></option>
                                @foreach ($serviceCenters as $serviceCenter)
                                    <option value="{{ $serviceCenter->id }}" {{ $serviceCenter->id == old('service_center', '') ? ' selected' : '' }}>{{ $serviceCenter->name }}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('service_center'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('service_center') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="login" class="col-sm-4 col-form-label text-md-right">
                            {{ __('auth.field.login') }}
                        </label>

                        <div class="col-md-6">
                            <input id="login" type="text" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }}" name="login" value="{{ old('login') }}" required autofocus>

                            @if ($errors->has('login'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('login') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('auth.field.password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('auth.login.button') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
