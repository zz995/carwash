<div class="form-group row car-number mb-0">
    <label for="new_car_number" class="col-md-4 col-form-label text-md-right">
        {{ __('discountCard.field.new_car_number') }}
    </label>

    <div class="col-md-6 d-flex">
        <input autocomplete="off" id="new_car_number" type="text" class="car-number__input form-control discount-card-form__new-car-number">
    </div>

    <div class="offset-md-4 col-md-6">
        <label>
            <input type="checkbox" autocomplete="off" class="car-number__foreign">
            {{ __('car.field.foreignNumber') }}
        </label>
    </div>
</div>

@include('employees.order.partials._markAndModel', ['car' => null, 'carMarks' => $carMarks, 'carModels' => collect(), 'showModel' => true, 'notRequired' => true])

<div class="form-group row car-number">
    <div class="offset-md-4 col-md-6">
        <button type="button" disabled class="discount-card-form__add-new-car btn">
            {{ __('car.action.addCar') }}
        </button>
    </div>
</div>

<div class="form-group row discount-card-form__new-cars-block {{ is_null(old('new_cars', isset($car) && isset($car->number) ? $car->number : null)) ? 'hidden' : '' }}">
    <span class="col-md-4 col-form-label text-md-right">
        {{ __('discountCard.field.new_cars') }}
    </span>

    <div class="col-md-6 discount-card-form__new-cars">

        @foreach(old('new_cars', isset($car) && isset($car->number) ? [$car->number] : []) as $index => $carNumber)
            <div class="discount-card-form__new-car mb-1">
                <input type="hidden" name="new_cars[]" value="{{ $carNumber }}">
                @if (($markAndModel = old('markAndModel', [])) && isset($markAndModel[$carNumber]))
                    <input type="hidden" name="markAndModel[{{ $carNumber }}][mark]" value="{{ $markAndModel[$carNumber]['mark'] }}">
                    <input type="hidden" name="markAndModel[{{ $carNumber }}][new_mark]" value="{{ $markAndModel[$carNumber]['new_mark'] }}">
                    <input type="hidden" name="markAndModel[{{ $carNumber }}][model]" value="{{ $markAndModel[$carNumber]['model'] }}">
                    <input type="hidden" name="markAndModel[{{ $carNumber }}][model_category]" value="{{ $markAndModel[$carNumber]['model_category'] }}">
                    <input type="hidden" name="markAndModel[{{ $carNumber }}][new_model]" value="{{ $markAndModel[$carNumber]['new_model'] }}">
                    <input type="hidden" name="markAndModel[{{ $carNumber }}][title_mark]" value="{{ $markAndModel[$carNumber]['title_mark'] }}">
                    <input type="hidden" name="markAndModel[{{ $carNumber }}][title_model]" value="{{ $markAndModel[$carNumber]['title_model'] }}">
                @endif
                <span>{{ $carNumber }}</span>
                @if (($markAndModel = old('markAndModel', [])) && isset($markAndModel[$carNumber]))
                    ({{ $markAndModel[$carNumber]['title_mark'] }} {{ $markAndModel[$carNumber]['title_model'] }})
                @endif
                <button type="button" class="btn btn-danger btn-sm discount-card-form__delete-car">{{ __("discountCard.action.deleteCar") }}</button>
                @if ($errors->has('new_cars.' . $index))
                    <div>
                        <span class="invalid-feedback d-block">
                            <strong>{{ $errors->first('new_cars.' . $index) }}</strong>
                        </span>
                    </div>
                @endif
            </div>
        @endforeach

        <script type="text/template" class="discount-card-form__new-car-template">
            <div class="discount-card-form__new-car mb-1">
                <input type="hidden" name="new_cars[]" value="<%-data.number%>">
                <% if(data.titleCarMark !== undefined) { %>
                    <input type="hidden" name="markAndModel[<%-data.number%>][mark]" value="<%-data.markAndModel.mark%>">
                    <input type="hidden" name="markAndModel[<%-data.number%>][new_mark]" value="<%-data.markAndModel.new_mark%>">
                    <input type="hidden" name="markAndModel[<%-data.number%>][model]" value="<%-data.markAndModel.model%>">
                    <input type="hidden" name="markAndModel[<%-data.number%>][model_category]" value="<%-data.markAndModel.model_category%>">
                    <input type="hidden" name="markAndModel[<%-data.number%>][new_model]" value="<%-data.markAndModel.new_model%>">
                    <input type="hidden" name="markAndModel[<%-data.number%>][title_mark]" value="<%-data.titleCarMark%>">
                    <input type="hidden" name="markAndModel[<%-data.number%>][title_model]" value="<%-data.titleCarModel%>">
                <% } %>
                <span>
                    <%-data.number%>
                    <% if(data.titleCarMark !== undefined) { %>
                        (<%-data.titleCarMark%> <%-data.titleCarModel%>)
                    <% } %>
                </span>
                <button type="button" class="btn btn-danger btn-sm discount-card-form__delete-car">{{ __("discountCard.action.deleteCar") }}</button>
            </div>
        </script>
    </div>
</div>
