<div class="discount-type-field form-group row">
    <label for="type" class="col-md-4 col-form-label text-md-right">
        {{ __('discountCard.field.type') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <select required autocomplete="off" id="type" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type">
            <option {{ old('type', $discountCard->type) == 0 ? 'selected' : '' }} value="0">{{ __('discountCard.types.cumulative') }}</option>
            <option {{ old('type', $discountCard->type) == 1 ? 'selected' : '' }} value="1">{{ __('discountCard.types.fixed') }}</option>
        </select>

        @if ($errors->has('type'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('type') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="fixed-discount-field form-group row">
    <label for="disc" class="col-md-4 col-form-label text-md-right">
        {{ __('discountCard.field.disc') }}
    </label>

    <div class="col-md-6">
        <div class="input-group">
            <input autocomplete="off" id="disc" type="number" class="form-control{{ $errors->has('disc') ? ' is-invalid' : '' }}" name="disc" value="{{ old('disc', isset($discountCard->id) ? $discountCard->getDisc() : '') }}">
            <div class="input-group-prepend">
                <div class="input-group-text">%</div>
            </div>
        </div>

        @if ($errors->has('disc'))
            <span class="invalid-feedback d-block">
                <strong>{{ $errors->first('disc') }}</strong>
            </span>
        @endif
    </div>
</div>