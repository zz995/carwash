<div class="form-group row discount-card-form__saved-cars-block">
    <span class="col-md-4 col-form-label text-md-right">
        {{ __('discountCard.field.saved_cars') }}
    </span>

    <div class="col-md-6 discount-card-form__saved-cars">

        @foreach($discountCard->cars as $car)
            <div class="discount-card-form__saved-car mb-1">
                <input type="hidden" name="saved_cars[]" value="{{ $car->id }}">
                <span>{{ $car->number }}</span>
                <button type="button" class="btn btn-danger btn-sm discount-card-form__delete-saved-car">{{ __("discountCard.action.deleteCar") }}</button>
            </div>
        @endforeach
    </div>
</div>
