<div class="discount-card-number form-group row">
    <label for="number" class="col-md-4 col-form-label text-md-right">
        {{ __('discountCard.field.number') }}
    </label>

    <div class="col-md-6">
        <input autocomplete="off" id="number" type="text" data-number="{{ $discountCard->number }}" data-location="{{ route('employees.discountCard.check') }}" class="discount-card-number__input form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" value="{{ old('number', $discountCard->number) }}">

        <script type="text/template" class="discount-card-number__error-template">
            <span class="invalid-feedback discount-card-number__error d-block">
                <strong>{!! '<%=data.message%>' !!}</strong>
            </span>
        </script>

        @if ($errors->has('number'))
            <span class="invalid-feedback discount-card-number__error">
                <strong>{{ $errors->first('number') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="phone" class="col-md-4 col-form-label text-md-right">
        {{ __('discountCard.field.phone') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input required autocomplete="off" id="phone" type="tel" class="phone-input form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone', $discountCard->getClientPhone()) }}">

        @if ($errors->has('phone'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">
        {{ __('discountCard.field.name') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input required autocomplete="off" id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $discountCard->getClientName()) }}">

        @if ($errors->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="comment" class="col-md-4 col-form-label text-md-right">
        {{ __('discountCard.field.comment') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <textarea required autocomplete="off" id="comment" type="text" class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}" name="comment">{{ old('comment', '') }}</textarea>

        @if ($errors->has('comment'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('comment') }}</strong>
            </span>
        @endif
    </div>
</div>
