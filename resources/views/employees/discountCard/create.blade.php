@extends('employees.layouts.admin')

@section('scripts')
    <script src="{{ mix('js/discount-card-form.js', 'build') }}" defer></script>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf
                <input id="car_id" type="hidden" value="{{ isset($car) ? $car->id : '' }}">
                @include('employees.discountCard.form._main', ['discountCard' => $discountCard])
                @include('employees.discountCard.form._disc', ['discountCard' => $discountCard])

                <hr>
                @include('employees.discountCard.form._carTitle')
                @include('employees.discountCard.form._newCarNumber', ['discountCard' => $discountCard, 'car' => $car, 'carMarks' => $carMarks])
                <hr>

                @include('employees.discountCard.form._submit', ['buttonTitle' => __('discountCard.action.create')])
            </form>
        </div>
    </div>


@endsection