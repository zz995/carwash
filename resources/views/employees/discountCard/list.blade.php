@extends('employees.layouts.admin')

@section('content')
    <p>
        <a href="{{ route('employees.discountCard.create') }}" class="btn btn-success">
            {{ __('discountCard.action.create') }}
        </a>
    </p>

    <div class="filter">
        <div class="filter__header">{{ __('filtrationBlock.header') }}</div>
        <div class="filter__body">
            <form action="?" method="GET">
                <div class="filter__fields">
                    <div class="filter__field">
                        <label for="number" class="filter__label">{{ __('discountCard.field.number') }}</label>
                        <input id="number" class="filter__value" name="number" value="{{ request('number') }}">
                    </div>
                    <div class="filter__field car-number">
                        <label for="car_number" class="filter__label">{{ __('discountCard.field.car_number') }}</label>
                        <input id="car_number" class="filter__value car-number__input" name="car_number" value="{{ request('car_number') }}">
                        <label>
                            <input type="checkbox" autocomplete="off" name="foreign-number" value="1" {{ request('foreign-number') ? 'checked' : '' }} class="car-number__foreign skip-focus">
                            {{ __('car.field.foreignNumber') }}
                        </label>
                    </div>
                    <div class="filter__field">
                        <label class="filter__label filter__label_empty">&nbsp;</label>
                        <button type="submit" class="filter__button">
                            {{ __('filtrationBlock.search') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card orders-list">
        <div class="list">
            @forelse($discountCards as $discountCard)
                <div class="list__item">
                    <p class="list__header">
                        <a href="{{ route('employees.discountCard.show', $discountCard) }}">
                            @if ($discountCard->hasNumber())
                                {{ __('discountCard.discountTitle', ['num' => $discountCard->number]) }}
                            @else
                                {{ __('discountCard.discountTitleNotIssued') }}
                            @endif
                        </a>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('discountCard.field.disc') }}:
                        </span>
                        <span class="attribute__value">
                            {{ $discountCard->getDisc() }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('discountCard.field.car_numbers') }}:
                        </span>
                        <span class="attribute__value">
                            @foreach($discountCard->cars as $car)
                                {{ $car->number }}{{ $loop->last ? '' : ', ' }}
                            @endforeach
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('discountCard.field.name') }}:
                        </span>
                        <span class="attribute__value">
                            {{ $discountCard->getClientName() }}
                        </span>
                    </p>
                </div>
            @empty
                <div class="p-3">
                    {{ __('discountCard.notExist') }}
                </div>
            @endforelse
        </div>
    </div>

    <div class="container mt-3">
        <div class="row justify-content-center">
            {{ $discountCards->appends($_GET)->links() }}
        </div>
    </div>

@endsection
