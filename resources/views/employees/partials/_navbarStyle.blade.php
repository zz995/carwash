<style>
    .navbar_custom-color.navbar {
        background: {{ backgroundColor() }} !important;
    }
    .navbar_custom-color .navbar__brand, .navbar_custom-color .nav-link.active {
        color: {{ fontColorRgba(1) }} !important;
    }
    .navbar_custom-color .nav-link {
        color: {{ fontColorRgba(0.75) }} !important;
    }
</style>