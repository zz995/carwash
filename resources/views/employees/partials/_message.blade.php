<div class="server-notifications">
    <script type="text/template" class="server-notifications__example-alert">
        <div class="server-notifications__alert alert" role="alert">
            <%=message%>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </script>
</div>