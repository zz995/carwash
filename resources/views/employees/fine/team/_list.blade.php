<div class="list">
    @forelse($teamFines as $fine)
        <div class="list__item">
            <p class="list__header">
                <a href="{{ route('employees.order.show', $fine->order) }}">
                    {{ __('teamFine.fineTitle', ['num' => $fine->id]) }}
                </a>
            </p>
            <p class="list__attribute attribute">
                <span class="attribute__name">
                    {{ __('teamFine.field.employees') }}:
                </span>
                <span class="attribute__value">
                    {{ listEmployees($fine->users) }}
                </span>
            </p>
            <p class="list__attribute attribute">
                <span class="attribute__name">
                    {{ __('teamFine.field.sum') }}:
                </span>
                <span class="attribute__value rub">
                    {{ moneyFormat($fine->value) }}
                </span>
            </p>
            <p class="list__attribute attribute">
                <span class="attribute__name">
                    {{ __('personalFine.field.date_create') }}:
                </span>
                <span class="attribute__value">
                    {{ dataTimeFormat($fine->created_at) }}
                </span>
            </p>
            <p class="list__attribute attribute">
                <span class="attribute__name">
                    {{ __('personalFine.field.create_reason') }}:
                </span>
                <span class="attribute__value">
                    {{ $fine->lastComment() }}
                </span>
            </p>
        </div>
    @empty
        <div class="p-3">
            {{ __('teamFine.notExist') }}
        </div>
    @endforelse
</div>

<div class="container">
    <div class="row justify-content-center">
        {{ $teamFines->appends($_GET)->links() }}
    </div>
</div>