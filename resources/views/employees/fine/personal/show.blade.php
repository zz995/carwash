@extends('employees.layouts.admin')

@section('content')
    <div class="d-flex flex-row mb-3">
        @if($fine->isActive())
            <a class="btn btn-danger mr-1" href="{{ route('employees.washer.disablePersonalFine', $fine) }}">
                {{ __('personalFine.action.disable') }}
            </a>
        @endif
    </div>

    <div class="card mb-3">
        <div class="card-body">
            <div>
                <p class="attribute">
                    <span class="attribute__name">{{ __('personalFine.field.status') }}:</span>
                    <span class="attribute__value">
                        @include('employees.fine._status', ['fine' => $fine])
                    </span>
                </p>
                <p class="attribute">
                    <span class="attribute__name">{{ __('personalFine.field.employee') }}:</span>
                    <span class="attribute__value">
                        <a href="{{ route('employees.washer.show', $fine->user) }}">
                            @include('employees.teams._employee', ['employee' => $fine->user])
                        </a>
                    </span>
                </p>
                <p class="attribute">
                    <span class="attribute__name">{{ __('personalFine.field.sum') }}:</span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($fine->value) }}
                    </span>
                </p>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            {{ __('personalFine.historyBlock.title') }}
        </div>
        <div class="card-body">
            <div class="list">
                @forelse($fine->histories as $history)
                    <div class="list__item">
                        <p class="list__attribute attribute">
                            <span class="attribute__name">{{ __('personalFine.field.sum') }}:</span>
                            <span class="attribute__value rub">
                                {{ moneyFormat($history->value) }}
                            </span>
                        </p>
                        <p class="list__attribute attribute">
                            <span class="attribute__name">{{ __('personalFine.field.date') }}:</span>
                            <span class="attribute__value">
                                {{ dataTimeFormat($history->timestamp) }}
                            </span>
                        </p>
                        <p class="list__attribute attribute">
                            <span class="attribute__name">{{ __('personalFine.field.comment') }}:</span>
                            <span class="attribute__value">
                                {!! nl2br(e($history->comment)) !!}
                            </span>
                        </p>
                    </div>
                @empty
                    <div class="p-3">
                        {{ __('personalFine.historyBlock.notFound') }}
                    </div>
                @endforelse
            </div>
        </div>
    </div>
@endsection