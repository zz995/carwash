<div class="list">
    @forelse($personalFines as $fine)
        <div class="list__item">
            <p class="list__header">
                <a href="{{ route('employees.washer.showPersonalFine', $fine) }}">
                    {{ __('personalFine.fineTitle', ['num' => $fine->id]) }}
                </a>
            </p>
            <p class="list__attribute attribute">
                <span class="attribute__name">
                    {{ __('personalFine.field.status') }}:
                </span>
                <span class="attribute__value">
                    @include('employees.fine._status', ['fine' => $fine])
                </span>
            </p>
            <p class="list__attribute attribute">
                <span class="attribute__name">
                    {{ __('personalFine.field.employee') }}:
                </span>
                <span class="attribute__value">
                    <a href="{{ route('employees.washer.show', $fine->user) }}">
                        @include('employees.teams._employee', ['employee' => $fine->user])
                    </a>
                </span>
            </p>
            <p class="list__attribute attribute">
                <span class="attribute__name">
                    {{ __('personalFine.field.sum') }}:
                </span>
                <span class="attribute__value rub">
                    {{ moneyFormat($fine->value) }}
                </span>
            </p>
            <p class="list__attribute attribute">
                <span class="attribute__name">
                    {{ __('personalFine.field.date_create') }}:
                </span>
                <span class="attribute__value">
                    {{ dataTimeFormat($fine->created_at) }}
                </span>
            </p>
            <p class="list__attribute attribute">
                <span class="attribute__name">
                    {{ __('personalFine.field.create_reason') }}:
                </span>
                <span class="attribute__value">
                    {{ $fine->lastComment() }}
                </span>
            </p>
        </div>
    @empty
        <div class="p-3">
            {{ __('personalFine.notExist') }}
        </div>
    @endforelse
</div>

<div class="container">
    <div class="row justify-content-center">
        {{ $personalFines->appends($_GET)->links() }}
    </div>
</div>