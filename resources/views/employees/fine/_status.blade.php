@if ($fine->isActive())
    <span class="badge badge-success">{{ __('personalFine.status.active') }}</span>
@else
    <span class="badge badge-danger">{{ __('personalFine.status.disable') }}</span>
@endif