@extends('employees.layouts.admin')


@section('content')
    @include('employees.washers._actions', ['user' => $user])
    @include('employees.washers._nav', ['user' => $user])

    <div class="card">
        @include('employees.fine.team._list', ['teamFines' => $teamFines])
    </div>
@endsection