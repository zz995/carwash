@extends('employees.layouts.admin')


@section('content')
    @include('employees.washers._actions', ['user' => $user])
    @include('employees.washers._nav', ['user' => $user])

    <div class="card">
        <div class="card-body">
            <div>
                <p><strong>{{ __('user.field.id') }}: </strong>{{ $user->id }}</p>
                <p><strong>{{ __('user.fio') }}: </strong>{{ $user->getFio()->getFullName() }}</p>
                <p><strong>{{ __('user.field.login') }}: </strong>{{ $user->login }}</p>
                <p><strong>{{ __('user.field.email') }}: </strong>{{ $user->email }}</p>
                <p><strong>{{ __('user.field.phones') }}: </strong>{!! phonesFormat($user->phones, true) !!}</p>
                <p>
                    <strong>{{ __('user.field.status') }}: </strong>
                    @if ($user->isActive())
                        <span class="badge badge-primary">{{ __('user.status.active') }}</span>
                    @endif
                    @if ($user->isDelete())
                        <span class="badge badge-light">{{ __('user.status.delete') }}</span>
                    @endif
                </p>
                @if (isset($user->service_center_id))
                    <p><strong>{{ __('user.field.serviceCenter') }}: </strong>{{ $user->serviceCenter->name }}</p>
                @endif
            </div>
        </div>
    </div>
@endsection