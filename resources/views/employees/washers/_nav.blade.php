<ul class="nav nav-tabs mb-3">
    @can ('manage-washers')
        <li class="nav-item">
            <a class="nav-link {{ Route::currentRouteNamed('employees.washer.show') ? 'active' : '' }}" href="{{ route('employees.washer.show', $user) }}">
                {{ __('navbar.employees.washer.common') }}
            </a>
        </li>
    @endcan
    @can ('manage-team-fines')
        <li class="nav-item">
            <a class="nav-link {{ Route::currentRouteNamed('employees.washer.personalFines') ? 'active' : '' }}" href="{{ route('employees.washer.personalFines', $user) }}">
                {{ __('navbar.employees.accrual.personalFine') }}
            </a>
        </li>
    @endcan
    @can ('manage-personal-fines')
        <li class="nav-item">
            <a class="nav-link {{ Route::currentRouteNamed('employees.washer.teamFines') ? 'active' : '' }}" href="{{ route('employees.washer.teamFines', $user) }}">
                {{ __('navbar.employees.accrual.teamFine') }}
            </a>
        </li>
    @endcan
    @can ('manage-salary-breakdown')
        <li class="nav-item">
            <a class="nav-link {{ Route::currentRouteNamed('employees.washer.salaryBreakdown') ? 'active' : '' }}" href="{{ route('employees.washer.salaryBreakdown', $user) }}">
                {{ __('user.page.salaryBreakdown.header') }}
            </a>
        </li>
    @endcan
</ul>