<div class="d-flex flex-row mb-3">
    @can ('manage-personal-fine', $user)
        <a href="{{ route('employees.washer.createFine', ['user' => $user]) }}" class="btn btn-danger mr-1">
            {{ __('personalFine.action.create') }}
        </a>
    @endcan
</div>