@extends('employees.layouts.admin')

@section('content')
    @include('employees.washers._actions', ['user' => $user])
    @include('employees.washers._nav', ['user' => $user])

    <form action="?" method="GET">
        <div class="filter">
            <div class="filter__header">{{ __('filtrationBlock.header') }}</div>
            <div class="filter__body">
                <div class="filter__fields">
                    <div class="filter__field">
                        <label for="range" class="col-form-label">{{ __('balance.field.date') }}</label>
                        <div class="d-flex">
                            <button
                                    class="btn btn-sm btn-primary change-select-data"
                                    data-target="#range"
                                    data-date="{{ getCurrentDay() }} - {{ getCurrentDay() }}"
                            >
                                {{ __('filtrationBlock.currentDay') }}
                            </button>
                            <button
                                    class="ml-1 mr-1 btn btn-sm btn-primary change-select-data"
                                    data-target="#range"
                                    data-date="{{ getCurrentMonth() }} - {{ getCurrentDay() }}"
                            >
                                {{ __('filtrationBlock.currentMonth') }}
                            </button>
                            <input
                                    autocomplete="off"
                                    type="text"
                                    id="range"
                                    name="range"
                                    data-range="true"
                                    @if (isset($dataRange))
                                    value="{{ dateToPickerFormat($dataRange->getFrom()) }} - {{ dateToPickerFormat($dataRange->getTo()) }}"
                                    @endif
                                    data-multiple-dates-separator=" - "
                                    class="form-control datepicker-here"
                            />
                        </div>
                    </div>
                    <div class="filter__field">
                        <label class="filter__label filter__label_empty">&nbsp;</label>
                        <button type="submit" class="filter__button">
                            {{ __('filtrationBlock.search') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="card">

        @if (isset($dataRange))
            <div class="card-body">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{ __('salaryBreakdown.message.range', ['from' => dataTimeFormat($dataRange->getFrom()), 'to' => dataTimeFormat($dataRange->getTo())]) }}
                </div>
            </div>
        @endif

        <div class="card-body pb-0 pt-0">
            @include('partials.salaryBreakdown._sum', ['accrualsSum' => $accrualsSum])
        </div>

        <div class="list">
            @forelse ($accruals as $accrual)
                <div class="list__item">
                    <p class="list__header">
                        {{ __('salaryBreakdown.title', ['num' => $accrual->id]) }}
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('salaryBreakdown.field.type') }}:
                        </span>
                        <span class="attribute__value">
                            @if ($accrual->isAccrualByOrder())
                                 <a href="{{ route('employees.order.show', $accrual->initiator_id) }}">
                                     {{ __('salaryBreakdown.type.by_order') }}
                                 </a>
                            @elseif ($accrual->isAccrualByPersonalFine())
                                <a href="{{ route('employees.washer.showPersonalFine', $accrual->initiator_id) }}">
                                    {{ __('salaryBreakdown.type.by_personal_fine') }}
                                </a>
                            @elseif ($accrual->isAccrualByTeamFine())
                                <a href="{{ route('employees.order.show', $accrual->initiator->order_id) }}">
                                    {{ __('salaryBreakdown.type.by_team_fine') }}
                                </a>
                            @elseif ($accrual->isAccrualByDaily())
                                {{ __('salaryBreakdown.type.by_daily') }}
                            @elseif ($accrual->isAccrualBySalary())
                                {{ __('salaryBreakdown.type.by_salary') }}
                            @endif
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('salaryBreakdown.field.value') }}:
                        </span>
                        <span class="attribute__value rub">
                            {{ moneyFormat($accrual->value) }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('salaryBreakdown.field.date') }}:
                        </span>
                        <span class="attribute__value">
                            {{ dataTimeFormat($accrual->timestamp) }}
                        </span>
                    </p>
                </div>
            @empty
                <div class="p-3">
                    {{ __('salaryBreakdown.notExist') }}
                </div>
            @endforelse
        </div>

        <div class="container">
            <div class="row justify-content-center">
                {{ $accruals->appends($_GET)->links() }}
            </div>
        </div>
    </div>
@endsection