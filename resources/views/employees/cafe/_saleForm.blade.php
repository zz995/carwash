
<div class="form-group row payment-types">
    <label class="col-md-4 col-form-label text-md-right">
        {{ __('order.selectTypePayment') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <div class="">
            <div class="d-inline-block">
                <input id="cashType" type="radio" name="payment-type" value="1" {{ old('payment-type', '1') == 1 ? 'checked' : '' }}>
                <label for="cashType">{{ __('order.paymentType.cash') }}</label>
            </div>
            <div class="d-inline-block">
                <input id="cardType" type="radio" name="payment-type" value="2" {{ old('payment-type', '1') == 2 ? 'checked' : '' }}>
                <label for="cardType">{{ __('order.paymentType.card') }}</label>
            </div>
        </div>

        @if ($errors->has('payment-type'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('payment-type') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="sum" class="col-md-4 col-form-label text-md-right">
        {{ __('cafe.field.sum') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="sum" type="number" class="form-control{{ $errors->has('sum') ? ' is-invalid' : '' }}" name="sum" value="{{ old('sum', $sale->sum) }}" required>

        @if ($errors->has('sum'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('sum') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="sale-type" class="col-md-4 col-form-label text-md-right">
        {{ __('cafe.field.type') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <select id="sale-type" class="form-control{{ $errors->has('sale-type') ? ' is-invalid' : '' }}" name="sale-type">
            <option value=""></option>
            @foreach ($saleTypes as $saleType)
                <option value="{{ $saleType->id }}" {{ $saleType->id == old('saleType', isset($sale->id) ? $sale->saleType->id : '') ? ' selected' : '' }}>{{ $saleType->name }}</option>
            @endforeach;
        </select>

        @if ($errors->has('sale-type'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('sale-type') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        <button type="submit" class="btn btn-primary">
            {{ $buttonTitle }}
        </button>
    </div>
</div>
