@extends('employees.layouts.cashier')

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf
                @include('employees.cafe._saleForm', ['sale' => $sale, 'buttonTitle' => __('cafe.action.createSale')])
            </form>
        </div>
    </div>
@endsection