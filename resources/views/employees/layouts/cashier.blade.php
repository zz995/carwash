<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('pageTitle', isset($pageTitle) ? $pageTitle : config('app.name', 'Laravel'))</title>

    <!-- Scripts -->
    @section('scripts')
        <script src="{{ mix('js/admin.js', 'build') }}" defer></script>
    @endsection
    @yield('scripts')

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('css/admin.css', 'build') }}" rel="stylesheet">
    @include('employees.partials._navbarStyle')
</head>
<body>
    <header>
        <nav class="navbar navbar_custom-color">
            <div class="container">
                <span class="navbar__brand">
                    @include('partials._brand')
                </span>
                <button class="navbar__toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="navbar__links show" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @can('manage-payment-order')
                            <li>
                                <a class="nav-link {{ Route::currentRouteNamed('employees.order.payment.list') ? 'active' : '' }}" href="{{ route('employees.order.payment.list') }}">
                                    {{ __('navbar.employees.order') }}
                                </a>
                            </li>
                        @endcan
                        @can('manage-sale-cafe')
                            <li>
                                <a class="nav-link {{ Route::currentRouteNamed('employees.cafe.createSale') ? 'active' : '' }}" href="{{ route('employees.cafe.createSale') }}">
                                    {{ __('navbar.employees.cafe') }}
                                </a>
                            </li>
                        @endcan
                        @can('manage-balance')
                            <li>
                                <a class="nav-link {{ Route::currentRouteNamed('employees.balance.common') ? 'active' : '' }}" href="{{ route('employees.balance.common') }}">
                                    {{ __('navbar.employees.balance') }}
                                </a>
                            </li>
                        @endcan
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();"
                                >
                                    {{ __('navbar.logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        @yield('secondaryNavbar')
    </header>

    @include('partials.return')
    <main class="app-content py-4">
        <div class="container container_white">
            @section('breadcrumbs', Breadcrumbs::render())
            @yield('breadcrumbs')
            @include('partials.flash.flash')
            @yield('content')
        </div>
    </main>

    <footer>
        <div class="container">
            <div class="border-top pt-3">
                <p>&copy; {{ date('Y') }} - {{ config('app.name', 'Laravel') }}</p>
            </div>
        </div>
    </footer>
</body>
</html>
