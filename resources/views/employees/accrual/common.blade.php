@extends('employees.layouts.admin')


@section('content')
    @include('employees.accrual._nav')

    <div class="card">
        <div class="list">
            <div class="list__item">
                <p class="list__header">
                    {{ __('accrual.field.today') }}
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.orderCount') }}:
                    </span>
                    <span class="attribute__value">
                        {{ $commonAccrual->today->getOrderCount() }}
                    </span>
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.receipt') }}:
                    </span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($commonAccrual->today->getReceipt()) }}
                    </span>
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.salary') }}:
                    </span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($commonAccrual->today->getSalary()) }}
                    </span>
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.fine') }}:
                    </span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($commonAccrual->today->getFine()) }}
                    </span>
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.result') }}:
                    </span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($commonAccrual->today->getResult()) }}
                    </span>
                </p>
            </div>
            <div class="list__item">
                <p class="list__header">
                    {{ __('accrual.field.currentMonth') }}
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.orderCount') }}:
                    </span>
                    <span class="attribute__value">
                        {{ $commonAccrual->currentMonth->getOrderCount() }}
                    </span>
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.receipt') }}:
                    </span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($commonAccrual->currentMonth->getReceipt()) }}
                    </span>
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.salary') }}:
                    </span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($commonAccrual->currentMonth->getSalary()) }}
                    </span>
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.fine') }}:
                    </span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($commonAccrual->currentMonth->getFine()) }}
                    </span>
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.result') }}:
                    </span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($commonAccrual->currentMonth->getResult()) }}
                    </span>
                </p>
            </div>
            <div class="list__item">
                <p class="list__header">
                    {{ __('accrual.field.lastMonth') }}
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.orderCount') }}:
                    </span>
                    <span class="attribute__value">
                        {{ $commonAccrual->lastMonth->getOrderCount() }}
                    </span>
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.receipt') }}:
                    </span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($commonAccrual->lastMonth->getReceipt()) }}
                    </span>
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.salary') }}:
                    </span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($commonAccrual->lastMonth->getSalary()) }}
                    </span>
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.fine') }}:
                    </span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($commonAccrual->lastMonth->getFine()) }}
                    </span>
                </p>
                <p class="list__attribute attribute">
                    <span class="attribute__name">
                        {{ __('accrual.field.result') }}:
                    </span>
                    <span class="attribute__value rub">
                        {{ moneyFormat($commonAccrual->lastMonth->getResult()) }}
                    </span>
                </p>
            </div>
        </div>
    </div>
@endsection