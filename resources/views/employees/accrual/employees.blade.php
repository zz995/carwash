@extends('employees.layouts.admin')


@section('content')
    @include('employees.accrual._nav')

    <div class="filter">
        <div class="filter__header">{{ __('balance.block.group') }}</div>
        <div class="filter__body">
            <form action="?" method="GET">
                <div class="filter__fields">
                    <div class="filter__field col-md-6">
                        <label for="range" class="filter__label">{{ __('balance.field.date') }}</label>
                        <div class="d-flex">
                            <button
                                    class="btn btn-sm btn-primary change-select-data"
                                    data-target="#dataRange"
                                    data-date="{{ getCurrentDay() }} - {{ getCurrentDay() }}"
                            >
                                {{ __('filtrationBlock.currentDay') }}
                            </button>
                            <button
                                    class="ml-1 mr-1 btn btn-sm btn-primary change-select-data"
                                    data-target="#dataRange"
                                    data-date="{{ getCurrentMonth() }} - {{ getCurrentDay() }}"
                            >
                                {{ __('filtrationBlock.currentMonth') }}
                            </button>
                            <input id="dataRange" autocomplete="off" type="text" name="range" data-range="true" value="{{ dateToPickerFormat($dataRange->getFrom()) }} - {{ dateToPickerFormat($dataRange->getTo()) }}" data-multiple-dates-separator=" - " class="datepicker-here filter__value"/>
                        </div>
                    </div>
                    <div class="filter__field">
                        <label class="filter__label filter__label_empty">&nbsp;</label>
                        <button type="submit" class="filter__button">
                            {{ __('balance.action.showStatistic') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
        <div class="list">
            @forelse($accrualCollection->getAccruals() as $employeeId => $accrual)
                <div class="list__item">
                    <p class="list__header">
                        @if ($accrual->getEmployee()->isWasher())
                            <a href="{{ route('employees.washer.show', $employeeId) }}">
                                {{ $accrual->getName() }}
                            </a>
                        @else
                            {{ $accrual->getName() }}
                        @endif
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('accrual.page.employees.field.salary') }}:
                        </span>
                        <span class="attribute__value rub">
                            {{ moneyFormat($accrual->getSalary()) }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('accrual.page.employees.field.fine') }}:
                        </span>
                        <span class="attribute__value rub">
                            {{ moneyFormat($accrual->getFine()) }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('accrual.page.employees.field.result') }}:
                        </span>
                        <span class="attribute__value rub">
                            {{ moneyFormat($accrual->getResult()) }}
                        </span>
                    </p>
                    @if ($accrual->getEmployee()->isWasher())
                        <p class="list__attribute attribute">
                            <a href="{{ route('employees.washer.salaryBreakdown', $employeeId) }}" class="attribute__name">
                                {{ __('accrual.detailed') }}
                            </a>
                        </p>
                    @endif
                </div>
            @empty
                <div class="p-3">
                    {{ __('accrual.page.employees.notExist') }}
                </div>
            @endforelse
        </div>

        <div class="container">
            <div class="row justify-content-center">
                {{ $accrualCollection->getEmployees()->appends($_GET)->links() }}
            </div>
        </div>
    </div>
@endsection