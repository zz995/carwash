<ul class="nav nav-tabs mb-3">
    @can ('manage-accrual')
        <li class="nav-item">
            <a class="nav-link {{ Route::currentRouteNamed('employees.accrual.common') ? 'active' : '' }}" href="{{ route('employees.accrual.common') }}">
                {{ __('navbar.employees.accrual.common') }}
            </a>
        </li>
    @endcan
    @can ('manage-team-fines')
        <li class="nav-item">
            <a class="nav-link {{ Route::currentRouteNamed('employees.accrual.personalFines') ? 'active' : '' }}" href="{{ route('employees.accrual.personalFines') }}">
                {{ __('navbar.employees.accrual.personalFine') }}
            </a>
        </li>
    @endcan
    @can ('manage-personal-fines')
        <li class="nav-item">
            <a class="nav-link {{ Route::currentRouteNamed('employees.accrual.teamFines') ? 'active' : '' }}" href="{{ route('employees.accrual.teamFines') }}">
                {{ __('navbar.employees.accrual.teamFine') }}
            </a>
        </li>
    @endcan
    @can ('manage-accrual')
        <li class="nav-item">
            <a class="nav-link {{ Route::currentRouteNamed('employees.accrual.employees') ? 'active' : '' }}" href="{{ route('employees.accrual.employees') }}">
                {{ __('navbar.employees.accrual.employees') }}
            </a>
        </li>
    @endcan
</ul>