@extends('employees.layouts.admin')


@section('content')
    @include('employees.accrual._nav')

    <div class="card">
        @include('employees.fine.personal._list', ['personalFines' => $personalFines])
    </div>
@endsection