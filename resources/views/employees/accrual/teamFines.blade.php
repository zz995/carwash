@extends('employees.layouts.admin')


@section('content')
    @include('employees.accrual._nav')

    <div class="card">
        @include('employees.fine.team._list', ['teamFines' => $teamFines])
    </div>
@endsection