
@foreach($washers as $washer)
    <div class="list__item">
        <p class="list__header">
            <a href="{{ route('employees.washer.show', $washer) }}">
                @include('employees.teams._employee', ['employee' => $washer])
            </a>
        </p>
        <p class="list__attribute attribute">
            <span class="attribute__name">
                {{ __('user.field.phones') }}:
            </span>
            <span class="attribute__value">
                {!! phonesFormat($washer->phones, true) !!}
            </span>
        </p>

        @if ($washer->getTeam() !== null)
            <p class="list__attribute attribute">
                <span class="attribute__name">
                    {{ __('user.field.team') }}:
                </span>
                <a href="{{ route('employees.teams.show', $washer->getTeam()) }}" class="attribute__value">
                    {{ $washer->getTeam()->name }}
                </a>
            </p>
        @endif

        @if ($showCurrentServiceCenter && !$washer->currentEqualConstantServiceCenter() && $washer->getCurrentServiceCenter() !== null)
            <p class="list__attribute attribute">
                <span class="attribute__name">
                    {{ __('user.field.currentServiceCenter') }}:
                </span>
                <span class="attribute__value">
                    {{ $washer->getCurrentServiceCenter()->name }}
                </span>
            </p>
        @endif
    </div>
@endforeach