@foreach ($employees as $employe)
    <p>
        <input autocomplete="off" type="checkbox" name="employees[]" value="{{ $employe->id }}" {{ in_array($employe->id, old('employees', $employeesIds)) ? ' checked' : '' }}>
        <label class="mb-0">
            @include('employees.teams._employee', ['employee' => $employe])
            @if ($employe->getTeam() !== null)
                <br>
                <a href="{{ route('employees.teams.show', $employe->getTeam()) }}" class="attribute__value small">
                    {{ $employe->getTeam()->name }}
                </a>
            @endif
        </label>

    </p>
@endforeach