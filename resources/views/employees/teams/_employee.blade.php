{{ $employee->getFio()->getFullName() }}
@if ($employee->service_center_id == Auth::user()->service_center_id)
    <sup>
        <span class="badge badge-success">{{ __('team.field.our') }}</span>
    </sup>
@elseif (isset($employee->service_center_id))
    <sup>
        <span class="badge badge-warning">{{ $employee->serviceCenter->name }}</span>
    </sup>
@endif