<div class="col-md-6">
    <div class="team-employees-list">
        <p class="team-employees-list__header">{{ __('team.sep.our') }}</p>
        <div class="team-employees-list__our">
            @include('employees.teams._employeeList', ['employeesIds' => $employeesIds, 'employees' => $employees->getOur()])
        </div>

        <p class="team-employees-list__header">{{ __('team.sep.foreign') }}</p>
        <div class="team-employees-list__foreign">
            @include('employees.teams._employeeList', ['employeesIds' => $employeesIds, 'employees' => $employees->getForeign()])
        </div>
    </div>

    @if ($errors->has('employees'))
        <span class="invalid-feedback d-block">
            <strong>{{ $errors->first('employees') }}</strong>
        </span>
    @elseif ($errors->has('employees.*'))
        <span class="invalid-feedback d-block">
            <strong>{{ $errors->first('employees.*') }}</strong>
        </span>
    @endif
</div>