@extends('employees.layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf
                @include('employees.teams._form', ['team' => $team, 'employees' => $employees, 'buttonTitle' => __('team.action.create')])
            </form>
        </div>
    </div>
@endsection