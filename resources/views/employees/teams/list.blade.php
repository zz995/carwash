@extends('employees.layouts.admin')

@section('content')
    @include('employees.teams._nav')

    <p>
        <a href="{{ route('employees.teams.create') }}" class="btn btn-success">
            {{ __('team.action.create') }}
        </a>
    </p>

    <div class="card">
        <div class="list">
            @forelse($teams as $team)
                <div class="list__item">
                    <p class="list__header">
                        <a href="{{ route('employees.teams.show', $team) }}">{{ $team->name }}</a>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('team.field.employees') }}:
                        </span>
                        <span class="attribute__value">
                            {{ listEmployees($team->employees) }}
                        </span>
                    </p>
                </div>
            @empty
                <div class="p-3">
                    {{ __('team.notExist') }}
                </div>
            @endforelse
        </div>
    </div>
@endsection
