@extends('employees.layouts.admin')


@section('content')

    <div class="d-flex flex-row mb-3">
        @if (!$team->isTemporary())
            <a href="{{ route('employees.teams.edit', ['team' => $team]) }}" class="btn btn-success mr-1">
                {{ __('team.action.edit') }}
            </a>
        @endif

        <button class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete">
            {{ __('team.action.delete') }}
        </button>
        @include(
            'partials.modal.confirm',
            [
                'target' => "confirmDelete",
                'body' => __('team.confirm.delete', ['num' => $team->id]),
                'action' => route('employees.teams.delete', ['team' => $team]),
                'method' => 'DELETE'
            ]
        )
    </div>

    <div class="card">
        <div class="card-body">
            <div>
                <p><strong>{{ __('team.field.name') }}: </strong>{{ $team->name }}</p>
                <p><strong>{{ __('team.field.employees') }}:<br></strong>{{ listEmployees($team->employees) }}</p>
            </div>
        </div>
    </div>
@endsection