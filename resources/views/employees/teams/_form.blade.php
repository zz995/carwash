<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">
        {{ __('team.field.name') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $team->name) }}" required>

        @if ($errors->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="employees" class="col-md-4 col-form-label text-md-right">
        {{ __('team.field.availableEmployees') }}<sup class="text-danger">*</sup>
    </label>

    @include('employees.teams._employees', ['employees' => $employees, 'employeesIds' => array_pluck($team->employees, 'id')])
</div>

<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        <button type="submit" class="btn btn-primary">
            {{ $buttonTitle }}
        </button>
    </div>
</div>
