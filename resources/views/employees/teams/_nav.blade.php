<ul class="nav nav-tabs mb-3">
    @can ('manage-teams')
        <li class="nav-item">
            <a class="nav-link {{ Route::currentRouteNamed('employees.teams.list') ? 'active' : '' }}" href="{{ route('employees.teams.list') }}">
                {{ __('navbar.employees.teams') }}
            </a>
        </li>
    @endcan
    @can ('manage-washers')
        <li class="nav-item">
            <a class="nav-link {{ Route::currentRouteNamed('employees.washer.list') ? 'active' : '' }}" href="{{ route('employees.washer.list') }}">
                {{ __('navbar.employees.washers') }}
            </a>
        </li>
    @endcan
</ul>