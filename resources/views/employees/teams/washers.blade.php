@extends('employees.layouts.admin')

@section('content')
    @include('employees.teams._nav')

    <div class="card">
        <div class="list">

            @if(! $washers->isOurEmpty())
                <div class="list__row">
                    <p>{{ __('team.sep.our') }}</p>
                </div>

                @include('employees.teams._washersList', ['washers' => $washers->getOur(), 'showCurrentServiceCenter' => 0])
            @endif

            @if(! $washers->isForeignEmpty())
                <div class="list__row">
                    <p>{{ __('team.sep.foreign') }}</p>
                </div>

                @include('employees.teams._washersList', ['washers' => $washers->getForeign(), 'showCurrentServiceCenter' => 1])
            @endif

            @if ($washers->isEmpty())
                <div class="p-3">
                    {{ __('washer.notExist') }}
                </div>
            @endif
        </div>
    </div>
@endsection
