@extends('employees.layouts.cashier')

@section('content')
    <div class="filter">
        <div class="filter__header">{{ __('balance.block.group') }}</div>
        <div class="filter__body">
            <form action="?" method="GET">
                <div class="filter__fields">
                    <div class="filter__field">
                        <label for="range" class="filter__label">{{ __('balance.field.date') }}</label>
                        <input autocomplete="off" type="text" name="range" data-range="true" value="{{ dateToPickerFormat($dataRange->getFrom()) }} - {{ dateToPickerFormat($dataRange->getTo()) }}" data-multiple-dates-separator=" - " class="filter__value datepicker-here"/>
                    </div>
                    <div class="filter__field">
                        <label for="payment" class="filter__label">{{ __('balance.field.payment') }}</label>
                        <select id="payment" class="filter__value" name="payment">
                            <option value=""></option>
                            <option value="1"{{ '1' === request('payment') ? ' selected' : '' }}>
                                {{ __('order.paymentType.cash') }}
                            </option>
                            <option value="2"{{ '2' === request('payment') ? ' selected' : '' }}>
                                {{ __('order.paymentType.card') }}
                            </option>
                            <option value="3"{{ '3' === request('payment') ? ' selected' : '' }}>
                                {{ __('order.paymentType.contract') }}
                            </option>
                        </select>
                    </div>
                    <div class="filter__field">
                        <label for="point" class="filter__label">{{ __('balance.field.point') }}</label>
                        <select id="point" class="filter__value" name="point">
                            <option value=""></option>
                            <option value="1"{{ '1' === request('point') ? ' selected' : '' }}>
                                {{ __('balance.point.carWash') }}
                            </option>
                            <option value="2"{{ '2' === request('point') ? ' selected' : '' }}>
                                {{ __('balance.point.cafe') }}
                            </option>
                        </select>
                    </div>
                    <div class="filter__field">
                        <label class="filter__label filter__label_empty">&nbsp;</label>
                        <button type="submit" class="filter__button">
                            {{ __('balance.action.showStatistic') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
        <span class="card-body">
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ __('balance.message.range', ['from' => dataTimeFormat($dataRange->getFrom()), 'to' => dataTimeFormat($dataRange->getTo())]) }}
            </div>

            @if ($balance->isBase())
                @if ($balance->pointBalance->hasCash())
                    <p>
                        <strong>
                            {{ __('order.paymentType.cash') }}:
                        </strong>

                        <span class="rub">
                            {{ moneyFormat($balance->pointBalance->getCash()) }}
                        </span>
                    </p>
                @endif
                @if ($balance->pointBalance->hasCard())
                    <p>
                        <strong>
                            {{ __('order.paymentType.card') }}:
                        </strong>

                        <span class="rub">
                            {{ moneyFormat($balance->pointBalance->getCard()) }}
                        </span>
                    </p>
                @endif
                @if ($balance->pointBalance->hasContract())
                    <p>
                        <strong>
                            {{ __('order.paymentType.contract') }}:
                        </strong>

                        <span class="rub">
                            {{ moneyFormat($balance->pointBalance->getContract()) }}
                        </span>
                    </p>
                @endif
            @elseif ($balance->isContract())
                @forelse($balance->contracts as $contract)
                    <p>
                        <strong>
                            {{ $contract->number }} {{ $contract->contractor }}:
                        </strong>
                        <span class="rub">
                            {{  moneyFormat($contract->sum) }}
                        </span>
                    </p>
                @empty
                    <span>
                        {{ __('contracts.notExist') }}
                    </span>
                @endforelse

            @elseif ($balance->isCafeSaleType())
                @foreach($balance->cafeSaleTypes as $cafeSaleType)
                    <p>
                        <strong>
                            {{ $cafeSaleType->name }}:
                        </strong>
                        <span class="rub">
                            {{ moneyFormat($cafeSaleType->sum) }}
                        </span>
                    </p>
                @endforeach
            @elseif ($balance->isReport())
                <p>
                    <strong>
                        {{ __('balance.report.carwash') }}
                        {{ __('balance.report.cash') }}:
                    </strong>
                    <span class="rub">
                        {{ moneyFormat($balance->report->carwash->getCash()) }}
                    </span>,
                    {{ numberFormat($balance->countReport->carwash->getCash()) }}
                </p>
                <p>
                    <strong>
                        {{ __('balance.report.carwash') }}
                        {{ __('balance.report.card') }}:
                    </strong>

                    <span class="rub">
                        {{ moneyFormat($balance->report->carwash->getCard()) }}
                    </span>,
                    {{ numberFormat($balance->countReport->carwash->getCard()) }}
                </p>
                @foreach($balance->report->contracts as $index => $contract)
                    <p>
                        <strong>
                            {{ $contract->contractor }}:
                        </strong>
                        <span class="rub">
                            {{ $contract->sum }}
                        </span>,
                        {{ numberFormat($balance->countReport->contracts[$index]->sum) }}
                    </p>
                @endforeach
                @foreach($balance->report->cafe as $index => $cafe)
                    <p>
                        <strong>
                            {{ $cafe->name }}
                            {{ __('balance.report.cash') }}:
                        </strong>

                        <span class="rub">
                            {{ moneyFormat($cafe->balance->getCash()) }}
                        </span>,
                        {{ numberFormat($balance->countReport->cafe[$index]->balance->getCash()) }}
                    </p>
                    <p>
                        <strong>
                            {{ $cafe->name }}
                            {{ __('balance.report.card') }}:
                        </strong>

                        <span class="rub">
                            {{ moneyFormat($cafe->balance->getCard()) }}
                        </span>,
                        {{ numberFormat($balance->countReport->cafe[$index]->balance->getCard()) }}
                    </p>
                @endforeach
                <p>
                    <strong>
                        {{ __('balance.report.common') }}:
                    </strong>

                    <span class="rub">
                        {{ moneyFormat($balance->report->common) }}
                    </span>
                </p>
                <p>
                    <strong>
                        {{ __('balance.report.sum') }}:
                    </strong>

                    <span class="rub">
                        {{ moneyFormat($balance->report->sum) }}
                    </span>
                </p>
            @endif
        </div>
    </div>
@endsection