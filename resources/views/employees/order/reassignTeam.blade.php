@extends('employees.layouts.admin')

@section('content')
    <form method="POST" class="order-form" action="">
        @csrf
        @method("PATCH")

        <div class="card">
            <div class="card-header">
                {{ $pageTitle }}
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-6 offset-lg-4">
                        @include('partials.form.markerRequireFields')
                    </div>
                </div>

                @include('employees.order.partials._team', ['employees' => $employees, 'teams' => $teams, 'viewOrder' => $viewOrder, 'create' => 0])

                <div class="submit form-group row mb-0">
                    <div class="col-lg-6 offset-lg-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('order.phrase.submit') }}
                        </button>
                    </div>
                </div>

            </div>
        </div>

    </form>
@endsection