@extends('employees.layouts.admin')

@section('scripts')
    <script src="{{ mix('js/order-ordering.js', 'build') }}" defer></script>
@endsection

@section('content')
    <form method="POST" class="order-form" action="">
        @csrf
        <input type="hidden" name="car_id" value="{{ $car->id }}">
        <input type="hidden" name="client_id" value="{{ $client->id }}">

        <div class="card mb-3 discount-card">
            <div class="card-header">
                <span class="discount-card__title">
                    {{ !isset($discountCard) ? __('discountCard.title.base') : __('discountCard.title.defined', ['disc' => $discountCard->getDisc()]) }}
                    <i class="discount-card__angle fa fa-angle-down"></i>
                    <i class="discount-card__angle fa fa-angle-up hidden"></i>
                </span>
            </div>
            <div class="card-body discount-card__body hidden">
                @if (isset($discountCard))
                    <p><strong>{{ __('discountCard.field.number') }}: </strong>{{ $discountCard->number }}</p>
                    <p><strong>{{ __('discountCard.field.disc') }}: </strong>{{ $discountCard->getDisc() }}%</p>
                    @if (!empty($discountCard->cars))
                        <p>
                            <strong>{{ __('discountCard.field.car_numbers') }}: </strong>
                            @foreach($discountCard->cars as $car)
                                {{ $car->number }}{{ $loop->last ? '' : ', ' }}
                            @endforeach
                        </p>
                    @endif
                    <p><strong>{{ __('discountCard.field.name') }}: </strong>{{ $discountCard->getClientName() }}</p>

                    <a href="{{ route('employees.discountCard.show', [$discountCard]) }}">{{ __('discountCard.action.info') }}</a><br>
                    <a href="{{ route('employees.discountCard.edit', [$discountCard]) }}">{{ __('discountCard.action.edit') }}</a>
                @else
                    @if ($clientsWithDiscount->count())
                        <div>
                            <span>{{ __('discountCard.action.bindClientDisc') }}:</span>
                            <ul>
                                @foreach($clientsWithDiscount as $client)
                                    <li><a href="{{ route('employees.discountCard.edit', [$client->discountCard]) }}?car_id={{ $car->id }}">{{ $client->name }}, {{$client->discountCard->number}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <a href="{{ route('employees.discountCard.create') }}">{{ __('discountCard.action.create') }}</a>
                @endif
            </div>

            <input id="currentDiscountSize" type="hidden" value="{{ isset($discountCard) ? $discountCard->getDisc() : 0 }}">
        </div>

        <div class="card mb-3 client-information">
            <div class="card-header">
                <span class="client-information__title">
                    {{ __('order.phrase.clientInformation') }}
                    <i class="client-information__angle fa fa-angle-down"></i>
                    <i class="client-information__angle fa fa-angle-up hidden"></i>
                </span>
            </div>
            <div class="card-body client-information__body hidden">
                <p class="mb-0">{{ __('order.phrase.mainInformation') }}</p><hr class="mt-0">
                <p class="mb-0">
                    <strong>{{ __('order.field.clientName') }}:</strong>
                    {{ $client->name }}
                </p>
                <p class="mb-0">
                    <strong>{{ __('order.field.phone') }}:</strong>
                    {{ phoneFormat($client->phone) }}
                </p>
                @if ($client->hasDiscount())
                    <p class="mb-0">
                        <strong>
                            {{ __('discountCard.title.base') }}:
                        </strong>
                        @if ($client->discountCard->hasNumber())
                            {{ $client->discountCard->number }},
                        @endif
                        {{ $client->discountCard->disc }}%
                    </p>
                @endif

                <p class="mb-0 mt-3">{{ __('order.phrase.cars') }}</p><hr class="mt-0">
                @foreach($clientCars as $car)
                    <p class="mb-0 {{ $loop->first ? '' : 'mt-3' }}">
                        <strong>
                            {{ __('order.field.number') }}:
                        </strong>
                        <span>
                            {{ $car->number }} ({{ $car->carMark ? $car->carMark->name : '' }} {{ $car->carModel ? $car->carModel->name : '' }})
                        </span>
                    </p>
                    <p class="mb-0">
                        <strong>
                            {{ __('order.phrase.drivers') }}:
                        </strong>
                        <span>
                            @foreach($car->clients as $carClient)
                                {{ $carClient->name }}{{ $loop->last ? '' : ',' }}
                            @endforeach
                        </span>
                    </p>
                @endforeach

                <p class="mb-0 mt-3">{{ __('order.phrase.serviceHistory') }}</p><hr class="mt-0">
                @forelse($lastOrders as $lastOrder)
                    <p class="mb-0 {{ $loop->first ? '' : 'mt-3' }}">
                        <strong>
                            {{ __('order.field.date') }}:
                        </strong>
                        <span>
                            {{ dataTimeFormat($lastOrder->created_at) }}
                        </span>
                    </p>
                    <p class="mb-0">
                        <strong>
                            {{ __('order.field.number') }}:
                        </strong>
                        <span>
                            {{ $lastOrder->car->number }} ({{ $lastOrder->car->carMark ? $lastOrder->car->carMark->name : '' }} {{ $lastOrder->car->carModel ? $lastOrder->car->carModel->name : '' }})
                        </span>
                    </p>
                    <p class="mb-0">
                        <strong>
                            {{ __('order.field.works') }}:
                        </strong>
                        <span>
                            @foreach($lastOrder->basketItems as $item)
                                {{ $item->work->number }}{{ $loop->last ? '' : ',' }}
                            @endforeach
                        </span>
                    </p>
                    <p class="mb-0">
                        <strong>
                            {{ __('order.phrase.sum') }}:
                        </strong>
                        <span class="rub">
                            {{ moneyFormat($lastOrder->price()) }}
                        </span>
                    </p>
                    <p class="mb-0">
                        <strong>
                            {{ __('team.block') }}:
                        </strong>
                        <span>
                            {!! listEmployees($lastOrder->employees, 'employees.washer.show') !!}
                        </span>
                    </p>
                    @if (isset($lastOrder->admin_comment))
                        <p class="mb-0">
                            <strong>{{ __('order.field.adminComment') }}:</strong><br>
                            {{ $lastOrder->admin_comment }}
                        </p>
                    @endif
                    @if (isset($order->comment))
                        <p class="mb-0">
                            <strong>{{ __('order.field.cashierComment') }}:</strong><br>
                            {{ $lastOrder->comment }}
                        </p>
                    @endif
                    <a href="{{ route('employees.order.show', $lastOrder) }}">
                        {{ __('order.phrase.openOrderCard') }}
                    </a>
                @empty
                    {{ __('order.phrase.firstTime') }}
                @endforelse

            </div>
        </div>

        <div class="card">
            <div class="card-header">
                {{ $pageTitle }}
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6 offset-md-2">
                        @include('partials.form.markerRequireFields')
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6 offset-md-2">
                        <a href="#team" class="btn btn-light">{{ __('order.goto.team') }}</a>
                    </div>
                </div>
                @include('employees.order.partials._works', ['carWashWorks' => $carWashWorks, 'viewOrder' => $memorizedOrderData])
                @include('employees.order.partials._team', ['employees' => $employees, 'teams' => $teams, 'viewOrder' => $memorizedOrderData, 'create' => 1])

                <div class="form-group row">
                    <label for="key" class="col-md-4 col-form-label text-md-right">
                        {{ __('order.field.key') }}<sup class="text-danger">*</sup>
                    </label>

                    <div class="col-md-6">
                        <input autocomplete="off" id="key" type="number" class="form-control{{ $errors->has('key') ? ' is-invalid' : '' }}" name="key" value="{{ old('key', $memorizedOrderData->getKey()) }}" required>

                        @if ($errors->has('key'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('key') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="comment" class="col-md-4 col-form-label text-md-right">
                        {{ __('order.field.comment') }}
                    </label>

                    <div class="col-md-6">
                        <textarea autocomplete="off" id="comment" class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}" name="comment">{{ old('comment', $memorizedOrderData->getComment()) }}</textarea>

                        @if ($errors->has('comment'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('comment') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="submit form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('order.phrase.submit') }}
                        </button>
                    </div>
                </div>

            </div>
        </div>

    </form>
@endsection