<div class="form-group row payment-types">
    <label class="col-md-4 col-form-label text-md-right">
        {{ __('order.selectTypePayment') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <div class="payment-types__select">
            <div class="d-inline-block">
                <input id="cashType" type="radio" name="type" value="1" {{ old('type', '1') == 1 ? 'checked' : '' }}>
                <label for="cashType">{{ __('order.paymentType.cash') }}</label>
            </div>
            <div class="d-inline-block">
                <input id="cardType" type="radio" name="type" value="2" {{ old('type', '1') == 2 ? 'checked' : '' }}>
                <label for="cardType">{{ __('order.paymentType.card') }}</label>
            </div>
            <div class="d-inline-block">
                <input id="contractType" type="radio" name="type" value="3" {{ old('type', '1') == 3 ? 'checked' : '' }}>
                <label for="contractType">{{ __('order.paymentType.contract') }}</label>
            </div>
        </div>

        @if ($errors->has('type'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('type') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row contract-number {{ old('type', '') == 3 ? '' : 'hidden' }}" data-location="{{ route('employees.contract.autocomplete') }}">
    <label for="number" class="col-md-4 col-form-label text-md-right">
        {{ __('order.contractTypePayment') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <select
                id="number"
                class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}"
                name="number"
                data-map_disc="{{ json_encode($mapDiscForContract) }}"
        >
            <option value=""></option>
            @foreach ($contracts as $contract)
                <option value="{{ $contract->number }}" {{ $contract->number == old('number', '') ? ' selected' : '' }}>
                    {{ $contract->number }}, {{ $contract->contractor }}
                </option>
            @endforeach;
        </select>

        @if ($errors->has('number'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('number') }}</strong>
            </span>
        @endif
    </div>
</div>