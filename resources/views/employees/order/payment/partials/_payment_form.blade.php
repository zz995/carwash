<div class="card payment_form {{ $isHidden ? 'hidden' : '' }}">
    <div class="card-body">
        <form method="POST" action="">
            @csrf

            <input type="hidden" name="order_closing_type" value="payment_form">

            @if ($order->hasDiscountCard() && !$order->discountCard->hasNumber())
                <div class="form-group row">
                    <label for="discount_card_number" class="col-md-4 col-form-label text-md-right">
                        @if ($order->discountCard->isCumulative())
                            {{ __('order.field.discountCardCumulative', ['disc' => $order->discountCard->getDisc()]) }}<sup class="text-danger">*</sup>
                        @else
                            {{ __('order.field.discountCardFixed', ['disc' => $order->discountCard->getDisc()]) }}<sup class="text-danger">*</sup>
                        @endif
                    </label>

                    <div class="col-md-6">
                        <input class="form-control{{ $errors->has('discount_card_number') ? ' is-invalid' : '' }}" autocomplete="off" id="discount_card_number" name="discount_card_number" value="{{ old('discount_card_number', '') }}">

                        @if ($errors->has('discount_card_number'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('discount_card_number') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            @endif

            @include('employees.order.payment.partials._payment_type', ['contracts' => $contracts, 'mapDiscForContract' => $mapDiscForContract])

            <div
                    class="form-group row contract-number-applying-discount {{ old('type', '') != 3 ? '' : 'hidden' }}"
                    data-location="{{ route('employees.contract.autocomplete') }}"
            >
                <label for="contract_number_applying_discount" class="col-md-4 col-form-label text-md-right">
                    {{ __('order.applyingDiscountFromContract') }}
                </label>

                <div class="col-md-6">
                    <select
                            id="contract_number_applying_discount"
                            class="form-control{{ $errors->has('contract_number_applying_discount') ? ' is-invalid' : '' }}"
                            name="contract_number_applying_discount"
                            data-map_disc="{{ json_encode($mapDiscForContract) }}"
                    >
                        <option value=""></option>
                        @foreach ($contracts as $contract)
                            <option value="{{ $contract->number }}" {{ $contract->number == old('contract_number_applying_discount', '') ? ' selected' : '' }}>
                                {{ $contract->number }}, {{ $contract->contractor }} ({{ $contract->disc }}%)
                            </option>
                        @endforeach
                    </select>

                    @if ($errors->has('contract_number_applying_discount'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('contract_number_applying_discount') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="employees" class="col-md-4 col-form-label text-md-right">
                    {{ __('order.field.works') }}
                </label>

                <div class="col-md-6 mb-2">
                    <button type="button" class="btn btn-light" data-toggle="collapse" data-target=".payment-list-works">
                        {{ __('order.phrase.showListWorks') }}
                    </button>
                </div>

                <div class="payment-list-works collapse col-12">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-6">
                            @foreach($order->basketItems as $item)
                                <p class="payment-list-works__work">
                                    {{ $item->work->name }},
                                    <span class="rub">
                                        {{ moneyFormat($item->price->percent(100 - $order->getDisc())) }}
                                    </span>
                                    <span>x</span>
                                    <span>{{ $item->count }}</span>

                                    <input
                                            type="hidden"
                                            name="items[{{ $item->work_id }}]"
                                            class="payment-list-works__work-state"
                                            value="{{ array_key_exists($item->work_id, old('items', [])) ? old('items', [])[$item->work_id] : 'in' }}"
                                    >
                                    <button
                                            type="button"
                                            class="
                                                btn
                                                btn-sm
                                                {{ (array_key_exists($item->work_id, old('items', [])) ? old('items', [])[$item->work_id] : 'in') == 'in' ? 'btn-danger' : 'btn-success' }}
                                                payment-list-works__button-work
                                            "
                                            data-sum="{{ moneyFormat($item->price->multiply($item->count)->percent(100 - $order->getDisc())) }}"
                                            data-title="{{ json_encode(__('order.phrase.worksState'), JSON_UNESCAPED_UNICODE) }}"
                                            data-state="{{ array_key_exists($item->work_id, old('items', [])) ? old('items', [])[$item->work_id] : 'in' }}"
                                    >
                                        @if((array_key_exists($item->work_id, old('items', [])) ? old('items', [])[$item->work_id] : 'in') == 'in')
                                            {{ __('order.phrase.worksState.out') }}
                                        @else
                                            {{ __('order.phrase.worksState.in') }}
                                        @endif
                                    </button>
                                </p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group row payment-order-sum">
                <label for="sum" class="col-md-4 col-form-label text-md-right">
                    {{ __('order.phrase.sum') }}<sup class="text-danger">*</sup>
                </label>

                <div class="col-md-6 mb-1">
                    @foreach([0, 5, 10, 15, 20] as $percent)
                        <button
                                data-percent="{{ $percent }}"
                                type="button"
                                class="payment-order-sum__reduction-percentage btn btn-sm {{ $percent == 0 ? 'btn-primary' : 'btn-danger' }}"
                        >
                            -{{ $percent }}%
                        </button>
                    @endforeach
                </div>

                <div class="col-md-6 offset-md-4">
                    <input
                            type="hidden"
                            id="price"
                            name="price"
                            data-base_price="{{ moneyInputFormat($order->basePrice()) }}"
                            data-price="{{ moneyInputFormat($order->price()) }}"
                            value="{{ old('price', moneyInputFormat($order->price())) }}"
                    >
                    <input required autocomplete="off" id="sum" type="number" name="sum" value="{{ old('sum', moneyInputFormat($order->price())) }}" class="payment-order-sum__sum form-control{{ $errors->has('sum') ? ' is-invalid' : '' }}">

                    @if ($errors->has('sum'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('sum') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <label for="comment" class="col-md-4 col-form-label text-md-right">
                    {{ __('order.field.comment') }}
                </label>

                <div class="col-md-6">
                    <textarea autocomplete="off" id="comment" class="comment-input form-control{{ ($errors->has('comment') && old('order_closing_type', null) == 'payment_form') ? ' is-invalid' : '' }}" name="comment">{{ old('comment', $order->comment) }}</textarea>

                    @if ($errors->has('comment') && old('order_closing_type', null) == 'payment_form')
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('comment') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#confirmPayment">
                        {{ __('order.action.payment') }}
                    </button>
                </div>
            </div>

            <div class="modal fade" id="confirmPayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{{ __('modalWindow.header') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{ __('order.confirm.payment') }}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('modalWindow.no') }}</button>
                            <button type="submit" class="btn btn-primary">{{ __('modalWindow.yes') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>