<div class="card refusal_form {{ $isHidden ? 'hidden' : '' }}">
    <div class="card-body">
        <form method="POST" action="{{ route('employees.order.paymentRefusal', $order) }}">
            @csrf

            <input type="hidden" name="order_closing_type" value="refusal_form">

            <div class="form-group row">
                <label for="comment" class="col-md-4 col-form-label text-md-right">
                    {{ __('order.field.comment') }}<sup class="text-danger">*</sup>
                </label>

                <div class="col-md-6">
                    <textarea autocomplete="off" required id="comment" class="comment-input form-control{{ ($errors->has('comment') && old('order_closing_type', null) == 'refusal_form') ? ' is-invalid' : '' }}" name="comment">{{ old('comment', $order->comment) }}</textarea>

                    @if ($errors->has('comment') && old('order_closing_type', null) == 'refusal_form')
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('comment') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmRefusal">
                        {{ __('order.action.refusal') }}
                    </button>
                </div>
            </div>

            <div class="modal fade" id="confirmRefusal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{{ __('modalWindow.header') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{ __('order.confirm.refusal') }}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('modalWindow.no') }}</button>
                            <button type="submit" class="btn btn-primary">{{ __('modalWindow.yes') }}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>