@extends('employees.layouts.cashier')

@section('scripts')
    <script src="{{ mix('js/order-payment.js', 'build') }}" defer></script>
@endsection

@section('content')
    @if($order->hasDiscountCard() && !$order->discountCard->hasNumber())
        @include('partials.flash._template', ['type' => 'danger', 'message' => __('discountCard.message.notExistNumber') ])
    @endif

    <div class="card mb-3">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <p><strong>{{ __('order.field.id') }}: </strong>{{ $order->id }}</p>
            <p>
                <strong>
                    {{ __('order.field.status') }}:
                </strong>
                @include('employees.order.partials._status', ['order' => $order])
            </p>
            <p><strong>{{ __('order.field.teamName') }}: </strong>{{ $order->team_name }}</p>
            <p><strong>{{ __('order.field.key') }}: </strong>{{ $order->number_key }}</p>
            <p>
                <strong>
                    {{ __('order.field.number') }}:
                </strong>
                {{ $order->car->number }}
                {{ $order->car->carMark->name }}
                {{ $order->car->carModel->name }}
            </p>
            <p><strong>{{ __('order.field.phone') }}: </strong>{{ phoneFormat($order->client->phone) }}</p>
            <p><strong>{{ __('order.field.date') }}: </strong>{{ dataTimeFormat($order->created_at) }}</p>
            <p>
                <strong>{{ __('order.field.works') }}: </strong>
                @foreach($order->basketItems as $item)
                    {{ $item->work->name }}{{ $loop->last ? '' : ',' }}
                @endforeach
            </p>

            @if ($order->isPaid())
                <p><strong>{{ __('order.field.paymentType') }}: </strong>{{ orderPaymentType($order->payment_type) }}</p>
            @endif

            @include('employees.order.partials._orderPrice', ['order' => $order])

            @if (isset($order->admin_comment))
                <p>
                    <strong>{{ __('order.field.adminComment') }}:</strong><br>
                    {!! nl2br(e($order->admin_comment)) !!}
                </p>
            @endif
            @if (isset($order->comment))
                <p>
                    <strong>{{ __('order.field.cashierComment') }}:</strong><br>
                    {!! nl2br(e($order->comment)) !!}
                </p>
            @endif
        </div>
    </div>

    @if ($order->isExecute() || $order->isExecuted())

        <ul class="nav nav-tabs mb-0 order-closing-form-toggle">
            <li class="nav-item order-closing-form-toggle__item" data-class="payment_form">
                <span class="nav-link {{ old('order_closing_type', 'payment_form') == 'payment_form' ? 'active' : '' }}">
                    {{ __('order.paymentForm') }}
                </span>
            </li>
            <li class="nav-item order-closing-form-toggle__item" data-class="refusal_form">
                <span class="nav-link {{ old('order_closing_type', null) == 'refusal_form' ? 'active' : '' }}">
                    {{ __('order.refusalForm') }}
                </span>
            </li>
        </ul>

        @include('employees.order.payment.partials._payment_form', [
            'order' => $order,
            'errors' => $errors,
            'contracts' => $contracts,
            'mapDiscForContract' => $mapDiscForContract,
            'isHidden' => old('order_closing_type', 'payment_form') != 'payment_form'
        ])
        @include('employees.order.payment.partials._refusal_form', [
            'order' => $order,
            'errors' => $errors,
            'isHidden' => old('order_closing_type', null) != 'refusal_form'
        ])
    @endif

    @if ($order->isPaid())
        <div class="card mb-3">
            <div class="card-header">
                {{ __('order.repaymentForm') }}
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('employees.order.repayment', [$order]) }}">
                    @csrf
                    @method("PUT")

                    @include('employees.order.payment.partials._payment_type', ['contracts' => $contracts, 'mapDiscForContract' => $mapDiscForContract])

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#confirmRepayment">
                                {{ __('order.action.repayment') }}
                            </button>
                        </div>
                    </div>

                    <div class="modal fade" id="confirmRepayment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('modalWindow.header') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {{ __('order.confirm.repayment') }}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('modalWindow.no') }}</button>
                                    <button type="submit" class="btn btn-primary">{{ __('modalWindow.yes') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif

    @if ($order->isCompleted())
        <div class="card">
            <div class="card-header">
                {{ __('order.field.comment') }}
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('employees.order.comment', [$order]) }}">
                    @csrf
                    @method("PATCH")

                    <div class="form-group row">
                        <label for="comment" class="col-md-4 col-form-label text-md-right">
                            {{ __('order.field.comment') }}
                        </label>

                        <div class="col-md-6">
                            <textarea required autocomplete="off" id="comment" class="comment-input form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}" name="comment">{{ old('comment', $order->comment) }}</textarea>

                            @if ($errors->has('comment'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('comment') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button class="btn btn-success">
                                {{ __('order.action.comment') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif
@endsection
