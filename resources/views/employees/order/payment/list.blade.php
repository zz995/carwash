@extends('employees.layouts.cashier')

@section('scripts')
    <script src="{{ mix('js/order-list-payment.js', 'build') }}" defer></script>
@endsection

@section('content')
    <form action="?" method="GET">
        <div class="filter">
            <div class="filter__header">{{ __('filtrationBlock.header') }}</div>
            <div class="filter__body">
                <div class="filter__fields">
                    <div class="filter__field">
                        <label for="key" class="filter__label">{{ __('order.field.numberKey') }}</label>
                        <input id="key" type="number" class="filter__value" name="key" value="{{ request('key') }}">
                    </div>
                    <div class="filter__field">
                        <label class="filter__label filter__label_empty">&nbsp;</label>
                        <button type="submit" class="filter__button">
                            {{ __('filtrationBlock.search') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>

        @if (! empty($statuses))
            <input type="hidden" name="status" value="{{ request('status') }}">
            <ul class="nav nav-tabs mb-0">
                @foreach($statuses as $status)
                    <li class="nav-item order-filter-status" data-value="{{ $status['value'] }}">
                        <a class="nav-link {{ request('status') == $status['value'] ? 'active' : '' }}" href="#">
                            {{ $status['label'] }}
                        </a>
                    </li>
                @endforeach
            </ul>
        @endif
    </form>

    <div class="card orders-list" data-timestamp="{{ Carbon\Carbon::now()->timestamp }}" data-location="{{ route('employees.order.checkNewOrder') }}">
        <div class="list">
            @forelse($orders as $order)
                <div class="list__item">
                    <p class="list__header">
                        <a href="{{ route('employees.order.payment', $order) }}">
                            {{ __('order.orderTitle', ['num' => $order->id]) }}
                        </a>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.status') }}:
                        </span>
                        <span class="attribute__value">
                             @include('employees.order.partials._status', ['order' => $order])
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.key') }}:
                        </span>
                        <span class="attribute__value">
                            {{ $order->number_key }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.number') }}:
                        </span>
                        <span class="attribute__value">
                            {{ $order->car->number }}
                            {{ $order->car->carMark->name }}
                            {{ $order->car->carModel->name }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.clientName') }}:
                        </span>
                        <span class="attribute__value">
                            {{ $order->client->name }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.phone') }}:
                        </span>
                        <span class="attribute__value">
                            {{ phoneFormat($order->client->phone) }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.ordered') }}:
                        </span>
                        <span class="attribute__value">
                            {{ dataUpdate($order->created_at) }}
                            {{ __('time.back') }}
                        </span>
                    </p>
                    @if (isset($order->admin_comment))
                        <p class="list__attribute attribute">
                            <span class="attribute__name">
                                {{ __('order.field.adminComment') }}:
                            </span><br>
                            <span class="attribute__value">
                                {!! nl2br(e($order->admin_comment)) !!}
                            </span>
                        </p>
                    @endif
                </div>
            @empty
                <div class="p-3">
                    {{ __('order.notExist') }}
                </div>
            @endforelse
        </div>
    </div>

    @include('employees.partials._message')
@endsection


