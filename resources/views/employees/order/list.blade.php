@extends('employees.layouts.admin')

@section('content')

    <form method="GET" action="{{ route('employees.order.list') }}">
        <input type="hidden" name="status" value="{{ request('status') }}">
        @include('employees.order.partials._nav', ['statuses' => $statuses, 'currentStatus' => $status])
    </form>

    @if(!$teamExist)
        @include('partials.flash._template', ['type' => 'warning', 'message' => __('order.notFoundTeam') ])
    @endif

    <p>
        <a href="{{ route('employees.order.clientRegister') }}" class="btn btn-success">
            {{ __('order.action.create') }}
        </a>
    </p>

    <div class="card orders-list">
        <div class="list">
            @forelse($orders as $order)
                <div class="list__item">
                    <p class="list__header">
                        <a href="{{ route('employees.order.show', $order) }}">
                            {{ __('order.orderTitle', ['num' => $order->id]) }}
                        </a>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.status') }}:
                        </span>
                        <span class="attribute__value">
                             @include('employees.order.partials._status', ['order' => $order])
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.teamName') }}:
                        </span>
                        <span class="attribute__value">
                            {{ $order->team_name }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.key') }}:
                        </span>
                        <span class="attribute__value">
                            {{ $order->number_key }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.number') }}:
                        </span>
                        <span class="attribute__value">
                            {{ $order->car->number }}
                            {{ $order->car->carMark->name }}
                            {{ $order->car->carModel->name }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.phone') }}:
                        </span>
                        <span class="attribute__value">
                            {{ phoneFormat($order->client->phone) }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.date') }}:
                        </span>
                        <span class="attribute__value">
                            {{ dataTimeFormat($order->created_at) }}
                        </span>
                    </p>
                    @if (isset($order->admin_comment))
                        <p class="list__attribute attribute">
                            <span class="attribute__name">
                                {{ __('order.field.adminComment') }}
                            </span>
                            <span class="attribute__value">
                                {{ $order->admin_comment }}
                            </span>
                        </p>
                    @endif
                </div>
            @empty
                <div class="p-3">
                    {{ __('order.notExist') }}
                </div>
            @endforelse
        </div>
    </div>
@endsection
