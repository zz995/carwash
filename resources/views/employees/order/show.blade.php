@extends('employees.layouts.admin')


@section('content')

    <div class="d-flex flex-row mb-3">
        @if ($order->isExecute() || $order->isQueue())
            <a href="{{ route('employees.order.editWorks', ['order' => $order]) }}" class="btn btn-success mr-1">
                {{ __('order.action.editWorks') }}
            </a>
        @endif

        @if ($order->isExecute() && $order->hasTemporaryTeam())
            <button class="btn btn-success mr-1" data-toggle="modal" data-target="#confirmExecute">
                {{ __('order.action.executed') }}
            </button>
            @include(
                'partials.modal.confirm',
                [
                    'target' => "confirmExecute",
                    'body' => __('order.confirm.execute'),
                    'action' => route('employees.order.executed', $order),
                    'method' => 'PATCH'
                ]
            )
        @endif

        @if ($order->isQueue())
            <a href="{{ route('employees.order.appointTeam', ['order' => $order]) }}" class="btn btn-success mr-1">
                {{ __('order.action.appointTeam') }}
            </a>
        @endif

        @if ($order->isExecute())
            <a href="{{ route('employees.order.reassignTeam', ['order' => $order]) }}" class="btn btn-success mr-1">
                {{ __('order.action.reassignTeam') }}
            </a>
        @endif

        @can('manage-team-fine', $order)
            <a href="{{ route('employees.order.createFine', ['order' => $order]) }}" class="btn btn-danger">
                {{ __('teamFine.action.create') }}
            </a>
        @endcan

        @if($order->hasTeamFine() && $order->getBaseTeamFine()->isActive())
            <a class="btn btn-danger mr-1" href="{{ route('employees.order.disableFine', $order->getBaseTeamFine()) }}">
                {{ __('teamFine.action.disable') }}
            </a>
        @endif

        @can('manage-cross-order', $order)
            <a class="btn btn-danger mr-1" href="{{ route('employees.order.cross', $order) }}">
                {{ __('order.action.crossItOut') }}
            </a>
        @endcan
    </div>

    @if($order->hasDiscountCard() && !$order->discountCard->hasNumber())
        @include('partials.flash._template', ['type' => 'warning', 'message' => __('discountCard.message.notExistNumber') ])
    @endif

    <div class="card">
        <div class="card-body">
            <div>
                <p><strong>{{ __('order.field.id') }}: </strong>{{ $order->id }}</p>
                <p>
                    <strong>
                        {{ __('order.field.status') }}:
                    </strong>
                    @include('employees.order.partials._status', ['order' => $order])
                </p>
                <p><strong>{{ __('order.field.teamName') }}: </strong>{{ $order->team_name }}</p>
                <p><strong>{{ __('order.field.key') }}: </strong>{{ $order->number_key }}</p>
                <p>
                    <strong>
                        {{ __('order.field.number') }}:
                    </strong>
                    {{ $order->car->number }}
                    {{ $order->car->carMark->name }}
                    {{ $order->car->carModel->name }}
                </p>
                <p><strong>{{ __('order.field.phone') }}: </strong>{{ phoneFormat($order->client->phone) }}</p>
                <p><strong>{{ __('order.field.date') }}: </strong>{{ dataTimeFormat($order->created_at) }}</p>
                <p>
                    <strong>{{ __('order.field.works') }}: </strong>
                    @foreach($order->basketItems as $item)
                        {{ $item->work->number }}{{ $loop->last ? '' : ',' }}
                    @endforeach
                </p>
                @include('employees.order.partials._orderPrice', ['order' => $order])
                @if (isset($order->admin_comment))
                    <p>
                        <strong>{{ __('order.field.adminComment') }}:</strong><br>
                        {!! nl2br(e($order->admin_comment)) !!}
                    </p>
                @endif
                @if (isset($order->comment))
                    <p>
                        <strong>{{ __('order.field.cashierComment') }}:</strong><br>
                        {!! nl2br(e($order->comment)) !!}
                    </p>
                @endif
            </div>
        </div>
    </div>

    @if (! $order->isCompleted())
        <div class="card mt-3">
            <div class="card-header">
                {{ __('order.field.comment') }}
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('employees.order.adminComment', [$order]) }}">
                    @csrf
                    @method("PATCH")

                    <div class="form-group row">
                        <label for="comment" class="col-md-4 col-form-label text-md-right">
                            {{ __('order.field.comment') }}
                        </label>

                        <div class="col-md-6">
                            <textarea required autocomplete="off" id="comment" class="comment-input form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}" name="comment">{{ old('comment', $order->admin_comment) }}</textarea>

                            @if ($errors->has('comment'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('comment') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                            <button class="btn btn-success">
                                {{ __('order.action.comment') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif

    <div class="card mt-3">
        <div class="card-header">
            {{ __('team.block') }}
        </div>
        <div class="card-body">
            <div>
                <p><strong>{{ __('team.field.name') }}: </strong>{{ $order->team_name }}</p>
                <p><strong>{{ __('team.field.admin') }}: </strong>{{ $order->admin->getFio()->getFullName() }}</p>
                @if ($order->employees->count())
                    <p>
                        <strong>{{ __('team.field.employees') }}: </strong>
                        {{ listEmployees($order->employees) }}
                    </p>
                @endif
            </div>
        </div>
    </div>

    @if ($order->hasTeamFine())
        <div class="card mt-3">
            <div class="card-header">
                {{ __('teamFine.block') }}
            </div>
            <div class="card-body pb-0">
                <div>
                    <p>
                        <strong>{{ __('teamFine.field.status') }}: </strong>
                        @include('employees.fine._status', ['fine' => $order->getBaseTeamFine()])
                    </p>
                    <p>
                        <strong>{{ __('teamFine.field.value') }}: </strong>
                        <span class="rub">
                            {{ moneyFormat($order->getBaseTeamFine()->value) }}
                        </span>
                    </p>
                </div>
            </div>

            <div class="list">
                @forelse($order->getBaseTeamFine()->histories as $history)
                    <div class="list__item">
                        <p class="list__attribute attribute">
                            <span class="attribute__name">{{ __('personalFine.field.sum') }}:</span>
                            <span class="attribute__value rub">
                                {{ moneyFormat($history->value) }}
                            </span>
                        </p>
                        <p class="list__attribute attribute">
                            <span class="attribute__name">{{ __('personalFine.field.date') }}:</span>
                            <span class="attribute__value">
                                {{ dataTimeFormat($history->timestamp) }}
                            </span>
                        </p>
                        <p class="list__attribute attribute">
                            <span class="attribute__name">{{ __('personalFine.field.comment') }}:</span>
                            <span class="attribute__value">
                                {!! nl2br(e($history->comment)) !!}
                            </span>
                        </p>
                    </div>
                @empty
                    <div class="p-3">
                        {{ __('teamFine.historyBlock.notFound') }}
                    </div>
                @endforelse
            </div>
        </div>
    @endif
@endsection