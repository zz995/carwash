@extends('employees.layouts.admin')

@section('scripts')
    <script src="{{ mix('js/order-edit-works.js', 'build') }}" defer></script>
@endsection

@section('content')
    <form method="POST" class="order-form" action="">
        @csrf
        @method("PATCH")

        <input id="currentDiscountSize" type="hidden" value="{{ $order->getDisc() }}">

        <div class="card">
            <div class="card-header">
                {{ $pageTitle }}
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-10 offset-lg-2">
                        @include('partials.form.markerRequireFields')
                    </div>
                </div>

                @include('employees.order.partials._works', ['carWashWorks' => $carWashWorks, 'viewOrder' => $viewOrder])

                <div class="submit form-group row mb-0">
                    <div class="col-lg-10 offset-lg-2">
                        <button type="submit" class="btn btn-primary">
                            {{ __('order.phrase.submit') }}
                        </button>
                    </div>
                </div>

            </div>
        </div>

    </form>
@endsection