@extends('employees.layouts.admin')

@section('content')
    <form action="?" method="GET">
        <div class="filter">
            <div class="filter__header">{{ __('filtrationBlock.header') }}</div>
            <div class="filter__body">
                <div class="filter__fields">
                    <div class="filter__field">
                        <label for="discount_card" class="filter__label">{{ __('discountCard.title.base') }}</label>
                        <input id="discount_card" class="filter__value" name="discount_card" value="{{ request('discount_card') }}">
                    </div>
                    <div class="filter__field">
                        <label for="phone" class="filter__label">{{ __('order.field.phone') }}</label>
                        <input id="phone" class="filter__value phone-input" name="phone" value="{{ request('phone') }}">
                    </div>
                    <div class="filter__field car-number">
                        <label for="car_number" class="filter__label">{{ __('order.field.number') }}</label>
                        <input id="car_number" class="filter__value car-number__input" name="car_number" value="{{ request('car_number') }}">
                        <label>
                            <input type="checkbox" autocomplete="off" name="foreign-number" value="1" {{ request('foreign-number') ? 'checked' : '' }} class="car-number__foreign skip-focus">
                            {{ __('car.field.foreignNumber') }}
                        </label>
                    </div>
                    <div class="filter__field">
                        <label class="filter__label filter__label_empty">&nbsp;</label>
                        <button type="submit" class="filter__button">
                            {{ __('filtrationBlock.search') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="card orders-list">
        <div class="list">
            @forelse($orders as $order)
                <div class="list__item">
                    <p class="list__header">
                        <a href="{{ route('employees.order.show', $order) }}">
                            {{ __('order.orderTitle', ['num' => $order->id]) }}
                        </a>
                    </p>

                    @if ($order->hasContract())
                        <p class="list__attribute attribute">
                            <span class="attribute__name">
                                {{ __('contracts.title') }}:
                            </span>
                            <span class="attribute__value">
                                 {{ $order->contract->number }}, {{ $order->contract->disc }}%
                            </span>
                        </p>
                    @elseif ($order->hasDiscountCard())
                        <p class="list__attribute attribute">
                            <span class="attribute__name">
                                {{ __('discountCard.title.base') }}:
                            </span>
                            <span class="attribute__value">
                                @if ($order->discountCard->hasNumber())
                                    {{ $order->discountCard->number }},
                                @endif
                                {{ $order->discountCard->disc }}%
                            </span>
                        </p>
                    @endif

                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('client.title') }}:
                        </span>
                        <span class="attribute__value">
                            {{ $order->client->name }}, {{ phoneFormat($order->client->phone) }}
                        </span>
                    </p>

                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.number') }}:
                        </span>
                        <span class="attribute__value">
                            {{ $order->car->number }} ({{ $order->car->carMark ? $order->car->carMark->name : '' }} {{ $order->car->carModel ? $order->car->carModel->name : '' }})
                        </span>
                    </p>

                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.works') }}:
                        </span>
                        <span class="attribute__value">
                             @foreach($order->basketItems as $item)
                                {{ $item->work->number }}{{ $loop->last ? '' : ',' }}
                             @endforeach
                        </span>
                    </p>

                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.phrase.sum') }}:
                        </span>
                        <span class="attribute__value rub">
                            @if (isset($order->price))
                                {{ moneyFormat($order->price) }}
                            @else
                                {{ moneyFormat($order->price()) }}
                            @endif
                        </span>
                    </p>

                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.date') }}:
                        </span>
                        <span class="attribute__value">
                             {{ dataTimeFormat($order->created_at) }}
                        </span>
                    </p>
                </div>
            @empty
                <div class="p-3">
                    {{ __('order.page.serviceHistory.notFound') }}
                </div>
            @endforelse
        </div>
    </div>

    <div class="container mt-3">
        <div class="row justify-content-center">
            {{ $orders->appends($_GET)->links() }}
        </div>
    </div>
@endsection
