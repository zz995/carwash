@extends('employees.layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf

                <div class="form-group row">
                    <label for="reason" class="col-md-4 col-form-label text-md-right">
                        {{ __('personalFine.field.comment') }}<sup class="text-danger">*</sup>
                    </label>

                    <div class="col-md-6">
                        <textarea required id="reason" class="form-control{{ $errors->has('reason') ? ' is-invalid' : '' }}" name="reason">{{ old('reason', '') }}</textarea>

                        @if ($errors->has('reason'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('reason') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('order.action.crossItOut') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection