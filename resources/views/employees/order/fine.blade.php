@extends('employees.layouts.admin')

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf

                <div class="form-group row">
                    <label for="value" class="col-md-4 col-form-label text-md-right">
                        {{ __('teamFine.field.value') }}<sup class="text-danger">*</sup>
                    </label>

                    <div class="col-md-6">
                        <input id="value" type="text" class="form-control{{ $errors->has('value') ? ' is-invalid' : '' }}" name="value" value="{{ old('value', $teamFine->value) }}" required>

                        @if ($errors->has('value'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('value') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="reason" class="col-md-4 col-form-label text-md-right">
                        {{ __('teamFine.field.create_reason') }}<sup class="text-danger">*</sup>
                    </label>

                    <div class="col-md-6">
                        <textarea required id="reason" class="form-control{{ $errors->has('reason') ? ' is-invalid' : '' }}" name="reason">{{ old('reason', $teamFine->create_reason) }}</textarea>

                        @if ($errors->has('reason'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('reason') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('teamFine.action.create') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection