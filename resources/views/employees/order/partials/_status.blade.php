@if ($order->isExecute())
    <span class="badge badge-danger">{{ __('order.status.execute') }}</span>
@elseif ($order->isExecuted())
    <span class="badge badge-info">{{ __('order.status.executed') }}</span>
@elseif ($order->isPaid())
    <span class="badge badge-success">{{ __('order.status.paid') }}</span>
@elseif ($order->isQueue())
    <span class="badge badge-warning">{{ __('order.status.queue') }}</span>
@elseif ($order->isPaymentRefusal())
    <span class="badge badge-dark">{{ __('order.status.paymentRefusal') }}</span>
@elseif ($order->isCanceled())
    <span class="badge badge-secondary">{{ __('order.status.canceled') }}</span>
@endif

@if (!(isset($notShowCross) && $notShowCross))
    @if ($order->hasCrossTeamFine())
        <span class="badge badge-danger">{{ __('order.status.cross') }}</span>
    @endif
@endif