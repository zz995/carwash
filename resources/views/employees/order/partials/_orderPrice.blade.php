@if ($order->hasContract())
    <div class="cost mb-2">
        <div class="cost__label">
            <strong>
                {{ __('order.phrase.sumByContract') }}
            </strong>
            ({{ __('order.phrase.discount', ['disc' => $order->getDisc()]) }})<strong>:</strong>
        </div>
        <div class="cost__value">{{ moneyFormat($order->price()) }}</div>
    </div>
@elseif ($order->isApplyingDiscountFromContract())
    <div class="cost mb-2">
        <div class="cost__label">
            <strong>
                {{ __('order.phrase.sumWithDisc') }}
            </strong>
            ({{ __('order.phrase.discountFromContract', ['disc' => $order->getDisc()]) }})<strong>:</strong>
        </div>
        <div class="cost__value">{{ moneyFormat($order->price()) }}</div>
    </div>
@elseif ($order->hasDiscountCard())
    <div class="cost mb-2">
        <div class="cost__label">
            <strong>
                {{ __('order.phrase.sumWithDisc') }}
            </strong>
            ({{ __('discountCard.title.defined', ['disc' => $order->getDisc()]) }})<strong>:</strong>
        </div>
        <div class="cost__value">{{ moneyFormat($order->price()) }}</div>
    </div>
@else
    <div class="cost mb-2">
        <div class="cost__label">{{ __('order.phrase.sum') }}:</div>
        <div class="cost__value">{{ moneyFormat($order->price()) }}</div>
    </div>
@endif

@if ($order->isCompleted() && $order->isOtherPaymentPrice())
    <div class="cost mb-2">
        <div class="cost__label">{{ __('order.phrase.paymentSum') }}:</div>
        <div class="cost__value">{{ moneyFormat($order->paymentPrice()) }}</div>
    </div>
@endif