<div class="mark-and-model">
    <div class="mark-and-model__mark car-mark form-group row {{ (old('mark', '') != '' || isset($car) || isset($showModel)) ? '' : 'hidden' }}">
        <label for="mark" class="col-md-4 col-form-label text-md-right">
            {{ __('order.field.mark') }}@if (!isset($notRequired) || !$notRequired)<sup class="text-danger">*</sup>@endif
        </label>

        <div class="car-mark__select col-md-6 {{ old('new-mark', null) === null ? '' : 'hidden' }}">
            <select
                    autocomplete="off"
                    id="mark"
                    data-location_car_model="{{ route('employees.carModel.byMark') }}"
                    @if (!isset($notRequired) || !$notRequired)
                        required
                    @endif
                    class="form-control{{ $errors->has('mark') ? ' is-invalid' : '' }}"
                    name="mark"
            >
                <option></option>
                @foreach ($carMarks as $carMark)
                    <option value="{{ $carMark->id }}" {{ $carMark->id == old('mark', isset($car) ? $car->carMark->id : '') ? ' selected' : '' }}>{{ $carMark->name }}</option>
                @endforeach
                <option value="0" {{ '0' === old('mark', isset($car) ? $car->carMark->id : '') ? ' selected' : '' }}>{{ __('order.anotherMark') }}</option>
                <option value="-1" {{ '-1' === old('mark', isset($car) ? $car->carMark->id : '') ? ' selected' : '' }}>{{ __('order.createMark') }}</option>
            </select>

            @if ($errors->has('mark'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('mark') }}</strong>
                </span>
            @endif
        </div>

        <div class="car-mark__input col-md-6 {{ old('new-mark', null) === null ? 'hidden' : '' }}">
            <input id="new-mark" type="text" class="form-control{{ $errors->has('new-mark') ? ' is-invalid' : '' }}" name="new-mark" value="{{ old('new-mark', '') }}">

            @if ($errors->has('new-mark'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('new-mark') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="car-model form-group row {{ (old('mark', '') != '' || isset($car) || isset($showModel)) ? '' : 'hidden' }}">
        <label for="model" class="col-md-4 col-form-label text-md-right">
            {{ __('order.field.model') }}@if (!isset($notRequired) || !$notRequired)<sup class="text-danger">*</sup>@endif
        </label>

        <div class="col-md-6 car-model__input {{ old('new-mark', null) === null && old('new-model', null) === null ? 'hidden' : '' }}">
            <input id="new-model" type="text" class="form-control{{ $errors->has('new-model') ? ' is-invalid' : '' }}" name="new-model" value="{{ old('new-model', '') }}">
        </div>

        <div class="col-md-6 car-model__select">
            <select
                    autocomplete="off"
                    id="model"
                    @if (!isset($notRequired) || !$notRequired)
                        required
                    @endif
                    class="form-control{{ $errors->has('model') ? ' is-invalid' : '' }}"
                    name="model"
            >
                @foreach ($carModels as $carModel)
                    <option value="{{ $carModel->id }}" {{ $carModel->id == old('model', isset($car) ? $car->carModel->id : '') ? ' selected' : '' }}>
                        {{ $carModel->name }}
                    </option>
                @endforeach
            </select>

            <script type="text/template" class="model__items-template">
                <% items.forEach(function(item) { %>
                    <option value="<%-item.id%>" <%= modelId === item.id ? 'selected' : '' %>>
                        <%-item.name%>
                        <% if(item.category !== undefined) {%>
                            - <%-item.category.name%>
                       <% } %>
                    </option>
                <% }); %>
            </script>

            @if ($errors->has('model'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('model') }}</strong>
                </span>
            @endif
        </div>
    </div>
</div>