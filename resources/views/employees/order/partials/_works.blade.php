<div class="form-group row">
    <label for="works" class="col-lg-2 col-form-label text-lg-right">
        {{ __('order.field.works') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-lg-10">

        <div class="works-fast-access">
            <span class="works-fast-access__number badge badge-light" data-number="3">3</span>
            <span class="works-fast-access__number badge badge-light" data-number="4">4</span>
            <span class="works-fast-access__number badge badge-light" data-number="11">11</span>
            <span class="works-fast-access__number badge badge-light" data-number="15">15</span>
            <span class="works-fast-access__number badge badge-light" data-number="17">17</span>
            <span class="works-fast-access__number badge badge-light" data-number="39">39</span>
            <span class="works-fast-access__empty badge badge-light" data-number="">&nbsp;</span>
            <span class="works-fast-access__empty badge badge-light" data-number="">&nbsp;</span>
        </div>

        <div id="works" class="works-list" >
            @foreach ($carWashWorks as $type)
                <div class="works-list__type">
                    {{ $type->name }}
                </div>
                @foreach ($type->works as $work)
                    <div class="works-list__item row mx-0">
                        <div class="col-12 col-md-6">
                            <input
                                    autocomplete="off"
                                    type="checkbox"
                                    name="works[]"
                                    class="works-list__work"
                                    data-number="{{ $work->number }}"
                                    value="{{ $work->id }}"
                                    {{ in_array($work->id, old('works', $viewOrder->getWorks())) ? ' checked' : '' }}
                            >
                            <label>
                                {{ $work->number }}.
                                {{ $work->name }}
                            </label>
                        </div>
                        <div class="col-6 col-md-3">
                            <input
                                    autocomplete="off"
                                    class="works-list__price form-control col-12"
                                    data-work_id="{{ $work->id }}"
                                    name="prices[{{ $work->id }}]"
                                    type="number"
                                    value="{{ old('prices.' . $work->id, $viewOrder->getPrice($work->id) !== null ? moneyInputFormat($viewOrder->getPrice($work->id)) : moneyInputFormat($work->getDefaultPrice())) }}"
                            >
                        </div>
                        @if ($work->resolutionSeveral())
                            <div class="col-6 col-md-3">
                                <div class="input-count">
                                    <i class="fa fa-minus input-count__dec"></i>
                                    <input
                                            type="hidden"
                                            autocomplete="off"
                                            class="input-count__active-input"
                                            name="counts[{{ $work->id }}]"
                                            value="{{ old('counts.' . $work->id, $viewOrder->getCount($work->id) !== null ? $viewOrder->getCount($work->id) : ($work->resolutionSeveral() ? 1 : '')) }}"
                                    >
                                    <input
                                            autocomplete="off"
                                            disabled
                                            type="text"
                                            class="works-list__count input-count__input form-control col-12"
                                            data-work_id="{{ $work->id }}"
                                            value="{{ old('counts.' . $work->id, $viewOrder->getCount($work->id) !== null ? $viewOrder->getCount($work->id) : ($work->resolutionSeveral() ? 1 : '')) }}"
                                    >
                                    <i class="fa fa-plus input-count__inc"></i>
                                </div>
                            </div>
                        @endif
                    </div>
                @endforeach
            @endforeach
        </div>

        <div>
            @if ($errors->has('works'))
                <span class="invalid-feedback d-block">
                    <strong>{{ $errors->first('works') }}</strong>
                </span>
            @endif

            @if ($errors->has('works.*'))
                <span class="invalid-feedback d-block">
                    <strong>{{ $errors->first('works.*') }}</strong>
                </span>
            @endif
        </div>

        <div class="order-works-numbers">
            {{ __('order.selectNumberWorks') }}:
            <span class="works-numbers"></span>
        </div>

        <div class="order-sum">
            <input autocomplete="off" type="hidden" name="price">
            {{ __('order.phrase.sum') }}: <span class="ordering-sum">0</span>&nbsp;&#8381;
            <div class="hidden ordering-sum-with-disc">{{ __('order.phrase.sumWithDisc') }}: <span class="ordering-sum-with-disc__value">0</span>&nbsp;&#8381;</div>
        </div>
    </div>
</div>