<div class="form-group row">
    <label for="team" class="col-md-4 col-form-label text-md-right">
        {{ __('order.selectTeam') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">

        <select autocomplete="off" id="team" class="form-control{{ $errors->has('team') ? ' is-invalid' : '' }}" name="team">
            @foreach ($teams as $team)
                <option
                        value="{{ $team->id }}"
                        {{ $team->id == old('team', $viewOrder->getTeam()) ? ' selected' : '' }}
                        class="option-space"
                >
                    {{ $team->name }}
                </option>
            @endforeach

            @if($create)
                <option
                        value="0"
                        {{ 0 == old('team', $viewOrder->getTeam()) ? ' selected' : '' }}
                        class="option-space"
                >
                    {{ __('team.withoutTeam') }}
                </option>
            @endif
        </select>

        @if ($errors->has('team'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('team') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="employees" class="col-md-4 col-form-label text-md-right">
        {{ __('order.createTeam') }}
    </label>

    <div class="col-md-6 mb-2">
        <button type="button" class="btn btn-light" data-toggle="collapse" data-target="#employees">{{ __('team.showEmployees') }}</button>
    </div>

    <div id="employees" class="collapse col-12">
        <div class="row">
            <div class="col-md-4"></div>
            @include('employees.teams._employees', ['employees' => $employees, 'employeesIds' => $viewOrder->getEmployees()])
        </div>
    </div>

</div>