<ul class="nav nav-tabs mb-3">
    @can ('manage-order')
        @foreach($statuses as $status)
            <li class="nav-item order-filter-status" data-value="{{ isset($status['value']) ? $status['value'] : -1 }}">
                <a
                        class="
                            nav-link
                            {{
                                (
                                    Route::currentRouteNamed('employees.order.list')
                                    && (
                                        ($currentStatus == -1 && !isset($status['value']))
                                        || ($currentStatus == $status['value'] && isset($status['value']) && $currentStatus !== null)
                                    )
                                ) ? 'active' : ''
                            }}"
                        href="#"
                >
                    {{ $status['label'] }}
                </a>
            </li>
        @endforeach
    @endcan
</ul>