@extends('employees.layouts.admin')

@section('content')
    <div class="card orders-list">
        <div class="list">
            @forelse($orders as $order)
                <div class="list__item">
                    <p class="list__header">
                        <a href="{{ route('employees.order.appointTeam', $order) }}">
                            {{ __('order.orderTitle', ['num' => $order->id]) }}
                        </a>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.teamName') }}:
                        </span>
                        <span class="attribute__value">
                            {{ $order->team_name }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.number') }}:
                        </span>
                        <span class="attribute__value">
                            {{ $order->car->number }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.phone') }}:
                        </span>
                        <span class="attribute__value">
                            {{ phoneFormat($order->client->phone) }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.date') }}:
                        </span>
                        <span class="attribute__value">
                            {{ dataTimeFormat($order->created_at) }}
                        </span>
                    </p>
                    <p class="list__attribute attribute">
                        <span class="attribute__name">
                            {{ __('order.field.status') }}:
                        </span>
                        <span class="attribute__value">
                             @include('employees.order.partials._status', ['order' => $order])
                        </span>
                    </p>

                </div>
            @empty
                <div class="p-3">
                    {{ __('order.notExist') }}
                </div>
            @endforelse
        </div>

    </div>
@endsection
