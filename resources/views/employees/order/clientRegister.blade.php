@extends('employees.layouts.admin')

@section('scripts')
    <script src="{{ mix('js/order-client-register.js', 'build') }}" defer></script>
@endsection

@section('content')
    <div class="card client-register">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf

                <input type="hidden" name="car_id" value="{{ old('car_id', isset($car) ? $car->id : '') }}">
                <input type="hidden" name="client_id" value="{{ old('client_id', isset($client) ? $client->id : '') }}">

                <div class="car-number-search car-number form-group row" data-location="{{ route('employees.car.autocomplete') }}">
                    <label for="number" class="col-md-4 col-form-label text-md-right">
                        {{ __('order.field.number') }}<sup class="text-danger">*</sup>
                    </label>

                    <div class="col-md-6">
                        <div class="autocomplete">
                            <input
                                    id="number"
                                    type="text"
                                    class="car-number__input autocomplete__input form-control{{ $errors->has('number') ? ' is-invalid' : '' }}"
                                    name="number"
                                    value="{{ old('number', isset($car) ? $car->number : '') }}"
                                    autocomplete="off"
                                    required
                            >

                            <script type="text/template" class="autocomplete__items-template">
                                <div class="autocomplete__items">
                                    <% items.forEach(function(car) { %>
                                        <div
                                                class="autocomplete__item"
                                                data-id="<%-car.id%>"
                                                data-number="<%-car.number%>"
                                                data-car_model_id="<%-car.carModel.id%>"
                                                data-car_mark_id="<%-car.carMark.id%>"
                                        >
                                            <%-car.number%>
                                            <% if (car.discountCard !== null) { %>
                                                (<%-car.discountCard.number%>, <%-car.discountCard.disc%>%)
                                            <% } %>
                                        </div>

                                        <% car.clients.forEach(function(client) { %>
                                            <div
                                                    class="pl-3 autocomplete__item"
                                                    data-id="<%-car.id%>"
                                                    data-number="<%-car.number%>"
                                                    data-client_id="<%-client.id%>"
                                                    data-name="<%-client.name%>"
                                                    data-phone="<%-client.phone%>"
                                                    data-car_model_id="<%-car.carModel.id%>"
                                                    data-car_mark_id="<%-car.carMark.id%>"
                                            >
                                                <%-client.name%>
                                                <%-client.phone%>
                                            </div>
                                        <% }); %>

                                        <div
                                                class="pl-3 autocomplete__item"
                                                data-id="<%-car.id%>"
                                                data-number="<%-car.number%>"
                                                data-car_model_id="<%-car.carModel.id%>"
                                                data-car_mark_id="<%-car.carMark.id%>"
                                        >
                                            {{ __('order.phrase.newDriver') }}
                                        </div>
                                    <% }); %>
                                    <div
                                            class="autocomplete__item"
                                            data-id=""
                                            data-number=""
                                            data-client_id=""
                                            data-name=""
                                            data-phone=""
                                    >
                                        {{ __('order.phrase.newCar') }}
                                    </div>
                                </div>
                            </script>
                        </div>

                        @if ($errors->has('number'))
                            <span class="invalid-feedback d-block">
                                <strong>{{ $errors->first('number') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="offset-md-4 col-md-6">
                        <label>
                            <input type="checkbox" autocomplete="off" name="foreign-number" value="1" {{ old('foreign-number', isset($car) ? $car->isForeignNumber() : '') ? 'checked' : '' }} class="car-number__foreign skip-focus">
                            {{ __('car.field.foreignNumber') }}
                        </label>
                    </div>
                </div>

                <div class="client-phone-search {{ (old('phone', '') != '' || isset($client)) ? '' : 'hidden' }} form-group row" data-location="{{ route('employees.client.autocomplete') }}">
                    <label for="phone" class="col-md-4 col-form-label text-md-right">
                        {{ __('order.field.phone') }}<sup class="text-danger">*</sup>
                    </label>

                    <div class="col-md-6">
                        <div class="autocomplete">
                            <input
                                    id="phone"
                                    type="tel"
                                    class="phone-input autocomplete__input form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                    name="phone"
                                    value="{{ old('phone', isset($client) ? $client->phone : '') }}"
                                    autocomplete="off"
                                    required
                            >
                            <script type="text/template" class="autocomplete__items-template">
                                <div class="autocomplete__items">
                                    <% items.items.forEach(function(item) { %>
                                        <div
                                                class="autocomplete__item"
                                                data-id="<%-item.id%>"
                                                data-name="<%-item.name%>"
                                                data-phone="<%-item.phone%>"
                                        >
                                            <%-item.name%>
                                            <%-item.phone%>
                                        </div>
                                    <% }); %>
                                    <% if(items.showNewClient) { %>
                                        <div
                                                class="autocomplete__item"
                                                data-id=""
                                                data-name=""
                                                data-phone=""
                                        >
                                            {{ __('order.phrase.newClient') }}
                                        </div>
                                    <% } %>
                                </div>
                            </script>
                        </div>

                        @if ($errors->has('phone'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="client-name form-group row {{ (old('name', '') != '' || isset($client)) ? '' : 'hidden' }}">
                    <label for="name" class="col-md-4 col-form-label text-md-right">
                        {{ __('order.field.client.name') }}<sup class="text-danger">*</sup>
                    </label>

                    <div class="col-md-6">
                        <input autocomplete="off" id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', isset($client) ? $client->name : '') }}" required>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                @include('employees.order.partials._markAndModel', ['car' => $car, 'carMarks' => $carMarks, 'carModels' => $carModels])

                <div class="submit form-group {{ (old('phone', '') != '' || isset($client)) ? '' : 'hidden' }} row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('order.phrase.submit') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection