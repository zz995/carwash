<div class="nav-scroller bg-white box-shadow">
    <div class="container">
        <nav class="nav nav-underline">
            @can('manage-employe-view')
                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.employees.list') ? 'active' : '' }}" href="{{ route('superAdmin.employees.list') }}">
                    {{ __('navbar.superAdmin.employees') }}
                </a>
            @endcan
            @can('manage-role-view')
                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.employees.roles.list') ? 'active' : '' }}" href="{{ route('superAdmin.employees.roles.list') }}">
                    {{ __('navbar.superAdmin.roles') }}
                </a>
            @endcan
        </nav>
    </div>
</div>