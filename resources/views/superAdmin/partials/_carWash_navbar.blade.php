<div class="nav-scroller bg-white box-shadow">
    <div class="container">
        <nav class="nav nav-underline">
            @can('manage-car-wash-works')
                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.carWash.works.list') ? 'active' : '' }}" href="{{ route('superAdmin.carWash.works.list') }}">
                    {{ __('navbar.superAdmin.carWashWorks') }}
                </a>
            @endcan
            @can('administration-order')
                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.carWash.order.list') ? 'active' : '' }}" href="{{ route('superAdmin.carWash.order.list') }}">
                    {{ __('navbar.superAdmin.carWashOrder') }}
                </a>
            @endcan
            @can('administration-order')
                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.carWash.order.serviceHistory') ? 'active' : '' }}" href="{{ route('superAdmin.carWash.order.serviceHistory') }}">
                    {{ __('navbar.superAdmin.carWashServiceHistory') }}
                </a>
            @endcan
        </nav>
    </div>
</div>