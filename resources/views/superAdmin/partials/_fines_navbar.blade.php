<div class="nav-scroller bg-white box-shadow">
    <div class="container">
        <nav class="nav nav-underline">
            @can('administration-fine')
                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.fine.paymentRefusalFineSize') ? 'active' : '' }}" href="{{ route('superAdmin.fine.paymentRefusalFineSize') }}">
                    {{ __('navbar.superAdmin.fines.paymentRefusalFineSize') }}
                </a>
            @endcan
        </nav>
    </div>
</div>