<div class="nav-scroller bg-white box-shadow">
    <div class="container">
        <nav class="nav nav-underline">
            @can('manage-employe-view')
                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.discountCard.list') ? 'active' : '' }}" href="{{ route('superAdmin.discountCard.list') }}">
                    {{ __('navbar.superAdmin.discountCards') }}
                </a>
            @endcan
            @can('manage-role-view')
                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.discountCard.steps.list') ? 'active' : '' }}" href="{{ route('superAdmin.discountCard.steps.list') }}">
                    {{ __('navbar.superAdmin.discountCardSteps') }}
                </a>
            @endcan
        </nav>
    </div>
</div>