<div class="sort-field">

    <label>
        <input
                type="radio"
                class="hidden"
                name="sort"
                value="{{ $currentSort->getNextValue($field) }}"
                {{ $currentSort->equal($field) ? 'checked' : '' }}
        >
        <span class="sort-field__title">
            {{ $title }}&nbsp;@if ($currentSort->equal($field, 'asc'))<span class="fa fa-sort-down d-inline"></span>@elseif ($currentSort->equal($field, 'desc'))<span class="fa fa-sort-up d-inline"></span>@else<span class="fa fa-sort d-inline"></span>@endif
        </span>
    </label>

</div>