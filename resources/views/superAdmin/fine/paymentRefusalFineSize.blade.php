@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._fines_navbar')
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf
                @method('PUT')

                <div class="form-group row">
                    <label for="admin_fine" class="col-md-4 col-form-label text-md-right">
                        {{ __('teamFine.paymentRefusalFineSize.admin_fine') }}<sup class="text-danger">*</sup>
                    </label>

                    <div class="col-md-6">
                        <input id="admin_fine" type="number" class="form-control{{ $errors->has('admin_fine') ? ' is-invalid' : '' }}" name="admin_fine" value="{{ old('admin_fine', moneyInputFormat($fineSize->admin_fine)) }}" required>

                        @if ($errors->has('admin_fine'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('admin_fine') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="washer_fine" class="col-md-4 col-form-label text-md-right">
                        {{ __('teamFine.paymentRefusalFineSize.washer_fine') }}<sup class="text-danger">*</sup>
                    </label>

                    <div class="col-md-6">
                        <input id="washer_fine" type="number" class="form-control{{ $errors->has('washer_fine') ? ' is-invalid' : '' }}" name="washer_fine" value="{{ old('washer_fine', moneyInputFormat($fineSize->washer_fine)) }}" required>

                        @if ($errors->has('washer_fine'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('washer_fine') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('teamFine.page.paymentRefusalFineSize.submit') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection