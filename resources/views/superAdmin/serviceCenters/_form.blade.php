<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">
        {{ __('serviceCenter.field.name') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $serviceCenter->name) }}" required>

        @if ($errors->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="background_color" class="col-md-4 col-form-label text-md-right">
        {{ __('serviceCenter.field.background_color') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="background_color" type="color" class="form-control{{ $errors->has('background_color') ? ' is-invalid' : '' }}" name="background_color" value="{{ old('background_color', (isset($serviceCenter->id) ? $serviceCenter->background_color : '#343a40')) }}" required>

        @if ($errors->has('color'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('background_color') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="font_color" class="col-md-4 col-form-label text-md-right">
        {{ __('serviceCenter.field.font_color') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="font_color" type="color" class="form-control{{ $errors->has('font_color') ? ' is-invalid' : '' }}" name="font_color" value="{{ old('font_color', (isset($serviceCenter->id) ? $serviceCenter->font_color : '#ffffff')) }}" required>

        @if ($errors->has('color'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('font_color') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="disabled" class="col-md-4 col-form-label text-md-right">
        {{ __('serviceCenter.field.disabled') }}
    </label>

    <div class="col-md-6">
        <input id="disabled" type="checkbox" class="{{ $errors->has('disabled') ? ' is-invalid' : '' }}" name="disabled" value="1" {{ old('disabled', $serviceCenter->disabled) == 1 ? 'checked' : '' }}>

        @if ($errors->has('disabled'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('disabled') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="desc" class="col-md-4 col-form-label text-md-right">
        {{ __('serviceCenter.field.desc') }}
    </label>

    <div class="col-md-6">
        <textarea id="desc" class="form-control{{ $errors->has('desc') ? ' is-invalid' : '' }}" name="desc">{{ old('desc', $serviceCenter->desc) }}</textarea>

        @if ($errors->has('desc'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('desc') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        <button type="submit" class="btn btn-primary">
            {{ $buttonTitle }}
        </button>
    </div>
</div>
