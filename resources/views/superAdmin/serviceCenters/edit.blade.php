@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._employees_navbar')
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf
                @method('PUT')
                @include('superAdmin.serviceCenters._form', ['serviceCenter' => $serviceCenter, 'buttonTitle' => __('serviceCenter.action.edit')])
            </form>
        </div>
    </div>
@endsection