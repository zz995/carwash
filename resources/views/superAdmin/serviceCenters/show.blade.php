@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._employees_navbar')
@endsection

@section('content')

    <div class="d-flex flex-row mb-3">
        <a href="{{ route('superAdmin.serviceCenter.edit', ['serviceCenter' => $serviceCenter]) }}" class="btn btn-success mr-1">
            {{ __('serviceCenter.action.edit') }}
        </a>
    </div>

    <div class="card">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>{{ __('serviceCenter.field.id') }}</th>
                <th>{{ __('serviceCenter.field.name') }}</th>
                <th>{{ __('serviceCenter.field.desc') }}</th>
                <th>{{ __('serviceCenter.field.status') }}</th>
            </tr>
            </thead>
            <tbody>
                <tr class="service-centers__service-center" data-location="{{ route('superAdmin.serviceCenter.show', [$serviceCenter]) }}" data-toggle="tooltip" data-placement="bottom" title="{{ __('serviceCenter.tooltip.show') }}">
                    <td>{{ $serviceCenter->id }}</td>
                    <td>{{ $serviceCenter->name }}</td>
                    <td>{{ $serviceCenter->desc }}</td>
                    <td>
                        @if ($serviceCenter->disabled == 1)
                            <span class="badge badge-danger">{{ __('serviceCenter.field.disabled') }}</span>
                        @else
                            <span class="badge badge-light">{{ __('serviceCenter.field.visible') }}</span>
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endsection