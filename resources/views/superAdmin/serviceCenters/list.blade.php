@extends('superAdmin.layouts.app')

@section('content')

    <p>
        <a href="{{ route('superAdmin.serviceCenter.create') }}" class="btn btn-success">
            {{ __('serviceCenter.action.create') }}
        </a>
    </p>

    <div class="card">
        <table class="service-centers table table-bordered table-striped">
            <thead>
                <tr>
                    <th>{{ __('serviceCenter.field.id') }}</th>
                    <th>{{ __('serviceCenter.field.name') }}</th>
                    <th>{{ __('serviceCenter.field.desc') }}</th>
                    <th>{{ __('serviceCenter.field.status') }}</th>
                    <th>{{ __('serviceCenter.field.background_color') }}</th>
                    <th>{{ __('serviceCenter.field.font_color') }}</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($serviceCenters as $serviceCenter)
                    <tr class="service-centers__service-center" data-location="{{ route('superAdmin.serviceCenter.show', [$serviceCenter]) }}" data-toggle="tooltip" data-placement="bottom" title="{{ __('serviceCenter.tooltip.show') }}">
                        <td>{{ $serviceCenter->id }}</td>
                        <td>{{ $serviceCenter->name }}</td>
                        <td>{{ $serviceCenter->desc }}</td>
                        <td>
                            @if ($serviceCenter->disabled == 1)
                                <span class="badge badge-danger">{{ __('serviceCenter.field.disabled') }}</span>
                            @else
                                <span class="badge badge-light">{{ __('serviceCenter.field.visible') }}</span>
                            @endif
                        </td>
                        <td style="background: {{ $serviceCenter->background_color }}"></td>
                        <td style="background: {{ $serviceCenter->font_color }}"></td>
                    </tr>
                @endforeach

            </tbody>
        </table>

        <div class="container">
            <div class="row justify-content-center">
                {{ $serviceCenters->appends($_GET)->links() }}
            </div>
        </div>

    </div>
@endsection