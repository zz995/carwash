@extends('superAdmin.layouts.app')

@section('content')

    <div class="card mb-3">
        <div class="card-header">{{ __('filtrationBlock.header') }}</div>
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="range" class="col-form-label">{{ __('balance.field.date') }}</label>
                            <input
                                    autocomplete="off"
                                    type="text"
                                    name="range"
                                    data-range="true"
                                    @if (isset($dataRange))
                                        value="{{ dateToPickerFormat($dataRange->getFrom()) }} - {{ dateToPickerFormat($dataRange->getTo()) }}"
                                    @endif
                                    data-multiple-dates-separator=" - "
                                    class="form-control datepicker-here"
                            />
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br />
                            <button type="submit" class="btn btn-primary">
                                {{ __('filtrationBlock.search') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">

        @if (isset($dataRange))
            <div class="card-body">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    {{ __('salaryBreakdown.message.range', ['from' => dataTimeFormat($dataRange->getFrom()), 'to' => dataTimeFormat($dataRange->getTo())]) }}
                </div>
            </div>
        @endif

        <div class="card-body">
            @include('partials.salaryBreakdown._sum', ['accrualsSum' => $accrualsSum])
        </div>

        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>{{ __('salaryBreakdown.field.type') }}</th>
                    <th>{{ __('salaryBreakdown.field.value') }}</th>
                    <th>{{ __('salaryBreakdown.field.date') }}</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($accruals as $accrual)
                    <tr class="">
                        <td>
                            @if ($accrual->isAccrualByOrder())
                                <a href="{{ route('superAdmin.carWash.order.show', $accrual->initiator_id) }}">
                                    {{ __('salaryBreakdown.type.by_order') }}
                                </a>
                            @elseif ($accrual->isAccrualByPersonalFine())
                                {{ __('salaryBreakdown.type.by_personal_fine') }}
                            @elseif ($accrual->isAccrualByTeamFine())
                                {{ __('salaryBreakdown.type.by_team_fine') }}
                            @elseif ($accrual->isAccrualByDaily())
                                {{ __('salaryBreakdown.type.by_daily') }}
                            @elseif ($accrual->isAccrualBySalary())
                                {{ __('salaryBreakdown.type.by_salary') }}
                            @endif
                        </td>
                        <td>
                            <span class="rub">
                                {{ moneyFormat($accrual->value) }}
                            </span>
                        </td>
                        <td>{{ dataTimeFormat($accrual->timestamp) }}</td>
                    </tr>
                @endforeach

            </tbody>
        </table>

        <div class="container">
            <div class="row justify-content-center">
                {{ $accruals->appends($_GET)->links() }}
            </div>
        </div>

    </div>
@endsection