@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._employees_navbar')
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf
                @include('superAdmin.employees.form._main', ['employe' => $employe])
                @include('superAdmin.employees.form._password')
                @include('superAdmin.employees.form._submit', ['title' => __('user.action.create')])
            </form>
        </div>
    </div>
@endsection