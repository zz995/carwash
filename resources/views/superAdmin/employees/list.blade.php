@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._employees_navbar')
@endsection

@section('content')

    @can('manage-employe-create')
        <p>
            <a href="{{ route('superAdmin.employees.create') }}" class="btn btn-success">
                {{ __('user.action.create') }}
            </a>
        </p>
    @endcan

    <div class="card mb-3">
        <div class="card-header">{{ __('filtrationBlock.header') }}</div>
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="id" class="col-form-label">{{ __('user.field.id') }}</label>
                            <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="surname" class="col-form-label">{{ __('user.field.surname') }}</label>
                            <input id="surname" class="form-control" name="surname" value="{{ request('surname') }}">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="phone" class="col-form-label">{{ __('user.field.phone') }}</label>
                            <input id="phone" class="form-control phone-input" name="phone" value="{{ request('phone') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="status" class="col-form-label">{{ __('user.field.status') }}</label>
                            <select id="status" class="form-control" name="status">
                                <option value=""></option>
                                @foreach ($statuses as $value => $label)
                                    <option value="{{ $value }}"{{ $value === request('status') ? ' selected' : '' }}>{{ $label }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="role" class="col-form-label">{{ __('user.field.role') }}</label>
                            <select id="role" class="form-control" name="role">
                                <option value=""></option>
                                @foreach ($roles as $value => $label)
                                    <option value="{{ $value }}"{{ $value == request('role') ? ' selected' : '' }}>{{ $label }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="service_center" class="col-form-label">{{ __('user.field.serviceCenter') }}</label>
                            <select id="service_center" class="form-control" name="service_center">
                                <option value=""></option>
                                @foreach ($serviceCenters as $serviceCenter)
                                    <option value="{{ $serviceCenter->id }}"{{ $serviceCenter->id == request('service_center') ? ' selected' : '' }}>{{ $serviceCenter->name }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br />
                            <button type="submit" class="btn btn-primary">
                                {{ __('filtrationBlock.search') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
        <table class="employees table table-bordered table-striped">
            <thead>
                <tr>
                    <th>{{ __('user.field.id') }}</th>
                    <th>{{ __('user.fio') }}</th>
                    <th>{{ __('user.field.login') }}</th>
                    <th>{{ __('user.field.email') }}</th>
                    <th>{{ __('user.field.phones') }}</th>
                    <th>{{ __('user.field.status') }}</th>
                    <th>{{ __('user.field.role') }}</th>
                    <th>{{ __('user.field.serviceCenter') }}</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($employees as $employe)
                    <tr class="employees__employe" data-location="{{ route('superAdmin.employees.show', [$employe]) }}" data-toggle="tooltip" data-placement="bottom" title="{{ __('user.tooltip.show') }}">
                        <td>{{ $employe->id }}</td>
                        <td>{{ $employe->getFio()->getFullName() }}</td>
                        <td>{{ $employe->login }}</td>
                        <td>{{ $employe->email }}</td>
                        <td>{!! phonesFormat($employe->phones, true) !!}</td>
                        <td>
                            @if ($employe->isActive())
                                <span class="badge badge-primary">{{ __('user.status.active') }}</span>
                            @endif
                            @if ($employe->isDelete())
                                <span class="badge badge-light">{{ __('user.status.delete') }}</span>
                            @endif
                        </td>
                        <td>
                            @if ($employe->isCashier())
                                <span class="badge badge-info">{{ $employe->role->name }}</span>
                            @elseif ($employe->isWasher())
                                <span class="badge badge-warning">{{ $employe->role->name }}</span>
                            @elseif ($employe->isSuperAdmin())
                                <span class="badge badge-danger">{{ $employe->role->name }}</span>
                            @elseif ($employe->isAdmin())
                                <span class="badge badge-success">{{ $employe->role->name }}</span>
                            @endif

                            @if ($employe->isOverdrive())
                                    <span class="badge badge-primary">{{ __('user.field.overdrive') }}</span>
                            @endif
                        </td>
                        <td>
                            @if (isset($employe->serviceCenter))
                                {{ $employe->serviceCenter->name }}
                            @endif
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>

        <div class="container">
            <div class="row justify-content-center">
                {{ $employees->appends($_GET)->links() }}
            </div>
        </div>

    </div>
@endsection