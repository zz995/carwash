@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._employees_navbar')
@endsection

@section('content')
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="card">
                <div class="card-header">
                    {{ $pageTitle }}
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 offset-md-4">
                            @include('partials.form.markerRequireFields')
                        </div>
                    </div>

                    <form method="POST" action="">
                        @csrf

                        <div class="form-group row">
                            <label for="sum" class="col-md-4 col-form-label text-md-right">
                                {{ __('user.page.payout.field.sum') }}<sup class="text-danger">*</sup>
                            </label>

                            <div class="col-md-6">
                                <input id="sum" type="text" class="form-control{{ $errors->has('sum') ? ' is-invalid' : '' }}" name="sum" value="{{ old('sum', moneyInputFormat($payoutSum)) }}" required>

                                @if ($errors->has('sum'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('sum') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('user.action.payout') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="card mb-3">
                <div class="card-header">
                    {{ __('user.page.payout.commonInfo') }}
                </div>
                <div class="card-body">
                    <div>
                        <strong>{{ __('user.fio') }}:</strong>
                        <span>{{ $employe->getFio()->getFullName() }}</span>
                    </div>
                    <div class="employee-balance">
                        <div class="employee-balance__label">{{ __('user.balance') }}:</div>
                        <div class="employee-balance__value {{ $employe->balance == '0' ? '' : 'text-success' }}">{{ moneyFormat($employe->balance) }}&nbsp;&#8381;</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection