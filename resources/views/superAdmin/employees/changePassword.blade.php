@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._employees_navbar')
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf
                @method('PATCH')

                @include('superAdmin.employees.form._password')

                @if($employe->isSuperAdmin())
                    <div class="form-group row">
                        <label for="current" class="col-md-4 col-form-label text-md-right">
                            {{ __('user.field.currentPassword') }}<sup class="text-danger">*</sup>
                        </label>

                        <div class="col-md-6">
                            <input id="current" type="password" class="form-control{{ $errors->has('current') ? ' is-invalid' : '' }}" name="current" required>

                            @if ($errors->has('current'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('current') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                @endif

                @include('superAdmin.employees.form._submit', ['title' => __('user.action.changePassword')])
            </form>
        </div>
    </div>
@endsection