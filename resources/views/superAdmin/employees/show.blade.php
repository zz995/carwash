@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._employees_navbar')
@endsection

@section('content')

    <div class="d-flex flex-row mb-3">

        @can('manage-employe-payout', [$employe])
            <a href="{{ route('superAdmin.employees.payout', $employe) }}" class="btn btn-success mr-1">
                {{ __('user.action.payout') }}
            </a>
        @endcan

        @can('manage-employe-edit', [$employe])
            <a href="{{ route('superAdmin.employees.edit', ['employe' => $employe]) }}" class="btn btn-success mr-1">
                {{ __('user.action.edit') }}
            </a>
        @endcan

        @can('manage-employe-edit', [$employe])
            @if($employe->isActive())
                <button class="btn btn-success mr-1" data-toggle="modal" data-target="#confirmDisable">
                    {{ __('user.action.disable') }}
                </button>

                @include(
                    'partials.modal.confirm',
                    [
                        'target' => "confirmDisable",
                        'body' => __('user.confirm.disable', ['employe' => $employe->getFio()->getFullName()]),
                        'action' => route('superAdmin.employees.disable', ['employe' => $employe]),
                        'method' => 'PATCH'
                    ]
                )
            @endif
        @endcan

        @can('manage-employe-delete', [$employe])
            @if(!$employe->isDelete())
                <button class="btn btn-danger mr-1" data-toggle="modal" data-target="#confirmDelete">
                    {{ __('user.action.delete') }}
                </button>
                @include(
                    'partials.modal.confirm',
                    [
                        'target' => "confirmDelete",
                        'body' => __('user.confirm.delete', ['employe' => $employe->getFio()->getFullName()]),
                        'action' => route('superAdmin.employees.delete', ['employe' => $employe]),
                        'method' => 'DELETE'
                    ]
                )
            @else
                <button class="btn btn-light mr-1" data-toggle="modal" data-target="#confirmRecovery">
                    {{ __('user.action.recovery') }}
                </button>

                @include(
                    'partials.modal.confirm',
                    [
                        'target' => "confirmRecovery",
                        'body' => __('user.confirm.recovery', ['employe' => $employe->getFio()->getFullName()]),
                        'action' => route('superAdmin.employees.recovery', ['employe' => $employe]),
                        'method' => 'PATCH'
                    ]
                )
            @endif

            @can('manage-salary-breakdown', [$employe])
                <a href="{{ route('superAdmin.employees.salaryBreakdown', ['employe' => $employe]) }}" class="btn btn-primary mr-1">
                    {{ __('user.page.salaryBreakdown.header') }}
                </a>
            @endcan
        @endcan
    </div>

    <div class="card employe-card">
        <div class="card-body">
            <div class="employee-balance">
                <div class="employee-balance__label">{{ __('user.balance') }}:</div>
                <div class="employee-balance__value {{ $employe->balance == '0' ? '' : 'text-success' }}">{{ moneyFormat($employe->balance) }}&nbsp;&#8381;</div>
            </div>
        </div>
    </div>

    <div class="card employe-card mt-4">
        <div class="card-header">
            {{ __('user.page.show.userInfo') }}
        </div>

        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>{{ __('user.field.id') }}</th>
                    <th>{{ __('user.fio') }}</th>
                    <th>{{ __('user.field.login') }}</th>
                    <th>{{ __('user.field.email') }}</th>
                    <th>{{ __('user.field.phones') }}</th>
                    <th>{{ __('user.field.status') }}</th>
                    <th>{{ __('user.field.role') }}</th>
                    <th>{{ __('user.field.serviceCenter') }}</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $employe->id }}</td>
                    <td>{{ $employe->getFio()->getFullName() }}</td>
                    <td>{{ $employe->login }}</td>
                    <td>{{ $employe->email }}</td>
                    <td>{!! phonesFormat($employe->phones, true) !!}</td>
                    <td>
                        @if ($employe->isActive())
                            <span class="badge badge-primary">{{ __('user.status.active') }}</span>
                        @endif
                        @if ($employe->isDelete())
                            <span class="badge badge-light">{{ __('user.status.delete') }}</span>
                        @endif
                    </td>
                    <td>
                        @if ($employe->isCashier())
                            <span class="badge badge-info">{{ $employe->role->name }}</span>
                        @elseif ($employe->isWasher())
                            <span class="badge badge-warning">{{ $employe->role->name }}</span>
                        @elseif ($employe->isSuperAdmin())
                            <span class="badge badge-danger">{{ $employe->role->name }}</span>
                        @elseif ($employe->isAdmin())
                            <span class="badge badge-success">{{ $employe->role->name }}</span>
                        @endif

                        @if ($employe->isOverdrive())
                            <span class="badge badge-primary">{{ __('user.field.overdrive') }}</span>
                        @endif
                    </td>
                    <td>
                        @if (isset($employe->serviceCenter))
                            {{ $employe->serviceCenter->name }}
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="card mt-4">
        <div class="card-header">
            {{ __('user.page.show.userLoginHistory') }}
        </div>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>{{ __('user.loginHistoryField.id') }}</th>
                <th>{{ __('user.loginHistoryField.created_at') }}</th>
                <th>{{ __('user.loginHistoryField.type') }}</th>
                <th>{{ __('user.field.serviceCenter') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($loginHistories as $item)
                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ dataTimeFormat($item->created_at) }}</td>
                    <td>
                        @if ($item->isLogin())
                            <span class="badge badge-success">{{ __('user.loginHistories.status.login') }}</span>
                        @endif
                        @if ($item->isLogout())
                            <span class="badge badge-danger">{{ __('user.loginHistories.status.logout') }}</span>
                        @endif
                    </td>
                    <td>
                        @if (isset($item->serviceCenter))
                            {{ $item->serviceCenter->name }}
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if ($loginHistories->isEmpty())
            <p class="text-center">{{ __('user.loginHistoriesNotFound') }}</p>
        @endif
    </div>
@endsection