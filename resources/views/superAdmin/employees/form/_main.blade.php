<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">
        {{ __('user.field.name') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $employe->name) }}" required>

        @if ($errors->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="surname" class="col-md-4 col-form-label text-md-right">
        {{ __('user.field.surname') }}
    </label>

    <div class="col-md-6">
        <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname', $employe->surname) }}">

        @if ($errors->has('surname'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('surname') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="patronymic" class="col-md-4 col-form-label text-md-right">
        {{ __('user.field.patronymic') }}
    </label>

    <div class="col-md-6">
        <input id="patronymic" type="text" class="form-control{{ $errors->has('patronymic') ? ' is-invalid' : '' }}" name="patronymic" value="{{ old('patronymic', $employe->patronymic) }}">

        @if ($errors->has('patronymic'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('patronymic') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="login" class="col-md-4 col-form-label text-md-right">
        {{ __('user.field.login') }}
    </label>

    <div class="col-md-6">
        <input id="login" type="text" class="form-control{{ $errors->has('login') ? ' is-invalid' : '' }}" name="login" value="{{ old('login', $employe->login) }}">

        @if ($errors->has('login'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('login') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="email" class="col-md-4 col-form-label text-md-right">
        {{ __('user.field.email') }}
    </label>

    <div class="col-md-6">
        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $employe->email) }}">

        @if ($errors->has('email'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="phone" class="col-md-4 col-form-label text-md-right">
        {{ __('user.field.phones') }}
    </label>

    <div class="col-md-6 employee-phones">
        <div class="d-flex">
            <input id="phone" type="text" class="employee-phones__input phone-input form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone', $employe->phone) }}">
            <button class="btn btn-primary employee-phones__button" disabled>+</button>
        </div>
        <div class="employee-phones__list">
            @if (isset($employe->phones))
                @foreach($employe->phones as $phone)
                    <span class="badge-light employee-phones__phone">
                        <input type="hidden" name="phones[]" value="{{ $phone }}">
                        <nobr>
                            {{ phoneFormat($phone) }}
                            <span class="fa fa-close employee-phones__close"></span>
                        </nobr>
                    </span>
                @endforeach
            @endif
            @foreach(old('phones', []) as $phone)
                <span class="badge-light employee-phones__phone">
                    <input type="hidden" name="phones[]" value="{{ $phone }}">
                    <nobr>
                        {{ $phone }}
                        <span class="fa fa-close employee-phones__close"></span>
                    </nobr>
                </span>
            @endforeach
            <script type="text/template" class="employee-phones__template-phone">
                <span class="badge-light employee-phones__phone">
                    <input type="hidden" name="phones[]" value="<%-phone%>">
                    <nobr>
                        <%-phone%>
                        <span class="fa fa-close employee-phones__close"></span>
                    </nobr>
                </span>
            </script>
        </div>
        @if ($errors->has('phones'))
            <span class="invalid-feedback d-block">
                <strong>{{ $errors->first('phones') }}</strong>
            </span>
        @elseif ($errors->has('phones.*'))
            <span class="invalid-feedback d-block">
                <strong>{{ $errors->first('phones.*') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="role" class="col-md-4 col-form-label text-md-right">
        {{ __('user.field.role') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <select id="role" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role">
            <option value=""></option>
            @foreach ($roles as $value => $label)
                <option value="{{ $value }}" {{ $value == old('role', isset($employe->id) ? $employe->role->id : '') ? ' selected' : '' }}>{{ $label }}</option>
            @endforeach;
        </select>

        @if ($errors->has('role'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('role') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group row">
    <label for="overdrive" class="col-md-4 col-form-label text-md-right">
        {{ __('user.field.overdrive') }}
    </label>

    <div class="col-md-6">
        <input id="overdrive" type="checkbox" class="{{ $errors->has('overdrive') ? ' is-invalid' : '' }}" name="overdrive" {{ old('overdrive', $employe->overdrive) == 1 ? 'checked' : '' }} value="1">

        @if ($errors->has('overdrive'))
            <span class="invalid-feedback d-block">
                <strong>{{ $errors->first('overdrive') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="service_center" class="col-md-4 col-form-label text-md-right">
        {{ __('user.field.serviceCenter') }}
    </label>

    <div class="col-md-6">
        <select id="service_center" class="form-control{{ $errors->has('service_center') ? ' is-invalid' : '' }}" name="service_center">
            <option value=""></option>
            @foreach ($serviceCenters as $serviceCenter)
                <option value="{{ $serviceCenter->id }}" {{ $serviceCenter->id == old('service_center', $employe->service_center_id) ? ' selected' : '' }}>{{ $serviceCenter->name }}</option>
            @endforeach
        </select>

        @if ($errors->has('service_center'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('service_center') }}</strong>
            </span>
        @endif
    </div>
</div>