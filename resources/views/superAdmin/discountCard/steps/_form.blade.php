<div class="form-group row">
    <label for="disc" class="col-md-4 col-form-label text-md-right">
        {{ __('discountCard.field.step.disc') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="disc" type="number" class="form-control{{ $errors->has('disc') ? ' is-invalid' : '' }}" name="disc" value="{{ old('disc', $step->disc) }}" required>

        @if ($errors->has('disc'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('disc') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="sum" class="col-md-4 col-form-label text-md-right">
        {{ __('discountCard.field.step.sum') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="sum" type="number" class="form-control{{ $errors->has('sum') ? ' is-invalid' : '' }}" name="sum" value="{{ old('sum', moneyInputFormat($step->sum)) }}" required>

        @if ($errors->has('sum'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('sum') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        <button type="submit" class="btn btn-primary">
            {{ $buttonTitle }}
        </button>
    </div>
</div>