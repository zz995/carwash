@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._discountCard_navbar')
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf
                @include('superAdmin.discountCard.steps._form', ['step' => $step, 'buttonTitle' => __('discountCard.action.createCalculationStep')])
            </form>
        </div>
    </div>
@endsection