@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._discountCard_navbar')
@endsection

@section('content')

    <p>
        <a href="{{ route('superAdmin.discountCard.steps.create') }}" class="btn btn-success">
            {{ __('discountCard.action.createCalculationStep') }}
        </a>
    </p>

    <div class="card">
        <table class="spreadsheet">
            <thead>
                <tr>
                    <th>{{ __('discountCard.field.step.disc') }}</th>
                    <th>{{ __('discountCard.field.step.sum') }}</th>
                    <th>{{ __('discountCard.field.step.created_at') }}</th>
                    <th>{{ __('discountCard.field.step.updated_at') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($steps as $step)
                    <tr class="spreadsheet__row" data-location="{{ route('superAdmin.discountCard.steps.edit', $step) }}" data-toggle="tooltip" data-placement="bottom" title="{{ __('discountCard.tooltip.editCalculationStep') }}">
                        <td>{{ $step->disc }}</td>
                        <td>
                            <span class="rub">
                                {{ moneyFormat($step->sum) }}
                            </span>
                        </td>
                        <td>{{ dataTimeFormat($step->created_at) }}</td>
                        <td>{{ dataTimeFormat($step->updated_at) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection