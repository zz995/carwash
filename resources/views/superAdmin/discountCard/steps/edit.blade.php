@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._discountCard_navbar')
@endsection

@section('content')

    <div class="d-flex flex-row mb-3">
        <button class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete">
            {{ __('discountCard.action.deleteCalculationStep') }}
        </button>
        @include(
            'partials.modal.confirm',
            [
                'target' => "confirmDelete",
                'body' => __('discountCard.confirm.deleteCalculationStep'),
                'action' => route('superAdmin.discountCard.steps.delete', ['step' => $step]),
                'method' => 'DELETE'
            ]
        )
    </div>

    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf
                @method('PUT')

                @include('superAdmin.discountCard.steps._form', ['step' => $step, 'buttonTitle' => __('discountCard.action.editCalculationStep')])
            </form>
        </div>
    </div>
@endsection