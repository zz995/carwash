@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._discountCard_navbar')
@endsection

@section('scripts')
    <script src="{{ mix('js/discount-card-form.js', 'build') }}" defer></script>
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf
                @method("PUT")
                @include('employees.discountCard.form._disc', ['discountCard' => $discountCard])
                @include('employees.discountCard.form._submit', ['buttonTitle' => __('discountCard.action.editDisc')])
            </form>
        </div>
    </div>
@endsection