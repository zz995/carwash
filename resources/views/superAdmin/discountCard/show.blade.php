@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._discountCard_navbar')
@endsection

@section('content')

    <div class="d-flex flex-row mb-3">
        <a href="{{ route('superAdmin.discountCard.edit', [$discountCard]) }}" class="btn btn-success mr-1">
            {{ __('discountCard.action.edit') }}
        </a>
        <a href="{{ route('superAdmin.discountCard.editDisc', [$discountCard]) }}" class="btn btn-success mr-1">
            {{ __('discountCard.action.editDisc') }}
        </a>
        @if($discountCard->canAdminChange())
            <form method="POST" action="{{ route('superAdmin.discountCard.denyEdit', [$discountCard]) }}">
                @csrf
                <button type="submit" class="btn btn-success mr-1">
                    {{ __('discountCard.action.denyEdit') }}
                </button>
            </form>
        @else
            <form method="POST" action="{{ route('superAdmin.discountCard.allowEdit', [$discountCard]) }}">
                @csrf
                <button type="submit" class="btn btn-success mr-1">
                    {{ __('discountCard.action.allowEdit') }}
                </button>
            </form>
        @endif
    </div>

    @if(!$discountCard->hasNumber())
        @include('partials.flash._template', ['type' => 'warning', 'message' => __('discountCard.message.notExistNumber') ])
    @endif

    <div class="card">
        <div class="card-body">
            <div>
                <p><strong>{{ __('discountCard.field.number') }}: </strong>{{ $discountCard->number }}</p>
                <p><strong>{{ __('discountCard.field.phone') }}: </strong>{{ phoneFormat($discountCard->getClientPhone()) }}</p>
                <p><strong>{{ __('discountCard.field.name') }}: </strong>{{ $discountCard->getClientName() }}</p>
                <p><strong>{{ __('discountCard.field.disc') }}: </strong>{{ $discountCard->getDisc() }}%</p>
                <p>
                    <strong>{{ __('discountCard.field.type') }}: </strong>
                    @if ($discountCard->isCumulative())
                        <span class="badge badge-info">{{ __('discountCard.types.cumulative') }}</span>
                    @elseif ($discountCard->isFixed())
                        <span class="badge badge-primary">{{ __('discountCard.types.fixed') }}</span>
                    @endif
                </p>
                <p><strong>{{ __('discountCard.field.created_at') }}: </strong>{{ dataTimeFormat($discountCard->created_at) }}</p>
                <p><strong>{{ __('discountCard.field.updated_at') }}: </strong>{{ dataTimeFormat($discountCard->updated_at) }}</p>

                @if (!empty($discountCard->cars))
                    <p>
                        <strong>{{ __('discountCard.field.car_numbers') }}: </strong>
                        @foreach($discountCard->cars as $car)
                            {{ $car->number }}
                            @if (isset($car->carMark) || isset($car->carModel))
                                ({{ $car->carMark ? $car->carMark->name : '' }} {{ $car->carModel ? $car->carModel->name : '' }}){{ $loop->last ? '' : ', ' }}
                            @endif
                        @endforeach
                    </p>
                @endif
            </div>
        </div>
    </div>
@endsection