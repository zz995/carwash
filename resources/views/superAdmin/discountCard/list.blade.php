@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._discountCard_navbar')
@endsection

@section('content')

    <div class="card mb-3">
        <div class="card-header">{{ __('filtrationBlock.header') }}</div>
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="number" class="col-form-label">{{ __('discountCard.field.number') }}</label>
                            <input id="number" class="form-control" name="number" value="{{ request('number') }}">
                        </div>
                    </div>
                    <div class="col-sm-3 car-number form-group">
                        <label for="car_number" class="col-form-label">{{ __('discountCard.field.car_number') }}</label>
                        <input id="car_number" class="form-control car-number__input" name="car_number" value="{{ request('car_number') }}">
                        <label>
                            <input type="checkbox" autocomplete="off" name="foreign-number" value="1" {{ request('foreign-number') ? 'checked' : '' }} class="car-number__foreign skip-focus">
                            {{ __('car.field.foreignNumber') }}
                        </label>
                    </div>
                    <div class="col-sm-3 form-group">
                        <label for="phone" class="col-form-label">{{ __('discountCard.field.phone') }}</label>
                        <input id="phone" class="form-control" name="phone" value="{{ request('phone') }}">
                    </div>
                    <div class="col-sm-3 form-group">
                        <label class="ccol-form-label">&nbsp;</label><br />
                        <button type="submit" class="btn btn-primary">
                            {{ __('filtrationBlock.search') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
        <table class="employees table table-bordered table-striped">
            <thead>
                <tr>
                    <th>{{ __('discountCard.field.number') }}</th>
                    <th>{{ __('discountCard.field.disc') }}</th>
                    <th>{{ __('discountCard.field.car_numbers') }}</th>
                    <th>{{ __('discountCard.field.name') }}</th>
                    <th>{{ __('discountCard.field.phone') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($discountCards as $discountCard)
                    <tr class="employees__employe" data-location="{{ route('superAdmin.discountCard.show', [$discountCard]) }}" data-toggle="tooltip" data-placement="bottom" title="{{ __('discountCard.tooltip.show') }}">
                        <td>{{ $discountCard->number }}</td>
                        <td>{{ $discountCard->getDisc() }}</td>
                        <td>
                            @foreach($discountCard->cars as $car)
                                {{ $car->number }}{{ $loop->last ? '' : ', ' }}
                            @endforeach
                        </td>
                        <td>{{ $discountCard->getClientName() }}</td>
                        <td>{{ $discountCard->getClientPhone() }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="container mt-3">
        <div class="row justify-content-center">
            {{ $discountCards->appends($_GET)->links() }}
        </div>
    </div>

@endsection
