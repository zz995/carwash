
<div class="form-group row">
    <div class="col-md-4 col-form-label text-md-right">
        <span class="h4">{{ __("works.field.category{$num}") }}</span>
    </div>
</div>

<div class="form-group row">
    <label for="category{{ $num }}_price" class="col-md-4 col-form-label text-md-right">
        {{ __("works.field.categoryN_price") }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="category{{ $num }}_price" type="number" class="form-control{{ $errors->has("category{$num}_price") ? ' is-invalid' : '' }}" name="category{{ $num }}_price" value="{{ old("category{$num}_price", moneyInputFormat($work->{"category{$num}_price"})) }}" required>

        @if ($errors->has("category{$num}_price"))
            <span class="invalid-feedback">
                <strong>{{ $errors->first("category{$num}_price") }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="category{{ $num }}_cost" class="col-md-4 col-form-label text-md-right">
        {{ __("works.field.categoryN_cost") }}
    </label>

    <div class="col-md-6">
        <input id="category{{ $num }}_cost" type="number" class="form-control{{ $errors->has("category{$num}_cost") ? ' is-invalid' : '' }}" name="category{{ $num }}_cost" value="{{ old("category{$num}_cost", moneyInputFormat($work->{"category{$num}_cost"})) }}">

        @if ($errors->has("category{$num}_cost"))
            <span class="invalid-feedback">
                <strong>{{ $errors->first("category{$num}_cost") }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="category{{ $num }}_salary" class="col-md-4 col-form-label text-md-right">
        {{ __("works.field.categoryN_salary") }}
    </label>

    <div class="col-md-6">
        <input id="category{{ $num }}_salary" type="number" class="form-control{{ $errors->has("category{$num}_salary") ? ' is-invalid' : '' }}" name="category{{ $num }}_salary" value="{{ old("category{$num}_salary", moneyInputFormat($work->{"category{$num}_salary"})) }}">

        @if ($errors->has("category{$num}_salary"))
            <span class="invalid-feedback">
                <strong>{{ $errors->first("category{$num}_salary") }}</strong>
            </span>
        @endif
    </div>
</div>