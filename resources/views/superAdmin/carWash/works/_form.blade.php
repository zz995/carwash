<div class="form-group row">
    <label for="number" class="col-md-4 col-form-label text-md-right">
        {{ __('user.field.id') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="number" type="text" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" value="{{ old('number', $work->number) }}" required>

        @if ($errors->has('number'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('number') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">
        {{ __('works.field.name') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $work->name) }}" required>

        @if ($errors->has('name'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="type" class="col-md-4 col-form-label text-md-right">
        {{ __('works.field.type') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <select id="type" class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" name="type">
            <option value=""></option>
            @foreach ($types as $type)
                <option value="{{ $type->id }}" {{ $type->id == old('type', isset($work->id) ? $work->type->id : '') ? ' selected' : '' }}>{{ $type->name }}</option>
            @endforeach;
        </select>

        @if ($errors->has('type'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('type') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="several" class="col-md-4 col-form-label text-md-right">
        {{ __('works.field.several') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input
                id="several"
                type="checkbox"
                class="mt-2{{ $errors->has('several') ? ' is-invalid' : '' }}"
                name="several"
                value="1"
                {{ old('several',  isset($work->id) ? $work->several : '') == 1 ? 'checked' : '' }}
        >

        @if ($errors->has('several'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('several') }}</strong>
            </span>
        @endif
    </div>
</div>

@include('superAdmin.carWash.works._category', ['work' => $work, 'num' => 1])
@include('superAdmin.carWash.works._category', ['work' => $work, 'num' => 2])
@include('superAdmin.carWash.works._category', ['work' => $work, 'num' => 3])
@include('superAdmin.carWash.works._category', ['work' => $work, 'num' => 4])

<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        <button type="submit" class="btn btn-primary">
            {{ $buttonTitle }}
        </button>
    </div>
</div>
