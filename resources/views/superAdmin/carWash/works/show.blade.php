@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._employees_navbar')
@endsection

@section('content')

    <div class="d-flex flex-row mb-3">
        <a href="{{ route('superAdmin.carWash.works.edit', ['work' => $work]) }}" class="btn btn-success mr-1">
            {{ __('works.action.edit') }}
        </a>

        <button class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete">
            {{ __('works.action.delete') }}
        </button>
        @include(
            'partials.modal.confirm',
            [
                'target' => "confirmDelete",
                'body' => __('works.confirm.delete'),
                'action' => route('superAdmin.carWash.works.delete', ['work' => $work]),
                'method' => 'DELETE'
            ]
        )
    </div>

    <div class="card">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>{{ __('works.field.id') }}</th>
                <th>{{ __('works.field.name') }}</th>
                <th>{{ __('works.field.type') }}</th>
                <th>{{ __('works.field.several') }}</th>
                <th>{{ __('works.field.category1') }}</th>
                <th>{{ __('works.field.category2') }}</th>
                <th>{{ __('works.field.category3') }}</th>
                <th>{{ __('works.field.category4') }}</th>
            </tr>
            </thead>
            <tbody>

                <tr class="" data-location="{{ route('superAdmin.employees.show', [$work]) }}" data-toggle="tooltip" data-placement="bottom" title="{{ __('works.tooltip.show') }}">
                    <td>{{ $work->number }}</td>
                    <td>{{ $work->name }}</td>
                    <td>{{ $work->type->name }}</td>
                    <td>
                        @if ($work->several)
                            <span class="badge badge-success">{{ __('works.several.yes') }}</span>
                        @else
                            <span class="badge badge-danger">{{ __('works.several.no') }}</span>
                        @endif
                    </td>
                    <td>
                        <span class="rub">
                            {{ moneyFormat($work->category1_price) }}
                        </span>
                    </td>
                    <td>
                        <span class="rub">
                            {{ moneyFormat($work->category2_price) }}
                        </span>
                    </td>
                    <td>
                        <span class="rub">
                            {{ moneyFormat($work->category3_price) }}
                        </span>
                    </td>
                    <td>
                        <span class="rub">
                            {{ moneyFormat($work->category4_price) }}
                        </span>
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
@endsection