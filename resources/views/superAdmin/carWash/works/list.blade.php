@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._carWash_navbar')
@endsection

@section('content')
    <p>
        <a href="{{ route('superAdmin.carWash.works.create') }}" class="btn btn-success">
            {{ __('works.action.create') }}
        </a>
    </p>

    <div class="card mb-3">
        <div class="card-header">{{ __('filtrationBlock.header') }}</div>
        <div class="card-body">
            <form action="?" method="GET">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="id" class="col-form-label">{{ __('works.field.id') }}</label>
                            <input id="id" class="form-control" name="id" value="{{ request('id') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="name" class="col-form-label">{{ __('works.field.name') }}</label>
                            <input id="name" class="form-control" name="name" value="{{ request('name') }}">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="type" class="col-form-label">{{ __('works.field.type') }}</label>
                            <select id="type" class="form-control" name="type">
                                <option value=""></option>
                                @foreach ($types as $type)
                                    <option value="{{ $type->id }}"{{ $type->id == request('type') ? ' selected' : '' }}>{{ $type->name }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br />
                            <button type="submit" class="btn btn-primary">
                                {{ __('filtrationBlock.search') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
        <table class="car-wash-works table table-bordered table-striped">
            <thead>
                <tr>
                    <th>{{ __('works.field.id') }}</th>
                    <th>{{ __('works.field.name') }}</th>
                    <th>{{ __('works.field.type') }}</th>
                    <th>{{ __('works.field.category1') }}</th>
                    <th>{{ __('works.field.category2') }}</th>
                    <th>{{ __('works.field.category3') }}</th>
                    <th>{{ __('works.field.category4') }}</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($works as $work)
                    <tr class="car-wash-works__work" data-location="{{ route('superAdmin.carWash.works.show', [$work]) }}" data-toggle="tooltip" data-placement="bottom" title="{{ __('works.tooltip.show') }}">
                        <td>{{ $work->number }}</td>
                        <td>{{ $work->name }}</td>
                        <td>{{ $work->type->name }}</td>
                        <td>
                            <span class="rub">
                                {{ moneyFormat($work->category1_price) }}
                            </span>
                        </td>
                        <td>
                            <span class="rub">
                                {{ moneyFormat($work->category2_price) }}
                            </span>
                        </td>
                        <td>
                            <span class="rub">
                                {{ moneyFormat($work->category3_price) }}
                            </span>
                        </td>
                        <td>
                            <span class="rub">
                                {{ moneyFormat($work->category4_price) }}
                            </span>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>

        <div class="container">
            <div class="row justify-content-center">
                {{ $works->appends($_GET)->links() }}
            </div>
        </div>

    </div>
@endsection