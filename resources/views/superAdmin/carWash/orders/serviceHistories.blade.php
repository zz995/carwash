@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._carWash_navbar')
@endsection

@section('content')
    <form action="?" method="GET">

        <div class="card mb-3">
            <div class="card-header">{{ __('filtrationBlock.header') }}</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="contract_number" class="col-form-label">{{ __('order.contractTypePayment') }}</label>
                            <input id="contract_number" class="form-control" name="contract_number" value="{{ request('contract_number') }}">
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="discount_card" class="col-form-label">{{ __('discountCard.title.base') }}</label>
                            <input id="discount_card" class="form-control" name="discount_card" value="{{ request('discount_card') }}">
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="phone" class="col-form-label">{{ __('order.field.phone') }}</label>
                            <input id="phone" class="form-control phone-input" name="phone" value="{{ request('phone') }}">
                        </div>
                    </div>

                    <div class="col-sm-3 car-number">
                        <div class="form-group">
                            <label for="car_number" class="col-form-label">{{ __('order.field.number') }}</label>
                            <input id="car_number" class="form-control car-number__input" name="car_number" value="{{ request('car_number') }}">
                            <label>
                                <input type="checkbox" autocomplete="off" name="foreign-number" value="1" {{ request('car_number') !== null && \App\Entity\Car::isForeignNumberStatic(request('car_number')) ? 'checked' : '' }} class="car-number__foreign skip-focus">
                                {{ __('car.field.foreignNumber') }}
                            </label>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="service_center" class="col-form-label">{{ __('order.field.serviceCenter') }}</label>
                            <select id="service_center" class="form-control" name="service_center">
                                <option value=""></option>
                                @foreach ($serviceCenters as $serviceCenter)
                                    <option value="{{ $serviceCenter->id }}"{{ $serviceCenter->id == request('service_center') ? ' selected' : '' }}>{{ $serviceCenter->name }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br />
                            <button type="submit" class="btn btn-primary">
                                {{ __('filtrationBlock.search') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <table class="orders table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>{{ __('order.field.id') }}</th>
                        <th>{{ __('order.field.status') }}</th>
                        <th>{{ __('order.field.serviceCenter') }}</th>
                        <th>{{ __('contracts.title') }} / {{ __('discountCard.title.base') }}</th>
                        <th>{{ __('client.title') }}</th>
                        <th>{{ __('order.field.number') }}</th>
                        <th>{{ __('order.field.date') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                        <tr class="orders__order" data-location="{{ route('superAdmin.carWash.order.show', [$order]) }}" data-toggle="tooltip" data-placement="bottom" title="{{ __('order.tooltip.show') }}">
                            <td>{{ $order->id }}</td>
                            <td>
                                @include('employees.order.partials._status', ['order' => $order])
                            </td>
                            <td>
                                {{ $order->serviceCenter->name }}
                            </td>
                            <td>
                                @if ($order->hasContract())
                                    <a href="{{ route('superAdmin.contracts.show', $order->contract) }}">
                                        {{ $order->contract->number }}, {{ $order->contract->disc }}%
                                    </a>
                                @elseif ($order->hasDiscountCard())
                                    @if ($order->discountCard->hasNumber())
                                        {{ $order->discountCard->number }},
                                    @endif
                                    {{ $order->discountCard->disc }}%
                                @endif
                            </td>
                            <td>
                                {{ $order->client->name }}, {{ phoneFormat($order->client->phone) }}
                            </td>
                            <td>
                                {{ $order->car->number }} ({{ $order->car->carMark ? $order->car->carMark->name : '' }} {{ $order->car->carModel ? $order->car->carModel->name : '' }})
                            </td>
                            <td>
                                {{ dataTimeFormat($order->created_at) }}
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>

            <div class="container">
                <div class="row justify-content-center">
                    {{ $orders->appends($_GET)->links() }}
                </div>
            </div>

        </div>
    </form>
@endsection