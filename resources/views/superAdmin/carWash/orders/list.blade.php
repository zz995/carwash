@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._carWash_navbar')
@endsection

@section('content')
    <form action="?" method="GET">

        <div class="card mb-3">
            <div class="card-header">{{ __('filtrationBlock.header') }}</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="work" class="col-form-label">{{ __('order.field.work') }}</label>
                            <input id="work" class="form-control" name="work" value="{{ request('work') }}">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="status" class="col-form-label">{{ __('order.field.status') }}</label>
                            <select id="status" class="form-control" name="status">
                                <option value=""></option>
                                @foreach ($statuses as $status)
                                    <option value="{{ $status['value'] }}"{{ $status['value'] == request('status') && request('status') !== null ? ' selected' : '' }}>{{ $status['label'] }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="payment" class="col-form-label">{{ __('order.field.paymentType') }}</label>
                            <select id="payment" class="form-control" name="payment">
                                <<option value=""></option>
                                <option value="1"{{ '1' === request('payment') ? ' selected' : '' }}>
                                    {{ __('order.paymentType.cash') }}
                                </option>
                                <option value="2"{{ '2' === request('payment') ? ' selected' : '' }}>
                                    {{ __('order.paymentType.card') }}
                                </option>
                                <option value="3"{{ '3' === request('payment') ? ' selected' : '' }}>
                                    {{ __('order.paymentType.contract') }}
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="service_center" class="col-form-label">{{ __('order.field.serviceCenter') }}</label>
                            <select id="service_center" class="form-control" name="service_center">
                                <option value=""></option>
                                @foreach ($serviceCenters as $serviceCenter)
                                    <option value="{{ $serviceCenter->id }}"{{ $serviceCenter->id == request('service_center') ? ' selected' : '' }}>{{ $serviceCenter->name }}</option>
                                @endforeach;
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="services_standard_price" class="col-form-label">{{ __('order.field.services_standard_price') }}</label>
                            <select id="services_standard_price" class="form-control" name="services_standard_price">
                                <<option value=""></option>
                                <option value="1"{{ '1' === request('services_standard_price') ? ' selected' : '' }}>
                                    {{ __('order.yes') }}
                                </option>
                                <option value="0"{{ '0' === request('services_standard_price') ? ' selected' : '' }}>
                                    {{ __('order.no') }}
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="date" class="col-form-label">{{ __('order.field.date') }}</label>
                            <input
                                    autocomplete="off"
                                    type="text"
                                    name="date"
                                    data-range="true"
                                    @if (isset($date))
                                        value="{{ dateToPickerFormat($date->getFrom()) }} - {{ dateToPickerFormat($date->getTo()) }}"
                                    @endif
                                    data-multiple-dates-separator=" - "
                                    class="form-control datepicker-here"
                            />
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="price_from" class="col-form-label">{{ __('order.phrase.sumRange') }}</label>
                            <div class="d-flex align-items-center">
                                <input id="price_from" class="form-control" name="price_from" value="{{ request('price_from') }}">
                                <span>&nbsp;-&nbsp;</span>
                                <input id="price_to" class="form-control" name="price_to" value="{{ request('price_to') }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="time_execute_from" class="col-form-label">{{ __('order.phrase.timeExecuteInMinute') }}</label>
                            <div class="d-flex align-items-center">
                                <input id="time_execute_from" class="form-control" name="time_execute_from" value="{{ request('time_execute_from') }}">
                                <span>&nbsp;-&nbsp;</span>
                                <input id="time_execute_to" class="form-control" name="time_execute_to" value="{{ request('time_execute_to') }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="time_queue_from" class="col-form-label">{{ __('order.phrase.timeInQueueInMinute') }}</label>
                            <div class="d-flex align-items-center">
                                <input id="time_queue_from" class="form-control" name="time_queue_from" value="{{ request('time_queue_from') }}">
                                <span>&nbsp;-&nbsp;</span>
                                <input id="time_queue_to" class="form-control" name="time_queue_to" value="{{ request('time_queue_to') }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label class="col-form-label">&nbsp;</label><br />
                            <button type="submit" class="btn btn-primary">
                                {{ __('filtrationBlock.search') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <table class="orders table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            @include('superAdmin.partials._sort', [
                                'title' => __('order.field.id'),
                                'field' => 'id',
                                'currentSort' => $sort
                            ])
                        </th>
                        <th>{{ __('order.field.works') }}</th>
                        <th>{{ __('order.field.status') }}</th>
                        <th>{{ __('order.field.paymentType') }}</th>
                        <th>{{ __('order.field.serviceCenter') }}</th>
                        <th>
                            @include('superAdmin.partials._sort', [
                                'title' => __('order.phrase.sum'),
                                'field' => 'price',
                                'currentSort' => $sort
                            ])
                        </th>
                        <th>
                            @include('superAdmin.partials._sort', [
                                'title' => __('order.field.date'),
                                'field' => 'created_at',
                                'currentSort' => $sort
                            ])
                        </th>
                        <th>
                            @include('superAdmin.partials._sort', [
                                'title' => __('order.phrase.timeExecute'),
                                'field' => 'time_execute',
                                'currentSort' => $sort
                            ])
                        </th>
                        <th>
                            @include('superAdmin.partials._sort', [
                                'title' => __('order.phrase.timeInQueue'),
                                'field' => 'time_queue',
                                'currentSort' => $sort
                            ])
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                        <tr class="orders__order" data-location="{{ route('superAdmin.carWash.order.show', [$order]) }}" data-toggle="tooltip" data-placement="bottom" title="{{ __('order.tooltip.show') }}">
                            <td>{{ $order->id }}</td>
                            <td>
                                @foreach($order->basketItems as $item)
                                    {{ $item->work->number }}{{ $loop->last ? '' : ',' }}
                                @endforeach
                            </td>
                            <td>
                                @include('employees.order.partials._status', ['order' => $order])
                            </td>
                            <td>
                                @if ($order->isPaid())
                                    {{ orderPaymentType($order->payment_type) }}
                                @endif
                            </td>
                            <td>{{ $order->serviceCenter->name }}</td>
                            <td>
                                @if (isset($order->price))
                                    <nobr class="rub">
                                        {{ moneyFormat($order->price) }}
                                    </nobr>
                                @endif
                            </td>
                            <td>{{ dataTimeFormat($order->created_at) }}</td>
                            <td>{{ timePassed($order->time_execute) }}</td>
                            <td>{{ timePassed($order->time_queue) }}</td>
                        </tr>
                    @endforeach

                </tbody>
            </table>

            <div class="container">
                <div class="row justify-content-center">
                    {{ $orders->appends($_GET)->links() }}
                </div>
            </div>

        </div>
    </form>
@endsection