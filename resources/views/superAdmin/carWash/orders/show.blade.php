@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._carWash_navbar')
@endsection

@section('content')

    @if ($order->isExecute() || $order->isQueue())
        <div class="d-flex flex-row mb-3">
            <button class="btn btn-danger mr-1" data-toggle="modal" data-target="#confirmCanceled">
                {{ __('order.action.cancel') }}
            </button>

            @include(
                'partials.modal.confirm',
                [
                    'target' => "confirmCanceled",
                    'body' => __('order.confirm.canceled'),
                    'action' => route('superAdmin.carWash.order.cancel', $order),
                    'method' => 'PUT'
                ]
            )
        </div>
    @endif

    <div class="card">
        <div class="card-body">
            <div>
                <p><strong>{{ __('order.field.id') }}: </strong>{{ $order->id }}</p>
                <p>
                    <strong>
                        {{ __('order.field.status') }}:
                    </strong>
                    @include('employees.order.partials._status', ['order' => $order])
                </p>
                <p>
                    <strong>{{ __('order.field.serviceCenter') }}: </strong>
                    <a href="{{ route('superAdmin.serviceCenter.show', $order->serviceCenter) }}">
                        {{ $order->serviceCenter->name }}
                    </a>
                </p>
                <p><strong>{{ __('order.field.teamName') }}: </strong>{{ $order->team_name }}</p>
                <p>
                    <strong>
                        {{ __('order.field.number') }}:
                    </strong>
                    <a href="{{ route('superAdmin.carWash.order.serviceHistory', ['car_number' => $order->car->number]) }}">
                        {{ $order->car->number }}
                        {{ $order->car->carMark->name }}
                        {{ $order->car->carModel->name }}
                    </a>
                </p>
                <p>
                    <strong>{{ __('order.field.phone') }}: </strong>
                    <a href="{{ route('superAdmin.carWash.order.serviceHistory', ['phone' => $order->client->phone]) }}">
                        {{ phoneFormat($order->client->phone) }}
                    </a>
                </p>
                <p><strong>{{ __('order.field.date') }}: </strong>{{ dataTimeFormat($order->created_at) }}</p>
                <p>
                    <strong>{{ __('order.field.works') }}: </strong>
                    @foreach($order->basketItems as $item)
                        <a href="{{ route('superAdmin.carWash.works.show', $item->work) }}">{{ $item->work->name }}</a>{{ $loop->last ? '' : ',' }}
                    @endforeach
                </p>
                @if ($order->isPaid())
                    <p><strong>{{ __('order.field.paymentType') }}: </strong>{{ orderPaymentType($order->payment_type) }}</p>
                @endif
                @if ($order->isPaid() && $order->isContractPayment())
                    <p>
                        <strong>{{ __('order.contractTypePayment') }}: </strong>
                        <a href="{{ route('superAdmin.contracts.show', $order->contract) }}">
                            {{ $order->contract->number }}
                            {{ $order->contract->contractor }}
                        </a>
                    </p>
                @endif
                @if ($order->isPaid() && !$order->isContractPayment() && $order->isApplyingDiscountFromContract())
                    <p>
                        <strong>{{ __('order.applyingDiscountFromContract') }}: </strong>
                        <a href="{{ route('superAdmin.contracts.show', $order->contractApplyingDiscount) }}">
                            {{ $order->contractApplyingDiscount->number }}
                            {{ $order->contractApplyingDiscount->contractor }}
                        </a>
                    </p>
                @endif
                @if ($order->hasDiscountCard() && !$order->isContractPayment() && !$order->isApplyingDiscountFromContract())
                    <p>
                        <strong>{{ __('discountCard.title.base') }}: </strong>
                        @if ($order->discountCard->hasNumber())
                            {{ $order->discountCard->number }},
                        @endif
                        {{ $order->discountCard->disc }}%
                    </p>
                @endif
                @include('employees.order.partials._orderPrice', ['order' => $order])
                @if (isset($order->admin_comment))
                    <p>
                        <strong>{{ __('order.field.adminComment') }}:</strong><br>
                        {!! nl2br(e($order->admin_comment)) !!}
                    </p>
                @endif
                @if (isset($order->comment))
                    <p>
                        <strong>{{ __('order.field.cashierComment') }}:</strong><br>
                        {!! nl2br(e($order->comment)) !!}
                    </p>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="card mt-3">
                <div class="card-header">
                    {{ __('team.block') }}
                </div>
                <div class="card-body">
                    <div>
                        <p><strong>{{ __('team.field.name') }}: </strong>{{ $order->team_name }}</p>
                        <p><strong>{{ __('team.field.admin') }}: </strong>
                            <a href="{{ route('superAdmin.employees.show', $order->admin) }}">
                                {{ $order->admin->getFio()->getFullName() }}
                            </a>
                        </p>

                        @if ($order->employees->count())
                            <p>
                                <strong>{{ __('team.field.employees') }}: </strong>
                                {!! listEmployees($order->employees, 'superAdmin.employees.show') !!}
                            </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-6">
            <div class="card mt-3">
                <div class="card-header">
                    {{ __('order.phrase.statistic') }}
                </div>
                <div class="card-body">
                    <div>
                        <p><strong>{{ __('order.phrase.timeExecute') }}: </strong>{{ timePassed($order->time_execute) }}</p>
                        <p><strong>{{ __('order.phrase.timeInQueue') }}: </strong>{{ timePassed($order->time_queue) }}</p>
                        <p><strong>{{ __('order.field.services_standard_price') }}: </strong>{{ $order->services_standard_price ? __('order.yes') : __('order.no') }}</p>

                        @if (! $order->services_standard_price)
                            @foreach($order->getBasketItemsWithNonStandardPrice() as $basketItem)
                                <a href="{{ route('superAdmin.carWash.works.show', $basketItem->work) }}">{{ $basketItem->work->name }}</a>
                                <span class="rub">
                                    {{ moneyFormat($basketItem->default_price) }}
                                </span>
                                <i class="fa fa-arrow-right"></i>
                                <span class="rub">
                                    {{ moneyFormat($basketItem->price) }}
                                </span>
                                <br>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card mt-3">
        <div class="card-header">
            {{ __('order.phrase.orderStatusHistory') }}
        </div>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>{{ __('order.field.status') }}</th>
                <th>{{ __('order.field.statusHistory.timestamp') }}</th>
                <th>{{ __('order.field.statusHistory.initiator') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($statusHistories as $statusHistory)
                <tr>
                    <td>
                        @include('employees.order.partials._status', [
                            'order' => $order->temporaryReturnToHistoryStatus($statusHistory),
                            'notShowCross' => true
                        ])
                    </td>
                    <td>{{ dataTimeFormat($statusHistory->timestamp) }}</td>
                    <td>
                        <a href="{{ route('superAdmin.employees.show', $statusHistory->initiator_id) }}">
                            {{ $statusHistory->initiator->getFio()->getFullName() }}
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if ($statusHistories->isEmpty())
            <p class="text-center">{{ __('order.statusHistoriesNotFound') }}</p>
        @endif
    </div>
@endsection