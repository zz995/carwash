@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._employees_navbar')
@endsection

@section('content')
    <div class="card">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>{{ __('role.field.id') }}</th>
                    <th>{{ __('role.field.name') }}</th>
                    <th>{{ __('role.operation') }}</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($roles as $role)
                    <tr class="">
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>
                        <td>
                            @can('manage-role-change-salary', [$role])
                                <a href="{{ route('superAdmin.employees.roles.changeSalary', $role) }}" class="btn btn-success">
                                    {{ __('role.action.changeSalary') }}
                                </a>
                            @endcan
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>

        <div class="container">
            <div class="row justify-content-center">
                {{ $roles->appends($_GET)->links() }}
            </div>
        </div>

    </div>
@endsection