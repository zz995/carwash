@extends('superAdmin.layouts.app')

@section('secondaryNavbar')
    @include('superAdmin.partials._employees_navbar')
@endsection


@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                </div>
            </div>

            <form method="POST" action="">
                @csrf
                @method('PATCH')

                <div class="row">
                    <div class="col-md-6 offset-md-4">
                        @include('partials.form.markerRequireFields')
                    </div>
                </div>

                <div class="form-group row">
                    <label for="salary" class="col-md-4 col-form-label text-md-right">
                        {{ __('role.field.salary') }}<sup class="text-danger">*</sup>
                    </label>

                    <div class="col-md-6">
                        <div class="input-group">
                            <input
                                    id="salary"
                                    type="number"
                                    class="form-control-sm form-control{{ $errors->has('salary') ? ' is-invalid' : '' }}"
                                    name="salary"
                                    value="{{ old('salary', $role->salary) }}"
                                    required
                            >
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    @if($role->isCashier())
                                        {!! __('role.salaryType.byDay') !!}
                                    @elseif($role->isWasher())
                                        {!! __('role.salaryType.teamPercent') !!}
                                    @elseif($role->isAdmin())
                                        {!! __('role.salaryType.selfPercent') !!}
                                    @endif
                                </div>
                            </div>
                        </div>

                        @if ($errors->has('salary'))
                            <span class="invalid-feedback d-block">
                                <strong>{{ $errors->first('salary') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('role.action.changeSalary') }}
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection