@extends('superAdmin.layouts.app')

@section('content')

    <div class="d-flex flex-row mb-3">
        <a href="{{ route('superAdmin.contracts.edit', ['contract' => $contract]) }}" class="btn btn-success mr-1">
            {{ __('contracts.action.edit') }}
        </a>

        @if (! $contract->hasOrder())
            <button class="btn btn-danger" data-toggle="modal" data-target="#confirmDelete">
                {{ __('contracts.action.delete') }}
            </button>
            @include(
                'partials.modal.confirm',
                [
                    'target' => "confirmDelete",
                    'body' => __('contracts.confirm.delete'),
                    'action' => route('superAdmin.contracts.delete', ['contract' => $contract]),
                    'method' => 'DELETE'
                ]
            )
        @endif
    </div>

    <div class="card">
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>{{ __('contracts.field.number') }}</th>
                <th>{{ __('contracts.field.contractor') }}</th>
                <th>{{ __('contracts.field.from') }}</th>
                <th>{{ __('contracts.field.to') }}</th>
                <th>{{ __('contracts.field.disc') }}</th>
            </tr>
            </thead>
            <tbody>

                <tr class="" data-location="{{ route('superAdmin.contracts.show', [$contract]) }}">
                    <td>{{ $contract->number }}</td>
                    <td>{{ $contract->contractor }}</td>
                    <td>{{ dataTimeFormat($contract->from) }}</td>
                    <td>{{ dataTimeFormat($contract->to) }}</td>
                    <td>{{ $contract->disc }}</td>
                </tr>

            </tbody>
        </table>
    </div>
@endsection