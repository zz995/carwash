@extends('superAdmin.layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            {{ $pageTitle }}
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 offset-md-4">
                    @include('partials.form.markerRequireFields')
                </div>
            </div>

            <form method="POST" action="">
                @csrf
                @include('superAdmin.contracts._form', ['contract' => $contract, 'buttonTitle' => __('contracts.action.create')])
            </form>
        </div>
    </div>
@endsection