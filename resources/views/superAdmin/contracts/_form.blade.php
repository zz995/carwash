<div class="form-group row">
    <label for="number" class="col-md-4 col-form-label text-md-right">
        {{ __('contracts.field.number') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="number" type="text" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" value="{{ old('number', $contract->number) }}" required>

        @if ($errors->has('number'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('number') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="contractor" class="col-md-4 col-form-label text-md-right">
        {{ __('contracts.field.contractor') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="contractor" type="text" class="form-control{{ $errors->has('contractor') ? ' is-invalid' : '' }}" name="contractor" value="{{ old('contractor', $contract->contractor) }}" required>

        @if ($errors->has('contractor'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('contractor') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="from" class="col-md-4 col-form-label text-md-right">
        {{ __('contracts.field.from') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="from" type="date" class="form-control{{ $errors->has('from') ? ' is-invalid' : '' }}" name="from" value="{{ old('from', dateToInputFormat($contract->from)) }}" required>

        @if ($errors->has('from'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('from') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="to" class="col-md-4 col-form-label text-md-right">
        {{ __('contracts.field.to') }}<sup class="text-danger">*</sup>
    </label>

    <div class="col-md-6">
        <input id="to" type="date" class="form-control{{ $errors->has('to') ? ' is-invalid' : '' }}" name="to" value="{{ old('to', dateToInputFormat($contract->to)) }}" required>

        @if ($errors->has('to'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('to') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="disc" class="col-md-4 col-form-label text-md-right">
        {{ __('contracts.field.disc') }}
    </label>

    <div class="col-md-6">
        <input id="disc" type="number" class="form-control{{ $errors->has('disc') ? ' is-invalid' : '' }}" name="disc" value="{{ old('disc', $contract->disc) }}">

        @if ($errors->has('disc'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('disc') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        <button type="submit" class="btn btn-primary">
            {{ $buttonTitle }}
        </button>
    </div>
</div>
