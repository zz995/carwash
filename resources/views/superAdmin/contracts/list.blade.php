@extends('superAdmin.layouts.app')

@section('content')
    <p>
        <a href="{{ route('superAdmin.contracts.create') }}" class="btn btn-success">
            {{ __('contracts.action.create') }}
        </a>
    </p>

    <div class="card">
        <table class="car-wash-contracts table table-bordered table-striped">
            <thead>
                <tr>
                    <th>{{ __('contracts.field.number') }}</th>
                    <th>{{ __('contracts.field.contractor') }}</th>
                    <th>{{ __('contracts.field.from') }}</th>
                    <th>{{ __('contracts.field.to') }}</th>
                    <th>{{ __('contracts.field.disc') }}</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($contracts as $contract)
                    <tr class="car-wash-contracts__contract" data-location="{{ route('superAdmin.contracts.show', [$contract]) }}" data-toggle="tooltip" data-placement="bottom" title="{{ __('contracts.tooltip.show') }}">
                        <td>{{ $contract->number }}</td>
                        <td>{{ $contract->contractor }}</td>
                        <td>{{ dataTimeFormat($contract->from) }}</td>
                        <td>{{ dataTimeFormat($contract->to) }}</td>
                        <td>{{ $contract->disc }}</td>
                    </tr>
                @endforeach

            </tbody>
        </table>

        <div class="container">
            <div class="row justify-content-center">
                {{ $contracts->appends($_GET)->links() }}
            </div>
        </div>

    </div>
@endsection