<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('pageTitle', isset($pageTitle) ? $pageTitle : config('app.name', 'Laravel'))</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js', 'build') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ mix('css/app.css', 'build') }}" rel="stylesheet">
</head>
<body>
    <header>
        <nav class="navbar">
            <div class="container">
                <span class="navbar__brand">
                    {{ config('app.name', 'Laravel') }}
                </span>
                <button class="navbar__toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="navbar__links" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @can('manage-employe-view')
                            <li>
                                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.employees.list') ? 'active' : '' }}" href="{{ route('superAdmin.employees.list') }}">
                                    {{ __('navbar.superAdmin.employees') }}
                                </a>
                            </li>
                        @endcan
                        @can('manage-car-wash-works')
                            <li>
                                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.carWash.works.list') ? 'active' : '' }}" href="{{ route('superAdmin.carWash.works.list') }}">
                                    {{ __('navbar.superAdmin.carWash') }}
                                </a>
                            </li>
                        @endcan
                        @can('manage-contracts')
                             <li>
                                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.contracts.list') ? 'active' : '' }}" href="{{ route('superAdmin.contracts.list') }}">
                                    {{ __('navbar.superAdmin.contracts') }}
                                </a>
                             </li>
                        @endcan
                        @can('manage-service-centers')
                             <li>
                                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.serviceCenter.list') ? 'active' : '' }}" href="{{ route('superAdmin.serviceCenter.list') }}">
                                    {{ __('navbar.superAdmin.serviceCenters') }}
                                </a>
                             </li>
                        @endcan
                        @can('administration-discount-card')
                             <li>
                                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.discountCard.list') ? 'active' : '' }}" href="{{ route('superAdmin.discountCard.list') }}">
                                    {{ __('navbar.superAdmin.discountCards') }}
                                </a>
                             </li>
                        @endcan
                        @can('administration-fine')
                             <li>
                                <a class="nav-link {{ Route::currentRouteNamed('superAdmin.fine.paymentRefusalFineSize') ? 'active' : '' }}" href="{{ route('superAdmin.fine.paymentRefusalFineSize') }}">
                                    {{ __('navbar.superAdmin.fines.fines') }}
                                </a>
                             </li>
                        @endcan
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();"
                                >
                                    {{ __('navbar.logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        @yield('secondaryNavbar')
    </header>

    <main class="app-content py-4">
        <div class="container container_white">
            @section('breadcrumbs', Breadcrumbs::render())
            @yield('breadcrumbs')
            @include('partials.flash.flash')
            @yield('content')
        </div>
    </main>

    <footer>
        <div class="container">
            <div class="border-top pt-3">
                <p>&copy; {{ date('Y') }} - {{ config('app.name', 'Laravel') }}</p>
            </div>
        </div>
    </footer>
</body>
</html>
